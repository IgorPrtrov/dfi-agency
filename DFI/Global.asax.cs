﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DFI.Helpers;
using FluentValidation.Mvc;

namespace DFI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RolesConfig rolesAdd = new RolesConfig();
            rolesAdd.AddRoles();

            AdminConfig adminAdd = new AdminConfig();
            adminAdd.AddAdmin();

            FluentValidationModelValidatorProvider.Configure();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            HangfireBootstrapper.Instance.Stop();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            ILogger loger = new Logger();
            Exception ex = Server.GetLastError();
            ExceptionLoggerAttribute.ErrorMessages = ex.Message;

            loger.Error($"Application_Error:: {ex.Message}");
            Response.Redirect("/Home/ErrorInform");
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) && HttpContext.Current.Request.IsLocal.Equals(false))
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"]
                                             + HttpContext.Current.Request.RawUrl);
            }
        }
    }
}
