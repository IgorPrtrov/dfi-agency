﻿using System.Configuration;
using DFI.Data.Accounts;
using DFI.Helpers;
using DFI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DFI
{
    public class AdminConfig
    {
        private readonly ApplicationUserManager _userManager =
            new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

        public void AddAdmin()
        {
            var name = _userManager.FindByName(Setting.Admin);
            if (name == null)
            {
                ApplicationUser admin = new ApplicationUser {Email = Setting.AdministratorEmail, UserName = Setting.Admin, Status = (int)Helper.EState.Active, EmailConfirmed = true};
                IdentityResult result = _userManager.Create(admin, Setting.Password);
                if (result.Succeeded)
                {
                    _userManager.AddToRole(admin.Id, Setting.Admin);
                }
            }
        }
    }
}