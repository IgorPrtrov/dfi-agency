﻿using System.Web;
using System.Web.Mvc;
using DFI.Helpers;

namespace DFI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ExceptionLoggerAttribute(new Logger()));
        }
    }
}
