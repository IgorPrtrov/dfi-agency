﻿using System.Linq;
using DFI.Data.Accounts;
using DFI.Data.Roles;
using DFI.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DFI
{
    public class RolesConfig
    {
        readonly ApplicationRoleManager _roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(new ApplicationDbContext()));
        public async void AddRoles()
        {
            var adminRole = _roleManager.Roles.FirstOrDefault(p => p.Name == Role.Admin);
            if (adminRole == null)
            {
                string[] semRoles = { Role.Admin };
                foreach (var item in semRoles)
                {
                    await _roleManager.CreateAsync(new ApplicationRole { Name = item, Description = $"{item}" });
                }
            }

            var officeRole = _roleManager.Roles.FirstOrDefault(p => p.Name == Role.Facility);
            if (officeRole == null)
            {
                string[] semRoles = { Role.Facility };
                foreach (var item in semRoles)
                {
                    await _roleManager.CreateAsync(new ApplicationRole { Name = item, Description = $"{item}" });
                }
            }

            var tempRole = _roleManager.Roles.FirstOrDefault(p => p.Name == Role.Temp);
            if (tempRole == null)
            {
                string[] semRoles = { Role.Temp };
                foreach (var item in semRoles)
                {
                    await _roleManager.CreateAsync(new ApplicationRole { Name = item, Description = $"{item}" });
                }
            }
        }
    }
}