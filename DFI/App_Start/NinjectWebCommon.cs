[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DFI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DFI.App_Start.NinjectWebCommon), "Stop")]

namespace DFI.App_Start
{
    using System;
    using System.Web;
    using DFI.Data.Facilities;
    using DFI.Data.Graders;
    using DFI.Data.ProfessionalReviews;
    using DFI.Data.Requests;
    using DFI.Factory;
    using DFI.Models;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using DFI.Data.ChartingSoftwares;
    using DFI.Data.Documents;
    using DFI.Data.Emails;
    using DFI.Data.GoogleMaps;
    using DFI.Data.Temps;
    using DFI.Data.XraySoftwares;
    using DFI.Helpers;
    using DFI.Data.TextMessages;
    using DFI.Data.Charts;
    using DFI.Data.Home;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application.
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                HangfireBootstrapper.Instance.Start(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IApplicationDbContext>().To<ApplicationDbContext>();
            kernel.Bind<ILogger>().To<Logger>();

            kernel.Bind<IGraderRepository>().To<GraderRepository>();
            kernel.Bind<IGraderMapper>().To<GraderMapper>();
            kernel.Bind<IGraderService>().To<GraderService>();

            kernel.Bind<IFacilityRepository>().To<FacilityRepository>();
            kernel.Bind<IFacilityService>().To<FacilityService>();
            kernel.Bind<IFacilityMapper>().To<FacilityMapper>();

            kernel.Bind<IEmailRepository>().To<EmailRepository>();
            kernel.Bind<IEmailService>().To<EmailService>();
            kernel.Bind<IMailMapper>().To<EmailMapper>();

            kernel.Bind<ITempRepository>().To<TempRepository>();
            kernel.Bind<ITempService>().To<TempService>();
            kernel.Bind<ITempMapper>().To<TempMapper>();

            kernel.Bind<IRequestRepository>().To<RequestRepository>();
            kernel.Bind<IRequestService>().To<RequestService>();
            kernel.Bind<IRequestMapper>().To<RequestMapper>();

            kernel.Bind<IProfessionalReviewMapper>().To<ProfessionalReviewMapper>();
            kernel.Bind<IProfessionalReviewRepository>().To<ProfessionalReviewRepository>();
            kernel.Bind<IProfessionalReviewService>().To<ProfessionalReviewService>();

            kernel.Bind<IDocumentMapper>().To<DocumentMapper>();

            kernel.Bind<IGoogleMapService>().To<GoogleMapService>();

            kernel.Bind<IChartingSoftwareRepository>().To<ChartingSoftwareRepository>();
            kernel.Bind<IXraySoftwareRepository>().To<XraySoftwareRepository>();
            kernel.Bind<IChartingSoftwareService>().To<ChartingSoftwareService>();
            kernel.Bind<IXraySoftwareService>().To<XraySoftwareService>();

            kernel.Bind<ITextMessageRepository>().To<TextMessageRepository>();
            kernel.Bind<ITextMessageService>().To<TextMessageService>();
            kernel.Bind<ITextMessageMapper>().To<TextMessageMapper>();

            kernel.Bind<IDailyEmailRepository>().To<DailyEmailRepository>();

            kernel.Bind<IApplicationUserManagerFactory>().To<ApplicationUserManagerFactory>();
            kernel.Bind<ITwilioService>().To<TwilioService>();

            kernel.Bind<IChartService>().To<ChartService>();
            kernel.Bind<IHomeServices>().To<HomeServices>();
            kernel.Bind<IYouTubeRepository>().To<YouTubeRepository>();
        }
    }
}