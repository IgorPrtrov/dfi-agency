using System.Web.Optimization;

namespace DFI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Bootstrap4/popper.min.js",
                      "~/Scripts/Bootstrap4/bootstrap.min.js",
                      "~/Scripts/Bootstrap4/bootstrap4-toggle.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                "~/Scripts/moment.min.js",
                "~/Scripts/daterangepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/tempRegistration").Include(
                "~/Scripts/tempRegistration.js"));

            bundles.Add(new ScriptBundle("~/bundles/tempTableJS").Include(
                "~/Scripts/tempTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/facilityTableJS").Include(
                "~/Scripts/facilityTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/requestTableJS").Include(
                "~/Scripts/requestTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/tempAccountCreate").Include(
                "~/Scripts/createTempAccount.js"));

            bundles.Add(new ScriptBundle("~/bundles/tempAvailableDays").Include(
                "~/Scripts/tempAvailableDays.js"));

            bundles.Add(new ScriptBundle("~/bundles/layoutJS").Include(
                "~/Scripts/layout.js"));

            bundles.Add(new ScriptBundle("~/bundles/facilityAccountCreate").Include(
                "~/Scripts/createFacilityAccount.js"));

            bundles.Add(new ScriptBundle("~/bundles/requestForm").Include(
                "~/Scripts/requestForm.js"));

            bundles.Add(new ScriptBundle("~/bundles/googleMap").Include(
                "~/Scripts/maps.js"));

            bundles.Add(new ScriptBundle("~/bundles/mapMarker").Include(
                "~/Scripts/setMapMarker.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-bar-rating").Include(
                "~/Scripts/jquery.barrating.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/professional-review").Include(
                "~/Scripts/professional-review.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Bootstrap4/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/validation.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/daterangepicker.css",
                      "~/Content/PagedList.css",
                      "~/Content/Bootstrap4/bootstrap4-toggle.min.css"));

            bundles.Add(new StyleBundle("~/Content/css/homePage").Include(
                "~/Content/Styles/home-page.css"));

            bundles.Add(new ScriptBundle("~/bundles/change_email").Include(
                "~/Scripts/changeEmail.js"));

            bundles.Add(new ScriptBundle("~/bundles/assigneesRequest").Include(
                "~/Scripts/assigneesForRequest.js"));

            bundles.Add(new ScriptBundle("~/bundles/fullCalendar").Include(
                "~/Scripts/full-calendar-main.js",
                "~/Scripts/full-calendar-daygrid.js",
                "~/Scripts/full-calendar-timegrid.js",
                "~/Scripts/full-calendar-interaction.js"));

            bundles.Add(new ScriptBundle("~/bundles/autocomplete_input").Include(
                "~/Scripts/jquery-ui.js",
                "~/Scripts/autocompleteInput.js"));

            bundles.Add(new ScriptBundle("~/bundles/reviewDetail").Include("~/Scripts/review-detail.js"));
            bundles.Add(new ScriptBundle("~/bundles/requestReviewDetail").Include("~/Scripts/request-review-detail.js"));
            

            bundles.Add(new StyleBundle("~/Content/Styles/autocomplite-input").Include(
                "~/Content/Styles/autocompleteInput.css"));

            bundles.Add(new StyleBundle("~/Content/css/fullCalendar").Include(
                "~/Content/full-calendar-main.css",
                "~/Content/full-calendar-daygrid.css",
                "~/Content/full-calendar-timegrid.css"));

            bundles.Add(new StyleBundle("~/Content/css/jquery-bar-rating").Include(
                "~/Content/bars-square.css"));

            bundles.Add(new StyleBundle("~/Content/css/nav-bar").Include(
                "~/Content/NavBar.css"));

            bundles.Add(new StyleBundle("~/Content/css/footer").Include(
               "~/Content/Footer.css"));

            bundles.Add(new ScriptBundle("~/bundles/impersonate-account").Include(
                    "~/Scripts/impersonate.js"));

            bundles.Add(new ScriptBundle("~/bundles/facility-location").Include(
                "~/Scripts/facilityLocation.js"));

            bundles.Add(new ScriptBundle("~/bundles/request-facility-location").Include(
                "~/Scripts/request-facility-location.js"));

            bundles.Add(new ScriptBundle("~/bundles/manual-fill-request").Include(
                "~/Scripts/manual-fill-request.js"));

            bundles.Add(new ScriptBundle("~/bundles/temp-apply-request").Include(
                "~/Scripts/temp-apply-from-calendar.js"));

            bundles.Add(new ScriptBundle("~/bundles/resend-registration-email").Include(
                "~/Scripts/reSendRegistrationEmail.js"));

            bundles.Add(new ScriptBundle("~/bundles/all-reviews-table").Include(
                "~/Scripts/allReviewsTable.js"));

            bundles.Add(new ScriptBundle("~/bundles/charts").Include(
                "~/Scripts/charts.js"));

            //if you want to test bundles uncomment this line
            //BundleTable.EnableOptimizations = true;
        }
    }
}
