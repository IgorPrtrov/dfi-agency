﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DFI.Data.Accounts;
using DFI.Data.Emails;
using DFI.Data.Requests;
using DFI.Data.Temps;
using DFI.Helpers;
using Hangfire;

namespace DFI.Controllers
{
    public class ProfessionalsController : Controller
    {
        private readonly ITempService _tempService;

        public ProfessionalsController(ITempService tempService)
        {
            _tempService = tempService;
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult Index()
        {
            var result = _tempService.GetTempSearchDto();
            return View(result);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public PartialViewResult Search(TempSearchDto dto)
        {
            var searchDto = dto == null ? new TempSearchDto() : dto;
            var result = _tempService.GetListTempDtos(searchDto);
            return PartialView("_TempsList", result);
        }

        [HttpGet]
        public ActionResult GetTempPhoto(int? id)
        {
            if (!User.IsInRole(Role.Admin) && !User.IsInRole(Role.Temp) && !User.IsInRole(Role.Facility))
            {
                return GetDefaultPhotoActionResult();
            }

            var fileDto = _tempService.GetFile(id);
            if (fileDto.Content == null)
            {
                return GetDefaultPhotoActionResult();
            }

            return File(fileDto.Content, fileDto.MimeType);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult Professional(int? id)
        {
            var temp = _tempService.GetTemp(id);
            return View(temp);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpPost]
        public ActionResult Professional(TempDto dto)
        {
            if (ModelState.IsValid)
            {
                dto.IsAdmin = HttpContext.User.IsInRole(Role.Admin);
                _tempService.UpdateTemp(dto);
                return new EmptyResult();
            }
            var errors = ModelState.Values.SelectMany(c => c.Errors).Select(x => x.ErrorMessage).ToList();
            return Json(string.Join("<br/>", errors));
        }

        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpGet]
        public ActionResult DownloadFile(int? id)
        {
            var fileDto = _tempService.GetFile(id);
            if (fileDto == null)
                return new EmptyResult();

            return File(fileDto.Content, fileDto.MimeType, fileDto.Name);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var validationStatus = _tempService.SetStatus(id, Helper.EState.Deleted);

            if (validationStatus.Success)
                validationStatus.Messages.Add(Helper.EState.Deleted.ToString().ToLowerInvariant());

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult Activate(int id)
        {
            var validationStatus = _tempService.SetStatus(id, Helper.EState.Active);

            if (validationStatus.Success)
            {
                validationStatus.Messages.Add(Helper.EState.Active.ToString().ToLowerInvariant());
                BackgroundJob.Enqueue<IRequestService>(c => c.SearchTempsForRequest(new List<ERequestStatus>() { ERequestStatus.NeedsApproval, ERequestStatus.Filled }, true));
            }

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult Deactivate(int id)
        {
            var validationStatus = _tempService.SetStatus(id, Helper.EState.InActive);

            if (validationStatus.Success)
                validationStatus.Messages.Add(Helper.EState.InActive.ToString().ToLowerInvariant());

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Temp)]
        [HttpGet]
        public ActionResult MyProfile()
        {
            var email = HttpContext.User.Identity.Name;
            return View("Professional", _tempService.GetProfile(email));
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult SendBulkEmails(TempEmailLightDto dto)
        {
            BackgroundJob.Enqueue<ITempService>(c => c.SendBulkEmails(dto));
            return new EmptyResult();
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult SendDirectlyEmail(EmailDto dto)
        {
            BackgroundJob.Enqueue<IEmailService>(c => c.AddEmail(dto));
            return new EmptyResult();
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetTempFirstNames()
        {
            return Json(_tempService.GetTempFirstName(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetTempFullNames(int requestId)
        {
            return Json(_tempService.GetTempFullName(requestId), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetTempLastNames()
        {
            return Json(_tempService.GetTempLastNames(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetTempEmail()
        {
            return Json(_tempService.GetTempEmail(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetTempPhoneNumber()
        {
            return Json(_tempService.GetTempPhoneNumber(), JsonRequestBehavior.AllowGet);
        }

        private ActionResult GetDefaultPhotoActionResult()
        {
            var bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/Images/defaultPhoto.png"));
            return File(bytes, MimeMapping.GetMimeMapping("defaultPhoto.png"));
        }
    }
}