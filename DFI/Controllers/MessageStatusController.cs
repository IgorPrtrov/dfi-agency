﻿using DFI.Data.TextMessages;
using DFI.Helpers;
using Hangfire;
using System.Web.Mvc;

namespace DFI.Controllers
{
    public class MessageStatusController : Controller
    {
        private readonly ILogger _logger;

        public MessageStatusController(ILogger logger)
        {
            _logger = logger;
        }

        // Twilio sends callback
        [HttpPost]
        public ActionResult Index()
        {
            // Log the message id and status
            var smsSid = Request.Form["MessageSid"];
            var messageStatus = Request.Form["MessageStatus"];
            _logger.Info($"MessageStatusController:: {smsSid}, {messageStatus}");

            BackgroundJob.Enqueue<ITextMessageService>(c => c.UpdateSmsStatus(smsSid, messageStatus));

            return Content("Handled");
        }
    }
}