﻿using System.Net;
using System.Web.Mvc;
using DFI.Data.Accounts;
using DFI.Data.Home;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeServices _homeServices;

        public HomeController(IHomeServices homeServices)
        {
            _homeServices = homeServices;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            var reviews = _homeServices.GetGoogleReviews();
            var videos = _homeServices.GetYouTubeVideos();

            return View(new ReviewAndVideoDto() { Reviews = reviews, Videos = videos });
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            var reviews = _homeServices.GetGoogleReviews();
            var videos = _homeServices.GetYouTubeVideos();

            return View(new ReviewAndVideoDto() { Reviews = reviews, Videos = videos });
        }

        [AllowAnonymous]
        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult ErrorInform()
        {
            ViewBag.Error = ExceptionLoggerAttribute.ErrorMessages;
            ExceptionLoggerAttribute.ErrorMessages = string.Empty;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Reviews()
        {
            var reviews = _homeServices.GetGoogleReviews();
            var videos = _homeServices.GetYouTubeVideos();

            return View(new ReviewAndVideoDto() { Reviews = reviews, Videos = videos});
        }

        public ActionResult ProxyImage(string imageUrl)
        {
            using (var client = new WebClient())
            {
                byte[] imageBytes = client.DownloadData(imageUrl);
                return File(imageBytes, "image/jpeg");
            }
        }


        // Index - Display the page with add, update, and delete functionality
        [Authorize(Roles = Role.Admin)]
        public ActionResult Videos()
        {
            var videos = _homeServices.GetAllVideos();
            return View(videos);
        }

        // Add - POST: Add a new video
        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult AddVideo(YouTubeVideo model)
        {
            _homeServices.AddVideo(model);
            return RedirectToAction("Videos");
        }

        // Update - POST: Update an existing video
        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult UpdateVideo(YouTubeVideo model)
        {
            _homeServices.UpdateVideo(model);
            return RedirectToAction("Videos");
        }

        // Delete - POST: Delete a video
        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult DeleteVideo(int id)
        {
            _homeServices.DeleteVideo(id);
            return RedirectToAction("Videos");
        }
    }
}