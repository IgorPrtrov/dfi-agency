using DFI.Data.Requests;
using System.Web.Mvc;
using DFI.Data.Accounts;
using DFI.Data.Common;
using DFI.Helpers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace DFI.Controllers
{
    public class RequestsController : Controller
    {
        private readonly IRequestService _requestService;

        public RequestsController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        [HttpGet]
        public ActionResult Index()
        {

            var model = _requestService.GetRequestSearchDto(User.IsInRole(Role.Temp), HttpContext.User.Identity.Name);

            return View(model);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        [HttpPost]
        public PartialViewResult Search(RequestSearchDto dto)
        {
            dto.ForFacility = User.IsInRole(Role.Facility);
            dto.ForTemp = User.IsInRole(Role.Temp);
            dto.ForAdmin = User.IsInRole(Role.Admin);
            dto.HttpContextUserEmail = HttpContext.User.Identity.Name;

            var result = _requestService.GetRequests(dto);
            return PartialView("_RequestsList", result);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpGet]
        public ActionResult Create()
        {
            var security = new SecurityAccess()
            {
                ForFacility = User.IsInRole(Role.Facility),
                HttpContextUserEmail = HttpContext.User.Identity.Name
            };

            var access = _requestService.GetAccessToCreateRequest(security);
            if (access.Success)
            {
                var model = _requestService.GetCreateModel(security);
                return View(model);
            }

            return RedirectToAction("RequestDenay", new { message = string.Join(", ", access.Messages) });
        }

        [HttpGet]
        [Authorize(Roles = Role.Facility)]
        public ActionResult RequestDenay(string message)
        {
            return View("RequestDenay", model: message);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var dto = new GetRequestDto()
            {
                Id = id,
                ForAdmin = User.IsInRole(Role.Admin),
                ForFacility = User.IsInRole(Role.Facility),
                ForTemp = User.IsInRole(Role.Temp),
                HttpContextUserEmail = HttpContext.User.Identity.Name
            };

            var model = _requestService.GetRequest(dto);
            return View(
                model.Status == ERequestStatus.Draft || model.Status == null
                    ? "Create" : dto.ForAdmin && (model.Status == ERequestStatus.Filled || model.Status == ERequestStatus.NeedsApproval) ? "HoursChange"
                    : "View",
                model);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpGet]
        public ActionResult Get(int id)
        {
            var requestFromDb = _requestService.GetRequest(new GetRequestDto()
            {
                Id = id,
                ForAdmin = User.IsInRole(Role.Admin),
                ForFacility = User.IsInRole(Role.Facility),
                HttpContextUserEmail = HttpContext.User.Identity.Name
            });
            return PartialView("_RequestEditor", requestFromDb);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult Update(RequestDto dto)
        {
            var validationStatus = UpdateRequest(dto);
            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult UpdateHours(RequestDto dto)
        {
            dto.SubmitterEmail = User.Identity.Name;
            dto.SubmitterIsAdmin = User.IsInRole(Role.Admin);

            var validationStatus = _requestService.UpdateRequestHours(dto);

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult SaveAndSubmit(RequestDto dto)
        {
            var validationStatus = UpdateRequest(dto, dto.IsAutoSearching != null && dto.IsAutoSearching == false ? ERequestStatus.NeedsApproval : ERequestStatus.Active);
            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult AddEmptyDay(RequestDto dto)
        {
            dto.SubmitterEmail = User.Identity.Name;
            dto.SubmitterIsAdmin = User.IsInRole(Role.Admin);
            var id = _requestService.AddEmptyDay(dto);
            return Json(id);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult AddEmptyDayWithSameHours(RequestDto dto)
        {
            dto.SubmitterEmail = User.Identity.Name;
            dto.SubmitterIsAdmin = User.IsInRole(Role.Admin);
            var id = _requestService.AddEmptyDayWithSameHours(dto);
            return Json(id);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult RemoveRequest(RemoveDto remove)
        {
            var request = _requestService.GetRequest(new GetRequestDto
            {
                Id = remove.EntityId,
                ForAdmin = User.IsInRole(Role.Admin),
                ForFacility = User.IsInRole(Role.Facility),
                HttpContextUserEmail = HttpContext.User.Identity.Name
            });

            if (request != null && (User.IsInRole(Role.Admin) || request.Status == ERequestStatus.Draft))
            {
                _requestService.RemoveRequest(remove.EntityId);
            }

            return new EmptyResult();
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult RemoveRequestDay(RemoveDto remove)
        {
            _requestService.RemoveRequestDay(remove.EntityId);
            return new EmptyResult();
        }

        [HttpGet]
        public async Task<ActionResult> ApplyToRequest(int tempId, string longGuid)
        {
            var applyTempDto = _requestService.GetApplyTempDto(tempId, longGuid);
            await AuthHelper.Impersonate(HttpContext, applyTempDto.Email);

            return RedirectToAction("Calendar", 
                new 
                { 
                    eCalendarEvent = ECalendarEvent.History, 
                    requestId = applyTempDto.RequestId,
                    requestDayId = applyTempDto.RequestDayId
                });
        }

        [HttpGet]
        public ActionResult Apply(int tempId, string longGuid, bool applyFromCalendar = false)
        {
            var result = _requestService.ApplyTempToRequest(new ApplyTempToRequestDto()
            {
                TempId = tempId,
                LongGuid = longGuid
            });

            if (applyFromCalendar)
                return Json(result, JsonRequestBehavior.AllowGet);

            return View(result);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        [HttpGet]
        public PartialViewResult GetAssigneesForRequest(GetAssigneesForRequestDto dto)
        {
            dto.ForTemp = User.IsInRole(Role.Temp);
            dto.ForAdmin = User.IsInRole(Role.Admin);
            dto.HttpContextUserEmail = HttpContext.User.Identity.Name;

            var result = _requestService.GetRequestAssigneeList(dto);
            return PartialView("_AssigneesForRequest", result);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult FillRequest(int requestAssigneeId)
        {
            var result = _requestService.FillRequest(requestAssigneeId);
            return Json(result);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpPost]
        public ActionResult CancelFilledRequest(int requestAssigneeId)
        {
            var access = new SecurityAccess()
            {
                ForTemp = User.IsInRole(Role.Temp),
                HttpContextUserEmail = HttpContext.User.Identity.Name
            };
            var result = _requestService.CancelFilledRequest(requestAssigneeId, access);
            return Json(result);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult CancelRequest(RequestCancelDto dto)
        {
            dto.EmailWhoCanceled = User.Identity.Name;
            dto.RoleWhoCanceled = User.IsInRole(Role.Admin) ? Role.Admin : Role.Facility;

            var result = _requestService.CancelRequest(dto);

            return Json(result);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        [HttpGet]
        public ActionResult Calendar(ECalendarEvent eCalendarEvent)
        {
            var dto = new RequestSearchDto
            {
                ForFacility = User.IsInRole(Role.Facility),
                ForTemp = User.IsInRole(Role.Temp),
                ForAdmin = User.IsInRole(Role.Admin),
                HttpContextUserEmail = HttpContext.User.Identity.Name,
                CalendarEvent = eCalendarEvent
            };

            var requests = _requestService.GetRequestsAsCalendarEvents(dto);

            var model = new RequestCalendarViewModel
            {
                SerializedEventsData = JsonConvert.SerializeObject(requests)
            };

            return View("Calendar", model);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        [HttpGet]
        public PartialViewResult CalendarRequest(int id, int requestDayId)
        {
            var request = _requestService.GetRequest(new GetRequestDto()
            {
                Id = id,
                ForFacility = User.IsInRole(Role.Facility),
                ForTemp = User.IsInRole(Role.Temp),
                ForAdmin = User.IsInRole(Role.Admin),
                HttpContextUserEmail = HttpContext.User.Identity.Name,
                RequestDayIdForCalendar = requestDayId
            });

            return PartialView("_CalendarRequest", request);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public PartialViewResult GetRequestForInactiveOrDeletedTemp(int tempId)
        {
            var requests = _requestService.GetRequestForInactiveOrDeletedTemp(tempId);
            return PartialView("_RequestForInactiveOrDeletedTemp", requests);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetRequestPrice(int requestId)
        {
            var result = _requestService.GetRequestPrice(requestId);
            return Json(result);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult ManualFillRequest(int? requestId, int? tempId)
        {
            var result = _requestService.ManualFillRequest(requestId, tempId);
            if (result.ValidationStatus.Success)
            {
                var fillResult = _requestService.FillRequest(result.RequestAssigneId);
                return Json(fillResult);
            }

            return Json(result.ValidationStatus);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpGet]
        public ActionResult GetTempsFullNamesForRequest(int requestId)
        {
            return Json(_requestService.GetTempsFullNamesForRequest(requestId), JsonRequestBehavior.AllowGet);
        }

        private ValidationStatus UpdateRequest(RequestDto dto, ERequestStatus? status = null)
        {
            dto.SubmitterEmail = User.Identity.Name;
            dto.SubmitterIsAdmin = User.IsInRole(Role.Admin);
            if (status.HasValue)
            {
                dto.Status = status;
            }
            return _requestService.UpdateRequest(dto);
        }
    }
}