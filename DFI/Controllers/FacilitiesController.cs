﻿using System.Linq;
using System.Web.Mvc;
using DFI.Data.Accounts;
using DFI.Data.ChartingSoftwares;
using DFI.Data.Emails;
using DFI.Data.Facilities;
using DFI.Data.XraySoftwares;
using DFI.Helpers;
using Hangfire;

namespace DFI.Controllers
{
    public class FacilitiesController : Controller
    {
        private readonly IFacilityService _facilityService;
        
        private readonly IXraySoftwareService _xraySoftwareService;
        
        private readonly IChartingSoftwareService _chartingSoftwareService;

        public FacilitiesController(
            IFacilityService facilityService,
            IXraySoftwareService xraySoftwareService,
            IChartingSoftwareService chartingSoftwareService)
        {
            _facilityService = facilityService;
            _xraySoftwareService = xraySoftwareService;
            _chartingSoftwareService = chartingSoftwareService;
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult Index()
        {
            var email = HttpContext.User.Identity.Name;
            var result = new FacilitySearchDto();
            return View(result);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public PartialViewResult Search(FacilitySearchDto dto)
        {
            var searchDto = dto ?? new FacilitySearchDto();
            var result = _facilityService.GetListFacilityDtos(searchDto);
            return PartialView("_FacilitiesList", result);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult Facility(int? id)
        {
            var facility = _facilityService.GetFacility(id);
            return View(facility);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult Facility(FacilityDto dto)
        {
            if (ModelState.IsValid)
            {
                _facilityService.UpdateFacility(dto);
                return new EmptyResult();
            }

            var errors = ModelState.Values.SelectMany(c => c.Errors).Select(x => x.ErrorMessage).ToList();
            return Json(string.Join("<br/>", errors));
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var validationStatus = _facilityService.SetStatus(id, Helper.EState.Deleted);

            if (validationStatus.Success)
                validationStatus.Messages.Add(Helper.EState.Deleted.ToString().ToLowerInvariant());

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult Activate(int id)
        {
            var validationStatus = _facilityService.SetStatus(id, Helper.EState.Active);

            if (validationStatus.Success)
                validationStatus.Messages.Add(Helper.EState.Active.ToString().ToLowerInvariant());

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult Deactivate(int id)
        {
            var validationStatus = _facilityService.SetStatus(id, Helper.EState.InActive);

            if (validationStatus.Success)
                validationStatus.Messages.Add(Helper.EState.InActive.ToString().ToLowerInvariant());

            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Facility)]
        [HttpGet]
        public ActionResult MyProfile()
        {
            var email = HttpContext.User.Identity.Name;
            return View("Facility", _facilityService.GetProfile(email));
        }

        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpGet]
        public ActionResult GetFacilityNames(EFacilityNameFor eFacilityNameFor)
        {
            return Json(_facilityService.GetFacilityNames(eFacilityNameFor), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpGet]
        public ActionResult GetFacilityEmails()
        {
            return Json(_facilityService.GetFacilityEmails(), JsonRequestBehavior.AllowGet);
        }
        
        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpGet]
        public ActionResult GetFacilityPhoneNumbers()
        {
            return Json(_facilityService.GetFacilityPhoneNumbers(), JsonRequestBehavior.AllowGet);
        }
        
        [Authorize(Roles = Role.Admin + "," + Role.Temp)]
        [HttpGet]
        public ActionResult GetFacilityCities()
        {
            return Json(_facilityService.GetFacilityCities(), JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public ActionResult GetXraySoftwareNames()
        {
            return Json(_xraySoftwareService.GetXraySoftwareNames(), JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public ActionResult GetChartingSoftwareNames()
        {
            return Json(_chartingSoftwareService.GetChartingSoftwareNames(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpGet]
        public ActionResult GetFacilityLocation(int? facilityId, int? facilityLocationId)
        {

            var dto = _facilityService.GetFacilityLocationDto(facilityId, facilityLocationId, User.IsInRole(Role.Admin), HttpContext.User.Identity.Name);
            return View("FacilityLocation", dto);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult CreateUpdateFacilityLocation(FacilityLocationDto dto)
        {
            if (ModelState.IsValid)
            {
                if(dto.Id.HasValue && dto.Id > 0)
                {
                    dto.WhoModified = HttpContext.User.Identity.Name;
                }
                else
                {
                    dto.WhoCreated = HttpContext.User.Identity.Name;
                }

                var result = _facilityService.SubmitFacilityLocationForm(dto);
                if (result.Success)
                {
                    if (result.Messages.Any())
                        return Json(int.Parse(result.Messages.FirstOrDefault())); //send location id for redirect to proper url

                    return new EmptyResult();
                }
                    
                return Json(string.Join("<br/>", result.Messages));
            }

            var errors = ModelState.Values.SelectMany(c => c.Errors).Select(x => x.ErrorMessage).ToList();
            return Json(string.Join("<br/>", errors));
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpGet]
        public ActionResult GetLocationsForFacility(int? facilityId)
        {
            return Json(_facilityService.GetFacilityLocations(facilityId ?? 0), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        [HttpPost]
        public ActionResult DeleteFacilityLocation(int locationId)
        {
            var validationStatus = _facilityService.DeleteLocation(locationId, User.IsInRole(Role.Admin), HttpContext.User.Identity.Name);
            return Json(validationStatus);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult SendBulkEmails(FacilityEmailLightDto dto)
        {
            BackgroundJob.Enqueue<IFacilityService>(c => c.SendBulkEmails(dto));
            return new EmptyResult();
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult SendDirectlyEmail(EmailDto dto)
        {
            BackgroundJob.Enqueue<IEmailService>(c => c.AddEmail(dto));
            return new EmptyResult();
        }
    }
}