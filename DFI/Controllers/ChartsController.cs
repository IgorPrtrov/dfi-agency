﻿using DFI.Data.Accounts;
using DFI.Data.Charts;
using System;
using System.Web.Mvc;

namespace DFI.Controllers
{
    public class ChartsController : Controller
    {
        private readonly IChartService _chartService;

        public ChartsController(IChartService chartService)
        {
            _chartService = chartService;
        }

        [Authorize(Roles = Role.Admin)]
        public ActionResult Index()
        {
            return View(new ChartsDto()
            {
                From = DateTime.Now.AddYears(-1),
                To = DateTime.Now
            });
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetProfit(DateTime from, DateTime to)
        {
            return Json(_chartService.GetProfitChar(from, to), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetRequestsWithStatuses(DateTime from, DateTime to)
        {
            return Json(_chartService.GetRequestsWithStatuses(from, to), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetRequestsForTemps(DateTime from, DateTime to)
        {
            return Json(_chartService.GetRequestsForTemps(from, to), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public ActionResult GetRequestsForOffices(DateTime from, DateTime to)
        {
            return Json(_chartService.GetRequestsForOffices(from, to), JsonRequestBehavior.AllowGet);
        }
    }
}