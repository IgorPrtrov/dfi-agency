using System;
using System.Web.Mvc;
using DFI.Data.Accounts;
using DFI.Data.ProfessionalReviews;

namespace DFI.Controllers
{
    public class ProfessionalReviewsController : Controller
    {
        private readonly IProfessionalReviewService _professionalReviewService;

        public ProfessionalReviewsController(IProfessionalReviewService professionalReviewService)
        {
            _professionalReviewService = professionalReviewService;
        }
        
        [HttpGet]
        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        // before change the URL of this action, pay attention to EmailConstants.ProfessionalReviewLinkTemplate constant
        public ActionResult Review(int tempId, int requestId)
        {
            var userEmail = User.Identity.Name;
            var hasAccessToReview = _professionalReviewService.HasAccessToCreateReview(tempId, requestId, userEmail);

            if (hasAccessToReview.Success == false)
                return RedirectToAction("ReviewDenay", new { message = string.Join(", ", hasAccessToReview.Messages) });

            return View();
        }

        [HttpPost]
        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        public ActionResult Review(ProfessionalReviewDto dto)
        {
            if (ModelState.IsValid)
            {
                var userEmail = User.Identity.Name;
            
                var result = _professionalReviewService.AddReview(dto, userEmail);

                if (result.Success)
                    return RedirectToAction("ReviewWasSentSuccessfully");
                else
                    return RedirectToAction("ReviewDenay", new { message = string.Join(", ", result.Messages) });
            }
            else
            {
                return View(dto);
            }
        }

        [HttpGet]
        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        public ActionResult ReviewDenay(string message)
        {
            return View("ReviewDenay", model: message);
        }
        
        [HttpGet]
        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        public ActionResult ReviewWasSentSuccessfully()
        {
            return View("ReviewWasSentSuccessfully");
        }

        [HttpGet]
        [Authorize(Roles = Role.Admin + "," + Role.Facility)]
        public PartialViewResult GetReviewDetailToLeaveReview(int tempId, int requestId)
        {
            var model = _professionalReviewService.CreateInitialModel(tempId, requestId);      
            return PartialView("_ReviewDetail", model);
        }


        [HttpGet]
        [Authorize(Roles = Role.Admin + "," + Role.Facility + "," + Role.Temp)]
        public PartialViewResult GetReadonlyReviewDetail(int tempId, int requestId)
        {
            var model = _professionalReviewService.GetReadonlyReview(tempId, requestId);
            if (model == null)
                return PartialView("_NotYetReview");

            return PartialView("_ReviewDetail", model);
        }

        [HttpGet]
        [Authorize(Roles = Role.Admin)]
        public PartialViewResult GetAllProfessionalReviews(int? tempId, int pageNumber)
        {
            if(tempId == null)
            {
                throw new ArgumentException("Temp is null");
            }
            var reviews = _professionalReviewService.GetListOfReviews(tempId.Value, pageNumber);
            return PartialView("_AllReviewsList", reviews);
        }

        [HttpGet]
        [Authorize(Roles = Role.Admin)]
        public ActionResult AllProfessionalReviews(int? tempId, string firstName, string lastName)
        {
            if (tempId == null)
            {
                throw new ArgumentException("Temp is null");
            }

            ViewBag.FirstName = firstName;
            ViewBag.LastName = lastName;

            return View();
        }
    }
}