﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using DFI.Data.Accounts;
using DFI.Data.Facilities;
using DFI.Data.GoogleMaps;
using DFI.Data.Temps;
using DFI.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace DFI.Controllers
{
    public class RegistratorController : Controller
    {
        private readonly ITempService _tempService;
        private readonly IGoogleMapService _googleMapService;
        private readonly IFacilityService _facilityService;
        private readonly ILogger _logger;

        public RegistratorController(ITempService tempService, IGoogleMapService googleMapService, IFacilityService facilityService, ILogger logger)
        {
            _tempService = tempService;
            _googleMapService = googleMapService;
            _facilityService = facilityService;
            _logger = logger;
        }
        private ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [HttpGet]
        public ActionResult TempRegistration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult TempRegistration(GetRegistrationFormDto dto)
        {
            if (!CloudflareTurnstile.IsValidRequest(Request, _logger))
            {
                ViewBag.ValidationMessages = "Oops! Something went wrong while submitting the form. Please try again later";
                return View(dto);
            }

            if (ModelState.IsValid)
            {
                var result = _tempService.GetRegistrationForm(dto);

                if (result.FindingResult == EStatusCode.LicenseNotFound)               
                    ModelState.AddModelError("error", "License info not found");
                
                else if (result.FindingResult == EStatusCode.AlreadyExists)
                    ModelState.AddModelError("error", "User already exists");

                else if (result.FindingResult == EStatusCode.AccountWasDeleted)
                    ModelState.AddModelError("error", "User was deleted");

                else if (result.FindingResult == EStatusCode.ValidationGraderError)
                    ModelState.AddModelError("error", result.ValidationGraderError);

                else
                {
                    TempData["TempRegistrationFormDto"] = result;
                    return RedirectToAction("TempRegistrationForm");
                }
            }

            var errors = ModelState.Values.SelectMany(c => c.Errors).Select(x => x.ErrorMessage).ToList();
            if (errors.Any())
                ViewBag.ValidationMessages = string.Join("<br/>", errors);

            return View(dto);
        }

        [HttpGet]
        public ActionResult TempRegistrationForm()
        {
            TempRegistrationFormDto dto = TempData["TempRegistrationFormDto"] as TempRegistrationFormDto;

            if (dto == null)
                return RedirectToAction("TempRegistration");

            return View(dto);
        }

        [HttpPost]
        public ActionResult TempRegistrationForm(TempRegistrationFormDto dto)
        {         
            if (ModelState.IsValid)
            {
                dto.CallbackUrl = Url.Action("ConfirmEmail", "Account",
                    new { userId = "userIdValue", code = "codeConfirmationValue" }, protocol: Request.Url.Scheme);

                _tempService.SubmitRegistrationForm(dto);

                var ip = IpHelper.GetIPAddress(Request);
                _logger.Info($"TempRegistrationForm:: ip = {ip}");

                return new EmptyResult();
            }
            var errors = ModelState.Values.SelectMany(c => c.Errors).Select(x => x.ErrorMessage).ToList();
            return Json(string.Join("<br/>", errors));
        }

        [HttpGet]
        public ActionResult FacilityRegistration()
        {
            var model = new FacilityRegistrationDto();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateFacilityAccount(FacilityRegistrationDto dto)
        {
            if (!CloudflareTurnstile.IsValidRequest(Request, _logger))
            {
                return Json("Oops! Something went wrong while submitting the form. Please try again later.");
            }

            if (ModelState.IsValid)
            {
                dto.CallbackUrl = Url.Action("ConfirmEmail", "Account",
                    new { userId = "userIdValue", code = "codeConfirmationValue" }, protocol: Request.Url.Scheme);

                var facilityId = _facilityService.SubmitRegistrationForm(dto);

                var ip = IpHelper.GetIPAddress(Request);
                _logger.Info($"CreateFacilityAccount:: ip = {ip}");

                if (!User.IsInRole(Role.Admin))
                    Impersonate(dto.FacilityDto.Email);

                return Json(facilityId);
            }
            var errors = ModelState.Values.SelectMany(c => c.Errors).Select(x => x.ErrorMessage).ToList();
            return Json(string.Join("<br/>", errors));
        }

        private void Impersonate(string email) 
        {
            var userToImpersonate = UserManager.FindByEmailAsync(email).Result;

            var identityToImpersonate = UserManager.CreateIdentityAsync(userToImpersonate,
                    DefaultAuthenticationTypes.ApplicationCookie).Result;

            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties()
            {
                IsPersistent = false
            }, identityToImpersonate);
        }
    }
}