﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Linq;
using DFI.Data.Accounts;
using DFI.Data.Emails;
using DFI.Data.Facilities;
using DFI.Data.Temps;
using DFI.Helpers;
using Hangfire;

namespace DFI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IEmailService _mailService;
        private readonly ILogger _logger;
        private readonly IFacilityRepository _facilityRepository;
        private readonly ITempRepository _tempRepository;

        public AccountController(IEmailService mailService, ILogger logger, IFacilityRepository facilityRepository, ITempRepository tempRepository)
        {
            _mailService = mailService;
            _logger = logger;
            _facilityRepository = facilityRepository;
            _tempRepository = tempRepository;
        }

        private ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();                 
        private IAuthenticationManager AuthenticationManager =>  HttpContext.GetOwinContext().Authentication;
            
        
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View(new Login());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(Login model, string returnUrl)
        {
            if (!CloudflareTurnstile.IsValidRequest(Request, _logger))
            {
                ModelState.AddModelError("NamePassword", "Oops! Something went wrong while submitting the form. Please try again later.");
                return View(model);
            }

            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Name, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("NamePassword", "Incorrect login or password");
                    return View(model);
                }

                var roles = UserManager.GetRoles(user?.Id).ToList();
                if (!roles.Any(c => c == Role.Admin) && user.Status != (int)Helper.EState.Active && user.Status != (int)Helper.EState.InActive && user.Status != (int)Helper.EState.Submitted)
                {
                    ModelState.AddModelError("NamePassword", "This account isn't able to login with the current status.");
                    return View(model);
                }
                else
                {
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    if (string.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index","Home");
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = UserManager.FindByEmail(model.Email);
                    if (user == null)
                    {
                        throw new Exception("ForgotPassword:: user is null");
                    }
                    string code = UserManager.GeneratePasswordResetToken(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new {code}, protocol: Request.Url.Scheme);

                    _mailService.AddEmail(new EmailDto()
                    {
                        Body = string.Format(EmailConstants.ForgotPasswordBody, user.UserName, callbackUrl),
                        Subject = EmailConstants.ForgotPasswordSubject,
                        To = model.Email 
                    });

                    user.CodeToChangePassword = code;
                    UserManager.Update(user);
                    return RedirectToAction("ForgotPasswordInform", "Account");
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                
            }
            return View(model);
        }

        public ActionResult ForgotPasswordInform()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            var user = UserManager.Users.FirstOrDefault(p => p.CodeToChangePassword == code);
            if (user == null)
                throw new Exception("ResetPassword::user is null");
            
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)           
                return View(model);
            
            var user = UserManager.Users.FirstOrDefault(p => p.Email == model.Email);

            if (user == null)            
                throw new Exception("ResetPassword:: user is null");
            
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);

            if (result.Succeeded == false)
            {
                if(result.Errors.Any(c => c.Contains("Invalid token")))
                    throw new Exception($"ResetPassword:: {string.Join(", ", result.Errors)} You need to click again forgot 'Forgot password' and receive a new email message with a new link.");
                else
                    throw new Exception($"ResetPassword:: {string.Join(", ", result.Errors)}");
            }

            return RedirectToAction("ResetPasswordConfirmation", "Account");
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ConfirmEmail(string userId, string code, string newEmail)
        {
            _logger.Info($"EmailConfirmEmail:: user id = {userId}, token = {code}.");

            if (userId == null || code == null)
                throw new Exception("User Id is null or code is null");

            IdentityResult result;
            try
            {
                if (UserManager.IsEmailConfirmed(userId) && string.IsNullOrEmpty(newEmail))
                {
                    ViewBag.Message = $"The email was confirmed successfully. Please add office locations. Our administration team is reviewing your profile and you will have full access shortly";
                    return View();
                }

                result = UserManager.ConfirmEmailAsync(userId, code).Result;
                _logger.Info($"EmailConfirmEmail:: result Succeeded = {result.Succeeded}, result Errors = {string.Join(", ", result.Errors)}");
            }
            catch (InvalidOperationException ex)
            {
                _logger.Error($"EmailConfirmEmail:: {ex.Message}, (throws when the userId is not found)");
                throw new Exception("Confirm Email throws when the user Id is not found");
            }

            var user = UserManager.Users.FirstOrDefault(c => c.Id == userId);

            if (result.Succeeded)
            {
                var facility = _facilityRepository.GetByEmail(user.Email);
                var temp = _tempRepository.GetByEmail(user.Email);

                if (!string.IsNullOrEmpty(newEmail))
                {
                    if (user.NewEmail.ToLower() == newEmail.ToLower())
                    {
                        user = _mailService.UpdateEmails(user, newEmail);
                        UserManager.Update(user);
                        _logger.Info($"EmailConfirmEmail:: result Succeeded = {result.Succeeded}, Email {newEmail} has been verified.");

                        ViewBag.Message = $"The email {user.Email} was confirmed successfully.";
                        return View();
                    }

                    else
                        throw new Exception("The new email was changed or already confirmed");
                }
                else if (facility != null && facility.Status != (int)Helper.EState.Active)
                {
                    _mailService.AddEmail(new EmailDto()
                    {
                        To = Setting.AdministratorEmail,
                        Body = string.Format(EmailConstants.NewApplicationBody, Helper.GetFacilityUrl(facility.FacilityId)),
                        Subject = string.Format(EmailConstants.NewApplicationSubject, Role.Facility.ToString())
                    });
                }
                else if(temp != null && temp.Status != (int)Helper.EState.Active)
                {
                    _mailService.AddEmail(new EmailDto()
                    {
                        To = Setting.AdministratorEmail,
                        Body = string.Format(EmailConstants.NewApplicationBody, Helper.GetTempUrl(temp.TempId)),
                        Subject = string.Format(EmailConstants.NewApplicationSubject, Role.Temp.ToString())
                    });
                }

                var infoAboutLocation = facility != null && !facility.FacilityLocations.Any() ? "Please add office locations." : string.Empty;

                ViewBag.Message = $"The email {user.Email} was confirmed successfully. {infoAboutLocation} Our administration team is reviewing your profile and you will have full access shortly";
                return View();
            }           
 
            if (user != null)
            {
                ResendEmailConfirmToken(user);
                ViewBag.Message = "The email confirmation link was sent again";
                return View();
            }
            else            
                throw new Exception("The email confirmation is failed, the user is not found");            
        }

        private void ResendEmailConfirmToken(ApplicationUser user)
        {
            var token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;

            var callbackUrl = string.Empty;

            if (string.IsNullOrEmpty(user.NewEmail))
                callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = token }, protocol: Request.Url.Scheme);

            else
                callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = token, newEmail = user.NewEmail }, protocol: Request.Url.Scheme);

            _mailService.AddEmail(new EmailDto()
            {
                To = string.IsNullOrEmpty(user.NewEmail) ? user.Email : user.NewEmail,
                Subject = EmailConstants.AccountConfirmSubject,
                Body = string.Format(EmailConstants.AccountConfirmBody, callbackUrl)
            });
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeEmail(EmailChangeDto dto)
        {
            var validationStatus = Helper.ValidateDto(dto);
            
            if (validationStatus.Success)
            {
                if (UserManager.Users.Any(c => c.Email.ToLower() == dto.NewEmail))
                {
                    validationStatus.Success = false;
                    validationStatus.Messages.Add("Email already exists");
                    return Json(validationStatus);
                }

                var user = UserManager.Users.FirstOrDefault(c => c.Email == dto.Email);
                if(user == null)
                {
                    validationStatus.Success = false;
                    validationStatus.Messages.Add("Can not find user");
                    return Json(validationStatus);
                }

                var token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                var callbackUrl = Url.Action("ConfirmEmail", "Account",
                    new { userId = user.Id, code = token, newEmail = dto.NewEmail }, protocol: Request.Url.Scheme);

                _mailService.AddEmail(new EmailDto()
                {
                    To = dto.NewEmail,
                    Subject = EmailConstants.EmailConfirmSubject,
                    Body = string.Format(EmailConstants.EmailConfirmBody, callbackUrl)
                });

                user.NewEmail = dto.NewEmail;
                UserManager.Update(user);
            }

            return Json(validationStatus);
        }

        [HttpPost]
        [Authorize(Roles = Role.Admin)]
        public async Task Impersonate(string email)
        {
            await AuthHelper.Impersonate(HttpContext, email);
        }

        [HttpPost]
        [Authorize(Roles = Role.Admin)]
        public void ReSendRegistrationEmail(string email)
        {
            BackgroundJob.Enqueue<IEmailService>(c => c.ResendRegistrationEmail(email));
        }
    }
}