﻿using System;
using System.Collections.Generic;
using System.Web.Hosting;
using DFI.Data.Emails;
using DFI.Data.Requests;
using DFI.Data.Temps;
using DFI.Data.TextMessages;
using Hangfire;
using Hangfire.SqlServer;
using Ninject;


namespace DFI
{
    public class HangfireBootstrapper : IRegisteredObject
    {
        public static readonly HangfireBootstrapper Instance = new HangfireBootstrapper();

        private readonly object _lockObject = new object();
        private bool _started;

        private BackgroundJobServer _backgroundJobServer;

        private HangfireBootstrapper(){}

        public void Start(IKernel kernel)
        {
            lock (_lockObject)
            {
                if (_started)
                    return;

                _started = true;

                HostingEnvironment.RegisterObject(this);
                GlobalConfiguration.Configuration
                    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                    .UseSimpleAssemblyNameTypeSerializer()
                    .UseRecommendedSerializerSettings()
                    .UseSqlServerStorage(Setting.ConnectionString, new SqlServerStorageOptions
                    {
                        CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                        SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                        QueuePollInterval = TimeSpan.Zero,
                        UseRecommendedIsolationLevel = true,
                        DisableGlobalLocks = true                      
                    })
                    .UseNinjectActivator(kernel);

                _backgroundJobServer = new BackgroundJobServer();

                RecurringJob.AddOrUpdate<IEmailService>("Send emails", c => c.Send(), Cron.Minutely());
                RecurringJob.AddOrUpdate<ITextMessageService>("Send SMS", c => c.SendSms(), Cron.Minutely());
                RecurringJob.AddOrUpdate<IRequestService>("Search Temps For Requests", c => c.SearchTempsForRequest(new List<ERequestStatus>() { ERequestStatus.Active }, false  ), "*/10 * * * *");
                RecurringJob.AddOrUpdate<IRequestService>("Handle Requests Statuses", c => c.HandleRequestsStatuses(), "*/3 * * * *");
                RecurringJob.AddOrUpdate<ITempService>("Check Temp Licenses Expiration Day", c => c.CheckTempLicensesExpirationDay(), Cron.Daily(18));
                RecurringJob.AddOrUpdate<IRequestService>("Send Confirmation Before Start Request", c => c.SendConfirmationBeforeStartRequest(), "*/15 * * * *");
                RecurringJob.AddOrUpdate<IEmailService>("Send Daily Emails", c => c.SendDailyEmails(), Cron.Daily(20));

                RecurringJob.AddOrUpdate<IEmailService>("(Obsolete) Send Temp Emails", c => c.SendTempEmails(), Cron.Never);
                RecurringJob.AddOrUpdate<IEmailService>("(Obsolete) Send Office Emails", c => c.SendOfficeEmails(), Cron.Never);
            }
        }

        public void Stop()
        {
            lock (_lockObject)
            {
                if (_backgroundJobServer != null)
                    _backgroundJobServer.Dispose();

                HostingEnvironment.UnregisterObject(this);
            }
        }

        void IRegisteredObject.Stop(bool immediate)
        {
            Stop();
        }
    }
}