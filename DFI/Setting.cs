using Microsoft.Extensions.Configuration;

namespace DFI
{
    public static class Setting
    {
        public static string DbName = CustomConfigurationManager.AppSetting["DbName"];
        public static string ConnectionString = CustomConfigurationManager.AppSetting["ConnectionString"];
        public static string FromEmail = CustomConfigurationManager.AppSetting["FromEmail"];
        public static string FromEmailPassword = CustomConfigurationManager.AppSetting["FromEmailPassword"];
        public static string GoogleMapsApi = CustomConfigurationManager.AppSetting["GoogleMapsApi"];
        public static string UrlSearchForPersonOrFacilty = CustomConfigurationManager.AppSetting["UrlSearchForPersonOrFacilty"];
        public static string UrlGetPersonOrFacilityDetails = CustomConfigurationManager.AppSetting["UrlGetPersonOrFacilityDetails"];
        public static string DpapiDataProtectionProvider = CustomConfigurationManager.AppSetting["DpapiDataProtectionProvider"];
        public static string DataProtectorTokenProviderAppUser = CustomConfigurationManager.AppSetting["DataProtectorTokenProviderAppUser"];
        public static string HangfireUrl = CustomConfigurationManager.AppSetting["HangfireUrl"];
        public static string LoginPath = CustomConfigurationManager.AppSetting["LoginPath"];
        public static string Password = CustomConfigurationManager.AppSetting["Password"];
        public static string Admin = CustomConfigurationManager.AppSetting["Admin"];
        public static string LocalLoginPageUrl = CustomConfigurationManager.AppSetting["LocalLoginPageUrl"];
        public static int MinHoursBeforRequestStart = int.Parse(CustomConfigurationManager.AppSetting["MinHoursBeforRequestStart"]);
        public static int MinHoursBeforeSendFacilityAddress = int.Parse(CustomConfigurationManager.AppSetting["MinHoursBeforeSendFacilityAddress"]);
        public static double MetersInMile = double.Parse(CustomConfigurationManager.AppSetting["MetersInMile"]);

        public static string MailGunUri = CustomConfigurationManager.AppSetting["MailGunUri"];
        public static string KeyMailGun = CustomConfigurationManager.AppSetting["KeyMailGun"];
        public static string Domain = CustomConfigurationManager.AppSetting["Domain"];
        public static string FromMailGun = CustomConfigurationManager.AppSetting["FromMailGun"];

        public static double TemporaryPlacementFee = double.Parse(CustomConfigurationManager.AppSetting["TemporaryPlacementFee"]);
        public static double DentistTemporaryPlacementFee = double.Parse(CustomConfigurationManager.AppSetting["DentistTemporaryPlacementFee"]);

        public static string AdministratorEmail = CustomConfigurationManager.AppSetting["AdministratorEmail"];

        public static bool SendEmails = bool.Parse(CustomConfigurationManager.AppSetting["SendEmails"]);
        public static string ApplicationBaseUrl = CustomConfigurationManager.AppSetting["ApplicationBaseUrl"];
        public static bool HasEmailAlias = bool.Parse(CustomConfigurationManager.AppSetting["HasEmailAlias"]);
        public static string EmailAlias = CustomConfigurationManager.AppSetting["EmailAlias"];

        public static string GoogleAnalyticId = CustomConfigurationManager.AppSetting["GoogleAnalyticId"];

        public static string[] TempEmails = CustomConfigurationManager.AppSetting["TempEmails"].Split(',');
        public static string[] OfficeEmails = CustomConfigurationManager.AppSetting["OfficeEmails"].Split(',');

        public static bool TwilioServiceIsActive = bool.Parse(CustomConfigurationManager.AppSetting["TwilioServiceIsActive"]);
        public static bool SendTextMessages = bool.Parse(CustomConfigurationManager.AppSetting["SendTextMessages"]);
        public static bool HasPhoneNumberFromAlias = bool.Parse(CustomConfigurationManager.AppSetting["HasPhoneNumberAlias"]);
        public static string PhoneNumberFromAlias = CustomConfigurationManager.AppSetting["PhoneNumberFromAlias"];

        public static string TwilioAccountSit = CustomConfigurationManager.AppSetting["TwilioAccountSit"];
        public static string TwilioAuthToken = CustomConfigurationManager.AppSetting["TwilioAuthToken"];
        public static string TwilioNumberFrom = CustomConfigurationManager.AppSetting["TwilioNumberFrom"];
        public static string CloudflareTurnstileSecretKey = CustomConfigurationManager.AppSetting["CloudflareTurnstileSecretKey"];
    }

    static class CustomConfigurationManager
    {
        public static IConfiguration AppSetting { get; }
        static CustomConfigurationManager()
        {
            AppSetting = new ConfigurationBuilder()
                .SetBasePath(System.AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("ApplicationSettings.json")
                .Build();
        }
    }
}
