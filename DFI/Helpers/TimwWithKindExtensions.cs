using System;
using System.IO;
using DFI.Data.Requests;
using DFI.Models;

namespace DFI.Helpers
{
    public static class TimeWithKindExtensions
    {
        public static DateTime ToDateTime(this TimeWithKind time)
        {
            return CommonConstants.TimeOptions[time.TimeOption].AddHours(time.ETimeKind == ETimeKind.PM ? 12 : 0);
        }
    }
}