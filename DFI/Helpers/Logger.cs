﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Helpers
{
    public class Logger : ILogger
    {
        private static NLog.Logger _logger;

        public Logger()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public void Trace(string message)
        {
            _logger.Trace(message);
        }
        public void Debug(string message)
        {
            _logger.Debug(message);
        }
        public void Info(string message)
        {
            _logger.Info(message);
        }
        public void Warn(string message)
        {
            _logger.Warn(message);
        }
        public void Error(string message)
        {
            _logger.Error(message);
        }
        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }
    }

    public interface ILogger
    {
        void Trace(string message);
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void Fatal(string message);
    }
}