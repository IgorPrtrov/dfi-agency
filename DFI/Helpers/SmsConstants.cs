﻿namespace DFI.Helpers
{
	public static class SmsConstants
	{
		public const string Confirmation48HoursBeforeRequestStart = "Reminder: Your shift is on {0} at {1} ({2})";

		public const string NotificationForNewRequest = "New job near you ({0})! Apply here: {1}";
	}
}