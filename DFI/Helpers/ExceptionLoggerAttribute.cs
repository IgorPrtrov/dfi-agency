﻿using System.Web.Mvc;

namespace DFI.Helpers
{
    public class ExceptionLoggerAttribute : FilterAttribute, IExceptionFilter
    {
        private readonly ILogger _logger;
        public static string ErrorMessages;
        public ExceptionLoggerAttribute(ILogger logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext filterContext)
        {
            var exceptionMessage = filterContext.Exception.Message;
            var stackTrace = filterContext.Exception.StackTrace;
            var controllerName = filterContext.RouteData.Values["controller"].ToString();
            var actionName = filterContext.RouteData.Values["action"].ToString();
            
            ErrorMessages = $"{exceptionMessage}";
            _logger.Error($"{exceptionMessage} \n {stackTrace} \n Controller Name: {controllerName} \n Action Name: {actionName}");
            filterContext.ExceptionHandled = true;
            filterContext.Result = new RedirectResult("/Home/ErrorInform");
        }
    }
}