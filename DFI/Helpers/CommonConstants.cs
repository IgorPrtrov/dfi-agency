using System;
using System.Collections.Generic;
using DFI.Data.Requests;

namespace DFI.Helpers
{
    public static class CommonConstants
    {
        public static DateTime BaseDateForTimeOnly = DateTime.SpecifyKind(new DateTime(2000, 1, 1), DateTimeKind.Utc);

        public static readonly Dictionary<string, DateTime> TimeOptions = new Dictionary<string, DateTime>
        {
            {
                "12:00", BaseDateForTimeOnly.AddHours(0)
            },
            {
                "12:15", BaseDateForTimeOnly.AddHours(0).AddMinutes(15)
            },
            {
                "12:30", BaseDateForTimeOnly.AddHours(0).AddMinutes(30)
            },
            {
                "12:45", BaseDateForTimeOnly.AddHours(0).AddMinutes(45)
            },
            {
                "01:00", BaseDateForTimeOnly.AddHours(1)
            },
            {
                "01:15", BaseDateForTimeOnly.AddHours(1).AddMinutes(15)
            },
            {
                "01:30", BaseDateForTimeOnly.AddHours(1).AddMinutes(30)
            },
            {
                "01:45", BaseDateForTimeOnly.AddHours(1).AddMinutes(45)
            },
            {
                "02:00", BaseDateForTimeOnly.AddHours(2)
            },
            {
                "02:15", BaseDateForTimeOnly.AddHours(2).AddMinutes(15)
            },
            {
                "02:30", BaseDateForTimeOnly.AddHours(2).AddMinutes(30)
            },
            {
                "02:45", BaseDateForTimeOnly.AddHours(2).AddMinutes(45)
            },
            {
                "03:00", BaseDateForTimeOnly.AddHours(3)
            },
            {
                "03:15", BaseDateForTimeOnly.AddHours(3).AddMinutes(15)
            },
            {
                "03:30", BaseDateForTimeOnly.AddHours(3).AddMinutes(30)
            },
            {
                "03:45", BaseDateForTimeOnly.AddHours(3).AddMinutes(45)
            },
            {
                "04:00", BaseDateForTimeOnly.AddHours(4)
            },
            {
                "04:15", BaseDateForTimeOnly.AddHours(4).AddMinutes(15)
            },
            {
                "04:30", BaseDateForTimeOnly.AddHours(4).AddMinutes(30)
            },
            {
                "04:45", BaseDateForTimeOnly.AddHours(4).AddMinutes(45)
            },
            {
                "05:00", BaseDateForTimeOnly.AddHours(5)
            },
            {
                "05:15", BaseDateForTimeOnly.AddHours(5).AddMinutes(15)
            },
            {
                "05:30", BaseDateForTimeOnly.AddHours(5).AddMinutes(30)
            },
            {
                "05:45", BaseDateForTimeOnly.AddHours(5).AddMinutes(45)
            },
            {
                "06:00", BaseDateForTimeOnly.AddHours(6)
            },
            {
                "06:15", BaseDateForTimeOnly.AddHours(6).AddMinutes(15)
            },
            {
                "06:30", BaseDateForTimeOnly.AddHours(6).AddMinutes(30)
            },
            {
                "06:45", BaseDateForTimeOnly.AddHours(6).AddMinutes(45)
            },
            {
                "07:00", BaseDateForTimeOnly.AddHours(7)
            },
            {
                "07:15", BaseDateForTimeOnly.AddHours(7).AddMinutes(15)
            },
            {
                "07:30", BaseDateForTimeOnly.AddHours(7).AddMinutes(30)
            },
            {
                "07:45", BaseDateForTimeOnly.AddHours(7).AddMinutes(45)
            },
            {
                "08:00", BaseDateForTimeOnly.AddHours(8)
            },
            {
                "08:15", BaseDateForTimeOnly.AddHours(8).AddMinutes(15)
            },
            {
                "08:30", BaseDateForTimeOnly.AddHours(8).AddMinutes(30)
            },
            {
                "08:45", BaseDateForTimeOnly.AddHours(8).AddMinutes(45)
            },
            {
                "09:00", BaseDateForTimeOnly.AddHours(9)
            },
            {
                "09:15", BaseDateForTimeOnly.AddHours(9).AddMinutes(15)
            },
            {
                "09:30", BaseDateForTimeOnly.AddHours(9).AddMinutes(30)
            },
            {
                "09:45", BaseDateForTimeOnly.AddHours(9).AddMinutes(45)
            },
            {
                "10:00", BaseDateForTimeOnly.AddHours(10)
            },
            {
                "10:15", BaseDateForTimeOnly.AddHours(10).AddMinutes(15)
            },
            {
                "10:30", BaseDateForTimeOnly.AddHours(10).AddMinutes(30)
            },
            {
                "10:45", BaseDateForTimeOnly.AddHours(10).AddMinutes(45)
            },
            {
                "11:00", BaseDateForTimeOnly.AddHours(11)
            },
            {
                "11:15", BaseDateForTimeOnly.AddHours(11).AddMinutes(15)
            },
            {
                "11:30", BaseDateForTimeOnly.AddHours(11).AddMinutes(30)
            },
            {
                "11:45", BaseDateForTimeOnly.AddHours(11).AddMinutes(45)
            }
        };

        public static Dictionary<string, TimeSpan> ArrivalTimeOptions = new Dictionary<string, TimeSpan>
        {
            {"15 min early", new TimeSpan(0, 0, 15, 0)},
            {"30 min early", new TimeSpan(0, 0, 30, 0)}
        };

        public static string[] LunchTimeOptions = {
            "None",
            "1 hour",
            "30 min",
            "Other"
        };

        public static string PhoneNumberFormatSeparator = "-";

        public static readonly TimeZoneInfo DefaultTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
    }
}