﻿using DFI.Data.Accounts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Web;

namespace DFI.Helpers
{
    public static class AuthHelper
    {
        public static async Task Impersonate(HttpContextBase httpContextBase, string email)
        {
            var applicationUserManager = httpContextBase.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var userToImpersonate = await applicationUserManager.FindByEmailAsync(email);

            var identityToImpersonate = await applicationUserManager.CreateIdentityAsync(userToImpersonate,
                    DefaultAuthenticationTypes.ApplicationCookie);

            var authenticationManager = httpContextBase.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties()
            {
                IsPersistent = false
            }, identityToImpersonate);
        }
    }
}