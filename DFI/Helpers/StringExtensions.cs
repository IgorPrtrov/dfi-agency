using System.Linq;
using System.Text;

namespace DFI.Helpers
{
    public static class StringExtensions
    {
        public static string AsPhoneNumber(this string phoneNumber)
        {
            var digitsOnly = string.Join(string.Empty, phoneNumber.ToCharArray().Where(char.IsDigit));

            if (string.IsNullOrEmpty(digitsOnly))
                return phoneNumber;

            var phoneBuilder = new StringBuilder(digitsOnly);
            phoneBuilder.Insert(3, "-");
            phoneBuilder.Insert(7, "-");

            return phoneBuilder.ToString();
        }

        public static string TrimPhoneNumber(this string phoneNumber)
        {
            return phoneNumber
                .Replace(CommonConstants.PhoneNumberFormatSeparator, string.Empty)
                .Replace(" ", string.Empty)
                .Trim();
        }
    }
}