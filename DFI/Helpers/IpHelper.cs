﻿using System.Web;

namespace DFI.Helpers
{
    public static class IpHelper
    {
        public static string GetIPAddress(HttpRequestBase request)
        {
            string szRemoteAddr = request.UserHostAddress;
            var cFConnectingIP = request.Headers["CF-Connecting-IP"];

            return $"UserHostAddress={szRemoteAddr}, cFConnectingIP={cFConnectingIP}";
        }
    }
}