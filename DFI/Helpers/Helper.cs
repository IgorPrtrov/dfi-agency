﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using DFI.Models;

namespace DFI.Helpers
{
    public class Helper
    {
        public static DateTime? Parse(string stringDate)
        {
            DateTime date;
            return DateTime.TryParse(stringDate, out date) ? date : DateTime.MaxValue;
        }

        public static ValidationStatus ValidateDto(object dto)
        {
            var response = new ValidationStatus();
            var validationContext = new ValidationContext(dto);
            var validationResults = new List<ValidationResult>();
            response.Success = Validator.TryValidateObject(dto, validationContext, validationResults, true);
            if (!response.Success)
            {
                foreach (var message in validationResults)
                {
                    response.Messages.Add(message.ErrorMessage);
                }
            }
            return response;
        }
       
        public static Document GetDocument(HttpPostedFileBase file, EDocumentType type)
        {
            MemoryStream target = new MemoryStream();
            file.InputStream.CopyTo(target);
            byte[] data = target.ToArray();

            return new Document()
            {
                Data = data,
                DocumentType = (int)type,
                Name = file.FileName
            };
        }

        public enum EPathView
        {
            [Display(Name = "Views/Email/MedicalMask.cshtml")]
            MedicalMask
        }

        public static string GenerateLogin(string email)
        {
            var atSignIndex = email.IndexOf('@');
            var name = email.Substring(0, atSignIndex);
            return name.Replace("-", string.Empty).Replace("_", string.Empty).Replace("*", string.Empty);
        }

        public static string GetRequestUrl(int requestId)
        {
            return $"{Setting.ApplicationBaseUrl}Requests/Edit/{requestId}";
        }

        public static string GetTempUrl(int tempId)
        {
            return $"{Setting.ApplicationBaseUrl}professionals/professional/{tempId}";
        }

        public static string GetFacilityUrl(int facilityId)
        {
            return $"{Setting.ApplicationBaseUrl}facilities/facility/{facilityId}";
        }

        public static string GetLoginUrl()
        {
            return $"{Setting.ApplicationBaseUrl}Account/Login";
        }

        public static string GetTempRegistrationFormUrl()
        {
            return $"{Setting.ApplicationBaseUrl}Registrator/TempRegistration";
        }

        public static string GetOfficeRegistrationFormUrl()
        {
            return $"{Setting.ApplicationBaseUrl}Registrator/FacilityRegistration";
        }

        public static string GetContactUsUrl()
        {
            return Setting.ApplicationBaseUrl + "#contact_us";
        }

        public static string GetLongGuid()
        {
            return Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "");
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                    RegexOptions.None, TimeSpan.FromMilliseconds(200));

                string DomainMapper(Match match)
                {
                    var idn = new IdnMapping();
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public enum ELicenseType
        {
            [Display(Name = "Dental Hygienist")]
            DentalHygienist,

            [Display(Name = "Dentist")]
            Dentist,

            [Display(Name = "Expanded Function Dental Asst")]
            ExpandedFunctionDentalAsst
        }

        public enum EBoard
        {
            [Display(Name = "State Board of Cosmetology")]
            StateBoardOfCosmetology = 20,

            [Display(Name = "State Board of Dentistry")]
            StateBoardOfDentistry = 13,

            [Display(Name = "State Board of Nursing")]
            StateBoardOfNursing = 43,

            [Display(Name = "State Board of Accountancy")]
            StateBoardofAccountancy = 3,

            [Display(Name = "State Architects Licensure Board")]
            StateArchitectsLicensureBoard = 1,

            [Display(Name = "State Board of Barber Examiners")]
            StateBoardofBarberExaminers = 15,

            [Display(Name = "State Board of Certified Real Estate Appraisers")]
            StateBoardofCertifiedRealEstateAppraisers = 5,

            [Display(Name = "State Board of Chiropractic")]
            StateBoardofChiropractic = 2,

            [Display(Name = "State Registration Board for Professional Engineers, Land Surveyors and Geologists")]
            StateRegistrationBoardforProfessionalEngineersLandSurveyorsandGeologists = 28,

            [Display(Name = "State Board of Funeral Directors")]
            StateBoardofFuneralDirectors = 17,

            [Display(Name = "Health Division Volunteers")]
            HealthDivisionVolunteers = 10,

            [Display(Name = "State Board of Landscape Architects")]
            StateBoardofLandscapeArchitects = 7,

            [Display(Name = "State Board of Massage Therapy")]
            StateBoardofMassageTherapy = 12,

            [Display(Name = "Navigation Commission for the Delaware River and its Navigable Tributaries")]
            NavigationCommissionfortheDelawareRiveranditsNavigableTributaries = 6,

            [Display(Name = "State Board of Examiners of Nursing Home Administrators")]
            StateBoardofExaminersofNursingHomeAdministrators = 24,

            [Display(Name = "State Board of Occupational Therapy")]
            StateBoardofOccupationalTherapy = 9,

            [Display(Name = "State Board of Optometry")]
            StateBoardofOptometry = 26,

            [Display(Name = "State Board of Pharmacy")]
            StateBoardofPharmacy = 8,

            [Display(Name = "State Real Estate Commission")]
            StateRealEstateCommission = 27,

            [Display(Name = "State Board of Social Workers, Marriage & Family Therapists and Professional Counselors")]
            StateBoardofSocialWorkersMarriageFamilyTherapistsandProfessionalCounselors = 29,

            [Display(Name = "State Board of Osteopathic Medicine")]
            StateBoardofOsteopathicMedicine = 36,

            [Display(Name = "State Board of Auctioneer Examiners")]
            StateBoardofAuctioneerExaminers = 31,

            [Display(Name = "State Board of Podiatry")]
            StateBoardofPodiatry = 33,

            [Display(Name = "State Board of Psychology")]
            StateBoardofPsychology = 30,

            [Display(Name = "State Board of Medicine")]
            StateBoardofMedicine = 37,

            [Display(Name = "State Board of Vehicle Manufacturers, Dealers and Salespersons")]
            StateBoardofVehicleManufacturersDealersandSalespersons = 38,

            [Display(Name = "Radiology Personnel")]
            RadiologyPersonnel = 39,

            [Display(Name = "State Board of Examiners in Speech-Language Pathology and Audiology")]
            StateBoardofExaminersinSpeechLanguagePathologyandAudiology = 40,

            [Display(Name = "State Board of Crane Operators")]
            StateBoardofCraneOperators = 41,

            [Display(Name = "State Board of Physical Therapy")]
            StateBoardofPhysicalTherapy = 42
        }

        public enum EState
        {
            New,
            Submitted,
            Active,
            InActive,
            Deleted
        }

        public enum EOrder
        {
            Asc,
            Desc
        }
        
        public enum EActor
        {
            Admin,
            System
        }

        public static string GetStateByName(string name)
        {
            switch (name.ToUpper())
            {
                case "ALABAMA":
                    return "AL";

                case "ALASKA":
                    return "AK";

                case "AMERICAN SAMOA":
                    return "AS";

                case "ARIZONA":
                    return "AZ";

                case "ARKANSAS":
                    return "AR";

                case "CALIFORNIA":
                    return "CA";

                case "COLORADO":
                    return "CO";

                case "CONNECTICUT":
                    return "CT";

                case "DELAWARE":
                    return "DE";

                case "DISTRICT OF COLUMBIA":
                    return "DC";

                case "FEDERATED STATES OF MICRONESIA":
                    return "FM";

                case "FLORIDA":
                    return "FL";

                case "GEORGIA":
                    return "GA";

                case "GUAM":
                    return "GU";

                case "HAWAII":
                    return "HI";

                case "IDAHO":
                    return "ID";

                case "ILLINOIS":
                    return "IL";

                case "INDIANA":
                    return "IN";

                case "IOWA":
                    return "IA";

                case "KANSAS":
                    return "KS";

                case "KENTUCKY":
                    return "KY";

                case "LOUISIANA":
                    return "LA";

                case "MAINE":
                    return "ME";

                case "MARSHALL ISLANDS":
                    return "MH";

                case "MARYLAND":
                    return "MD";

                case "MASSACHUSETTS":
                    return "MA";

                case "MICHIGAN":
                    return "MI";

                case "MINNESOTA":
                    return "MN";

                case "MISSISSIPPI":
                    return "MS";

                case "MISSOURI":
                    return "MO";

                case "MONTANA":
                    return "MT";

                case "NEBRASKA":
                    return "NE";

                case "NEVADA":
                    return "NV";

                case "NEW HAMPSHIRE":
                    return "NH";

                case "NEW JERSEY":
                    return "NJ";

                case "NEW MEXICO":
                    return "NM";

                case "NEW YORK":
                    return "NY";

                case "NORTH CAROLINA":
                    return "NC";

                case "NORTH DAKOTA":
                    return "ND";

                case "NORTHERN MARIANA ISLANDS":
                    return "MP";

                case "OHIO":
                    return "OH";

                case "OKLAHOMA":
                    return "OK";

                case "OREGON":
                    return "OR";

                case "PALAU":
                    return "PW";

                case "PENNSYLVANIA":
                    return "PA";

                case "PUERTO RICO":
                    return "PR";

                case "RHODE ISLAND":
                    return "RI";

                case "SOUTH CAROLINA":
                    return "SC";

                case "SOUTH DAKOTA":
                    return "SD";

                case "TENNESSEE":
                    return "TN";

                case "TEXAS":
                    return "TX";

                case "UTAH":
                    return "UT";

                case "VERMONT":
                    return "VT";

                case "VIRGIN ISLANDS":
                    return "VI";

                case "VIRGINIA":
                    return "VA";

                case "WASHINGTON":
                    return "WA";

                case "WEST VIRGINIA":
                    return "WV";

                case "WISCONSIN":
                    return "WI";

                case "WYOMING":
                    return "WY";
            }

            return "not found";
        }
    }

    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()
                .GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .GetName();
        }
    }

    public enum EStateUsa
    {
        AL,
        AK,
        AS,
        AZ,
        AR,
        CA,
        CO,
        CT,
        DE,
        DC,
        FM,
        FL,
        GA,
        GU,
        HI,
        ID,
        IL,
        IN,
        IA,
        KS,
        KY,
        LA,
        ME,
        MH,
        MD,
        MA,
        MI,
        MN,
        MS,
        MO,
        MT,
        NE,
        NV,
        NH,
        NJ,
        NM,
        NY,
        NC,
        ND,
        MP,
        OH,
        OK,
        OR,
        PW,
        PA,
        PR,
        RI,
        SC,
        SD,
        TN,
        TX,
        UT,
        VT,
        VI,
        VA,
        WA,
        WV,
        WI,
        WY
    }
   
    public enum EDayOfWeek
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }

    public enum EDocumentType
    {
        Certificate,
        Resume,
        Photo,
        RadiologyLicense
    }

    public class ValidationStatus
    {
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public ValidationStatus()
        {
            Messages = new List<string>();
            Success = true;
        }

        public ValidationStatus FailedValidation(string message)
        {
            Messages.Add(message);
            Success = false;

            return this;
        }
    }

    public class IdLabel
    {
        public int id { get; set; }

        public string label { get; set; }
    }
}


