﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;


namespace DFI.Helpers
{
    public static class CloudflareTurnstile
    {
        private static readonly HttpClient httpClient = new HttpClient();
        public static bool IsValidRequest(HttpRequestBase request, ILogger logger)
        {
            string szRemoteAddr = request.UserHostAddress;
            var cFConnectingIP = request.Headers["CF-Connecting-IP"];

            var ip = string.IsNullOrEmpty(cFConnectingIP) ? szRemoteAddr : cFConnectingIP;
            var token = request.Form.Get("cf-turnstile-response");

            // Prepare the form data
            var formData = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("secret", Setting.CloudflareTurnstileSecretKey),
                new KeyValuePair<string, string>("response", token),
                new KeyValuePair<string, string>("remoteip", ip)
            });

            var response = httpClient.PostAsync("https://challenges.cloudflare.com/turnstile/v0/siteverify", formData).Result;
            var jsonString = response.Content.ReadAsStringAsync().Result;

            // Deserialize the response
            var outcome = JsonConvert.DeserializeObject<TurnstileResponse>(jsonString);

            logger.Info($"{nameof(CloudflareTurnstile)}:: Is Sucess = {outcome.Success}, Errors = {string.Join(", ", outcome.ErrorCodes)}");

            return outcome.Success;

        }

        public class TurnstileResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public string[] ErrorCodes { get; set; }
        }
    }
}