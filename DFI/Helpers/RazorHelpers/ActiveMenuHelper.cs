using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace DFI.Helpers.RazorHelpers
{
    public static class ActiveMenuHelper
    {
        public static MvcHtmlString MenuLink(
            this HtmlHelper htmlHelper,
            string linkText,
            string actionName,
            string controllerName,
            bool isHighlightedOnControllerMatch = false)
        {
            return PrivateMenuLink(htmlHelper, linkText, actionName, controllerName, isHighlightedOnControllerMatch);
        }

        private static MvcHtmlString PrivateMenuLink(
            HtmlHelper htmlHelper,
            string linkText,
            string actionName,
            string controllerName,
            bool isHighlightedOnControllerMatch = false)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            var builder = new TagBuilder("li")
            {
                InnerHtml = htmlHelper.ActionLink(
                        linkText,
                        actionName,
                        controllerName,
                        null,
                        new {@class = "nav-link"})
                    .ToHtmlString()
            };

            builder.AddCssClass("nav-item");

            if (string.Equals(
                    controllerName,
                    currentController,
                    StringComparison.CurrentCultureIgnoreCase)
                && (isHighlightedOnControllerMatch
                    || string.Equals(actionName, currentAction, StringComparison.CurrentCultureIgnoreCase)))
            {
                builder.AddCssClass("active");
            }

            return new MvcHtmlString(builder.ToString());
        }
    }
}