namespace DFI.Helpers
{
    public static class ValidationConstants
    {
        public const string PhoneNumberValidationError = "Phone number format: XXX-XXX-XXXX";
    }
}