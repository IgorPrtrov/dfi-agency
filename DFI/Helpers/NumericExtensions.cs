namespace DFI.Helpers
{
    public static class NumericExtensions
    {
        public static string GetTwoDecimalPlacesStringOrNa(this double value)
        {
            return value.ToString("0.0");
        }
    }
}