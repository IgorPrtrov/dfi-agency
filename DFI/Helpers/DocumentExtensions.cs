using System;
using System.IO;
using DFI.Models;

namespace DFI.Helpers
{
    public static class DocumentExtensions
    {
        private const string Base64ImageFormat = "data:image/{0};base64,{1}";  
        
        public static string ToBase64Resource(this Document document)
        {
            if (string.IsNullOrEmpty(document?.Name) || document?.Data?.Length == 0)
            {
                return string.Empty;
            }
            
            var extension = Path.GetExtension(document.Name);
            var base64 = Convert.ToBase64String(document.Data);

            return string.Format(Base64ImageFormat, extension, base64);
        }
    }
}