﻿using RazorEngine;
using RazorEngine.Templating;
using System;
using System.IO;
using System.Reflection;

namespace DFI.Helpers
{
    public class EmailTemplate
    {
        public static string GetHtmlByView(string templateName)
        {
            string pathToView = new Uri(Assembly.GetExecutingAssembly().CodeBase)
                .AbsolutePath.Split(new[] { "bin" }, StringSplitOptions.None)[0] + "bin/Views/EmailTemplate/" + templateName;

            string view = File.ReadAllText(pathToView);
            var guid = Guid.NewGuid().ToString();
            return Engine.Razor.RunCompile(view, guid);
        }
    }
}