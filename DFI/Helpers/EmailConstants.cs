namespace DFI.Helpers
{
    public static class EmailConstants
    {
        public const string ProfessionalReviewSubject = "Professional review";
        public const string ProfessionalReviewLinkTemplate = "{0}/ProfessionalReviews/Review?tempId={1}&requestId={2}";

        public const string ProfessionalReviewTemplate =
            "Dear {0}. Our endeavor is to provide the best quality dental professionals to match the needs of our " +
            "dental practice clients. We would love to hear your thoughts and opinions regarding {1}. We would " +
            "encourage you to complete a <a href='{2}'>1 minute survey</a>. Thank you for your feedback, " +
            "Danielle and Wendy";

        public const string ApplyTempToRequestSubject = "ADMIN Needs to approve Temp";

        public const string ApplyTempToRequestBody =
            "Please approve <a href='{0}'>request</a>. Temp was applied for request Start day {1} " +
            "Position is {2}. Temp email = {3}";

        public const string SearchTempsForRequestSubject = "New temporary job opportunity available";

        public const string SearchTempsForRequestBody = "Hello. Are you available on {0}? " +
                                                        "The office is located in {1}. " +
                                                        "It is approximately {2} miles from you. " +
                                                        "To apply for this position please click <a href='{3}'>HERE</a>. " +
                                                        "If you have any questions about the position please text us before applying at 484-602-5656";

        public const string HeaderForDailyEmail = "Hello DFI team! We have added several new temporary placement opportunities to the calendar. You can apply to them directly from this email by clicking the link in the job description. " +
            "If you prefer to view them on your calendar or see more details about the position click <a href='{0}'>HERE</a> to go directly to the site.<br/><br/>";

        public const string SearchTempsForRequestDailyEmailBody = "{0}. " +
                                                "The office is located in {1}. " +
                                                "It is approximately {2} miles from you. " +
                                                "To apply for this position please click <a href='{3}'>HERE</a>.<br/><br/>";

        public const string FooterForDailyEmail = "If you have any questions about the position please text us before applying at 484-602-5656";

        public const string ConfirmationSubject = "Confirmation";

        public const string Cancellation = "Important Cancellation Notice";

        public const string FillRequestCancellationBody =
            "We are so sorry, but unfortunately the office no longer needs help on this date. <a href='{0}'>Go to the request.</a>. " +
            "We will contact you as soon as any other positions open on this date. Please contact Dental Fillins if " +
            "you have any questions. <a href='{1}'>Contact Us</a>";

        public const string FillRequestTempBody =
            @"Thank you for applying. The administration has confirmed your position at {0} on {1}.<br/>
                Click <a href='{2}'>here</a> for the details of your assignment.<br/><br/>
                If you have any questions please <a href='{3}'>contact us</a>.";

        public const string FillRequestFacilityBody =
            "Congratulations, We have filled your request. <a href='{0}'>Go to the request.</a> " +
            "Please <a href='{1}'>contact us</a> if you have any questions.";

        public const string ChangeRequestStatusSubject = "Request status is {0}";

        public const string ChangeRequestStatusBody =
            "Request status is {0}. For more information <a href='{1}'>go to the request</a> details. " +
            "If you have any questions, please <a href='{2}'>contact us</a>.";

        public const string SendToTempFacilityAddressSubject = "Your temporary position details and location on {0}";

        public const string SendToTempFacilityAddressBody =
            "Hello. Thank you for accepting this position ({0}). " +
            "Here are the details for {1} on {2} you should arrive at {3}. " +
            "If you have any questions, please <a href='{4}'>contact us</a>";

        public const string CanсelRequestSubject = "Temporary Position Request was canceled";

        public const string CanсelRequestBody =
            "Hello, thank you for your time but unfortunately, this <a href='{0}'>request</a> has been canceled. " +
            "If you have any questions, please <a href='{1}'>contact us</a>";

        public const string AssigneeCancelationSubject = "Request assignee has been canceled";

        public const string AssigneeCancelationBody =
            "Unfortunately request <a href='{0}'>assignee</a> has been canceled for <a href='{1}'>request</a> by {2}. " +
            "If you have any questions, please <a href='{3}'>contact us</a>";

        public const string NewApplicationSubject = "Attention: New {0} Account Needs Approval!";

        public const string NewApplicationBody =
            "Good News! You have a new applicant for Dental Fillins. Please review their credentials and verify their eligibility. " +
            "Email has been verified for this user. <a href='{0}'>Click here</a>";

        public const string EmailConfirmSubject = "Confirm New Email";
        public const string EmailConfirmBody = "Please confirm new email by clicking <a href='{0}'>here</a>";

        public const string AccountConfirmSubject = "Confirm Email";
        public const string AccountConfirmBody = "Please confirm your account by clicking <a href='{0}'>here</a>";

        public const string WaitingResponseSubject = "Waiting response from temps";
        public const string WaitingResponseBody =
            "Temps were found for  <a href={0}>request</a> to temporary job. Waiting response from temps.<br/> " +
            "Request Start day {1}, End day {2}. Position is {3}.";

        public const string FacilityRegistrationFormBody =
            "Thank you for submitted registration form. Please confirm your account by clicking <a href='{0}'>here</a>";
        public const string FacilityRegistrationFormSubject = "Registration form";

        public const string ForgotPasswordBody = "<p>Login: {0}</p>" +
                                                 "<p><a href='{1}'>Please click on the link</a></p>";

        public const string ForgotPasswordSubject = "Forgot Password";

        public const string TempRegistrationFormBody =
            "Thank you for submitted registration form. Please confirm your account by clicking <a href='{0}'>here</a>";

        public const string TempRegistrationFormSubject = "Registration form";

        public const string ChangeStatusBody = "Hello, your account status has been changed from {0} to {1}.";
        public const string ChangeStatusSubject = "Status has been changed";

        public const string LicenseVerificationBody = "License Verification result: {0}. License Number {1}. Last Name {2}.";
        public const string LicenseVerificationSubject = "Update Expiration date of licenses were failed";

        public const string RequestChangedStatusToNeedApprovalSubject =
            "Request status has been updated to Need Approval by ADMIN";
        public const string RequestChangedStatusToNeedApprovalBody =
            "Please fill some assignee from this <a href='{0}'>request</a>.";

        public const string ApproveTempAccountSubject = "Congratulations";
        public const string ApproveTempAccountBody = "Thank you, your account has been approved. " +
            "You can now access your account including your personal calendar. You will now begin to review requests " +
            "for temporary and permanent opportunities as they arise.<p><a href='{0}'>Login</a></p>";

        public const string ApproveFacilityAccountSubject = "Congratulations";
        public const string ApproveFacilityAccountBody = "Thank you, your account has been approved. " +
            "You can now access your account including your personal calendar. You will now begin to create requests " +
            "for temporary professionals.<p><a href='{0}'>Login</a></p>";

        public const string RequestHoursAreChangedSubject = "Changes in the schedule";
        public const string RequestHoursAreChangedBody = @"Due to changes in the schedule your hours have changed at {0}. Please open the request to verify hours {1}.<br/>
                                                                Please <a href='{2}'>contact us</a> if you have any questions or concerns.";

        public const string Confirmation48HoursBeforeRequestStartSubject = "Shift Reminder";
        public const string Confirmation48HoursBeforeRequestStartBody = "Reminder: Your shift is on {0} at {1} ({2})";
    }
}