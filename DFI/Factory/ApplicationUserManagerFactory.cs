﻿using DFI.Data.Accounts;
using DFI.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Web;

namespace DFI.Factory
{
    public class ApplicationUserManagerFactory : IApplicationUserManagerFactory
    {
        public ApplicationUserManager GetUserManager()
        {
            return HttpContext.Current?.GetOwinContext().GetUserManager<ApplicationUserManager>()
                ?? new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        }
    }

    public interface IApplicationUserManagerFactory
    {
        ApplicationUserManager GetUserManager();
    }
}