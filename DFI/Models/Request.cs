﻿using System;
using System.Collections.Generic;

namespace DFI.Models
{
    public class Request
    {
        public Request()
        {
            RequestDays = new HashSet<RequestDay>();
            RequestAssignees = new HashSet<RequestAssignee>();
            ProfessionalReviews = new HashSet<ProfessionalReview>();
        }

        public int RequestId { get; set; }
        public string LicenseType { get; set; }
        public int? Status { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public string RoleWhoCanceled { get; set; }
        public string EmailWhoCanceled { get; set; }
        public string ReasonOfCancelation { get; set; }
        public DateTime? DateOfCancelation { get; set; }
        public double? PriceForTemp { get; set; }
        public double? PriceForOffice { get; set; }
        public bool IsAutoSearching { get; set; }
        public bool IsAutoSearchingWithoutCheckingLicenseExpirationDay { get; set; }
        public virtual FacilityLocation FacilityLocation { get; set; }
        public int? FacilityLocationId { get; set; }
        public virtual ICollection<RequestDay> RequestDays { get; set; }
        public virtual ICollection<RequestAssignee> RequestAssignees { get; set; }
        public virtual ICollection<ProfessionalReview> ProfessionalReviews { get; set; }
    }
}