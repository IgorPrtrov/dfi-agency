﻿namespace DFI.Models
{
    public class YouTubeVideo
    {
        public int YouTubeVideoId { get; set; }
        public string Note { get; set; }
        public string VideoUrl { get; set; }
    }
}