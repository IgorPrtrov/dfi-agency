﻿using System;
using System.Collections.Generic;
using DFI.Helpers;

namespace DFI.Models
{
    public class Temp
    {
        public Temp()
        {
            Licenses = new HashSet<License>();
            Documents = new HashSet<Document>();
            RequestAssignees = new HashSet<RequestAssignee>();
            TempFacilityDistances = new HashSet<TempFacilityDistance>();
            ProfessionalReviews = new HashSet<ProfessionalReview>();
            TempAvailabilities = new HashSet<TempAvailability>();
        }
        public int TempId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? PersonId { get; set; }
        public string CellPhone { get; set; }
        public string HomePhone { get; set; }
        public string Email { get; set; }
        public int DistanceToWork { get; set; }
        public string WhoReferenced { get; set; }
        public bool? IsReadyToPermanentWork { get; set; }
        public string AddressLine1 { get; set; }
        public string Apartment { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
        public string Zipcode { get; set; }
        public string FullAddress { get; set; }
        public int? Status { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? DateOfModified { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime? StatusChangedDate { get; set; }
        public int? StatusChangedBy { get; set; }
        public string AdministratorsNotes { get; set; }
        public bool IsGetSMSForNewRequest { get; set; }
        public int? OffsetHours {
            get
            {
                var utcOffset = new DateTimeOffset(DateTime.UtcNow, TimeSpan.Zero);
                return TimeZoneInfo.GetUtcOffset(utcOffset).Hours;
            }
        }
        
        public string TimeZoneId { get; set; }
        
        public TimeZoneInfo TimeZoneInfo => 
            string.IsNullOrEmpty(TimeZoneId)
                ? CommonConstants.DefaultTimeZone
                : TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);

        public virtual ICollection<License> Licenses { get; set; }
        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<RequestAssignee> RequestAssignees { get; set; }
        public virtual ICollection<TempFacilityDistance> TempFacilityDistances { get; set; }
        public virtual ICollection<ProfessionalReview> ProfessionalReviews { get; set; }
        public virtual ICollection<TempAvailability> TempAvailabilities { get; set; }
    }
}
