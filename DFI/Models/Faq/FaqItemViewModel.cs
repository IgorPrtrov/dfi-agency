namespace DFI.Models.Faq
{
    public class FaqItemViewModel
    {
        public FaqItemViewModel(string question, string answer)
        {
            Question = question;
            Answer = answer;
        }
        
        public string Question { get; set; }
        
        public string Answer { get; set; }
    }
}