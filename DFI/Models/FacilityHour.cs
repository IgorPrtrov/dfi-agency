﻿namespace DFI.Models
{
    public class FacilityHour
    {
        public int FacilityHourId { get; set; }

        public string DayOfWeek { get; set; }
        public string RangeHours { get; set; }
        public virtual FacilityLocation FacilityLocation { get; set; }
        public int? FacilityLocationId { get; set; }
    }
}