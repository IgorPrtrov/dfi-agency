﻿using System;

namespace DFI.Models
{
    public class License
    {
        public int LicenseId { get; set; }
        public string Profession { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public string Status { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string BoardName { get; set; }
        public int DesiredHourlyRate { get; set; }
        public int YearsInPractice { get; set; }
        public string UniqueQualities { get; set; }
        public string CurrentEmployer { get; set; }
        public string CurrentEmployerPhoneNumber { get; set; }
        public string CurrentEmployerEmail { get; set; }

        public int? TempId { get; set; }
        public virtual Temp Temp { get; set; }
    }
}
