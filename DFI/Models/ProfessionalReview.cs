using System;
using System.ComponentModel.DataAnnotations;

namespace DFI.Models
{
    public class ProfessionalReview
    {
        [Key]
        public int Id { get; set; }            
        public int Rating { get; set; }      
        public bool IsBlocked { get; set; }     
        public string Description { get; set; }     
        public DateTime AddDate { get; set; }       
        public string SubmitterId { get; set; }
        
        public bool DidArriveInTimelyManner { get; set; }
        
        public bool DidPresentInProfessionalAttire { get; set; }
        
        public bool DidInteractWellWithStaff { get; set; }
        
        public bool DidInteractWellWithCustomers { get; set; }
        
        public bool WasCompetentInDigitalChartingAndDocumentation { get; set; }
        
        public int TempId { get; set; }
        public virtual Temp Temp { get; set; }
        
        public int RequestId { get; set; }
        public virtual Request Request { get; set; }
    }
}