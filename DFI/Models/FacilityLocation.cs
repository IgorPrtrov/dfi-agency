﻿using System;
using System.Collections.Generic;
using DFI.Helpers;

namespace DFI.Models
{
    public class FacilityLocation
    {
        public FacilityLocation()
        {
            Requests = new HashSet<Request>();
            FacilityHours = new HashSet<FacilityHour>();
            TempFacilityDistances = new HashSet<TempFacilityDistance>();
        }

        public int FacilityLocationId { get; set; }
        public string AddressLine1 { get; set; }
        public string Apartment { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
        public string Zipcode { get; set; }
        public string FullAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string DoctorsName { get; set; }
        public string WebsiteAddress { get; set; }
        public string ChartingSoftware { get; set; }
        public string XRaySoftware { get; set; }
        public string AllowedAdultProphylaxisTime { get; set; }
        public string AllowedChildProphylaxisTime { get; set; }
        public string PaidEndOfTheDay { get; set; }
        public string AdditionalInfo { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string TimeZoneId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public int LocationStatus { get; set; }

        public virtual Facility Facility { get; set; }
        public int FacilityId { get; set; }

        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<FacilityHour> FacilityHours { get; set; }
        public virtual ICollection<TempFacilityDistance> TempFacilityDistances { get; set; }
        
        public int? OffsetHours {
            get
            {
                var utcOffset = new DateTimeOffset(DateTime.UtcNow, TimeSpan.Zero);
                return TimeZoneInfo.GetUtcOffset(utcOffset).Hours;
            }
        }

        private TimeZoneInfo TimeZoneInfo => 
            string.IsNullOrEmpty(TimeZoneId)
                ? CommonConstants.DefaultTimeZone
                : TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
    }
}
