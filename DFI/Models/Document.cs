﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Models
{
    public class Document
    {
        public int DocumentId { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public int DocumentType { get; set; }

        public virtual Temp Temp { get; set; }
        public int? TempId { get; set; }

        public virtual Facility Facility { get; set; }
        public int? FacilityId { get; set; }
    }
}