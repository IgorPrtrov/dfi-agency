﻿using System;

namespace DFI.Models
{
    public class RequestAssignee
    {
        public int RequestAssigneeId { get; set; }
        public int Status { get; set; }
        public bool IsFacilityAddressSent { get; set; }
        public bool IsReviewEmailSent { get; set; }
        public string LongGuid { get; set; }

        public string ChagnedBy { get; set; }
        public DateTime? DateChanged { get; set; }

        public virtual Request Request { get; set; }
        public int RequestId { get; set; }
        
        public virtual Temp Temp { get; set; }
        public int TempId { get; set; }
    }
}