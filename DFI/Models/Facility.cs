﻿using System;
using System.Collections.Generic;

namespace DFI.Models
{
    public class Facility
    {
        public Facility()
        {
            Documents = new HashSet<Document>();
            FacilityLocations = new HashSet<FacilityLocation>();
        }
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string Email { get; set; }
        public string PayType { get; set; }
        public string BillingEmail { get; set; }
        public int Status { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? DateOfModified { get; set; }
        public DateTime? StatusChangedDate { get; set; }
        public int? StatusChangedBy { get; set; }
        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<FacilityLocation> FacilityLocations { get; set; }   
    }
}
