﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Models
{
    public class ValidationEmail
    {
        public int ValidationEmailId { get; set; }
        public string Address { get; set; }
        public bool IsDisposableAddress { get; set; }
        public bool IsRoleAddress { get; set; }
        public string Reason { get; set; }  
        public string Result { get; set; }
        public string Risk { get; set; }
    }
}