﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Models
{
    public class TempAvailability
    {
        public TempAvailability()
        {
            
        }
        public TempAvailability(DayOfWeek dayOfWeekAvailable)
        {
            DayOfWeekAvailable = dayOfWeekAvailable;
        }

        public int Id { get; set; }
        
        public DayOfWeek DayOfWeekAvailable { get; set; }

        public int? TempId { get; set; }
        public virtual Temp Temp { get; set; }
    }
}