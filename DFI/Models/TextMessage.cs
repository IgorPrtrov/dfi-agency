﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DFI.Models
{
    public class TextMessage
    {
        [Key]
        public int TextMessageId { get; set; }
        public string PhoneNumberTo { get; set; }
        public string PhoneNumberFrom { get; set; }
        public string Body { get; set; }
        public int Attempts { get; set; }
        public bool IsSent { get; set; }
        public string MessageSid { get; set; }
        public string Status { get; set; }
        public DateTime? SentDate { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}