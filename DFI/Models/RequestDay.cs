﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Models
{
    public class RequestDay
    {
        public int RequestDayId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime PatientHoursStart { get; set; }
        public string LunchTime { get; set; }
        public DateTime? PatientHoursEnd { get; set; }
        public string Description { get; set; }

        public virtual Request Request { get; set; }
        public int RequestId { get; set; }
    }
}