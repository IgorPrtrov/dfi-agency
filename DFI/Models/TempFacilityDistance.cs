﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Models
{
    public class TempFacilityDistance
    {
        public int TempFacilityDistanceId { get; set; }

        public long Distance { get; set; }
        public long Duration { get; set; }
        public int TempId { get; set; }
        public Temp Temp { get; set; }
        public virtual FacilityLocation FacilityLocation { get; set; }
        public int? FacilityLocationId { get; set; }
    }
}