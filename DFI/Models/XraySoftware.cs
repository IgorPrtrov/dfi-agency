namespace DFI.Models
{
    public class XraySoftware
    {
        public int XraySoftwareId { get; set; }
        
        public string Name { get; set; }
    }
}