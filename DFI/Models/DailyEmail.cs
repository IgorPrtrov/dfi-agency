﻿using System;

namespace DFI.Models
{
    public class DailyEmail
    {
        public int DailyEmailId { get; set; }
        public string ToEmail { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool Sent { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? SentOn { get; set; }
    }
}