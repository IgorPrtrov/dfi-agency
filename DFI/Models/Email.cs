﻿using System;

namespace DFI.Models
{
    public class Email
    {
        public int EmailId { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool Sent { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? SentDate { get; set; }
        public bool? BodyHtml { get; set; }
        public string ObjectTable { get; set; }
        public int? Attempt { get; set; }
    }
}