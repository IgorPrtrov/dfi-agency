namespace DFI.Models
{
    public class ChartingSoftware
    {
        public int ChartingSoftwareId { get; set; }
        
        public string Name { get; set; }
    }
}