﻿using System.Data.Entity;
using DFI.Data.Accounts;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DFI.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext() : base(Setting.ConnectionString){}

        public virtual IDbSet<Temp> Temps { get; set; }
        public virtual IDbSet<TempAvailability> TempAvailabilities { get; set; }
        public virtual IDbSet<License> Licenses { get; set; }
        public virtual IDbSet<Facility> Facilities { get; set; }
        public virtual IDbSet<Email> Emails { get; set; }
        public virtual IDbSet<Document> Documents { get; set; }
        public virtual IDbSet<FacilityHour> FacilityHours { get; set; }
        public virtual IDbSet<RequestDay> RequestDays { get; set; }
        public virtual IDbSet<RequestAssignee> RequestAssignees { get; set; }
        public virtual IDbSet<Request> Requests { get; set; }
        public virtual IDbSet<ProfessionalReview> ProfessionalReviews { get; set; }
        public virtual IDbSet<FacilityLocation> FacilityLocations { get; set; }
        public virtual IDbSet<TempFacilityDistance> TempFacilityDistances { get; set; }
        public virtual IDbSet<ChartingSoftware> ChartingSoftwares { get; set; }
        public virtual IDbSet<TextMessage> TextMessages { get; set; }       
        public virtual IDbSet<XraySoftware> XraySoftwares { get; set; }
        public virtual IDbSet<ValidationEmail> ValidationEmails { get; set; }
        public virtual IDbSet<DailyEmail> DailyEmails { get; set; }
        public virtual IDbSet<YouTubeVideo> YouTubeVideos { get; set; }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public virtual void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetDeleted(object entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public void SetValue(object currentValue, object newValue)
        {
            Entry(currentValue).CurrentValues.SetValues(newValue);
        }
        
        public virtual int SaveChanges()
        {         
            return base.SaveChanges();
        }
    }

    //EntityFramework6\Add-Migration YouTubeTable
    //EntityFramework6\update-database
    public interface IApplicationDbContext
    {
        IDbSet<Temp> Temps { get; set; }
        IDbSet<TempAvailability> TempAvailabilities { get; set; }
        IDbSet<License> Licenses { get; set; }
        IDbSet<Facility> Facilities { get; set; }
        IDbSet<Email> Emails { get; set; }
        IDbSet<Document> Documents { get; set; }
        IDbSet<FacilityHour> FacilityHours { get; set; }
        IDbSet<RequestDay> RequestDays { get; set; }
        IDbSet<RequestAssignee> RequestAssignees { get; set; }
        IDbSet<Request> Requests { get; set; }
        IDbSet<ProfessionalReview> ProfessionalReviews { get; set; }
        IDbSet<ChartingSoftware> ChartingSoftwares { get; set; }
        IDbSet<XraySoftware> XraySoftwares { get; set; }
        IDbSet<ValidationEmail> ValidationEmails { get; set; }
        IDbSet<FacilityLocation> FacilityLocations { get; set; }
        IDbSet<TempFacilityDistance> TempFacilityDistances { get; set; }
        IDbSet<TextMessage> TextMessages { get; set; }
        IDbSet<DailyEmail> DailyEmails { get; set; }
        IDbSet<YouTubeVideo> YouTubeVideos { get; set; }
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        void SetModified(object entity);
        void SetAdded(object entity);
        void SetDeleted(object entity);
        void SetValue(object currentValue, object newValue);
        int SaveChanges();
    }
}
