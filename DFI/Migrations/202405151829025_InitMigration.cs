﻿namespace DFI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChartingSoftwares",
                c => new
                    {
                        ChartingSoftwareId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ChartingSoftwareId);
            
            CreateTable(
                "dbo.DailyEmails",
                c => new
                    {
                        DailyEmailId = c.Int(nullable: false, identity: true),
                        ToEmail = c.String(),
                        Body = c.String(),
                        Subject = c.String(),
                        Sent = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        SentOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.DailyEmailId);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        DocumentId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Data = c.Binary(),
                        DocumentType = c.Int(nullable: false),
                        TempId = c.Int(),
                        FacilityId = c.Int(),
                    })
                .PrimaryKey(t => t.DocumentId)
                .ForeignKey("dbo.Facilities", t => t.FacilityId)
                .ForeignKey("dbo.Temps", t => t.TempId)
                .Index(t => t.TempId)
                .Index(t => t.FacilityId);
            
            CreateTable(
                "dbo.Facilities",
                c => new
                    {
                        FacilityId = c.Int(nullable: false, identity: true),
                        FacilityName = c.String(),
                        Email = c.String(),
                        BillingEmail = c.String(),
                        Status = c.Int(nullable: false),
                        AddDate = c.DateTime(nullable: false),
                        DateOfModified = c.DateTime(),
                        StatusChangedDate = c.DateTime(),
                        StatusChangedBy = c.Int(),
                    })
                .PrimaryKey(t => t.FacilityId);
            
            CreateTable(
                "dbo.FacilityLocations",
                c => new
                    {
                        FacilityLocationId = c.Int(nullable: false, identity: true),
                        AddressLine1 = c.String(),
                        Apartment = c.String(),
                        City = c.String(),
                        StateName = c.String(),
                        Zipcode = c.String(),
                        FullAddress = c.String(),
                        PhoneNumber = c.String(),
                        DoctorsName = c.String(),
                        WebsiteAddress = c.String(),
                        ChartingSoftware = c.String(),
                        XRaySoftware = c.String(),
                        AllowedAdultProphylaxisTime = c.String(),
                        AllowedChildProphylaxisTime = c.String(),
                        PaidEndOfTheDay = c.String(),
                        AdditionalInfo = c.String(),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TimeZoneId = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        LocationStatus = c.Int(nullable: false),
                        FacilityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FacilityLocationId)
                .ForeignKey("dbo.Facilities", t => t.FacilityId, cascadeDelete: true)
                .Index(t => t.FacilityId);
            
            CreateTable(
                "dbo.FacilityHours",
                c => new
                    {
                        FacilityHourId = c.Int(nullable: false, identity: true),
                        DayOfWeek = c.String(),
                        RangeHours = c.String(),
                        FacilityLocationId = c.Int(),
                    })
                .PrimaryKey(t => t.FacilityHourId)
                .ForeignKey("dbo.FacilityLocations", t => t.FacilityLocationId)
                .Index(t => t.FacilityLocationId);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        RequestId = c.Int(nullable: false, identity: true),
                        LicenseType = c.String(),
                        Status = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        Description = c.String(),
                        RoleWhoCanceled = c.String(),
                        EmailWhoCanceled = c.String(),
                        ReasonOfCancelation = c.String(),
                        DateOfCancelation = c.DateTime(),
                        PriceForTemp = c.Double(),
                        PriceForOffice = c.Double(),
                        IsAutoSearching = c.Boolean(nullable: false),
                        IsAutoSearchingWithoutCheckingLicenseExpirationDay = c.Boolean(nullable: false),
                        FacilityLocationId = c.Int(),
                    })
                .PrimaryKey(t => t.RequestId)
                .ForeignKey("dbo.FacilityLocations", t => t.FacilityLocationId)
                .Index(t => t.FacilityLocationId);
            
            CreateTable(
                "dbo.ProfessionalReviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                        Description = c.String(),
                        AddDate = c.DateTime(nullable: false),
                        SubmitterId = c.String(),
                        DidArriveInTimelyManner = c.Boolean(nullable: false),
                        DidPresentInProfessionalAttire = c.Boolean(nullable: false),
                        DidInteractWellWithStaff = c.Boolean(nullable: false),
                        DidInteractWellWithCustomers = c.Boolean(nullable: false),
                        WasCompetentInDigitalChartingAndDocumentation = c.Boolean(nullable: false),
                        TempId = c.Int(nullable: false),
                        RequestId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requests", t => t.RequestId, cascadeDelete: true)
                .ForeignKey("dbo.Temps", t => t.TempId, cascadeDelete: true)
                .Index(t => t.TempId)
                .Index(t => t.RequestId);
            
            CreateTable(
                "dbo.Temps",
                c => new
                    {
                        TempId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PersonId = c.Int(),
                        CellPhone = c.String(),
                        HomePhone = c.String(),
                        Email = c.String(),
                        DistanceToWork = c.Int(nullable: false),
                        WhoReferenced = c.String(),
                        IsReadyToPermanentWork = c.Boolean(),
                        AddressLine1 = c.String(),
                        Apartment = c.String(),
                        City = c.String(),
                        StateName = c.String(),
                        Zipcode = c.String(),
                        FullAddress = c.String(),
                        Status = c.Int(),
                        AddedDate = c.DateTime(),
                        DateOfModified = c.DateTime(),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatusChangedDate = c.DateTime(),
                        StatusChangedBy = c.Int(),
                        AdministratorsNotes = c.String(),
                        IsGetSMSForNewRequest = c.Boolean(nullable: false),
                        TimeZoneId = c.String(),
                    })
                .PrimaryKey(t => t.TempId);
            
            CreateTable(
                "dbo.Licenses",
                c => new
                    {
                        LicenseId = c.Int(nullable: false, identity: true),
                        Profession = c.String(),
                        LicenseType = c.String(),
                        LicenseNumber = c.String(),
                        Status = c.String(),
                        ExpiryDate = c.DateTime(),
                        BoardName = c.String(),
                        DesiredHourlyRate = c.Int(nullable: false),
                        YearsInPractice = c.Int(nullable: false),
                        UniqueQualities = c.String(),
                        CurrentEmployer = c.String(),
                        CurrentEmployerPhoneNumber = c.String(),
                        CurrentEmployerEmail = c.String(),
                        TempId = c.Int(),
                    })
                .PrimaryKey(t => t.LicenseId)
                .ForeignKey("dbo.Temps", t => t.TempId)
                .Index(t => t.TempId);
            
            CreateTable(
                "dbo.RequestAssignees",
                c => new
                    {
                        RequestAssigneeId = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        IsFacilityAddressSent = c.Boolean(nullable: false),
                        IsReviewEmailSent = c.Boolean(nullable: false),
                        LongGuid = c.String(),
                        ChagnedBy = c.String(),
                        DateChanged = c.DateTime(),
                        RequestId = c.Int(nullable: false),
                        TempId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RequestAssigneeId)
                .ForeignKey("dbo.Requests", t => t.RequestId, cascadeDelete: true)
                .ForeignKey("dbo.Temps", t => t.TempId, cascadeDelete: true)
                .Index(t => t.RequestId)
                .Index(t => t.TempId);
            
            CreateTable(
                "dbo.TempAvailabilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DayOfWeekAvailable = c.Int(nullable: false),
                        TempId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Temps", t => t.TempId)
                .Index(t => t.TempId);
            
            CreateTable(
                "dbo.TempFacilityDistances",
                c => new
                    {
                        TempFacilityDistanceId = c.Int(nullable: false, identity: true),
                        Distance = c.Long(nullable: false),
                        Duration = c.Long(nullable: false),
                        TempId = c.Int(nullable: false),
                        FacilityLocationId = c.Int(),
                    })
                .PrimaryKey(t => t.TempFacilityDistanceId)
                .ForeignKey("dbo.FacilityLocations", t => t.FacilityLocationId)
                .ForeignKey("dbo.Temps", t => t.TempId, cascadeDelete: true)
                .Index(t => t.TempId)
                .Index(t => t.FacilityLocationId);
            
            CreateTable(
                "dbo.RequestDays",
                c => new
                    {
                        RequestDayId = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        ArrivalTime = c.DateTime(nullable: false),
                        PatientHoursStart = c.DateTime(nullable: false),
                        LunchTime = c.String(),
                        PatientHoursEnd = c.DateTime(),
                        Description = c.String(),
                        RequestId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RequestDayId)
                .ForeignKey("dbo.Requests", t => t.RequestId, cascadeDelete: true)
                .Index(t => t.RequestId);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        EmailId = c.Int(nullable: false, identity: true),
                        FromEmail = c.String(),
                        ToEmail = c.String(),
                        Cc = c.String(),
                        Bcc = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                        Sent = c.Boolean(nullable: false),
                        AddDate = c.DateTime(nullable: false),
                        SentDate = c.DateTime(),
                        BodyHtml = c.Boolean(),
                        ObjectTable = c.String(),
                        Attempt = c.Int(),
                    })
                .PrimaryKey(t => t.EmailId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Description = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.TextMessages",
                c => new
                    {
                        TextMessageId = c.Int(nullable: false, identity: true),
                        PhoneNumberTo = c.String(),
                        PhoneNumberFrom = c.String(),
                        Body = c.String(),
                        Attempts = c.Int(nullable: false),
                        IsSent = c.Boolean(nullable: false),
                        MessageSid = c.String(),
                        Status = c.String(),
                        SentDate = c.DateTime(),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TextMessageId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        CodeToChangePassword = c.String(),
                        Status = c.Int(nullable: false),
                        NewEmail = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ValidationEmails",
                c => new
                    {
                        ValidationEmailId = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        IsDisposableAddress = c.Boolean(nullable: false),
                        IsRoleAddress = c.Boolean(nullable: false),
                        Reason = c.String(),
                        Result = c.String(),
                        Risk = c.String(),
                    })
                .PrimaryKey(t => t.ValidationEmailId);
            
            CreateTable(
                "dbo.XraySoftwares",
                c => new
                    {
                        XraySoftwareId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.XraySoftwareId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RequestDays", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.TempFacilityDistances", "TempId", "dbo.Temps");
            DropForeignKey("dbo.TempFacilityDistances", "FacilityLocationId", "dbo.FacilityLocations");
            DropForeignKey("dbo.TempAvailabilities", "TempId", "dbo.Temps");
            DropForeignKey("dbo.RequestAssignees", "TempId", "dbo.Temps");
            DropForeignKey("dbo.RequestAssignees", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.ProfessionalReviews", "TempId", "dbo.Temps");
            DropForeignKey("dbo.Licenses", "TempId", "dbo.Temps");
            DropForeignKey("dbo.Documents", "TempId", "dbo.Temps");
            DropForeignKey("dbo.ProfessionalReviews", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.Requests", "FacilityLocationId", "dbo.FacilityLocations");
            DropForeignKey("dbo.FacilityHours", "FacilityLocationId", "dbo.FacilityLocations");
            DropForeignKey("dbo.FacilityLocations", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.Documents", "FacilityId", "dbo.Facilities");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RequestDays", new[] { "RequestId" });
            DropIndex("dbo.TempFacilityDistances", new[] { "FacilityLocationId" });
            DropIndex("dbo.TempFacilityDistances", new[] { "TempId" });
            DropIndex("dbo.TempAvailabilities", new[] { "TempId" });
            DropIndex("dbo.RequestAssignees", new[] { "TempId" });
            DropIndex("dbo.RequestAssignees", new[] { "RequestId" });
            DropIndex("dbo.Licenses", new[] { "TempId" });
            DropIndex("dbo.ProfessionalReviews", new[] { "RequestId" });
            DropIndex("dbo.ProfessionalReviews", new[] { "TempId" });
            DropIndex("dbo.Requests", new[] { "FacilityLocationId" });
            DropIndex("dbo.FacilityHours", new[] { "FacilityLocationId" });
            DropIndex("dbo.FacilityLocations", new[] { "FacilityId" });
            DropIndex("dbo.Documents", new[] { "FacilityId" });
            DropIndex("dbo.Documents", new[] { "TempId" });
            DropTable("dbo.XraySoftwares");
            DropTable("dbo.ValidationEmails");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.TextMessages");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Emails");
            DropTable("dbo.RequestDays");
            DropTable("dbo.TempFacilityDistances");
            DropTable("dbo.TempAvailabilities");
            DropTable("dbo.RequestAssignees");
            DropTable("dbo.Licenses");
            DropTable("dbo.Temps");
            DropTable("dbo.ProfessionalReviews");
            DropTable("dbo.Requests");
            DropTable("dbo.FacilityHours");
            DropTable("dbo.FacilityLocations");
            DropTable("dbo.Facilities");
            DropTable("dbo.Documents");
            DropTable("dbo.DailyEmails");
            DropTable("dbo.ChartingSoftwares");
        }
    }
}
