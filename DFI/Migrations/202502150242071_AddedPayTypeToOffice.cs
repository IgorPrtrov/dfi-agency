﻿namespace DFI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPayTypeToOffice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Facilities", "PayType", c => c.String());
            // Update all existing records to default '1099'
            Sql("UPDATE dbo.Facilities SET PayType = '1099' WHERE PayType IS NULL");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Facilities", "PayType");
        }
    }
}
