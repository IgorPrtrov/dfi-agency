﻿namespace DFI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class YouTubeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.YouTubeVideos",
                c => new
                    {
                        YouTubeVideoId = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        VideoUrl = c.String(),
                    })
                .PrimaryKey(t => t.YouTubeVideoId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.YouTubeVideos");
        }
    }
}
