﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DFI.Data.DataGrader
{
    public class StateBoardOfNursing
    {
        public enum EBoardOfNursing
        {
            [DisplayName("State Board of Nursing")]
            StateBoardOfNursing = 43
        }

        public enum EPersonLicenseTypeNursing
        {
            [DisplayName("Registered Nurse")]
            RegisteredNurse = 63,

            [DisplayName("Certified Registered Nurse Practitioner")]
            CertifiedRegisteredNursePractitioner = 321,

            [DisplayName("Clinical Nurse Specialist")]
            ClinicalNurseSpecialist = 51,

            [DisplayName("Dietitian-Nutritionist (LDN)")]
            DietitianNutritionistLDN = 202,

            [DisplayName("Practical Nurse")]
            PracticalNurse = 286,

            [DisplayName("Prescriptive Authority")]
            PrescriptiveAuthority = 33,

            [DisplayName("Volunteer Registered Nurse")]
            VolunteerRegisteredNurse = 311
        }

        public enum EFacilityLicenseTypeNursing
        {

        }

        public enum EBoard
        {
            [DisplayName("State Board of Cosmetology")]
            StateBoardOfCosmetology = 20,

            [DisplayName("State Board of Dentistry")]
            StateBoardOfDentistry = 13,

            [DisplayName("State Board of Nursing")]
            StateBoardOfNursing = 43,

            [DisplayName("State Board of Accountancy")]
            StateBoardofAccountancy = 3,

            [DisplayName("State Architects Licensure Board")]
            StateArchitectsLicensureBoard = 1,

            [DisplayName("State Board of Barber Examiners")]
            StateBoardofBarberExaminers = 15,

            [DisplayName("State Board of Certified Real Estate Appraisers")]
            StateBoardofCertifiedRealEstateAppraisers = 5,

            [DisplayName("State Board of Chiropractic")]
            StateBoardofChiropractic = 2,

            [DisplayName("State Registration Board for Professional Engineers, Land Surveyors and Geologists")]
            StateRegistrationBoardforProfessionalEngineersLandSurveyorsandGeologists = 28,

            [DisplayName("State Board of Funeral Directors")]
            StateBoardofFuneralDirectors = 17,

            [DisplayName("Health Division Volunteers")]
            HealthDivisionVolunteers = 10,

            [DisplayName("State Board of Landscape Architects")]
            StateBoardofLandscapeArchitects = 7,

            [DisplayName("State Board of Massage Therapy")]
            StateBoardofMassageTherapy = 12,

            [DisplayName("Navigation Commission for the Delaware River and its Navigable Tributaries")]
            NavigationCommissionfortheDelawareRiveranditsNavigableTributaries = 6,

            [DisplayName("State Board of Examiners of Nursing Home Administrators")]
            StateBoardofExaminersofNursingHomeAdministrators = 24,

            [DisplayName("State Board of Occupational Therapy")]
            StateBoardofOccupationalTherapy = 9,

            [DisplayName("State Board of Optometry")]
            StateBoardofOptometry = 26,

            [DisplayName("State Board of Pharmacy")]
            StateBoardofPharmacy = 8,

            [DisplayName("State Real Estate Commission")]
            StateRealEstateCommission = 27,

            [DisplayName("State Board of Social Workers, Marriage &amp; Family Therapists and Professional Counselors")]
            StateBoardofSocialWorkersMarriageFamilyTherapistsandProfessionalCounselors = 29,

            [DisplayName("State Board of Osteopathic Medicine")]
            StateBoardofOsteopathicMedicine = 36,

            [DisplayName("State Board of Auctioneer Examiners")]
            StateBoardofAuctioneerExaminers = 31,

            [DisplayName("State Board of Podiatry")]
            StateBoardofPodiatry = 33,

            [DisplayName("State Board of Psychology")]
            StateBoardofPsychology = 30,

            [DisplayName("State Board of Medicine")]
            StateBoardofMedicine = 37,

            [DisplayName("State Board of Vehicle Manufacturers, Dealers and Salespersons")]
            StateBoardofVehicleManufacturersDealersandSalespersons = 38,

            [DisplayName("Radiology Personnel")]
            RadiologyPersonnel = 39,

            [DisplayName("State Board of Examiners in Speech-Language Pathology and Audiology")]
            StateBoardofExaminersinSpeechLanguagePathologyandAudiology = 40,

            [DisplayName("State Board of Crane Operators")]
            StateBoardofCraneOperators = 41,

            [DisplayName("State Board of Physical Therapy")]
            StateBoardofPhysicalTherapy = 42
        }
    }
}
