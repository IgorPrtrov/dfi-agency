﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DFI.Data.DataGrader
{
    public class StateBoardOfCosmetology
    {
        public enum EBoardOfCosmetology
        {
            [DisplayName("State Board of Cosmetology")]
            StateBoardOfCosmetology = 20
        }

        public enum EPersonLicenseTypeCosmetology
        {
            [DisplayName("Cosmetologist")]
            Cosmetologist = 249,

            [DisplayName("Cosmetologist Temp Authority to Practice")]
            CosmetologistTempAuthorityToPractice = 362,

            [DisplayName("Cosmetology Teacher")]
            CosmetologyTeacher = 313,

            [DisplayName("Esthetician")]
            Esthetician = 231,

            [DisplayName("Esthetician Temp Authority to Practice")]
            EstheticianTempAuthorityToPractice = 365,

            [DisplayName("Nail Technician")]
            NailTechnician = 360,

            [DisplayName("Nail Technician Temp Auth to Practice")]
            NailTechnicianTempAuthToPractice = 173
        }

        public enum EFacilityLicenseTypeCosmetology
        {
            [DisplayName("Cosmetology Salon")]
            CosmetologySalon = 26,

            [DisplayName("Cosmetology School")]
            CosmetologySchool = 133,

            [DisplayName("Esthetician Salon")]
            EstheticianSalon = 39,

            [DisplayName("NailTechnology Salon")]
            NailTechnologySalon = 317,

            [DisplayName("Natural Hair Braiding Salon")]
            NaturalHairBraidingSalon = 181
        }
    }
}
