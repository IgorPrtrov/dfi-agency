﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DFI.Data.DataGrader
{
    public class StateBoardOfDentistry
    {
        public enum EBoardOfDentistry
        {
            [DisplayName("State Board of Dentistry")]
            StateBoardOfDentistry = 13
        }

        public enum EPersonLicenseTypeDentistry
        {            
            [DisplayName("Anesthesia Permit-Restricted I")]
            AnesthesiaPermitRestrictedI = 327,

            [DisplayName("Anesthesia Permit-Restricted II")]
            AnesthesiaPermitRestrictedII = 245,

            [DisplayName("Anesthesia Permit-Unrestricted")]
            AnesthesiaPermitUnrestricted = 69,

            [DisplayName("Dental Hygienist")]
            DentalHygienist = 224,

            [DisplayName("Dental Hygienist Local Anesthesia")]
            DentalHygienistLocalAnesthesia = 358,

            [DisplayName("Dentist")]
            Dentist = 156,

            [DisplayName("Expanded Function Dental Asst")]
            ExpandedFunctionDentalAsst = 194,

            [DisplayName("Public Health DH Practitioner")]
            PublicHealthDHPractitioner = 348,
        }

        public enum EFacilityLicenseTypeDentistry
        {
            [DisplayName("Dental Facility")]
            DentalFacility = 350
        }
    }
}
