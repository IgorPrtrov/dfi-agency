﻿using DFI.Data.AppUsers;
using DFI.Data.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using static DFI.Data.DataGrader.StateBoardOfCosmetology;
using static DFI.Data.DataGrader.StateBoardOfDentistry;
using static DFI.Data.DataGrader.StateBoardOfNursing;

namespace DFI.Data.DataGrader
{
    public class Grader : IGrader
    {
        private readonly IGraderMapper _graderMapper;
        private readonly IGraderRepository _graderRepository;
        private readonly ILogger<Grader> _logger;

        public Grader(IGraderMapper graderMapper, IGraderRepository graderRepository, ILogger<Grader> logger)
        {
            _graderMapper = graderMapper;
            _graderRepository = graderRepository;
            _logger = logger;
        }

        public void  GratePersonsOrFacilties(SearchForPersonOrFaciltyDto dto)
        {
            _logger.LogInformation($"Grader_Start_Work: {DateTime.Now}");
            foreach (var zip in Setting.ZipsPa)
            {
                BackgroundJob.Enqueue(() => RunGraderByZipCode(dto, zip));
            }
            _logger.LogInformation($"Grader_End_Work: {DateTime.Now}");
        }

        public async Task RunGraderByZipCode(SearchForPersonOrFaciltyDto dto, int zip)
        {
            for (int pageNo = 1; pageNo < Int32.MaxValue; pageNo++)
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(Setting.UrlSearchForPersonOrFacilty);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = Timeout.Infinite;
                httpWebRequest.KeepAlive = true;
                httpWebRequest.ReadWriteTimeout = Timeout.Infinite;

                _logger.LogInformation($"Grader_Working: LicenseTypeId: {dto.LicenseTypeId}, ProfessionID: {dto.ProfessionId}, Zip: {zip}");

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var request = new PersonRequestDto
                    {
                        Country = dto.Country,
                        IsFacility = dto.IsFacility ? 1 : 0,
                        OptPersonFacility = dto.OptPersonFacility,
                        PageNo = pageNo,
                        //LicenseTypeId = dto.LicenseTypeId,
                        ProfessionID = dto.ProfessionId,
                        State = dto.State,
                        zipcode = zip.ToString()
                    };

                    streamWriter.Write(JsonConvert.SerializeObject(request));
                }
                try
                {
                    var httpResponse = await httpWebRequest.GetResponseAsync();
                    var stream = httpResponse.GetResponseStream();
                    if (stream == null)
                        throw new Exception("GratePersonsOrFacilties::streen is null");

                    using (var streamReader = new StreamReader(stream))
                    {
                        var jsonResult = await streamReader.ReadToEndAsync();
                        var results = JsonConvert.DeserializeObject<List<PersonOrFacilityDto>>(jsonResult);
                        if (results.Any())
                        {
                            for (var j = 0; j < results.Count; j++)
                            {
                                try
                                {
                                    if (results[j].Status == "Active")
                                    {
                                        var personOrFacilityDetailsDto = await GetPersonOrFacilityDetails(new GetPersonOrFacilityDetailsDto
                                        {
                                            IsFacility = results[j].IsFacility,
                                            LicenseId = results[j].LicenseId,
                                            LicenseNumber = results[j].LicenseNumber,
                                            PersonId = results[j].PersonId
                                        });
                                        if (dto.IsFacility)
                                        {
                                            var facility = _graderMapper.MapToFacility(results[j], personOrFacilityDetailsDto);
                                            _graderRepository.CreateOrUpdateFacilities(facility);
                                        }
                                        else
                                        {
                                           await _graderRepository.CreateOrUpdateTemps(results[j], personOrFacilityDetailsDto);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    _logger.LogInformation($"Grader_Error_2_Catch: {e.Message}");
                                    j--;
                                }
                            }
                            if (results.Count < 50)
                                break;
                        }
                        else
                            break;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogInformation($"Grader_Error_1_Catch: {e.Message}");
                    pageNo--;
                }
            }
        }


        private async Task<PersonOrFacilityDetailsDto> GetPersonOrFacilityDetails(GetPersonOrFacilityDetailsDto dto)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Setting.UrlGetPersonOrFacilityDetails);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(JsonConvert.SerializeObject(dto));
            }

            var httpResponse = await httpWebRequest.GetResponseAsync();
            var stream = httpResponse.GetResponseStream();
            if(stream == null)
                throw new Exception("GetPersonOrFacilityDetails::stream is null");

            using (var streamReader = new StreamReader(stream))
            {
                var jsonResult = await streamReader.ReadToEndAsync();
                return JsonConvert.DeserializeObject<PersonOrFacilityDetailsDto>(jsonResult);
            }
        }

        //private async Task RunGraderForCosmetologyTemps()
        //{
        //    foreach (var licenseType in (EPersonLicenseTypeCosmetology[])Enum.GetValues(typeof(EPersonLicenseTypeCosmetology)))
        //    {
        //        await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //        {
        //            Country = "United States",
        //            IsFacility = false,
        //            LicenseTypeId = (int)licenseType,
        //            OptPersonFacility = "Person",
        //            ProfessionId = (int)EBoardOfCosmetology.StateBoardOfCosmetology,
        //            State = "PA"
        //        });
        //    }
        //}

        //private async Task RunGraderForDentistryTemps()
        //{
        //    foreach (var licenseType in (EPersonLicenseTypeDentistry[])Enum.GetValues(typeof(EPersonLicenseTypeDentistry)))
        //    {
        //        await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //        {
        //            Country = "United States",
        //            IsFacility = false,
        //            LicenseTypeId = (int)licenseType,
        //            OptPersonFacility = "Person",
        //            ProfessionId = (int)EBoardOfDentistry.StateBoardOfDentistry,
        //            State = "PA"
        //        });
        //    }
        //}

        //private async Task RunGraderForNursingTemps()
        //{
        //    foreach (var licenseType in (EPersonLicenseTypeNursing[])Enum.GetValues(typeof(EPersonLicenseTypeNursing)))
        //    {
        //        await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //        {
        //            Country = "United States",
        //            IsFacility = false,
        //            LicenseTypeId = (int)licenseType,
        //            OptPersonFacility = "Person",
        //            ProfessionId = (int)EBoardOfNursing.StateBoardOfNursing,
        //            State = "PA"
        //        });               
        //    }
        //}

        //private async Task RunGraderForFacilityCosmetology()
        //{
        //    foreach (var licenseType in (EFacilityLicenseTypeCosmetology[])Enum.GetValues(typeof(EFacilityLicenseTypeCosmetology)))
        //    {
        //        await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //        {
        //            Country = "United States",
        //            IsFacility = true,
        //            LicenseTypeId = (int)licenseType,
        //            OptPersonFacility = "Facility",
        //            ProfessionId = (int)EBoardOfCosmetology.StateBoardOfCosmetology,
        //            State = "PA"
        //        });                
        //    }
        //}

        //private async Task RunGraderForFacilityDentistry()
        //{
        //    foreach (var licenseType in (EFacilityLicenseTypeDentistry[])Enum.GetValues(typeof(EFacilityLicenseTypeDentistry)))
        //    {
        //        await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //        {
        //            Country = "United States",
        //            IsFacility = true,
        //            LicenseTypeId = (int)licenseType,
        //            OptPersonFacility = "Facility",
        //            ProfessionId = (int)EBoardOfDentistry.StateBoardOfDentistry,
        //            State = "PA"
        //        });
        //    }
        //}

        private void RunGraderForAllBoards()
        {
            foreach (var boardType in (EBoard[])Enum.GetValues(typeof(EBoard)))
            {
                BackgroundJob.Enqueue(() => GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
                {
                    Country = "United States",
                    IsFacility = false,
                    OptPersonFacility = "Person",
                    ProfessionId = (int)boardType,
                    State = "PA"
                }));
            }
        }
        //public async Task RunGraderForStateBoardofPhysicalTherapy()
        //{
        //    await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //    {
        //        Country = "United States",
        //        IsFacility = false,
        //        OptPersonFacility = "Person",
        //        ProfessionId = (int)EBoard.StateBoardofPhysicalTherapy,
        //        State = "PA"
        //    });           
        //}
        //public async Task RunGraderForRadiologyPersonnel()
        //{
        //    await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //    {
        //        Country = "United States",
        //        IsFacility = false,
        //        OptPersonFacility = "Person",
        //        ProfessionId = (int)EBoard.RadiologyPersonnel,
        //        State = "PA"
        //    });
        //}
        //public async Task RunGraderForStateBoardofCraneOperators()
        //{
        //    await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //    {
        //        Country = "United States",
        //        IsFacility = false,
        //        OptPersonFacility = "Person",
        //        ProfessionId = (int)EBoard.StateBoardofCraneOperators,
        //        State = "PA"
        //    });
        //}
        //public async Task RunGraderForStateBoardofExaminersinSpeechLanguagePathologyandAudiology()
        //{
        //    await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //    {
        //        Country = "United States",
        //        IsFacility = false,
        //        OptPersonFacility = "Person",
        //        ProfessionId = (int)EBoard.StateBoardofExaminersinSpeechLanguagePathologyandAudiology,
        //        State = "PA"
        //    });
        //}
        //public async Task RunGraderForStateBoardofVehicleManufacturersDealersandSalespersons()
        //{
        //    await GratePersonsOrFacilties(new SearchForPersonOrFaciltyDto
        //    {
        //        Country = "United States",
        //        IsFacility = false,
        //        OptPersonFacility = "Person",
        //        ProfessionId = (int)EBoard.StateBoardofVehicleManufacturersDealersandSalespersons,
        //        State = "PA"
        //    });
        //}

        public void StartGrader()
        {
            RunGraderForAllBoards();
        }
    }
    public interface IGrader
    {
        void StartGrader();
    }
}
