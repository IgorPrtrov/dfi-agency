﻿using DFI.Data.Helpers;
using DFI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DFI.Data.DataGrader
{
    public class GraderMapper : IGraderMapper
    {
        public Temp MapToTemp(PersonOrFacilityDto dto, PersonOrFacilityDetailsDto dtoDetails)
        {
            var temp = new Temp
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                MiddleName = dto.MiddleName,
                PersonId = dto.PersonId,
                PhoneNumber = dto.PhoneNo1,
                Email = dtoDetails.OtherLicenseDetails.FirstOrDefault(c => !string.IsNullOrEmpty(c.EmailId))?.EmailId??dtoDetails.EmailId??dto.Emailid1,
                AddressLine1 = dtoDetails.AddressLine1,
                AddressLine2 = dtoDetails.AddressLine2,
                City = dtoDetails.City,
                CountryName = dtoDetails.CountryName,
                StateName = dtoDetails.StateName,
                Zipcode = dtoDetails.zipcode,
                Licenses = new List<License> { new License
                                                {
                                                    BoardName = dtoDetails.BoardName,
                                                    IsFacility = false,
                                                    LicenseFromGavSiteId = dtoDetails.LicenseId,
                                                    LicenseNumber = dtoDetails.LicenseNumber,
                                                    ExpiryDate = NullableDateTime.Parse(dtoDetails.ExpiryDate),
                                                    LastRenewalDate = NullableDateTime.Parse(dtoDetails.LastRenewalDate),
                                                    LicenseType = dtoDetails.LicenseType,
                                                    Status = dtoDetails.Status,
                                                    Profession = dtoDetails.Profession
                                                }
                }
            };
            foreach(var dtoDet in dtoDetails.OtherLicenseDetails)
            {
                temp.Licenses.Add(new License
                {
                    BoardName = dtoDet.BoardName,
                    IsFacility = false,
                    LicenseFromGavSiteId = dtoDet.LicenseId,
                    LicenseNumber = dtoDet.LicenseNumber,
                    ExpiryDate = NullableDateTime.Parse(dtoDet.ExpiryDate),
                    LastRenewalDate = NullableDateTime.Parse(dtoDet.LastRenewalDate),
                    LicenseType = dtoDet.LicenseType,
                    Status = dtoDet.Status,
                    Profession = dtoDet.Profession
                });
            }
            return temp;
        }

        public Facility MapToFacility(PersonOrFacilityDto dto, PersonOrFacilityDetailsDto dtoDetails)
        {
            var facility = new Facility()
            {
                FacilityName = dto.FacilityName,
                FacilityStreetAddress = dto.FacilityStreetAddress,
                PersonId = dto.PersonId,
                PhoneNumber = dto.PhoneNo1,
                Email = dtoDetails.OtherLicenseDetails.FirstOrDefault(c => !string.IsNullOrEmpty(c.EmailId))?.EmailId ?? dtoDetails.EmailId ?? dto.Emailid1,
                AddressLine1 = dtoDetails.AddressLine1,
                AddressLine2 = dtoDetails.AddressLine2,
                City = dtoDetails.City,
                CountryName = dtoDetails.CountryName,
                StateName = dtoDetails.StateName,
                Zipcode = dtoDetails.zipcode,
                Licenses = new List<License> { new License
                                                {
                                                    BoardName = dtoDetails.BoardName,
                                                    IsFacility = true,
                                                    LicenseFromGavSiteId = dtoDetails.LicenseId,
                                                    LicenseNumber = dtoDetails.LicenseNumber,
                                                    ExpiryDate = NullableDateTime.Parse(dtoDetails.ExpiryDate),
                                                    LastRenewalDate = NullableDateTime.Parse(dtoDetails.LastRenewalDate),
                                                    LicenseType = dtoDetails.LicenseType,
                                                    Status = dtoDetails.Status,
                                                    Profession = dtoDetails.Profession
                                                }
                }
            };
            foreach (var dtoDet in dtoDetails?.OtherLicenseDetails)
            {
                facility.Licenses.Add(new License
                {
                    BoardName = dtoDet.BoardName,
                    IsFacility = true,
                    LicenseFromGavSiteId = dtoDet.LicenseId,
                    LicenseNumber = dtoDet.LicenseNumber,
                    ExpiryDate = NullableDateTime.Parse(dtoDet.ExpiryDate),
                    LastRenewalDate = NullableDateTime.Parse(dtoDet.LastRenewalDate),
                    LicenseType = dtoDet.LicenseType,
                    Status = dtoDet.Status,
                    Profession = dtoDet.Profession
                });
            }
            return facility;
        }
    }
    public interface IGraderMapper
    {
        Temp MapToTemp(PersonOrFacilityDto dto, PersonOrFacilityDetailsDto dtoDetails);
        Facility MapToFacility(PersonOrFacilityDto dto, PersonOrFacilityDetailsDto dtoDetails);
    }
}
