﻿using DFI.Data.GenericRepository;
using DFI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DFI.Data.DataGrader
{
    public class GraderRepository : Repository<Temp>, IGraderRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IGraderMapper _graderMapper;
        public GraderRepository(ApplicationDbContext context, IGraderMapper graderMapper) : base(context)
        {
            _graderMapper = graderMapper;
            _context = context;
        }

        public async Task CreateOrUpdateTemps(PersonOrFacilityDto facilityDto, PersonOrFacilityDetailsDto personOrFacilityDetailsDto)
        {
            var temp = _graderMapper.MapToTemp(facilityDto, personOrFacilityDetailsDto);
            var existTemp = await _context.Temps.Include(c => c.Licenses).FirstOrDefaultAsync(c => c.PersonId == temp.PersonId);

            if (existTemp == null)
            {
                var activeLicenses = temp.Licenses.Where(c => c.Status == "Active").ToList();
                temp.Licenses = activeLicenses;
                await _context.Temps.AddAsync(temp);
            }                  
            else
            {
                existTemp.AddressLine1 = temp.AddressLine1;
                existTemp.AddressLine2 = temp.AddressLine2;
                existTemp.City = temp.City;
                existTemp.StateName = temp.StateName;
                existTemp.Zipcode = temp.Zipcode;
                existTemp.CountryName = temp.CountryName;
                existTemp.Email = temp.Email;
                existTemp.PhoneNumber = temp.PhoneNumber; 
                var activeLicenses = temp.Licenses.Where(c => c.Status == "Active").ToList();
                foreach (var licenseNew in activeLicenses)
                {
                    var licenseForUpdate = existTemp.Licenses.FirstOrDefault(c => c.LicenseNumber == licenseNew.LicenseNumber);

                    if (licenseForUpdate == null)
                        existTemp.Licenses.Add(licenseNew);
                    else
                    {
                        licenseForUpdate.ExpiryDate = licenseNew.ExpiryDate;
                        licenseForUpdate.Status = licenseNew.Status;
                        licenseForUpdate.LastRenewalDate = licenseNew.LastRenewalDate;
                    }                       
                }
                _context.Update(existTemp);
            }
            await _context.SaveChangesAsync();
        }

        public void CreateOrUpdateFacilities(Facility facility)
        {
            var existTemp = _context.Facilities.FirstOrDefault(c => c.PersonId == facility.PersonId);

            if (existTemp == null)
            {
                var activeLicenses = facility.Licenses.Where(c => c.Status == "Active").ToList();
                facility.Licenses = activeLicenses;
                _context.Facilities.Add(facility);
            }
            else
            {
                //TODO if account conferm does not update private information or send notification about changes
                existTemp.AddressLine1 = facility.AddressLine1;
                existTemp.AddressLine2 = facility.AddressLine2;
                existTemp.City = facility.City;
                existTemp.StateName = facility.StateName;
                existTemp.Zipcode = facility.Zipcode;
                existTemp.CountryName = facility.CountryName;
                existTemp.Email = facility.Email;
                existTemp.PhoneNumber = facility.PhoneNumber;
                var activeLicenses = facility.Licenses.Where(c => c.Status == "Active").ToList();
                foreach (var licenseNew in activeLicenses)
                {
                    var licenseForUpdate = existTemp.Licenses.FirstOrDefault(c => c.LicenseNumber == licenseNew.LicenseNumber);

                    if (licenseForUpdate == null)
                        existTemp.Licenses.Add(licenseNew);
                    else
                    {
                        licenseForUpdate.ExpiryDate = licenseNew.ExpiryDate;
                        licenseForUpdate.Status = licenseNew.Status;
                        licenseForUpdate.LastRenewalDate = licenseNew.LastRenewalDate;
                        _context.Update(existTemp);
                    }
                }
            }
            _context.SaveChanges();
        }

    }
    public interface IGraderRepository : IRepository<Temp>
    {
        Task CreateOrUpdateTemps(PersonOrFacilityDto facilityDto, PersonOrFacilityDetailsDto personOrFacilityDetailsDto);
        void CreateOrUpdateFacilities(Facility facility);
    }
}
