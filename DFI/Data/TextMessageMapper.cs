﻿using System;
using DFI.Models;

namespace DFI.Data.TextMessages
{
    public class TextMessageMapper : ITextMessageMapper
    {
        public TextMessage MapToTextMessage(TextMessageDto dto)
        {
            return new TextMessage()
            {
                Body = dto.Body,
                PhoneNumberTo = dto.PhoneNumberTo,
                CreatedOn = DateTime.UtcNow,
                PhoneNumberFrom = Setting.TwilioNumberFrom,
                IsSent = false
            };
        }
    }
    public interface ITextMessageMapper
    {
        TextMessage MapToTextMessage(TextMessageDto dto);
    }
}