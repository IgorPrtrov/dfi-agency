namespace DFI.Data.Common
{
    public class RemoveDto
    {
        public int EntityId { get; set; }
    }
}