﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Data.Common
{
    public class IdNameDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}