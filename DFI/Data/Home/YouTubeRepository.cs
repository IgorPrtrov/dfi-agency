﻿using DFI.Models;

namespace DFI.Data.Home
{
    public class YouTubeRepository : Repository<YouTubeVideo>, IYouTubeRepository
    {
        private readonly IApplicationDbContext _context;
        public YouTubeRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }    
    }

    public interface IYouTubeRepository : IRepository<YouTubeVideo>
    {

    }
}