﻿using DFI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace DFI.Data.Home
{
    public class HomeServices : IHomeServices
    {
        private static readonly HttpClient client = new HttpClient();
        private readonly IYouTubeRepository _youTubeRepository;

        public HomeServices(IYouTubeRepository youTubeRepository)
        {
            _youTubeRepository = youTubeRepository;
        }

        public List<Review> GetGoogleReviews()
        {
            string url = "https://maps.googleapis.com/maps/api/place/details/json" +
                $"?place_id=ChIJAQC88FPwxokRire0HPFaYGA&fields=name,rating,reviews&key={Setting.GoogleMapsApi}";

            // Send GET request
            HttpResponseMessage response = client.GetAsync(url).Result;
            
            // Ensure the request was successful
            response.EnsureSuccessStatusCode();

            // Read response as a string
            string responseBody = response.Content.ReadAsStringAsync().Result;

            // Deserialize the JSON response into C# objects
            GooglePlaceResponse placeResponse = JsonConvert.DeserializeObject<GooglePlaceResponse>(responseBody);

            // Access the list of reviews
            List<Review> reviews = placeResponse.Result.Reviews.OrderByDescending(c => c.Time).Where(c => c.Rating == 5 && c.Language == "en").Take(10).ToList();

            return reviews;
        }

        public List<string> GetYouTubeVideos()
        {
            return _youTubeRepository.GetAll().Select(c => $"https://www.youtube.com/embed/{ExtractVideoId(c.VideoUrl)}").ToList();
        }

        static string ExtractVideoId(string youtubeUrl)
        {
            Uri uri = new Uri(youtubeUrl);
            string query = uri.Query;
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(query);
            return queryDictionary["v"];
        }

        public IEnumerable<YouTubeVideo> GetAllVideos()
        {
            return _youTubeRepository.GetAll();
        }

        public void AddVideo(YouTubeVideo model)
        {
            _youTubeRepository.Add(model);
            _youTubeRepository.SaveChanges();
        }

        public void DeleteVideo(int id)
        {
            var model = _youTubeRepository.GetById(id);

            if(model != null)
            {
                _youTubeRepository.Delete(model);
                _youTubeRepository.SaveChanges();
            }
        }

        public void UpdateVideo(YouTubeVideo model)
        {
            _youTubeRepository.Update(model);
            _youTubeRepository.SaveChanges();           
        }
    }

    public interface IHomeServices
    {
        List<Review> GetGoogleReviews();
        List<string> GetYouTubeVideos();
        IEnumerable<YouTubeVideo> GetAllVideos();
        void AddVideo(YouTubeVideo model);
        void DeleteVideo(int id);
        void UpdateVideo(YouTubeVideo model);
    }
}