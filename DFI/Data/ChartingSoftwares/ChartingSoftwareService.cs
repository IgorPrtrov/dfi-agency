namespace DFI.Data.ChartingSoftwares
{
    public class ChartingSoftwareService : IChartingSoftwareService
    {
        private readonly IChartingSoftwareRepository _chartingSoftwareRepository;

        public ChartingSoftwareService(IChartingSoftwareRepository chartingSoftwareRepository)
        {
            _chartingSoftwareRepository = chartingSoftwareRepository;
        }
        
        public string[] GetChartingSoftwareNames()
        {
            return _chartingSoftwareRepository.GetChartingSoftwareNames();
        }

        public void AddOrUpdate(string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value.Trim()))
            {
                return;
            }
            
            _chartingSoftwareRepository.AddOrUpdate(value);
        }
    }

    public interface IChartingSoftwareService
    {
        string[] GetChartingSoftwareNames();

        void AddOrUpdate(string value);
    }
}