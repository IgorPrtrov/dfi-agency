using System;
using System.Linq;
using DFI.Models;

namespace DFI.Data.ChartingSoftwares
{
    public class ChartingSoftwareRepository : Repository<ChartingSoftware>, IChartingSoftwareRepository
    {
        private readonly IApplicationDbContext _context;

        public ChartingSoftwareRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        
        public string[] GetChartingSoftwareNames()
        {
            return _context.ChartingSoftwares.Select(x => x.Name).ToArray();
        }

        public void AddOrUpdate(string value)
        {
            if (!_context.ChartingSoftwares.Any(x => x.Name.Equals(value, StringComparison.InvariantCultureIgnoreCase)))
            {
                _context.ChartingSoftwares.Add(new ChartingSoftware
                {
                    Name = value
                });

                _context.SaveChanges();
            }
        }
    }

    public interface IChartingSoftwareRepository
    {
        string[] GetChartingSoftwareNames();
        
        void AddOrUpdate(string value);
    }
}