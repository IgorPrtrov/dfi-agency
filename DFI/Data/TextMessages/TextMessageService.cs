﻿using System;
using DFI.Helpers;
using DFI.Models;
using Twilio.Rest.Api.V2010.Account;

namespace DFI.Data.TextMessages
{
    public class TextMessageService : ITextMessageService
    {
        private readonly ITextMessageRepository _textMessageRepository;
        private readonly ITextMessageMapper _textMessageMapper;
        private readonly ILogger _logger;
        private readonly ITwilioService _twilioService;
        private bool HasPhoneNumberAlias = Setting.HasPhoneNumberFromAlias;
        private string PhoneNumberFromAlias = Setting.PhoneNumberFromAlias;
        private bool IsActiveSendTextMessages = Setting.SendTextMessages;
        private bool TwilioServiceIsActive = Setting.TwilioServiceIsActive;

        public TextMessageService(ITextMessageRepository textMessageRepository, ITextMessageMapper textMessageMapper, ILogger logger, ITwilioService twilioService)
        {
            _textMessageRepository = textMessageRepository;
            _textMessageMapper = textMessageMapper;
            _logger = logger;
            _twilioService = twilioService;
        }

        public void SendSms()
        {
            var textMessages = _textMessageRepository.GetSMSForDelivery();
            _logger.Info($"Text Messages Send:: hasPhoneNumberAlias: {HasPhoneNumberAlias}");
            _logger.Info($"Text Messages Send:: phoneNumberFromAlias: {PhoneNumberFromAlias}");
            _logger.Info($"Text Messages Send:: isActiveSendTextMessages: {IsActiveSendTextMessages}");

            foreach (var sms in textMessages)
            {
                try
                {
                    if (IsActiveSendTextMessages == false)
                    {
                        FakeSendSms(sms);
                        continue;
                    }

                    var phoneNumberTo = HasPhoneNumberAlias && !string.IsNullOrEmpty(PhoneNumberFromAlias) ? PhoneNumberFromAlias : sms.PhoneNumberTo;
                    _logger.Info($"Text Messages Send:: phoneNumberTo: {phoneNumberTo}");


                    if (_textMessageRepository.IsNotSentSms(sms.TextMessageId))
                    {
                        var response = _twilioService.SendTwilioSms(sms.PhoneNumberFrom, phoneNumberTo, sms.Body);

                        if (response.Status == MessageResource.StatusEnum.Sent || response.Status == MessageResource.StatusEnum.Queued)
                        {
                            sms.SentDate = DateTime.Now.ToUniversalTime();
                            sms.IsSent = true;
                            sms.MessageSid = response.Sid;
                            sms.Status = MessageResource.StatusEnum.Sent.ToString();
                            _logger.Info(
                                $"TextMessageService:: Send Successfully. id = {sms.TextMessageId}, PhoneNumberTo = {sms.PhoneNumberTo}," +
                                $" Status = {response.Status}");
                        }
                        else
                        {
                            _logger.Warn($"TextMessageService:: Send not good. id = {sms.TextMessageId}, PhoneNumberTo = {sms.PhoneNumberTo}," +
                                         $" Status = {response.Status}, error = {response.ErrorMessage}, error code = {response.ErrorCode}");
                        }

                        sms.Attempts++;
                    }
                }
                catch (Exception e)
                {
                    _logger.Error($"Text message Send:: Exception: {e.Message}");
                    sms.Attempts++;
                }

                _textMessageRepository.Update(sms);
                _textMessageRepository.SaveChanges();
            }
        }

        private void FakeSendSms(TextMessage sms)
        {
            sms.SentDate = DateTime.Now.ToUniversalTime();
            sms.IsSent = true;
            _textMessageRepository.Update(sms);
            _textMessageRepository.SaveChanges();

            _logger.Info($"Text Messages Send:: Text Messages id {sms.TextMessageId} has been saved as delivered in DB but really it was not sent");
        }

        public void AddSms(TextMessageDto dto)
        {
            if (TwilioServiceIsActive)
            {
                var sms = _textMessageMapper.MapToTextMessage(dto);
                _textMessageRepository.Add(sms);
                _textMessageRepository.SaveChanges();
            }
        }

        public void UpdateSmsStatus(string sid, string status)
        {
            var sms = _textMessageRepository.GetBySid(sid);

            if(sms != null)
            {
                sms.Status = status;

                _textMessageRepository.Update(sms);
                _textMessageRepository.SaveChanges();
            }
            else
            {
                _logger.Error($"TextMessageService:: UpdateSmsStatus:: SMS is null with sid = {sid}, status = {status}");
            }
        }
    }

    public interface ITextMessageService
    {
        void SendSms();
        void AddSms(TextMessageDto dto);
        void UpdateSmsStatus(string sid, string status);
    }
}