﻿using System.Collections.Generic;
using System.Linq;
using DFI.Models;

namespace DFI.Data.TextMessages
{
    public class TextMessageRepository : Repository<TextMessage>, ITextMessageRepository
    {
        private readonly IApplicationDbContext _context;
        public TextMessageRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public List<TextMessage> GetSMSForDelivery()
        {
            return _context.TextMessages.AsQueryable().Where(c => c.IsSent == false && (c.Attempts < 5)).Take(20).ToList();
        }

        public bool IsNotSentSms(int id)
        {
            var sms = _context.TextMessages.FirstOrDefault(c => c.TextMessageId == id);
            return sms.IsSent == false && sms.Attempts < 5;
        }

        public TextMessage GetBySid(string sid)
        {
            return _context.TextMessages.FirstOrDefault(c => c.MessageSid == sid);
        }
    }

    public interface ITextMessageRepository : IRepository<TextMessage>
    {
        List<TextMessage> GetSMSForDelivery();
        bool IsNotSentSms(int id);
        TextMessage GetBySid(string sid);
    }
}