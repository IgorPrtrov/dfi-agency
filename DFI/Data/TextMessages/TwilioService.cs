using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace DFI.Data.TextMessages
{
    public class TwilioService : ITwilioService
    {
        public TwilioService()
        {
           TwilioClient.Init(Setting.TwilioAccountSit, Setting.TwilioAuthToken);
        }

        public TwilioSmsResponse SendTwilioSms(string phoneNumberFrom, string phoneNumberTo, string body)
        {
            var smsResponse = MessageResource.Create(
                            from: new PhoneNumber(phoneNumberFrom),
                            statusCallback: new Uri($"{Setting.ApplicationBaseUrl}MessageStatus/Index"),
                            to: new PhoneNumber(phoneNumberTo),
                            body: body);

            return new TwilioSmsResponse 
            { 
                ErrorCode = smsResponse.ErrorCode,
                ErrorMessage = smsResponse.ErrorMessage,
                Sid = smsResponse.Sid,
                Status = smsResponse.Status,
            };
        }
    }

    public interface ITwilioService
    {
        TwilioSmsResponse SendTwilioSms(string phoneNumberFrom, string phoneNumberTo, string body);
    }

    public class TwilioSmsResponse
    {
        public MessageResource.StatusEnum Status { get; set; }
        public string Sid { get; set; }
        public string ErrorMessage { get; set; }
        public int? ErrorCode { get; set; }
    }
}