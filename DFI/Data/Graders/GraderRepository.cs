﻿using DFI.Models;


namespace DFI.Data.Graders
{
    public class GraderRepository : Repository<Temp>, IGraderRepository
    {
        private readonly IApplicationDbContext _context;
        public GraderRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
    public interface IGraderRepository : IRepository<Temp>
    {

    }
}
