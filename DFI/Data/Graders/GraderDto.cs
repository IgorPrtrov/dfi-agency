﻿using System;
using System.Collections.Generic;
using DFI.Helpers;

namespace DFI.Data.Graders
{
    public class SearchForPersonOrFaciltyDto
    {
        public int ProfessionId { get; set; } 
        public int LicenseTypeId { get; set; }
        public string OptPersonFacility { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public bool IsFacility { get; set; }
        public string LicenseNumber { get; set; }
        public string LastName { get; set; }
        public EGragerType GraderType { get; set; }
    }
    public class StartGraderDto
    {
        public string LicenseNumber { get; set; }
        public string LastName { get; set; }
        public EGragerType GraderType { get; set; }
    }
    public class GetPersonOrFacilityDetailsDto
    {
        public string IsFacility { get; set; }
        public string LicenseId { get; set; }
        public string LicenseNumber { get; set; }
        public string PersonId { get; set; }
    }
    public class PersonOrFacilityDto
    {
        public int? IsFacility { set; get; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public string FacilityName { set; get; }
        public int? ProfessionID { set; get; }
        public int? LicenseTypeId { set; get; }
        public string LicenseNumber { set; get; }
        public string LicenceType { set; get; }
        public string ProfessionType { set; get; }
        public string Status { set; get; }
        public string AddressLine1 { set; get; }
        public string AddressLine2 { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public string County { set; get; }
        public string Country { set; get; }
        public string zipcode { set; get; }
        public string OptPersonFacility { set; get; }
        public string FullName { set; get; }
        public int? PersonId { set; get; }
        public string FacilityStreetAddress { set; get; }
        public string DisciplinaryActionTypeId { set; get; }
        public string DisciplinaryAction { set; get; }
        public int? ComplaintNumber { set; get; }
        public DateTime? ComplaintFromDate { set; get; }
        public DateTime? ComplaintToDate { set; get; }
        public string AddressLine3 { set; get; }
        public string AddressLine4 { set; get; }
        public string CountryShortCode { set; get; }
        public string BoardName { set; get; }
        public string RequestedFileName { set; get; }
        public int? RequestedFileId { set; get; }
        public int? LicenseId { set; get; }
        public string RecaptchaResponse { set; get; }
        public int? Row { set; get; }
        public int? PageNo { set; get; }
        public int? TotalRecords { set; get; }
        public string NameSuffix { set; get; }
        public string PhoneNo1 { set; get; }
        public string Emailid1 { set; get; }
        public string McareDocketNumber { set; get; }
        public string McareCourtFiled { set; get; }
        public string McareServed { set; get; }
        public string FictitiousName { set; get; }
        public string DoingBusinessAs { set; get; }
    }
    public class PersonOrFacilityDetailsDto
    {
        public PersonOrFacilityDetailsDto()
        {
            OtherLicenseDetails = new List<OtherLicenseDetails>();
        }
        public string Name { set; get; }
        public string FacilityName { set; get; }
        public string EmailId { set; get; }
        public string AddressLine1 { set; get; }
        public string AddressLine2 { set; get; }
        public string City { set; get; }
        public string StateName { set; get; }
        public string zipcode { set; get; }
        public string Profession { set; get; }
        public string LicenseType { set; get; }
        public string LicenseTypeInstructions { set; get; }
        public string RelationshipLicenseInstructions { set; get; }
        public string obtainedBy { set; get; }
        public string SpecialityType { set; get; }
        public string LicenseNumber { set; get; }
        public string Status { set; get; }
        public string StatusEffectivedate { set; get; }
        public string IssueDate { set; get; }
        public string ExpiryDate { set; get; }
        public string LastRenewalDate { set; get; }
        public string OptPersonFacility { set; get; }
        public string PersonId { set; get; }
        public string NextRenewal { set; get; }
        public string Relationship { set; get; }
        public string AssociationDate { set; get; }
        public bool? IsFacility { set; get; }
        public int? LicenseId { set; get; }
        public int? ShowFullAddress { set; get; }
        public int? ProfessionId { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public bool? IsActiveLink { set; get; }
        public string AddressLine3 { set; get; }
        public string AddressLine4 { set; get; }
        public string CountryName { set; get; }
        public string BoardName { set; get; }
        public List<OtherLicenseDetails> OtherLicenseDetails { get; set; }
    }
    public class OtherLicenseDetails
    {
        public string Name { get; set; }
        public string FacilityName { get; set; }
        public string EmailId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
        public string zipcode { get; set; }
        public string Profession { get; set; }
        public string LicenseType { get; set; }
        public string LicenseTypeInstructions { get; set; }
        public string RelationshipLicenseInstructions { get; set; }
        public string obtainedBy { get; set; }
        public string SpecialityType { get; set; }
        public string LicenseNumber { get; set; }
        public string Status { get; set; }
        public string StatusEffectivedate { get; set; }
        public string IssueDate { get; set; }
        public string ExpiryDate { get; set; }
        public string LastRenewalDate { get; set; }
        public string OptPersonFacility { get; set; }
        public int? PersonId { get; set; }
        public string NextRenewal { get; set; }
        public string Relationship { get; set; }
        public string AssociationDate { get; set; }
        public bool? IsFacility { get; set; }
        public int? LicenseId { get; set; }
        public string ShowFullAddress { get; set; }
        public int? ProfessionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? IsActiveLink { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string CountryName { get; set; }
        public string BoardName { get; set; }
        public string DisciplinaryActionDetails { get; set; }
        public string PrerequisiteInformation { get; set; }
        public string LicenseCSRInformation { get; set; }
        public string PinItemList { get; set; }
        public string StatusHistoryList { get; set; }
    }
    public class PersonRequestDto
    {
        public string Country { set; get; }
        public string County { set; get; }
        public int IsFacility { set; get; }
        public string LicenseNumber { set; get; }
        public string OptPersonFacility { set; get; }
        public int PageNo { set; get; }
        public string PersonId { set; get; }
        public string State { set; get; }
        public int LicenseTypeId { set; get; }
        public int ProfessionID { set; get; }
        public string zipcode { set; get; }
        public string LastName { get; set; }
    }

    public class ValidationGraderStatus : ValidationStatus
    {
        public int? TempId { get; set; }
    }

    public enum EGragerType
    {
        CreateTemp,
        CheckLicense
    }
}
