﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using DFI.Data.Temps;
using DFI.Helpers;
using static DFI.Helpers.Helper;
namespace DFI.Data.Graders
{
    public class GraderService : IGraderService
    {
        private readonly IGraderMapper _graderMapper;
        private readonly ILogger _logger;
        private readonly ITempRepository _tempRepository;

        public GraderService(IGraderMapper graderMapper, ILogger logger, ITempRepository tempRepository)
        {
            _graderMapper = graderMapper;
            _logger = logger;
            _tempRepository = tempRepository;
        }


        public ValidationGraderStatus RunGraderByLicenseNumber(SearchForPersonOrFaciltyDto dto)
        {      
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Setting.UrlSearchForPersonOrFacilty);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = Timeout.Infinite;
            httpWebRequest.KeepAlive = true;
            httpWebRequest.ReadWriteTimeout = Timeout.Infinite;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                                    SecurityProtocolType.Tls11 |
                                                    SecurityProtocolType.Tls12;

            _logger.Info($"Grader_Working: LicenseTypeId: {dto.LicenseTypeId}, ProfessionID: {dto.ProfessionId}");

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var request = new PersonRequestDto
                {
                    Country = dto.Country,
                    IsFacility = dto.IsFacility ? 1 : 0,
                    OptPersonFacility = dto.OptPersonFacility,
                    PageNo = 1,
                    ProfessionID = dto.ProfessionId,
                    State = dto.State,
                    LicenseNumber = dto.LicenseNumber,
                    LastName = dto.LastName
                };

                streamWriter.Write(JsonConvert.SerializeObject(request));
            }
            var result = new ValidationGraderStatus() { Success = false, Messages = new List<string>(){"Licenses have not been found"}};
            try
            {
                var httpResponse = httpWebRequest.GetResponseAsync().Result;
                var stream = httpResponse.GetResponseStream();
                if (stream == null)
                    throw new Exception("GratePersonsOrFacilties::streen is null");

                using (var streamReader = new StreamReader(stream))
                {
                    var jsonResult = streamReader.ReadToEndAsync().Result;
                    var results = JsonConvert.DeserializeObject<List<PersonOrFacilityDto>>(jsonResult);
                    if (results.Any())
                    {
                        for (var j = 0; j < results.Count; j++)
                        {
                            if(results[j].LastName.ToLower().Trim() != dto.LastName.ToLower().Trim())
                                continue;

                            try
                            {
                                var personOrFacilityDetailsDto = GetPersonOrFacilityDetails(
                                new GetPersonOrFacilityDetailsDto
                                {
                                    IsFacility = results[j].IsFacility.ToString(),
                                    LicenseId = results[j].LicenseId.ToString(),
                                    LicenseNumber = results[j].LicenseNumber,
                                    PersonId = results[j].PersonId.ToString()
                                });                           
                                var temp = results[j];

                                if (dto.GraderType == EGragerType.CreateTemp)
                                    result = CreateOrUpdateTemps(temp, personOrFacilityDetailsDto);

                                else if (dto.GraderType == EGragerType.CheckLicense)
                                    result = UpdateLicenses(temp, personOrFacilityDetailsDto);                               
                            }
                            catch (Exception e)
                            {
                                _logger.Error($"Grader_Error_2_Catch: {e.Message}");
                                j--;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error($"Grader_Error_1_Catch: {e.Message}");
            }
            return result;
        }

        public ValidationGraderStatus CreateOrUpdateTemps(PersonOrFacilityDto facilityDto, PersonOrFacilityDetailsDto personOrFacilityDetailsDto)
        {
            var result = new ValidationGraderStatus(){Success = true};

            var temp = _graderMapper.MapToTemp(facilityDto, personOrFacilityDetailsDto);
            var existTemp = _tempRepository.GetByPersoneId(temp.PersonId);
            var activeLicenses = temp.Licenses.Where(c => c.Status == "Active" 
                                                          && (c.BoardName == EBoard.StateBoardOfDentistry.GetDisplayName() || c.BoardName == EBoard.RadiologyPersonnel.GetDisplayName()) 
                                                          && 
                                                          (c.LicenseType == ELicenseType.DentalHygienist.GetDisplayName() 
                                                           || c.LicenseType == ELicenseType.Dentist.GetDisplayName()
                                                           || c.LicenseType == ELicenseType.ExpandedFunctionDentalAsst.GetDisplayName())).ToList();

            if (!activeLicenses.Any())
            {
                result.Success = false;
                result.Messages.Add($"There are no active licenses or your licenses are not match our requirements.");
                return result;
            }

            foreach (var activeLicense in activeLicenses)
            {
                if (existTemp != null && existTemp.Status != (int)EState.New && _tempRepository.HasLicenseNumber(activeLicense.LicenseNumber))
                {
                    result.Success = false;
                    result.Messages.Add($"License number already exists in our system, {activeLicense.LicenseNumber}");
                }
            }

            if (result.Success == false)
                return result;

            if (existTemp == null)
            {              
                temp.Licenses = activeLicenses;
                temp.Status = (int) EState.New;
                temp.AddedDate = DateTime.Now.ToUniversalTime();
                _tempRepository.Add(temp);
            }
            else
            {
                foreach (var licenseNew in activeLicenses)
                {
                    var licenseForUpdate = existTemp.Licenses.FirstOrDefault(c => c.LicenseNumber == licenseNew.LicenseNumber);

                    if (licenseForUpdate == null)
                    {
                         existTemp.Licenses.Add(licenseNew);
                    }                     
                    else
                    {
                        licenseForUpdate.ExpiryDate = licenseNew.ExpiryDate;
                        licenseForUpdate.Status = licenseNew.Status;
                    }
                }
                _tempRepository.Update(existTemp);
            }
            _tempRepository.SaveChanges();

            result.TempId = temp.TempId == 0 ? existTemp?.TempId ?? 0 : temp.TempId;
            return result;
        }

        public ValidationGraderStatus UpdateLicenses(PersonOrFacilityDto facilityDto, PersonOrFacilityDetailsDto personOrFacilityDetailsDto)
        {
            var result = new ValidationGraderStatus() { Success = false };

            var temp = _graderMapper.MapToTemp(facilityDto, personOrFacilityDetailsDto);
            var existTemp = _tempRepository.GetByPersoneId(temp.PersonId);
            var tempLicenses = temp.Licenses.Where(c => (c.BoardName == EBoard.StateBoardOfDentistry.GetDisplayName() || c.BoardName == EBoard.RadiologyPersonnel.GetDisplayName())
                                                        &&
                                                        (c.LicenseType == ELicenseType.DentalHygienist.GetDisplayName()
                                                         || c.LicenseType == ELicenseType.Dentist.GetDisplayName()
                                                         || c.LicenseType == ELicenseType.ExpandedFunctionDentalAsst.GetDisplayName())).ToList();

            foreach (var license in tempLicenses)
            {
                var licenseForUpdate = existTemp.Licenses.FirstOrDefault(c => c.LicenseNumber == license.LicenseNumber);

                if (licenseForUpdate != null)                
                {
                    if (licenseForUpdate.ExpiryDate < license.ExpiryDate)
                    {
                        result.Success = true;
                        result.Messages.Add($"License number {licenseForUpdate.LicenseNumber} has been updated for " +
                                            $"Expiry Day from {licenseForUpdate.ExpiryDate} to {license.ExpiryDate} " +
                                            $"status {license.Status}. " +
                                            $"Pofessional {existTemp.FirstName} {existTemp.LastName} id = {existTemp.TempId}");
                    }
                    else
                    {
                        result.Messages.Add($"License number {licenseForUpdate.LicenseNumber} has NOT been updated for " +
                                            $"Expiry Day from {licenseForUpdate.ExpiryDate} to {license.ExpiryDate} " +
                                            $"status {license.Status}. " +
                                            $"Pofessional {existTemp.FirstName} {existTemp.LastName} id = {existTemp.TempId}");
                    }

                    licenseForUpdate.ExpiryDate = license.ExpiryDate;
                    licenseForUpdate.Status = license.Status;
                }
            }

            _tempRepository.Update(existTemp);            
            _tempRepository.SaveChanges();

            return result;
        }

        private PersonOrFacilityDetailsDto GetPersonOrFacilityDetails(GetPersonOrFacilityDetailsDto dto)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Setting.UrlGetPersonOrFacilityDetails);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = Timeout.Infinite;
            httpWebRequest.KeepAlive = true;
            httpWebRequest.ReadWriteTimeout = Timeout.Infinite;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                                   SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(JsonConvert.SerializeObject(dto));
            }

            var httpResponse = httpWebRequest.GetResponseAsync().Result;
            var stream = httpResponse.GetResponseStream();
            if(stream == null)
                throw new Exception("GetPersonOrFacilityDetails::stream is null");

            using (var streamReader = new StreamReader(stream))
            {
                var jsonResult = streamReader.ReadToEndAsync().Result;
                return JsonConvert.DeserializeObject<PersonOrFacilityDetailsDto>(jsonResult);
            }
        }
      
        public ValidationGraderStatus StartGrader(StartGraderDto dto)
        {
            return RunGraderByLicenseNumber(new SearchForPersonOrFaciltyDto
            {
                Country = "ALL",
                IsFacility = false,
                OptPersonFacility = "Person",
                State = "",
                LicenseNumber = dto.LicenseNumber,
                LastName = dto.LastName,
                GraderType = dto.GraderType
            });
        }
    }
    public interface IGraderService
    {
        ValidationGraderStatus StartGrader(StartGraderDto dto);
    }
}
