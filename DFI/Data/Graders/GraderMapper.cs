﻿using System.Collections.Generic;
using System.Linq;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Graders
{
    public class GraderMapper : IGraderMapper
    {
        public Temp MapToTemp(PersonOrFacilityDto dto, PersonOrFacilityDetailsDto dtoDetails)
        {
            var temp = new Temp
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PersonId = dto.PersonId,
                CellPhone = dto.PhoneNo1,
                Email = dtoDetails.OtherLicenseDetails.FirstOrDefault(c => !string.IsNullOrEmpty(c.EmailId))?.EmailId??dtoDetails.EmailId??dto.Emailid1,
                AddressLine1 = dtoDetails.AddressLine1,
                Apartment = dtoDetails.AddressLine2,
                City = dtoDetails.City,
                StateName = dtoDetails.StateName,
                Zipcode = dtoDetails.zipcode,
                FullAddress = $"{dtoDetails.AddressLine1}, {dtoDetails.City}, {dtoDetails.StateName}, {dtoDetails.zipcode}",
                Licenses = new List<License> {
                    new License
                    {
                        BoardName = dtoDetails.BoardName,
                        LicenseNumber = dtoDetails.LicenseNumber,
                        ExpiryDate = Helper.Parse(dtoDetails.ExpiryDate),
                        LicenseType = dtoDetails.LicenseType.ToLower().Contains("auxiliary person by") ? $"Dental Assistant ({dtoDetails.LicenseType})" : dtoDetails.LicenseType,
                        Status = dtoDetails.Status,
                        Profession = dtoDetails.Profession
                    }
                }
            };
            foreach(var dtoDet in dtoDetails.OtherLicenseDetails)
            {
                temp.Licenses.Add(new License
                {
                    BoardName = dtoDet.BoardName,
                    LicenseNumber = dtoDet.LicenseNumber,
                    ExpiryDate = Helper.Parse(dtoDet.ExpiryDate),
                    LicenseType = dtoDet.LicenseType.ToLower().Contains("auxiliary person by") ? $"Dental Assistant ({dtoDet.LicenseType})" : dtoDet.LicenseType,
                    Status = dtoDet.Status,
                    Profession = dtoDet.Profession
                });
            }
            return temp;
        }
    }
    public interface IGraderMapper
    {
        Temp MapToTemp(PersonOrFacilityDto dto, PersonOrFacilityDetailsDto dtoDetails);        
    }
}
