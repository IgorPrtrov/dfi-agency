using System;
using System.Linq;
using DFI.Models;

namespace DFI.Data.XraySoftwares
{
    public class XraySoftwareRepository : Repository<XraySoftware>, IXraySoftwareRepository
    {
        private readonly IApplicationDbContext _context;

        public XraySoftwareRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        
        public string[] GetXraySoftwareNames()
        {
            return _context.XraySoftwares.Select(x => x.Name).ToArray();
        }
        
        public void AddOrUpdate(string value)
        {
            if (!_context.XraySoftwares.Any(x => x.Name.Equals(value, StringComparison.InvariantCultureIgnoreCase)))
            {
                _context.XraySoftwares.Add(new XraySoftware
                {
                    Name = value
                });
                
                _context.SaveChanges();
            }
        }
    }

    public interface IXraySoftwareRepository
    {
        string[] GetXraySoftwareNames();

        void AddOrUpdate(string value);
    }
}