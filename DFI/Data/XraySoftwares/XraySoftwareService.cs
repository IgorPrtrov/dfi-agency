namespace DFI.Data.XraySoftwares
{
    public class XraySoftwareService : IXraySoftwareService
    {
        private readonly IXraySoftwareRepository _xraySoftwareRepository;

        public XraySoftwareService(IXraySoftwareRepository xraySoftwareRepository)
        {
            _xraySoftwareRepository = xraySoftwareRepository;
        }
        
        public string[] GetXraySoftwareNames()
        {
            return _xraySoftwareRepository.GetXraySoftwareNames();
        }
        
        public void AddOrUpdate(string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value.Trim()))
            {
                return;
            }
            
            _xraySoftwareRepository.AddOrUpdate(value);
        }
    }

    public interface IXraySoftwareService
    {
        string[] GetXraySoftwareNames();

        void AddOrUpdate(string value);
    }
}