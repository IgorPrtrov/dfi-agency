﻿using Microsoft.AspNet.Identity.EntityFramework;


namespace DFI.Data.Roles
{
    public class ApplicationRole : IdentityRole
{
    public ApplicationRole() { }

    public string Description { get; set; }
}
}

