﻿namespace DFI.Data.Roles
{
    public class CreateRoleModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}