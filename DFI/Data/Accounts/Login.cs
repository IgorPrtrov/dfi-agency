﻿using System.ComponentModel.DataAnnotations;

namespace DFI.Data.Accounts
{
    public class Login
    {
        [Required(ErrorMessage = "Required field")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Required field")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
