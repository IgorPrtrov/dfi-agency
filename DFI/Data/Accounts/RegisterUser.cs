﻿using System.ComponentModel.DataAnnotations;

namespace DFI.Data.Accounts
{
    public class RegisterUser
    {
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "The name must be at least 3 characters long (max length 20)")]
        public string Name { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email incorrect")]
        public string Email { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}