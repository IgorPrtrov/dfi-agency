﻿using DFI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace DFI.Data.Accounts
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store){}
        public ApplicationUserManager() : base(new UserStore<ApplicationUser>()){}
    
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            ApplicationDbContext db = context.Get<ApplicationDbContext>();
            ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));

            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,               
                RequireUniqueEmail = true
            };

            var provider = new DpapiDataProtectionProvider(Setting.DpapiDataProtectionProvider);
            if (manager.UserTokenProvider == null)
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create(Setting.DataProtectorTokenProviderAppUser));
            return manager;
        }
    }
}