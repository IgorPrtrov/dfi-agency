﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DFI.Data.Accounts
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser(){ }

        public string CodeToChangePassword { get; set; }
        public int Status { get; set; }
        public string NewEmail { get; set; }
    }

    public class Role
    {
        public const string Admin = "Admin";
        public const string Temp = "Temp";
        public const string Facility = "Facility";
    }
}