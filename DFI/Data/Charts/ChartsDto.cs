﻿using System;

namespace DFI.Data.Charts
{
    public class ChartsDto
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class ProfitAndRequests
    {
        public string Date { get; set; }
        public double Profit { get; set; }
        public int RequestsCount { get; set; }
    }

    public class ChartData
    {
        public string ChartType { get;set; }
        public string[] Labels { get; set; }
        public DataSet[] DataSets { get; set; }
    }

    public class DataSet
    {
        public string Label { get; set; }
        public double[] Data { get; set; }
    }

    public enum ChartTypes
    {
        line,
        bar
    }
}