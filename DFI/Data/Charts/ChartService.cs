﻿using DFI.Data.Requests;
using System;
using System.Linq;

namespace DFI.Data.Charts
{
    public class ChartService : IChartService
    {
        private readonly IRequestRepository _requestRepository;

        public ChartService(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        public ChartData GetProfitChar(DateTime from, DateTime to)
        {
            var result = _requestRepository.GetProfitAndRequestsChart(from, to);

            var chartData = new ChartData()
            {
                ChartType = ChartTypes.line.ToString(),
                Labels = result.Select(c => c.Date).ToArray(),
                DataSets = new DataSet[]
                {
                    new DataSet
                    {
                        Label = "Profits",
                        Data = result.Select(c => c.Profit).ToArray()
                    },
                    new DataSet
                    {
                        Label = "Count Requests",
                        Data = result.Select(c => (double)c.RequestsCount).ToArray()
                    }
                }
            };

            return chartData;
        }

        public ChartData GetRequestsWithStatuses(DateTime from, DateTime to)
        {
            return _requestRepository.GetRequestsStatusForChart(from, to);
        }

        public ChartData GetRequestsForTemps(DateTime from, DateTime to)
        {
            return _requestRepository.GetRequestsForTempsChart(from, to);
        }

        public ChartData GetRequestsForOffices(DateTime from, DateTime to)
        {
            return _requestRepository.GetRequestsForOfficesChart(from, to);
        }
    }

    public interface IChartService
    {
        ChartData GetProfitChar(DateTime from, DateTime to);
        ChartData GetRequestsWithStatuses(DateTime from, DateTime to);
        ChartData GetRequestsForTemps(DateTime from, DateTime to);
        ChartData GetRequestsForOffices(DateTime from, DateTime to);
    }
}