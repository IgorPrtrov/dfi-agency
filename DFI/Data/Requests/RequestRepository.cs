using System;
using System.Collections.Generic;
using System.Linq;
using DFI.Data.Charts;
using DFI.Data.Temps;
using DFI.Helpers;
using DFI.Models;
using static DFI.Helpers.Helper;

namespace DFI.Data.Requests
{
    public class RequestRepository : Repository<Request>, IRequestRepository
    {
        private readonly IApplicationDbContext _context;

        public RequestRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public RequestDay GetRequestDay(int id)
        {
            return _context.RequestDays.FirstOrDefault(c => c.RequestDayId == id);
        }

        public void DeleteRequestDay(RequestDay domain)
        {
            _context.RequestDays.Remove(domain);
        }

        public void UpdateRequestAssignee(RequestAssignee requestAssignee)
        {
            if (requestAssignee.RequestAssigneeId == 0)
            {
                _context.RequestAssignees.Add(requestAssignee);
            }
            else
            {
                _context.SetModified(requestAssignee);
            }
        }

        public List<License> GetLicenses(DateTime? requestEndDate, string position, bool isAutoSearchingWithoutCheckingLicenseExpirationDay)
        {
            if(isAutoSearchingWithoutCheckingLicenseExpirationDay)
            {
                return _context.Licenses.AsQueryable().Where(c =>
                c.LicenseType.ToLower() == position.ToLower() && c.Status == Helper.EState.Active.ToString()).ToList();
            }

            return _context.Licenses.AsQueryable().Where(c =>
                c.LicenseType.ToLower() == position.ToLower() && c.Status == Helper.EState.Active.ToString() &&
                c.ExpiryDate > requestEndDate).ToList();
        }

        public IEnumerable<RequestAssignee> GetFilledRequestAssignees()
        {
            return _context.Requests.Where(x => x.Status == (int) ERequestStatus.Success)
                .SelectMany(x => x.RequestAssignees)
                .Where(x => !x.IsReviewEmailSent && x.Status == (int)ERequestAssigneeStatus.Filled);
        }

        public List<CalendarRequestRepoDto> GetRequestDaysForCalendar(RequestSearchDto dto)
        {
            var domainRequests = _context.Requests.Where(x =>
                x.Status != (int)ERequestStatus.Deleted);

            if (dto.ForFacility)
                domainRequests = domainRequests.Where(c => c.FacilityLocation.Facility.Email == dto.HttpContextUserEmail);

            if (dto.ForTemp)
                domainRequests = domainRequests.Where(c => c.RequestAssignees
                                            .Any(r => r.Temp.Email == dto.HttpContextUserEmail &&
                                                (r.Status == (int)ERequestAssigneeStatus.Filled ))
                                                    || (c.Status == (int)ERequestStatus.NeedsApproval
                                                    && c.RequestAssignees.Any(v => v.Temp.Email == dto.HttpContextUserEmail
                                                        && (v.Status == (int)ERequestAssigneeStatus.WaitingForResponse 
                                                            || v.Status == (int)ERequestAssigneeStatus.If))));

            var requests = domainRequests.Where(x => x.RequestDays.Any());

            if(dto.ForAdmin || dto.ForFacility)
            {
                if(dto.CalendarEvent == ECalendarEvent.Scheduled)
                {
                    requests = requests.Where(c => c.Status == (int)ERequestStatus.Filled || c.Status == (int)ERequestStatus.InProgress);
                }
                else if (dto.CalendarEvent == ECalendarEvent.Open)
                {
                    requests = requests.Where(c => c.Status == (int)ERequestStatus.NeedsApproval);
                }
                else if(dto.CalendarEvent == ECalendarEvent.RecentAndFuture)
                {
                    var twoMonthsAgo = DateTime.UtcNow.AddMonths(-2);
                    requests = requests.Where(c => c.Status != (int)ERequestStatus.Canceled 
                        && c.RequestDays.OrderBy(v => v.StartDate).FirstOrDefault().StartDate >= twoMonthsAgo);
                }
            }

            var calendarRequests = requests.Where(c => c.Status != (int)ERequestStatus.Draft).SelectMany(c => c.RequestDays)
                .Select(domain =>  new CalendarRequestRepoDto
                {
                    FacilityName = domain.Request.FacilityLocation.Facility.FacilityName,
                    TempFirstName = domain.Request.RequestAssignees.FirstOrDefault(v => v.Status == (int)ERequestAssigneeStatus.Filled).Temp.FirstName,
                    TempLastName = domain.Request.RequestAssignees.FirstOrDefault(v => v.Status == (int)ERequestAssigneeStatus.Filled).Temp.LastName,
                    LicenseType = domain.Request.LicenseType,
                    ArrivalTime = domain.ArrivalTime,
                    PatientHoursEnd = domain.PatientHoursEnd,
                    Status = domain.Request.Status,
                    RequestId = domain.Request.RequestId,
                    RequestDayId = domain.RequestDayId
                }).
                    ToList();

            return calendarRequests;
        }


        public List<RequestShortDto> GetRequests(RequestSearchDto dto)
        {
            var domainRequests = _context.Requests.Where(x =>
                x.Status != (int)ERequestStatus.Deleted);

            if (dto.ForFacility)
                domainRequests = domainRequests.Where(c => c.FacilityLocation.Facility.Email == dto.HttpContextUserEmail);

            if (dto.ForTemp)
                domainRequests = domainRequests.Where(c => c.RequestAssignees.Any(r => r.Temp.Email == dto.HttpContextUserEmail &&
                            (r.Status == (int)ERequestAssigneeStatus.Filled))
                                                                || (c.Status == (int)ERequestStatus.NeedsApproval
                                                                && c.RequestAssignees.Any(v => v.Temp.Email == dto.HttpContextUserEmail
                                                                    && (v.Status == (int)ERequestAssigneeStatus.WaitingForResponse || v.Status == (int)ERequestAssigneeStatus.If)))
                                                                    );

            var requests = domainRequests.Where(x => x.RequestDays.Any());

            if (!string.IsNullOrEmpty(dto.FacilityName))
                requests = requests.Where(c => !string.IsNullOrEmpty(c.FacilityLocation.Facility.FacilityName) && c.FacilityLocation.Facility.FacilityName.ToLower().Contains(dto.FacilityName.ToLower()));

            if (dto.Licenses.Any())
                requests = requests.Where(c => !string.IsNullOrEmpty(c.LicenseType)
                                               && dto.Licenses.Contains(c.LicenseType));

            if (dto.RequestStatuses.Any())
                requests = requests.Where(c => dto.RequestStatuses.Contains((int)c.Status));

            var requestDtos = requests.Select(request => new RequestShortDto
            {
                RequestId = request.RequestId,
                FacilityName = request.FacilityLocation.Facility.FacilityName,
                LicenseType = request.LicenseType,
                RequestStart = request.RequestDays.Select(c => c.ArrivalTime).OrderBy(x => x).FirstOrDefault(),
                RequestEnd = request.RequestDays.OrderByDescending(x => x.ArrivalTime).FirstOrDefault().PatientHoursEnd.Value,
                Status = (ERequestStatus)(request.Status ?? 0)
            });

            switch (dto.RequestColumn)
            {
                case ERequestColumn.FacilityName:
                    requestDtos = dto.Order == Helper.EOrder.Asc
                        ? requestDtos.OrderBy(c => c.FacilityName)
                        : requestDtos.OrderByDescending(c => c.FacilityName);
                    break;
                case ERequestColumn.LicenseType:
                    requestDtos = dto.Order == Helper.EOrder.Asc
                        ? requestDtos.OrderBy(c => c.LicenseType)
                        : requestDtos.OrderByDescending(c => c.LicenseType);
                    break;
                case ERequestColumn.RequestStart:
                    requestDtos = dto.Order == Helper.EOrder.Asc
                        ? requestDtos.OrderBy(c => c.RequestStart)
                        : requestDtos.OrderByDescending(c => c.RequestStart);
                    break;
                case ERequestColumn.RequestEnd:
                    requestDtos = dto.Order == Helper.EOrder.Asc
                        ? requestDtos.OrderBy(c => c.RequestEnd)
                        : requestDtos.OrderByDescending(c => c.RequestEnd);
                    break;
                case ERequestColumn.Status:
                    requestDtos = dto.Order == Helper.EOrder.Asc
                        ? requestDtos.OrderBy(c => c.Status.ToString())
                        : requestDtos.OrderByDescending(c => c.Status.ToString());
                    break;
                default:
                    requestDtos = requestDtos.OrderByDescending(c => c.RequestId);
                    break;
            }

            return requestDtos.ToList();
        }

        public List<Request> GetRequestsByStatus(List<ERequestStatus> statuses)
        {
            return _context.Requests.Where(c => statuses.Contains((ERequestStatus)c.Status)).ToList();
        }

        public RequestAssignee GetRequestAssignee(int tempId, string longGuid)
        {
            return _context.RequestAssignees
                .Where(c => c.TempId == tempId && c.Status == (int) ERequestAssigneeStatus.WaitingForResponse || c.Status == (int) ERequestAssigneeStatus.If)
                .FirstOrDefault(c => c.LongGuid.ToLower() == longGuid.ToLower());
        }

        public RequestAssignee GetRequestAssignee(int id)
        {
            return _context.RequestAssignees.FirstOrDefault(c => c.RequestAssigneeId == id);
        }

        public List<RequestAssignee> GetRequestAssignee(int tempId, List<ERequestAssigneeStatus> statuses, List<ERequestStatus> requestStatuses = null)
        {
            if(requestStatuses != null)
            {
                return _context.RequestAssignees.Where(c => c.TempId == tempId 
                                && statuses.Any(b => (int)b == c.Status)
                                && requestStatuses.Any(r => (int)r == c.Request.Status)).ToList();
            }

            return _context.RequestAssignees.Where(c => c.TempId == tempId && statuses.Any(b => (int)b == c.Status)).ToList();
        }

        public List<Request> GetAlreadyExpairedRequests()
        {
            return _context.Requests.ToList().Where(x =>
                x.Status == (int) ERequestStatus.NeedsApproval
                && x.RequestDays.OrderBy(y => y.ArrivalTime).FirstOrDefault().ArrivalTime < DateTime.Now.ToUniversalTime().AddHours(x.FacilityLocation.OffsetHours ?? 0)).ToList();
        }

        public List<Request> GetAlreadyInProgressRequests()
        {
            return _context.Requests.ToList().Where(x =>
                x.Status == (int) ERequestStatus.Filled
                && x.RequestDays.OrderBy(y => y.ArrivalTime).FirstOrDefault().ArrivalTime <= DateTime.Now.ToUniversalTime().AddHours(x.FacilityLocation.OffsetHours ?? 0)).ToList();
        }

        public List<Request> GetAlreadySuccessRequests()
        {
            return _context.Requests.ToList().Where(x =>
                x.Status == (int) ERequestStatus.InProgress
                && x.RequestDays.OrderByDescending(y => y.PatientHoursEnd).FirstOrDefault().PatientHoursEnd < DateTime.Now.ToUniversalTime().AddHours(x.FacilityLocation.OffsetHours ?? 0)).ToList();
        }

        public List<string> GetPositionForRequests()
        {
            return _context.Requests.Where(c => c.Status != (int) ERequestStatus.Deleted).GroupBy(c => c.LicenseType).Select(c => c.Key)
                .Where(c => !string.IsNullOrEmpty(c))
                .ToList();
        }

        public List<Request> GetNeedsApprovalOrFilledRequestsForTemp(int tempId)
        {
            return _context.Requests.Where(c =>
                (c.Status == (int) ERequestStatus.NeedsApproval || c.Status == (int) ERequestStatus.Filled) &&
                c.RequestAssignees.Any(v => v.TempId == tempId)).ToList();
        }

        public Request GetRequestByStatus(int requestId, ERequestStatus status)
        {
            return _context.Requests.AsQueryable().FirstOrDefault(c => c.RequestId == requestId && c.Status == (int)status);
        }

        public List<Request> GetRequestsForSendingReminders(DateTime from, DateTime to)
        {
            var days = _context.RequestDays.Where(c => c.ArrivalTime >= from && c.ArrivalTime < to).ToList();
            var requests = days.Where(c => c.Request.Status == (int)ERequestStatus.Filled).Select(c => c.Request).ToList();

            return requests;
        }

        public IEnumerable<ProfitAndRequests> GetProfitAndRequestsChart(DateTime from, DateTime to)
        {
            var requests = _context.Requests.AsQueryable().Where(c => c.Status == (int)ERequestStatus.Success
                                        && c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate >= from 
                                        && c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate <= to)
                .Select(c => new
                {
                    Date = c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate,
                    Profit = c.PriceForOffice - c.PriceForTemp
                }).ToList();

            var profit = requests.GroupBy(c => new DateTime(c.Date.Year, c.Date.Month, 1))
            .OrderBy(c => c.Key)
                .Select(c => new ProfitAndRequests
                {
                    Date = c.Key.ToString("MM-yyyy"),
                    Profit = c.Select(b => b.Profit.Value).Sum(),
                    RequestsCount = c.Count()
                });

            return profit;
        }

        public ChartData GetRequestsStatusForChart(DateTime from, DateTime to)
        {
            var requests = _context.Requests.AsQueryable().Where(c =>
                                        c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate >= from
                                        && c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate <= to)
                .Select(c => c.Status).ToList().GroupBy(c => ((ERequestStatus)c).GetDisplayName()).OrderByDescending(c => c.Count());

            var chartData = new ChartData()
            {
                ChartType = ChartTypes.bar.ToString(),
                Labels = requests.Select(c => c.Key).ToArray(),
                DataSets = new DataSet[]
                {
                    new DataSet()
                    {
                        Label = "Requests",
                        Data = requests.Select(b => (double)b.Count()).ToArray()
                    } 
                }
            };

            return chartData;
        }

        public ChartData GetRequestsForTempsChart(DateTime from, DateTime to)
        {
            var requests = _context.Requests.AsQueryable().Where(c =>
                    c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate >= from
                    && c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate <= to
                    && c.Status == (int)ERequestStatus.Success)
                .Select(c => new
                {
                    FirstName = c.RequestAssignees.FirstOrDefault(v => v.Status == (int)ERequestAssigneeStatus.Filled).Temp.FirstName,
                    LastName = c.RequestAssignees.FirstOrDefault(v => v.Status == (int)ERequestAssigneeStatus.Filled).Temp.LastName,
                    EarnedMoney = c.RequestAssignees.FirstOrDefault(v => v.Status == (int)ERequestAssigneeStatus.Filled).Request.PriceForTemp
                }).ToList().GroupBy(c => $"{c.FirstName?.Substring(0, 1)}. {c.LastName}").OrderByDescending(c => c.Count());

            var chartData = new ChartData()
            {
                ChartType = ChartTypes.line.ToString(),
                Labels = requests.Select(c => c.Key).ToArray(),
                DataSets = new DataSet[]
                {
                    new DataSet()
                    {
                        Label = "Completed requests",
                        Data = requests.Select(b => (double)b.Count()).ToArray()
                    },
                    new DataSet()
                    {
                        Label = "Earned money",
                        Data = requests.Select(b => (double)b.Sum(c => c.EarnedMoney)).ToArray()
                    }
                }
            };

            return chartData;
        }

        public ChartData GetRequestsForOfficesChart(DateTime from, DateTime to)
        {
            var requests = _context.Requests.AsQueryable().Where(c =>
                    c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate >= from
                    && c.RequestDays.OrderByDescending(v => v.StartDate).FirstOrDefault().StartDate <= to
                    && c.Status == (int)ERequestStatus.Success)
                .Select(c => new
                    {
                        c.FacilityLocation.Facility.FacilityName,
                        c.PriceForOffice
                    }                                  
                ).ToList().GroupBy(c => c.FacilityName).OrderByDescending(c => c.Count());

            var chartData = new ChartData()
            {
                ChartType = ChartTypes.line.ToString(),
                Labels = requests.Select(c => c.Key).ToArray(),
                DataSets = new DataSet[]
                {
                    new DataSet()
                    {
                        Label = "Created requests",
                        Data = requests.Select(b => (double)b.Count()).ToArray()
                    },
                    new DataSet()
                    {
                        Label = "Payed money",
                        Data = requests.Select(b => (double)b.Sum(c => c.PriceForOffice)).ToArray()
                    }
                }
            };

            return chartData;
        }

        public List<RequestAssigneeDto> GetTempsForRequests(GetAssigneesForRequestDto dto)
        {
            IEnumerable<RequestAssigneeDto> requestAssigneeDtos;
            if (dto.ForTemp)
            {
                requestAssigneeDtos = _context.Requests.FirstOrDefault(c => c.RequestId == dto.RequestId).RequestAssignees
                    .Where(c => c.Temp.Email == dto.HttpContextUserEmail)
                    .Select(domain => new RequestAssigneeDto()
                    {
                        Status = (ERequestAssigneeStatus)domain.Status,
                        TempId = domain.TempId,
                        RequestId = domain.RequestId,
                        DistanceToWork = domain.Temp.TempFacilityDistances.FirstOrDefault(c => c.FacilityLocationId == domain.Request.FacilityLocationId)?.Distance,
                        PhoneNumber = domain.Temp.CellPhone.AsPhoneNumber() ?? domain.Temp.HomePhone.AsPhoneNumber(),
                        FullName = $"{domain.Temp.FirstName} {domain.Temp.LastName}",
                        Rating = domain.Temp.ProfessionalReviews.Count == 0 ? 10 : ((domain.Temp.ProfessionalReviews.Average(c => c.Rating) + 10) / 2),
                        RequestAssigneeId = domain.RequestAssigneeId,
                        Email = domain.Temp.Email,
                        RatePerHour = domain.Temp.Licenses.FirstOrDefault(c => c.LicenseType == domain.Request.LicenseType)?.DesiredHourlyRate,
                    });
            }
            else if (dto.ForAdmin && dto.RequestAssigneeId != null && dto.RequestAssigneeId > 0)
            {
                requestAssigneeDtos = _context.Requests.FirstOrDefault(c => c.RequestId == dto.RequestId).RequestAssignees
                    .Where(c => c.RequestAssigneeId == dto.RequestAssigneeId)
                    .Select(domain => new RequestAssigneeDto()
                    {
                        Status = (ERequestAssigneeStatus)domain.Status,
                        TempId = domain.TempId,
                        RequestId = domain.RequestId,
                        DistanceToWork = domain.Temp.TempFacilityDistances.FirstOrDefault(c => c.FacilityLocationId == domain.Request.FacilityLocationId)?.Distance,
                        PhoneNumber = domain.Temp.CellPhone.AsPhoneNumber() ?? domain.Temp.HomePhone.AsPhoneNumber(),
                        FullName = $"{domain.Temp.FirstName} {domain.Temp.LastName}",
                        Rating = domain.Temp.ProfessionalReviews.Count == 0 ? 10 : ((domain.Temp.ProfessionalReviews.Average(c => c.Rating) + 10) / 2),
                        RequestAssigneeId = domain.RequestAssigneeId,
                        Email = domain.Temp.Email,
                        RatePerHour = domain.Temp.Licenses.FirstOrDefault(c => c.LicenseType == domain.Request.LicenseType)?.DesiredHourlyRate,
                    });
            }
            else
            {
                requestAssigneeDtos = _context.Requests.FirstOrDefault(c => c.RequestId == dto.RequestId).RequestAssignees
                 .Select(domain => new RequestAssigneeDto()
                 {
                     Status = (ERequestAssigneeStatus)domain.Status,
                     TempId = domain.TempId,
                     RequestId = domain.RequestId,
                     DistanceToWork = domain.Temp.TempFacilityDistances.FirstOrDefault(c => c.FacilityLocationId == domain.Request.FacilityLocationId)?.Distance,
                     PhoneNumber = domain.Temp.CellPhone.AsPhoneNumber() ?? domain.Temp.HomePhone.AsPhoneNumber(),
                     FullName = $"{domain.Temp.FirstName} {domain.Temp.LastName}",
                     Rating = domain.Temp.ProfessionalReviews.Count == 0 ? 10 : ((domain.Temp.ProfessionalReviews.Average(c => c.Rating) + 10) / 2),
                     RequestAssigneeId = domain.RequestAssigneeId,
                     Email = domain.Temp.Email,
                     RatePerHour = domain.Temp.Licenses.FirstOrDefault(c => c.LicenseType == domain.Request.LicenseType)?.DesiredHourlyRate,
                 }).ToList();
            }

            switch (dto.RequestAssigneeColumn)
            {
                case ERequestAssigneeColumn.DistanceToWork:
                    requestAssigneeDtos = dto.Order == EOrder.Asc
                        ? requestAssigneeDtos.OrderBy(c => c.DistanceToWork)
                        : requestAssigneeDtos.OrderByDescending(c => c.DistanceToWork);
                    break;
                case ERequestAssigneeColumn.Rating:
                    requestAssigneeDtos = dto.Order == EOrder.Asc
                        ? requestAssigneeDtos.OrderBy(c => c.Rating)
                        : requestAssigneeDtos.OrderByDescending(c => c.Rating);
                    break;
                case ERequestAssigneeColumn.Status:
                    requestAssigneeDtos = dto.Order == EOrder.Asc
                        ? requestAssigneeDtos.OrderBy(c => c.Status)
                        : requestAssigneeDtos.OrderByDescending(c => c.Status);
                    break;
            }

            return requestAssigneeDtos.ToList();
        }

        public IdLabel[] GetTempsFullNamesForRequest(int requestId)
        {
            return _context.Requests.FirstOrDefault(c => c.RequestId == requestId).RequestAssignees
                .Select(c => new IdLabel()
                {
                    label = $"{c.Temp.FirstName} {c.Temp.LastName}",
                    id = c.RequestAssigneeId
                }).ToArray();
        }

        public RequestAssigneePriceDto GetRequestAssigneePriceDto(int requestId)
        {
            var request = _context.Requests.FirstOrDefault(c => c.RequestId == requestId);

            return new RequestAssigneePriceDto()
            {
                RequestId = request.RequestId,
                DateCreated = request.DateCreated,
                LicenseType = request.LicenseType,
                RequestStartTime = request.RequestDays.OrderBy(c => c.RequestDayId).FirstOrDefault().ArrivalTime,
                RequestDayAssigneePriceDtos = request.RequestDays.Select(c => new RequestDayAssigneePriceDto()
                {
                    ArrivalTime = c.ArrivalTime,
                    LunchTime = c.LunchTime,
                    PatientHoursEnd = c.PatientHoursEnd
                }).ToList()
            };
        }

        public int? GetRequestStatus(int requestId)
        {
            return _context.Requests.FirstOrDefault(c => c.RequestId == requestId).Status;
        }
    }

    public interface IRequestRepository : IRepository<Request>
    {
        RequestDay GetRequestDay(int id);
        void DeleteRequestDay(RequestDay domain);
        void UpdateRequestAssignee(RequestAssignee requestAssignee);
        List<License> GetLicenses(DateTime? requestEndDate, string position, bool isAutoSearchingWithoutCheckingLicenseExpirationDay);
        IEnumerable<RequestAssignee> GetFilledRequestAssignees();
        List<RequestShortDto> GetRequests(RequestSearchDto dto);
        List<Request> GetRequestsByStatus(List<ERequestStatus> statuses);
        RequestAssignee GetRequestAssignee(int tempId, string longGuid);
        RequestAssignee GetRequestAssignee(int id);
        List<RequestAssignee> GetRequestAssignee(int tempId, List<ERequestAssigneeStatus> statuses, List<ERequestStatus> requestStatuses = null);
        List<Request> GetAlreadyExpairedRequests();
        List<Request> GetAlreadyInProgressRequests();
        List<Request> GetAlreadySuccessRequests();
        List<CalendarRequestRepoDto> GetRequestDaysForCalendar(RequestSearchDto dto);
        List<string> GetPositionForRequests();
        List<Request> GetNeedsApprovalOrFilledRequestsForTemp(int tempId);
        Request GetRequestByStatus(int requestId, ERequestStatus status);
        List<Request> GetRequestsForSendingReminders(DateTime from, DateTime to);
        IEnumerable<ProfitAndRequests> GetProfitAndRequestsChart(DateTime from, DateTime to);
        ChartData GetRequestsStatusForChart(DateTime from, DateTime to);
        ChartData GetRequestsForTempsChart(DateTime from, DateTime to);
        ChartData GetRequestsForOfficesChart(DateTime from, DateTime to);
        List<RequestAssigneeDto> GetTempsForRequests(GetAssigneesForRequestDto dto);
        IdLabel[] GetTempsFullNamesForRequest(int requestId);
        RequestAssigneePriceDto GetRequestAssigneePriceDto(int requestId);
        int? GetRequestStatus(int requestId);
    }
}