using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DFI.Data.Common;
using DFI.Helpers;
using PagedList;

namespace DFI.Data.Requests
{
    public class RequestDto : RequestPriceDto, IValidatableObject
    {
        public RequestDto()
        {
            RequestDays = new List<RequestDayDto>();
            AvailableLicenseTypes = new List<string>();
            Facilities = new List<IdNameDto>();
            LocationDropDownLists = new List<LocationDropDownList>();
        }

        public int? Id { get; set; }

        [Required]
        public string LicenseType { get; set; }

        public ERequestStatus? Status { get; set; }

        public int? FacilityId { get; set; }

        public string FacilityName { get; set; }

        public int? FacilityLocationId { get; set; }

        public string FacilityLocationFullAddress { get; set; }

        public DateTime DateCreated { get; set; }

        public string CreatedBy { get; set; }

        public List<string> AvailableLicenseTypes { get; set; }

        public int SelectedRequestDayIndex { get; set; }
        public string SubmitterEmail { get; set; }
        public bool? SubmitterIsAdmin { get; set; }
        public string Description { get; set; }

        public string PayType { get; set; }
        public string PayTypeDisplay
        {
            get
            {
                return PayType == "1099" ? "1099 (Independent Contractor)" : "W4 (Employee)";
            }
        }

        public string RoleWhoCanceled { get; set; }
        public string EmailWhoCanceled { get; set; }
        public string ReasonOfCancelation { get; set; }
        public string DateOfCancelation { get; set; }

        public IList<RequestDayDto> RequestDays { get; set; }
        public IList<IdNameDto> Facilities { get; set; }
        public int? WhoCompletedRequestTempId { get; set; }

        public List<LocationDropDownList> LocationDropDownLists { get; set; }

        public bool? IsAutoSearching { get; set; }
        public bool? IsAutoSearchingWithoutCheckingLicenseExpirationDay { get; set; }

        // for apply to request from calendar page
        public int? TempId { get; set; }
        public string LongGuid { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (RequestDays.Count >= 2)
            {
                int y = 1;
                for (int i = 0; i < RequestDays.Count && y < RequestDays.Count; i++, y++)
                {
                    if (RequestDays[i].StartDate >= RequestDays[y].StartDate)
                        yield return new ValidationResult(
                            $"The {y + 1} request day must have Start Date more than {i + 1} request day");
                }
            }
            if (!RequestDays.Any())
                yield return new ValidationResult("Must be at least one day");
        }
    }

    public class GetRequestDto : SecurityAccess
    {
        public int Id { get; set; }
        public int? RequestDayIdForCalendar { get; set; }
    }

    public class SecurityAccess
    {
        public bool ForAdmin { get; set; }
        public bool ForTemp { get; set; }
        public bool ForFacility { get; set; }

        public string HttpContextUserEmail { get; set; }
    }

    public enum ERequestStatus
    {
        [Display(Name = "Draft")]
        Draft,
        [Display(Name = "Active")]
        Active,
        [Display(Name = "No Match")]
        NoMatch,
        [Display(Name = "Needs Approval")]
        NeedsApproval,
        [Display(Name = "Expired")]
        Expired,
        [Display(Name = "Filled")]
        Filled,
        [Display(Name = "Success")]
        Success,
        [Display(Name = "Canceled")]
        Canceled,
        [Display(Name = "Deleted")]
        Deleted,
        [Display(Name = "In Progress")]
        InProgress
    }

    public enum ERequestAssigneeStatus
    {
        [Display(Name = "Waiting For Response")]
        WaitingForResponse,
        If,
        Filled,
        [Display(Name = "Too Late")]
        TooLate,
        [Display(Name = "Canceled by professional")]
        CanceledByTemp,
        [Display(Name = "Deleted by admin")]
        Deleted,
        [Display(Name = "Inactive by admin")]
        Inactive
    }

    public class RequestCreateResultDto
    {
        public ValidationStatus ValidationStatus { get; set; }
        public int RequestId { get; set; }
    }

    public class RequestDayDto : IValidatableObject
    {
        public int RequestDayId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public string ArrivalTime { get; set; }

        [Required]
        public TimeWithKind PatientHoursStart { get; set; }

        public string LunchTime { get; set; }

        public string StartDateUi { get; set; }

        public TimeWithKind PatientHoursEnd { get; set; }

        public virtual RequestDto Request { get; set; }
        public int RequestId { get; set; }
        public string Description { get; set; }
        public int? FacilityOffsetHours { get; set; }
        public DateTime OriginalPatientHoursStart { get; set; }

        public bool? MarkByColorForCalendar { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(PatientHoursStart.TimeOption == "12:00" && PatientHoursStart.ETimeKind == ETimeKind.AM)
                yield return new ValidationResult("Patient hours start time should be min from 1:00 AM");

            var startTime = ArrivalTime != null && CommonConstants.ArrivalTimeOptions.ContainsKey(ArrivalTime)
                ? PatientHoursStart.ToDateTime() - CommonConstants.ArrivalTimeOptions[ArrivalTime]
                : PatientHoursStart.ToDateTime();

            startTime = StartDate.AddTicks(startTime.TimeOfDay.Ticks);

            if (startTime < DateTime.Now.ToUniversalTime().AddHours(FacilityOffsetHours ?? 0))
                yield return new ValidationResult("Start Date should be in future");

            if (PatientHoursStart.ToDateTime() == PatientHoursEnd.ToDateTime())
                yield return new ValidationResult("Patient hours start and patient hours end should be different");

            if (PatientHoursStart.ToDateTime() > PatientHoursEnd.ToDateTime())
                yield return new ValidationResult("Patient hours end must be greater than patient hours start");

            if (startTime <= DateTime.Now.ToUniversalTime().AddHours(FacilityOffsetHours ?? 0).AddHours(Setting.MinHoursBeforRequestStart))
                yield return new ValidationResult(
                    $"Request must have start day at least in {Setting.MinHoursBeforRequestStart} hours");
        }
    }

    public class RequestAssigneeListDto
    {
        public ERequestStatus RequestStatus { get; set; }
        public IPagedList<RequestAssigneeDto> RequestAssigneeDtos { get; set; }
    }

    public class RequestAssigneeDto
    {
        public int RequestAssigneeId { get; set; }
        public int RequestId { get; set; }
        public int TempId { get; set; }

        public string FullName { get; set; }
        public ERequestAssigneeStatus Status { get; set; }
        public string PhoneNumber { get; set; }
        public double Rating { get; set; }
        public decimal? DistanceToWork { get; set; }
        public string Email { get; set; }
        public string PriceForRequest { get; set; }
        public int? RatePerHour { get; set; }
    }

    public class RequestSearchDto : SecurityAccess
    {
        public RequestSearchDto()
        {
            RequestStatuses = new List<int>();
            Licenses = new List<string>();
        }

        public Helper.EOrder Order { get; set; }

        public ERequestColumn RequestColumn { get; set; }

        public string FacilityName { get; set; }

        public IList<int> RequestStatuses { get; set; }

        public int? Page { get; set; }

        public List<string> Licenses { get; set; }

        public ECalendarEvent CalendarEvent { get; set; }
    }

    public enum ERequestColumn
    {
        FacilityName,
        LicenseType,
        RequestStart,
        RequestEnd,
        Status
    }

    public enum ERequestAssigneeColumn
    {
        Rating,
        DistanceToWork,
        Status
    }

    public class RequestShortDto
    {
        public int RequestId { get; set; }
        public string FacilityName { get; set; }
        public string LicenseType { get; set; }
        public DateTime RequestStart { get; set; }
        public DateTime RequestEnd { get; set; }
        public ERequestStatus Status { get; set; }
    }

    public class TimeWithKind
    {
        public TimeWithKind()
        {
            this.ETimeKind = ETimeKind.AM;
            this.TimeOption = CommonConstants.TimeOptions.Keys.FirstOrDefault();    
        }

        public TimeWithKind(bool isStartDate)
        {
            if(isStartDate)
            {
                this.ETimeKind = ETimeKind.AM;
                this.TimeOption = CommonConstants.TimeOptions.Keys.FirstOrDefault(c => c == "08:00");
            }
            else
            {
                this.ETimeKind = ETimeKind.PM;
                this.TimeOption = CommonConstants.TimeOptions.Keys.FirstOrDefault(c => c == "05:00");
            }
        }

        public TimeWithKind(DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                this.TimeOption = CommonConstants.TimeOptions.KeyByValue(CommonConstants.BaseDateForTimeOnly.AddHours(dateTime.Value.Hour % 12).AddMinutes(dateTime.Value.Minute));
                this.ETimeKind = dateTime.Value.Hour / 12 > 0 ? ETimeKind.PM : ETimeKind.AM;
            }
        }

        // see CommonConstants.TimeOptions
        public string TimeOption { get; set; }

        public ETimeKind ETimeKind { get; set; }

        public DateTime AsDateTime => CommonConstants.TimeOptions[TimeOption];
    }

    public enum ETimeKind
    {
        AM,
        PM
    }

    public class ApplyTempToRequestDto
    {
        public int TempId { get; set; }
        public string LongGuid { get; set; }
    }

    public class GetAssigneesForRequestDto : SecurityAccess
    {
        public int RequestId { get; set; }
        public Helper.EOrder Order { get; set; }
        public ERequestAssigneeColumn RequestAssigneeColumn { get; set; }
        public int? Page { get; set; }
        public int? RequestAssigneeId { get; set; }
    }

    public class RequestCancelDto
    {
        public int RequestId { get; set; }
        public string RoleWhoCanceled { get; set; }
        public string EmailWhoCanceled { get; set; }
        public string ReasonOfCancelation { get; set; }
    }

    public class RequestForInactiveOrDeletedTemp
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }

    public class RequestPriceDto
    {
        public string PriceForOffice { get; set; }
        public string PriceForTemp { get; set; }
    }

    public class LocationDropDownList
    {
        public int LocationId { get; set; }
        public string LocationFullAddress { get; set; }
    }

    public class ManualFillRequestDto
    {
        public ValidationStatus ValidationStatus { get; set; }
        public int RequestAssigneId { get; set; }

        public ManualFillRequestDto()
        {
            ValidationStatus = new ValidationStatus();
            ValidationStatus.Success = true;
        }

        public ManualFillRequestDto SetErrorMessage(string message)
        {
            ValidationStatus.Messages.Add(message);
            ValidationStatus.Success = false;
            return this;
        }
    }

    public enum ECalendarEvent
    {
        History,
        Scheduled,
        Open,
        RecentAndFuture
    }

    public class CalendarRequestRepoDto
    {
        public string FacilityName { get; set; }
        public string TempFirstName { get; set; }
        public string TempLastName { get; set; }
        public string LicenseType { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime? PatientHoursEnd { get; set; }
        public int? Status { get; set; }
        public int RequestId { get; set; }
        public int RequestDayId { get; set; }
    }

    public class ApplyTempDto 
    {
        public string Email { get; set; }
        public int RequestId { get; set; }
        public int RequestDayId { get; set; }
    }

    public class RequestAssigneePriceDto
    {
        public int RequestId { get; set; }
        public DateTime RequestStartTime { get; set; }
        public string LicenseType { get; set; }
        public DateTime DateCreated { get; set; }

        public List<RequestDayAssigneePriceDto> RequestDayAssigneePriceDtos { get; set; }

        public RequestAssigneePriceDto()
        {
            RequestDayAssigneePriceDtos = new List<RequestDayAssigneePriceDto>();
        }
    }

    public class RequestDayAssigneePriceDto
    {
        public DateTime ArrivalTime { get; set; }
        public DateTime? PatientHoursEnd { get; set; }
        public string LunchTime { get; set; }
    }
}