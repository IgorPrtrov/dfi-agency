using System;
using System.Linq;
using DFI.Data.Common;
using DFI.Data.Facilities;
using DFI.Data.ProfessionalReviews;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Requests
{
    public class RequestMapper : IRequestMapper
    {
        public Request MapToRequest(RequestDto dto)
        {
            var request = new Request
            {
                RequestId = dto.Id ?? 0,
                LicenseType = dto.LicenseType,
                FacilityLocationId = dto.FacilityLocationId ?? 0,
                DateCreated = DateTime.UtcNow,
                CreatedBy = dto.CreatedBy,
                RequestDays = dto.RequestDays.Select(MapToRequestDay).ToList(),
                Description = dto.Description,
                Status = (int)(dto.Status ?? ERequestStatus.Draft),
                IsAutoSearching = dto.IsAutoSearching == true,
                IsAutoSearchingWithoutCheckingLicenseExpirationDay = dto.IsAutoSearchingWithoutCheckingLicenseExpirationDay == true
            };

            return request;
        }

        public Request MapToRequest(Request domain, RequestDto dto)
        {
            domain.RequestId = dto.Id ?? 0;
            domain.LicenseType = dto.LicenseType;
            domain.FacilityLocationId = dto.FacilityLocationId;
            domain.DateCreated = DateTime.UtcNow;
            domain.CreatedBy = dto.CreatedBy;
            domain.Description = dto.Description;

            if(domain.Status == (int)ERequestStatus.Draft && dto.IsAutoSearching != null)
            {
                domain.IsAutoSearching = dto.IsAutoSearching ?? false;
            }

            if (domain.Status == (int)ERequestStatus.Draft && dto.IsAutoSearchingWithoutCheckingLicenseExpirationDay != null)
            {
                domain.IsAutoSearchingWithoutCheckingLicenseExpirationDay = dto.IsAutoSearchingWithoutCheckingLicenseExpirationDay ?? false;
            }

            domain.Status = (int) (dto.Status ?? (ERequestStatus?)domain.Status ?? ERequestStatus.Draft);

            foreach (var requestDay in dto.RequestDays)
            {
                domain.RequestDays.Add(MapToRequestDay(requestDay));
            }

            return domain;
        }

        public RequestDto MapToRequestDto(Request domain)
        {
            var dto = new RequestDto
            {
                Id = domain.RequestId,
                LicenseType = domain.LicenseType,
                FacilityId = domain.FacilityLocation.FacilityId,
                DateCreated = domain.DateCreated,
                CreatedBy = domain.CreatedBy,
                RequestDays = domain.RequestDays.Select(MapToRequestDayDto).OrderBy(c => c.OriginalPatientHoursStart).ToList(),
                Description = domain.Description,
                PayType = domain.FacilityLocation.Facility.PayType,
                Status = (ERequestStatus?)domain.Status,
                FacilityName = domain.FacilityLocation.Facility.FacilityName,
                FacilityLocationId = domain.FacilityLocationId,
                FacilityLocationFullAddress = domain.FacilityLocation.FullAddress,
                RoleWhoCanceled = domain.RoleWhoCanceled,
                EmailWhoCanceled = domain.EmailWhoCanceled,
                ReasonOfCancelation = domain.ReasonOfCancelation,
                DateOfCancelation = domain.DateOfCancelation?.AddHours(domain.FacilityLocation.OffsetHours ?? 0).ToString("f"),
                WhoCompletedRequestTempId = domain.Status == (int)ERequestStatus.Success 
                    ? domain.RequestAssignees.FirstOrDefault(c => c.Status == (int)ERequestAssigneeStatus.Filled)?.TempId 
                    : null,
                PriceForOffice = domain.PriceForOffice?.GetTwoDecimalPlacesStringOrNa(),
                PriceForTemp = domain.PriceForTemp?.GetTwoDecimalPlacesStringOrNa(),
                LocationDropDownLists = domain.FacilityLocation.Facility.FacilityLocations.Where(c => c.LocationStatus == (int)EFacilityLocationStatus.Active)
                                .Select(c => new LocationDropDownList() 
                                { 
                                    LocationId = c.FacilityLocationId, 
                                    LocationFullAddress = c.FullAddress
                                }).ToList(),
                IsAutoSearching = domain.IsAutoSearching,
                IsAutoSearchingWithoutCheckingLicenseExpirationDay = domain.IsAutoSearchingWithoutCheckingLicenseExpirationDay
            };

            return dto;
        }

        public RequestDay MapToRequestDay(RequestDayDto dto)
        {
            var requestDay = new RequestDay
            {
                RequestDayId = dto.RequestDayId,
                StartDate = dto.StartDate,
                ArrivalTime = dto.ArrivalTime != null && CommonConstants.ArrivalTimeOptions.ContainsKey(dto.ArrivalTime)
                    ? dto.PatientHoursStart.ToDateTime() - CommonConstants.ArrivalTimeOptions[dto.ArrivalTime]
                    : dto.PatientHoursStart.ToDateTime() ,
                PatientHoursStart = dto.PatientHoursStart.ToDateTime(),
                PatientHoursEnd = dto.PatientHoursEnd.ToDateTime(),
                LunchTime = dto.LunchTime,
                RequestId = dto.RequestId,
                Description = dto.Description
            };

            return requestDay;
        }

        public RequestDay MapToRequestDay(RequestDay domain, RequestDayDto dto)
        {
            domain.StartDate = dto.StartDate;
            domain.ArrivalTime = dto.ArrivalTime != null && CommonConstants.ArrivalTimeOptions.ContainsKey(dto.ArrivalTime)
                 ? dto.PatientHoursStart.ToDateTime() - CommonConstants.ArrivalTimeOptions[dto.ArrivalTime]
                 : dto.PatientHoursStart.ToDateTime();
            domain.PatientHoursStart = dto.PatientHoursStart.ToDateTime();
            domain.PatientHoursEnd = dto.PatientHoursEnd.ToDateTime();
            domain.LunchTime = dto.LunchTime;
            domain.Description = dto.Description;

            return domain;
        }

        public RequestDayDto MapToRequestDayDto(RequestDay domain)
        {
            var dto = new RequestDayDto
            {
                RequestDayId = domain.RequestDayId,
                StartDate = domain.StartDate,
                ArrivalTime = CommonConstants.ArrivalTimeOptions.KeyByValue(domain.PatientHoursStart - domain.ArrivalTime),
                PatientHoursStart = new TimeWithKind(domain.PatientHoursStart),
                PatientHoursEnd = new TimeWithKind(domain.PatientHoursEnd),
                LunchTime = domain.LunchTime,
                RequestId = domain.RequestId,
                Description = domain.Description,
                StartDateUi = domain.StartDate.ToString("D"),
                OriginalPatientHoursStart = domain.PatientHoursStart
            };

            return dto;
        }

        public RequestDayDto MapToEmptyRequestDayDto(int? requestId, DateTime startDay)
        {
            return new RequestDayDto()
            {
                StartDate = startDay,
                ArrivalTime = CommonConstants.ArrivalTimeOptions.KeyByValue(CommonConstants.ArrivalTimeOptions.Values.First()),
                PatientHoursStart = new TimeWithKind(isStartDate: true),
                LunchTime = CommonConstants.LunchTimeOptions.Skip(1).First(),
                PatientHoursEnd = new TimeWithKind(isStartDate: false),
                RequestId = requestId ?? 0
            };
        }

        public IdNameDto MapToIdNameDto(Facility domain)
        {
            return new IdNameDto()
            {
                Id = domain.FacilityId,
                Name = domain.FacilityName
            };
        }

        public ProfessionalReviewEmailModel MapToProfessionalReviewEmailModel(RequestAssignee requestAssignee)
        {
            return new ProfessionalReviewEmailModel
            {
                Email = requestAssignee.Request.FacilityLocation.Facility.Email,
                PracticeName = requestAssignee.Request.FacilityLocation.Facility.FacilityName,
                TempId = requestAssignee.TempId,
                RequestId = requestAssignee.RequestId,
                RecentTempName = $"{requestAssignee.Temp.FirstName} {requestAssignee.Temp.LastName}"
            };
        }

        public Request MapToRequest(RequestCancelDto dto, Request domain)
        {
            domain.RoleWhoCanceled = dto.RoleWhoCanceled;
            domain.DateOfCancelation = DateTime.Now.ToUniversalTime();
            domain.EmailWhoCanceled = dto.EmailWhoCanceled;
            domain.ReasonOfCancelation = dto.ReasonOfCancelation;
            domain.Status = (int) ERequestStatus.Canceled;
            return domain;
        }

        public RequestCalendarEvent MapToCalendarEvent(CalendarRequestRepoDto domain)
        {
            var officeName = domain.FacilityName;
            var tempFullName = string.Empty;
            if(!string.IsNullOrEmpty(domain.TempFirstName) && !string.IsNullOrEmpty(domain.TempLastName))
            {
                tempFullName = "\n" + domain.TempFirstName + " " + domain.TempLastName;
            }

            return new RequestCalendarEvent
            {
                Title = $"{officeName}{tempFullName}\n{MapLicenseTypeForCalendarView(domain.LicenseType)}",
                Start = domain.ArrivalTime.ToString("s"),
                End = domain.PatientHoursEnd?.ToString("s"),
                Color = MapRequestStatusToColor((ERequestStatus)domain.Status),
                Id = domain.RequestId.ToString(),
                RequestDayId = domain.RequestDayId.ToString()
            };
        }

        private string MapLicenseTypeForCalendarView(string licenseType)
        {
            if(licenseType == "Dental Hygienist")
            {
                return "RDH";
            }
            if(licenseType.Contains("Dental Assistant"))
            {
                return "DA";
            }
            if (licenseType.Contains("Receptionist"))
            {
                return "Rec";
            }
            if (licenseType.Contains("Dentist"))
            {
                return "DMD";
            }
            if(licenseType.Contains("Office Manager"))
            {
                return "OM";
            }

            return licenseType;
        }

        public string MapRequestStatusToColor(ERequestStatus status)
        {
            switch (status)
            {
                case ERequestStatus.Draft:
                    return "#6c757d";
                case ERequestStatus.Active:
                    return "#ffc107";
                case ERequestStatus.Success:
                    return "#007bff";
                case ERequestStatus.Canceled:
                    return "#dc3545";
                case ERequestStatus.Expired:
                    return "#ff00ff";
                case ERequestStatus.Filled:
                    return "#007bff";
                case ERequestStatus.NeedsApproval:
                    return "#28a745";
                case ERequestStatus.Deleted:
                    return "#dc3545";
                case ERequestStatus.InProgress:
                    return "#007bff";
                case ERequestStatus.NoMatch:
                    return "#ff8c00";
                default:
                    return "#6c757d";
            }
        }

        public RequestForInactiveOrDeletedTemp MapToRequestForInactiveOrDeletedTemp(Request domain)
        {
            return new RequestForInactiveOrDeletedTemp()
            {
                Id = domain.RequestId,
                Status = ((ERequestStatus)domain.Status).GetDisplayName()
            };
        }
    }

    public interface IRequestMapper
    {
        Request MapToRequest(RequestDto dto);
        Request MapToRequest(Request domain, RequestDto dto);
        RequestDto MapToRequestDto(Request domain);
        RequestDay MapToRequestDay(RequestDayDto dto);
        RequestDay MapToRequestDay(RequestDay domain, RequestDayDto dto);
        RequestDayDto MapToRequestDayDto(RequestDay domain);
        RequestDayDto MapToEmptyRequestDayDto(int? requestId, DateTime startDay);
        IdNameDto MapToIdNameDto(Facility domain);
        ProfessionalReviewEmailModel MapToProfessionalReviewEmailModel(RequestAssignee requestAssignee);
        Request MapToRequest(RequestCancelDto dto, Request domain);
        RequestCalendarEvent MapToCalendarEvent(CalendarRequestRepoDto domain);
        RequestForInactiveOrDeletedTemp MapToRequestForInactiveOrDeletedTemp(Request domain);
    }
}