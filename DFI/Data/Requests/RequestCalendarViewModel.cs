namespace DFI.Data.Requests
{
    public class RequestCalendarViewModel
    {
        public string SerializedEventsData { get; set; }
    }
}