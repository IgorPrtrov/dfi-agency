using Newtonsoft.Json;

namespace DFI.Data.Requests
{
    public class RequestCalendarEvent
    {
        [JsonProperty("requestdayid")]
        public string RequestDayId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("start")]
        public string Start { get; set; }

        [JsonProperty("end")]
        public string End { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }
}