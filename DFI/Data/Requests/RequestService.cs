using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DFI.Data.Accounts;
using DFI.Data.Emails;
using DFI.Data.Facilities;
using DFI.Data.GoogleMaps;
using DFI.Data.ProfessionalReviews;
using DFI.Data.Temps;
using DFI.Data.TextMessages;
using DFI.Helpers;
using DFI.Models;
using PagedList;
using WebGrease.Css.Extensions;
using static DFI.Helpers.Helper;

namespace DFI.Data.Requests
{
    public class RequestService : IRequestService
    {
        private readonly IRequestRepository _requestRepository;
        private readonly ITempRepository _tempRepository;
        private readonly IRequestMapper _requestMapper;
        private readonly IFacilityRepository _facilityRepository;
        private readonly ILogger _logger;
        private readonly IGoogleMapService _googleMapService;
        private readonly IEmailService _emailService;
        private readonly ITextMessageService _textMessageService;

        public RequestService(
            IRequestRepository requestRepository,
            IRequestMapper requestMapper,
            ITempRepository tempRepository,
            IFacilityRepository facilityRepository, IEmailService emailService,
            ILogger logger, IGoogleMapService googleMapService, ITextMessageService textMessageService)
        {
            _requestRepository = requestRepository;
            _requestMapper = requestMapper;
            _tempRepository = tempRepository;
            _facilityRepository = facilityRepository;
            _logger = logger;
            _googleMapService = googleMapService;
            _emailService = emailService;
            _textMessageService = textMessageService;
        }

        public RequestDto GetRequest(GetRequestDto dto)
        {
            _logger.Info($"GetRequest:: Start. id = {dto.Id}");

            var domain = _requestRepository.GetById(dto.Id);

            if (domain == null || domain.Status == (int)ERequestStatus.Deleted)
            {
                _logger.Error($"GetRequest:: Request with {dto.Id} is not found");
                throw new Exception($"Request with {dto.Id} is not found");
            }

            if (dto.ForFacility && domain.FacilityLocation.Facility.Email != dto.HttpContextUserEmail)
            {
                _logger.Error($"GetRequest:: Request with {dto.Id} is unavailable for current facility");
                throw new Exception($"Request with {dto.Id} is unavailable for current facility");
            }

            int? tempId = 0;
            var guidId = "";

            if (dto.ForTemp)
            {

                var val1 = domain.RequestAssignees.Any(r => r.Temp.Email == dto.HttpContextUserEmail &&
                                                                                                (r.Status == (int)ERequestAssigneeStatus.Filled));

                var val2 = domain.Status == (int)ERequestStatus.NeedsApproval && domain.RequestAssignees.Any(v => v.Temp.Email == dto.HttpContextUserEmail
                                                                                                && (v.Status == (int)ERequestAssigneeStatus.WaitingForResponse 
                                                                                                || v.Status == (int)ERequestAssigneeStatus.If));
                                                                                                
                if (!(val1 || val2))
                {
                    _logger.Error($"GetRequest:: Request with {dto.Id} is unavailable for current professional");
                    throw new Exception($"Request with {dto.Id} is unavailable for current professional");
                }

                var tempAssignee = domain.RequestAssignees.FirstOrDefault(c => c.Temp.Email == dto.HttpContextUserEmail);

                if(domain.Status == (int)ERequestStatus.NeedsApproval && domain.RequestAssignees.Any(v => v.Temp.Email == dto.HttpContextUserEmail
                                                                                                && (v.Status == (int)ERequestAssigneeStatus.WaitingForResponse)))
                {
                    tempId = tempAssignee?.TempId;
                    guidId = tempAssignee?.LongGuid;
                }
            }

            var resultDto = _requestMapper.MapToRequestDto(domain);
            if (dto.ForAdmin == true)
            {
                var facilities = _facilityRepository.GetActiveFacilities();
                resultDto.Facilities = facilities.Select(c => _requestMapper.MapToIdNameDto(c)).ToList();
            }

            if(dto.ForTemp && tempId > 0 && guidId != "")
            {
                resultDto.TempId = tempId;
                resultDto.LongGuid = guidId;
            }

            AddLicensesToRequest(resultDto);

            if (dto.RequestDayIdForCalendar != null)
            {
                var requestDay = resultDto.RequestDays.FirstOrDefault(c => c.RequestDayId == dto.RequestDayIdForCalendar);
                if (requestDay != null)
                    requestDay.MarkByColorForCalendar = true;
            }

            return resultDto;
        }
        public RequestAssigneeListDto GetRequestAssigneeList(GetAssigneesForRequestDto dto)
        {
            var requestAssigneeDtos = _requestRepository.GetTempsForRequests(dto);

            _logger.Info($"{nameof(GetRequestAssigneeList)}:: Assignee Ids {string.Join(", ", requestAssigneeDtos.Select(c => c.RequestAssigneeId))}");

            int pageSize = 10;
            int pageNumber = dto.Page ?? 1;
            int skip = pageNumber * pageSize - 10;

            var requestAssigneeDtosList = requestAssigneeDtos.ToList();
            var requestAssigneePriceDto = _requestRepository.GetRequestAssigneePriceDto(dto.RequestId);

            foreach (var requestAssigneeDto in requestAssigneeDtosList.Skip(skip).Take(pageSize))
            {
                requestAssigneeDto.PriceForRequest = GetRequestAssignePrice(requestAssigneeDto.RatePerHour.Value, requestAssigneePriceDto, true);
            }

            return new RequestAssigneeListDto()
            {
                RequestStatus = (ERequestStatus)_requestRepository.GetRequestStatus(dto.RequestId),
                RequestAssigneeDtos = requestAssigneeDtosList.ToPagedList(pageNumber, pageSize)
            };
        }

        public ValidationStatus FillRequest(int requestAssigneeId)
        {
            _logger.Info($"{nameof(FillRequest)}:: requestAssigneeId = {requestAssigneeId}");
            var result = new ValidationStatus();

            var requestAssignee = _requestRepository.GetRequestAssignee(requestAssigneeId);

            if(requestAssignee == null)
                throw new Exception($"{nameof(FillRequest)}:: Request Assignee is null with {requestAssigneeId}");

            if (requestAssignee.Request.Status != (int) ERequestStatus.NeedsApproval && requestAssignee.Request.Status != (int)ERequestStatus.Filled)
            {
                result.Success = false;
                result.Messages.Add("Request doesn't have Needs Approval or Filled status");
                return result;
            }

            var resultVarification = GetFilledRequestsOnTheSameTime(requestAssignee);
            if (resultVarification.Success == false)
                return resultVarification;

            var alreadyFilledAssignees = requestAssignee.Request.RequestAssignees
                .Where(c => c.Status == (int) ERequestAssigneeStatus.Filled).ToList();

            foreach (var alreadyFilledAssignee in alreadyFilledAssignees)
            {
                alreadyFilledAssignee.Status = (int) ERequestAssigneeStatus.If;

                _logger.Info($"FillRequest:: alreadyFilledAssignee temp id = {alreadyFilledAssignee.Temp.TempId}");
                _requestRepository.SaveChanges();

                _emailService.AddEmail(new EmailDto()
                {
                    To = alreadyFilledAssignee.Temp.Email,
                    Subject = EmailConstants.Cancellation,
                    Body = string.Format(
                        EmailConstants.FillRequestCancellationBody,
                        Helper.GetRequestUrl(requestAssignee.Request.RequestId),
                        Helper.GetContactUsUrl())
                });
            }

            requestAssignee.Status = (int) ERequestAssigneeStatus.Filled;
            requestAssignee.Request.Status = (int) ERequestStatus.Filled;
            requestAssignee.ChagnedBy = Role.Admin.ToString();
            requestAssignee.DateChanged = DateTime.Now.ToUniversalTime();

            var tempRate = requestAssignee.Temp.Licenses
                                   .FirstOrDefault(c => c.LicenseType == requestAssignee.Request.LicenseType)?.DesiredHourlyRate;
            var requestAssigneePriceDto = _requestRepository.GetRequestAssigneePriceDto(requestAssignee.Request.RequestId);

            requestAssignee.Request.PriceForOffice = double.Parse(GetRequestAssignePrice(tempRate.Value, requestAssigneePriceDto, true));
            requestAssignee.Request.PriceForTemp = double.Parse(GetRequestAssignePrice(tempRate.Value, requestAssigneePriceDto, false));

            _logger.Info($"FillRequest:: request id = {requestAssignee.Request.RequestId}");

            _requestRepository.Update(requestAssignee.Request);
            _requestRepository.SaveChanges();

            var firstRequestDay = requestAssignee.Request.RequestDays.OrderBy(c => c.RequestDayId).FirstOrDefault()?.ArrivalTime;

            _emailService.AddEmail(new EmailDto()
            {
                To = requestAssignee.Temp.Email,
                Subject = EmailConstants.ConfirmationSubject,
                Body = string.Format(
                    EmailConstants.FillRequestTempBody, requestAssignee.Request.FacilityLocation.Facility.FacilityName, firstRequestDay,
                    Helper.GetRequestUrl(requestAssignee.Request.RequestId),
                    Helper.GetContactUsUrl())
            });

            _emailService.AddEmail(new EmailDto()
            {
                To = requestAssignee.Request.FacilityLocation.Facility.Email,
                Subject = EmailConstants.ConfirmationSubject,
                Body = string.Format(
                    EmailConstants.FillRequestFacilityBody,
                    Helper.GetRequestUrl(requestAssignee.Request.RequestId),
                    Helper.GetContactUsUrl())
            });

            SendToTempFacilityAddress(requestAssignee.Request);

            return result;
        }

        private ValidationStatus GetFilledRequestsOnTheSameTime(RequestAssignee requestAssignee)
        {
            var result = new ValidationStatus();

            var allRequestAssignees =
                _requestRepository.GetRequestAssignee(requestAssignee.TempId, 
                                new List<ERequestAssigneeStatus>() { ERequestAssigneeStatus.Filled },
                                new List<ERequestStatus>() { ERequestStatus.Filled, ERequestStatus.InProgress });

            if (!allRequestAssignees.Any())
                return result;

            var currentFirstRequestDay = requestAssignee.Request.RequestDays.OrderBy(c => c.RequestDayId).FirstOrDefault()?.ArrivalTime;
            var currentLastRequestDay = requestAssignee.Request.RequestDays.OrderByDescending(c => c.RequestDayId).FirstOrDefault()?.StartDate;
            if (currentFirstRequestDay == null || currentLastRequestDay == null)
            {
                result.Success = false;
                result.Messages.Add($"This temp can't be assigned on the request with first day or last day is null");//fix text
                return result;
            }

            foreach (var request in allRequestAssignees.Select(c => c.Request).ToList())
            {
                var resultOfChecking = HasRequestsDaysOverlap(request, requestAssignee.Request);

                if(resultOfChecking.Success == false)
                {
                    result.Success = resultOfChecking.Success;
                    resultOfChecking.Messages.ForEach(c => result.Messages.Add(c));
                }
            }

            return result;
        }

        private ValidationStatus HasRequestsDaysOverlap(Request firstRequest, Request secondRequeset)
        {
            var result = new ValidationStatus() { Success = true };

            foreach(var dayForFirstRequest in firstRequest.RequestDays.ToList())
            {
                foreach(var dayForSecondRequest in secondRequeset.RequestDays.ToList())
                {
                    if(dayForFirstRequest.ArrivalTime >= dayForSecondRequest.ArrivalTime && dayForFirstRequest.ArrivalTime <= dayForSecondRequest.PatientHoursEnd
                        ||
                       dayForFirstRequest.PatientHoursEnd >= dayForSecondRequest.ArrivalTime && dayForFirstRequest.PatientHoursEnd <= dayForSecondRequest.PatientHoursEnd)
                    {
                        // +1 because start from 0
                        result.Success = false;
                        result.Messages.Add($"{firstRequest.RequestDays.ToList().IndexOf(dayForFirstRequest) + 1} day from the request ({firstRequest.RequestId}) " +
                            $"has overlap with {secondRequeset.RequestDays.ToList().IndexOf(dayForSecondRequest) + 1} day from the request ({secondRequeset.RequestId})");
                    }
                }
            }

            return result;
        }

        public ValidationStatus UpdateRequest(RequestDto dto)
        {
            _logger.Info($"UpdateRequest:: Start. Update request {dto.Id}.");

            int? facilityOffsetHours = 0;

            if (dto.SubmitterIsAdmin == true)
            {
                var activeFacilities = _facilityRepository.GetFacilityNamesForCreateEditRequest();

                if (!activeFacilities.Any(c => c.id == dto.FacilityId))
                {
                    return new ValidationStatus()
                    {
                        Success = false, 
                        Messages = new List<string>() { "Facility name does not exists or does not have Active status" }
                    };
                }

                facilityOffsetHours = _facilityRepository.GetByIdFacilityLocation(dto.FacilityLocationId.Value)?.OffsetHours;
            }
            else
            {
                var facility = _facilityRepository.GetByEmail(dto.SubmitterEmail);

                if (facility?.Status != (int) Helper.EState.Active)
                {
                    return new ValidationStatus()
                    {
                        Success = false,
                        Messages = new List<string>() { facility == null ? "Facility is null" : "Request cannot be updated because facility does not have Active status." }
                    };
                }

                facilityOffsetHours = _facilityRepository.GetByIdFacilityLocation(dto.FacilityLocationId.Value)?.OffsetHours;
                dto.FacilityId = facility.FacilityId;
            }

            dto.RequestDays.ForEach(c => c.FacilityOffsetHours = facilityOffsetHours);

            var validationStatus = Helper.ValidateDto(dto);

            if (!validationStatus.Success)
                return validationStatus;

            foreach (var dtoRequestDay in dto.RequestDays)
            {
                validationStatus = Helper.ValidateDto(dtoRequestDay);
                if (!validationStatus.Success)
                    return validationStatus;
            }

            if (!dto.Id.HasValue || dto.Id == 0)
                validationStatus.Messages.Add(CreateRequestCore(dto).ToString());
            else
                validationStatus.Messages.Add(UpdateRequestCore(dto).ToString());

            return validationStatus;
        }

        public ValidationStatus GetAccessToCreateRequest(SecurityAccess access)
        {
            var result = new ValidationStatus(){Success = true};

            if (access.ForFacility)
            {
                var facility = _facilityRepository.GetByEmail(access.HttpContextUserEmail);
                if (facility?.Status != (int)Helper.EState.Active)
                {
                    result.Success = false;
                    result.Messages = new List<string>(){facility == null ? "Facility is null" : "Facility does not have access to create Request because it does not have Active status"};
                }
            }

            return result;
        }

        public RequestDto GetCreateModel(SecurityAccess access)
        {
            _logger.Info($"GetCreateModel:: Start.");

            var request = new RequestDto();
            var facilities = _facilityRepository.GetActiveFacilities();
            request.Facilities = facilities.Select(c => _requestMapper.MapToIdNameDto(c)).ToList();
            _logger.Info($"GetCreateModel:: Facilities count = {request.Facilities.Count}");

            if (access.ForFacility)
            {
                var facility = facilities.FirstOrDefault(c => c.Email == access.HttpContextUserEmail);
                request.FacilityId = facility?.FacilityId;
                request.FacilityLocationId = facility.FacilityLocations.FirstOrDefault().FacilityLocationId;
                request.LocationDropDownLists = facility.FacilityLocations.Where(c => c.LocationStatus == (int)EFacilityLocationStatus.Active)
                                .Select(c => new LocationDropDownList()
                                {
                                    LocationId = c.FacilityLocationId,
                                    LocationFullAddress = c.FullAddress
                                }).ToList();

                request.Facilities = request.Facilities.Where(c => c.Id == facility.FacilityId).ToList();

            }

            AddLicensesToRequest(request);
            request.RequestDays.Add(_requestMapper.MapToEmptyRequestDayDto(request.Id, DateTime.Today));

            return request;
        }

        public int AddEmptyDay(RequestDto request)
        {
            _logger.Info($"AddEmptyDay:: Add empty day for request id = {request.Id}.");

            var startDay = request.RequestDays.OrderByDescending(c => c.StartDate).FirstOrDefault().StartDate.AddDays(1);

            var dayToAdd = _requestMapper.MapToEmptyRequestDayDto(request.Id, startDay);
            dayToAdd.RequestDayId = 0;

            request.RequestDays.Add(dayToAdd);
            request.Status = ERequestStatus.Draft;

            if (request.Id.HasValue && request.Id != null)
                return UpdateRequestCore(request);
            else
                return CreateRequestCore(request);
        }

        public int AddEmptyDayWithSameHours(RequestDto request)
        {
            _logger.Info($"AddEmptyDayWithSameHours:: Add empty day with the same hours for request id = {request.Id}.");

            var dayToAdd = request.RequestDays[request.SelectedRequestDayIndex];
            dayToAdd.RequestDayId = 0;

            request.RequestDays.Add(dayToAdd);
            
            // need to add new day after specific day
            if(request.RequestDays.Count > 2 && request.SelectedRequestDayIndex != request.RequestDays.Count - 2)
            {
                var temp = request.RequestDays[request.SelectedRequestDayIndex + 1];
                request.RequestDays[request.SelectedRequestDayIndex + 1] = request.RequestDays.Last();
                request.RequestDays[request.RequestDays.Count - 1] = temp;
            }

            request.Status = ERequestStatus.Draft;

            if (request.Id.HasValue && request.Id != null)
                return UpdateRequestCore(request);
            else
                return CreateRequestCore(request);
        }

        public void RemoveRequestDay(int? id)
        {
            _logger.Info($"RemoveRequestDay:: Remove request day with id = {id}.");

            var requestDay = _requestRepository.GetRequestDay(id ?? 0);
            if(requestDay == null)
                throw new Exception($"RemoveRequestDay:: requestDay is null with id = {id}");

            _requestRepository.DeleteRequestDay(requestDay);
            _requestRepository.SaveChanges();
        }

        public void RemoveRequest(int? id)
        {
            _logger.Info($"RemoveRequest:: Remove request with id = {id}.");

            var request = _requestRepository.GetById(id ?? 0);
            if(request == null)
                throw new Exception($"RemoveRequest:: request is null with id = {id}");

            request.Status = (int)ERequestStatus.Deleted;
            _requestRepository.Update(request);
            _requestRepository.SaveChanges();
        }

        public IPagedList<RequestShortDto> GetRequests(RequestSearchDto dto)
        {
            var result = _requestRepository.GetRequests(dto);

            int pageSize = 10;
            int pageNumber = (dto.Page ?? 1);

            return result.ToPagedList(pageNumber, pageSize);
        }

        public IList<RequestCalendarEvent> GetRequestsAsCalendarEvents(RequestSearchDto dto)
        {
            var requestBag = new ConcurrentBag<RequestCalendarEvent>();

            var result = _requestRepository.GetRequestDaysForCalendar(dto);

            Parallel.ForEach(result, request =>
            {
                requestBag.Add(_requestMapper.MapToCalendarEvent(request));
            });
                    
            return requestBag.ToList();
        }

        private IList<ProfessionalReviewEmailModel> GetFilledProfessionalReviewRequests()
        {
            var pendingRequestAssignees = _requestRepository.GetFilledRequestAssignees().ToList();

            return pendingRequestAssignees.Select(c => _requestMapper.MapToProfessionalReviewEmailModel(c)).ToList();
        }

        private void MarkProfessionalReviewEmailsAsSent(IList<ProfessionalReviewEmailModel> professionalReviewEmailModels)
        {
            _logger.Info($"MarkProfessionalReviewEmailsAsSent:: professionalReviewEmailModels = {string.Join(", ", professionalReviewEmailModels.Select(c => c.Email))}.");

            foreach (var model in professionalReviewEmailModels)
            {
                _logger.Info($"MarkProfessionalReviewEmailsAsSent:: model = {model.Email} RequestId = {model.RequestId}.");

                var affectedRequest = _requestRepository.GetById(model.RequestId);
                var affectedAssignees = affectedRequest.RequestAssignees.Where(x => x.TempId == model.TempId);

                foreach (var assignee in affectedAssignees)
                {
                    _logger.Info($"MarkProfessionalReviewEmailsAsSent:: assignee = {assignee.RequestAssigneeId}");

                    assignee.IsReviewEmailSent = true;
                    _requestRepository.UpdateRequestAssignee(assignee);
                }
            }

            _requestRepository.SaveChanges();
        }

        public void SearchTempsForRequest(List<ERequestStatus> statuses, bool isForNewActiveTemp = false)
        {
            var requests = _requestRepository.GetRequestsByStatus(statuses).Where(c => c.IsAutoSearching == true).ToList();
            var emailsToSend = new List<EmailDto>();

            foreach (var request in requests)
            {
                _logger.Info($"{nameof(SearchTempsForRequest)}:: request id = {request.RequestId}");

                //Each request is split by day, with each request representing a single day.
                var firstRequestDay = request.RequestDays.OrderBy(c => c.ArrivalTime).FirstOrDefault()?.ArrivalTime;
                var lastRequestDay = request.RequestDays.OrderByDescending(c => c.ArrivalTime).FirstOrDefault()?.PatientHoursEnd;

                var position = request.LicenseType;
                var hasTemps = false;

                var licenses = _requestRepository.GetLicenses(lastRequestDay, position, request.IsAutoSearchingWithoutCheckingLicenseExpirationDay);

                _logger.Info($"{nameof(SearchTempsForRequest)}:: lastRequestDay = {lastRequestDay}, position = {position}.");

                foreach (var license in licenses)
                {
                    var temp = license.Temp;
                    if (temp == null)
                    {
                        _logger.Error($"{nameof(SearchTempsForRequest)}:: temp is null, license id = {license.LicenseId}");
                        continue;
                    }

                    _logger.Info($"{nameof(SearchTempsForRequest)}:: Temp id = {temp.TempId}, license id = {license.LicenseId}");

                    if (temp.Status != (int) Helper.EState.Active || temp.ProfessionalReviews.Any(c => c.Request.FacilityLocation.FacilityId == request.FacilityLocation.FacilityId && c.IsBlocked))
                    {
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: temp was skipped. Temp id = {temp.TempId}, temp.Status != Active or Facility was blocked this temp");
                        continue;
                    }

                    var desiredDistanceToWork = temp.DistanceToWork;
                    var curentDistance = request.FacilityLocation.TempFacilityDistances.FirstOrDefault(c => c.TempId == temp.TempId);

                    _logger.Info($"{nameof(SearchTempsForRequest)}:: desired Distance To Work = {desiredDistanceToWork}, curentDistance = {curentDistance?.Distance}");

                    if (curentDistance == null)
                    {
                        var distance = GetDistanceFromFacilityToTemp(request, temp);
                        if(distance.StatusCode == EStatusCode.Success)
                        {
                            curentDistance = new TempFacilityDistance();
                            curentDistance.Distance = distance.DistanceMi;
                        }
                        else
                        {
                            _logger.Error($"{nameof(SearchTempsForRequest)}:: Distance Error, status = {distance.StatusCode}");
                            continue;
                        }
                    }

                    if (curentDistance != null && curentDistance.Distance <= desiredDistanceToWork)
                    {
                        var longGuid = Helper.GetLongGuid();
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: longGuid = {longGuid}");

                        if (request.RequestAssignees.Any(c => c.Temp.TempId == temp.TempId))
                        {
                            _logger.Info($"{nameof(SearchTempsForRequest)}:: Temp skipped because this temp id ({temp.TempId}) already assigned on this request (id = {request.RequestId})");
                            continue;
                        }

                        request.RequestAssignees.Add(new RequestAssignee()
                        {
                            Temp = temp,
                            TempId = temp.TempId,
                            Status = (int)ERequestAssigneeStatus.WaitingForResponse,
                            LongGuid = longGuid
                        });

                        _logger.Info($"{nameof(SearchTempsForRequest)}:: temp was added to RequestAssignees, temp id = {temp.TempId}");

                        var applyUrl = $"{Setting.ApplicationBaseUrl}Requests/ApplyToRequest?tempId={temp.TempId}&longGuid={longGuid}";
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: applyUrl = {applyUrl}");
                       
                        NotifyTempAboutRequest(temp, request, firstRequestDay, curentDistance, applyUrl, emailsToSend, isForNewActiveTemp);

                        hasTemps = true;
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: hasTemps = {hasTemps}");
                    }
                    else
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: temp was skipped because he live so far, temp id = {temp.TempId}, " +
                                     $"curentDistance.Distance = {curentDistance.Distance}, desiredDistanceToWork = {desiredDistanceToWork}");
                }

                if(isForNewActiveTemp == false)
                {
                    request.Status = hasTemps ? (int) ERequestStatus.NeedsApproval : (int)ERequestStatus.NoMatch;

                    if(_requestRepository.GetRequestByStatus(request.RequestId, ERequestStatus.Active) != null)
                    {
                        _requestRepository.SaveChanges();
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: request.Status updated = {request.Status}");

                        if (hasTemps && request.CreatedBy != Role.Admin)
                        {
                            emailsToSend.Add(new EmailDto()
                            {
                                To = $"{Setting.AdministratorEmail}",
                                Subject = EmailConstants.WaitingResponseSubject,
                                Body = string.Format(EmailConstants.WaitingResponseBody, Helper.GetRequestUrl(request.RequestId), firstRequestDay, lastRequestDay, position)
                            });
                        }
                    }
                    else
                    {
                        _logger.Info($"{nameof(SearchTempsForRequest)}:: request already has been updated");
                    }
                }
                else
                {
                    _requestRepository.SaveChanges();
                }
            }

            _logger.Info($"{nameof(SearchTempsForRequest)}:: send emails count = {emailsToSend.Count}");
            _emailService.AddEmails(emailsToSend);
        }

        private void NotifyTempAboutRequest(Temp temp, Request request, DateTime? firstRequestDay, 
            TempFacilityDistance curentDistance, string applyUrl, List<EmailDto> emailsToSend, bool isThisProcessRunForNewActiveTemp)
        {
           
            if (temp.TempAvailabilities.All(c => (int)c.DayOfWeekAvailable != (int)firstRequestDay.Value.DayOfWeek))
            {
                _logger.Info($"{nameof(SearchTempsForRequest)}:: request notification was skipped. Temp id = {temp.TempId}," +
                    $" temp is not available on {firstRequestDay.Value.DayOfWeek}");
            }
            // Do not send SMS to new temps to avoid overloading them
            // The number of SMS messages can be huge. A daily email will suffice.
            else if (temp.IsGetSMSForNewRequest && isThisProcessRunForNewActiveTemp == false)
            {
                _textMessageService.AddSms(new TextMessageDto()
                {
                    PhoneNumberTo = temp.CellPhone,
                    Body = string.Format(SmsConstants.NotificationForNewRequest, request.FacilityLocation.City, applyUrl)
                });
            }
            else
            {
                var isDailyRequestNotification = DetectIfNotificationShouldBeDaily(firstRequestDay.Value);

                var properBody = isDailyRequestNotification ? EmailConstants.SearchTempsForRequestDailyEmailBody : EmailConstants.SearchTempsForRequestBody;

                emailsToSend.Add(new EmailDto()
                {
                    To = temp.Email,
                    Subject = EmailConstants.SearchTempsForRequestSubject,
                    Body = string.Format(
                       properBody,
                       firstRequestDay.Value.ToString("dddd MMMM d, yyyy"),
                       $"{request.FacilityLocation.City}, {request.FacilityLocation.StateName}",
                       curentDistance.Distance,
                       applyUrl),
                    IsDailyRequestEmail = isDailyRequestNotification
                });
            }
        }

        private bool DetectIfNotificationShouldBeDaily(DateTime requestStart)
        {
            var currentTimeEt = TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            //8 pm ET is time to send bulk daily emails
            var eightPmEt = new DateTime(currentTimeEt.Year, currentTimeEt.Month, currentTimeEt.Day, 20, 0, 0);

            if(currentTimeEt > eightPmEt)
            {
                eightPmEt = eightPmEt.AddDays(1);
            }

            if ((requestStart - eightPmEt).TotalHours >= 24)
            {
                return true;
            }
            
            return false;
        }

        private DistanceDto GetDistanceFromFacilityToTemp(Request request, Temp temp)
        {
            var curentDistance = new TempFacilityDistance();

            var distance = _googleMapService.GetDistance(new LocationDto() { Longitude = request.FacilityLocation.Longitude, Latitude = request.FacilityLocation.Latitude },
                                                            new LocationDto() { Longitude = temp.Longitude, Latitude = temp.Latitude });
            if (distance.StatusCode == EStatusCode.Success)
            {
                request.FacilityLocation.TempFacilityDistances.Add(new TempFacilityDistance()
                {
                    TempId = temp.TempId,
                    Distance = distance.DistanceMi,
                    Duration = distance.DurationSec
                });
                _facilityRepository.SaveChanges();

                _logger.Info($"{nameof(SearchTempsForRequest)}:: found curentDistance = {distance.DistanceMi}");

                return distance;
            }
            else
            {
                _logger.Error($"{nameof(SearchTempsForRequest)}:: Distance Error, status = {distance.StatusCode}");
                return distance;
            }
        }

        public void CheckIfRequestsIsExpired()
        {
            var openRequests = _requestRepository.GetRequestsByStatus(new List<ERequestStatus>() { ERequestStatus.NeedsApproval });
            foreach (var openRequest in openRequests)
            {
                var firstDay = openRequest.RequestDays.OrderBy(c => c.StartDate).FirstOrDefault();
                if (firstDay == null)
                {
                    _logger.Error($"CheckIfRequestsIsExpired:: request id = {openRequest.RequestId} does not have first day");
                    continue;
                }

                var mostCloseTempToFacility = openRequest.RequestAssignees
                    .Select(c => c.Temp.TempFacilityDistances.FirstOrDefault(x => x.FacilityLocation.FacilityId == openRequest.FacilityLocation.FacilityId))
                    .OrderBy(z => z.Duration).FirstOrDefault();

                if (firstDay.ArrivalTime < DateTime.Now.ToUniversalTime().AddHours(openRequest.FacilityLocation.OffsetHours ?? 0).AddMinutes(Decimal.ToDouble(mostCloseTempToFacility.Duration)))
                {
                    openRequest.Status = (int) ERequestStatus.Expired;
                    _requestRepository.SaveChanges();
                }
            }
        }

        private RequestDto AddLicensesToRequest(RequestDto requestDto)
        {
            var licenses = _tempRepository.GetSearchLicenses();
            requestDto.AvailableLicenseTypes = licenses;

            return requestDto;
        }

        private void AddDateForTimeProperties(Request request)
        {
            request.RequestDays.ForEach(x =>
            {
                x.ArrivalTime = x.StartDate.AddHours(x.ArrivalTime.Hour).AddMinutes(x.ArrivalTime.Minute);
                x.PatientHoursStart = x.StartDate.AddHours(x.PatientHoursStart.Hour).AddMinutes(x.PatientHoursStart.Minute);
                x.PatientHoursEnd = x.StartDate.AddHours(x.PatientHoursEnd?.Hour ?? 0).AddMinutes(x.PatientHoursEnd?.Minute ?? 0);
            });
        }

        private int UpdateRequestCore(RequestDto dto)
        {
            var requestDomain = _requestRepository.GetById(dto.Id);
            if (requestDomain == null)
                throw new Exception($"UpdateRequestCore:: request is null with id = {dto.Id}");

            var location = _facilityRepository.GetByIdFacilityLocation(dto.FacilityLocationId.Value);

            if (dto.SubmitterIsAdmin == false)
            {
                if (location.FacilityId != requestDomain.FacilityLocation.FacilityId)
                    throw new Exception($"UpdateRequestCore:: this user isn't able to edit requests of this practice");
            }

            var requestDayIds = requestDomain.RequestDays.Select(c => c.RequestDayId).ToList();
            requestDayIds.ForEach(id =>
            {
                var forDel = requestDomain.RequestDays.FirstOrDefault(c => c.RequestDayId == id);
                _requestRepository.DeleteRequestDay(forDel);
            });           
            _requestRepository.SaveChanges();

            //when request is submitted we need to split it by days. One request --> one day

            if((dto.Status == ERequestStatus.Active || dto.Status == ERequestStatus.NeedsApproval) && requestDomain.Status == (int)ERequestStatus.Draft)
            {
                var days = dto.RequestDays.ToList();
                
                for (int i = 0; i < days.Count; i ++)
                {
                    dto.RequestDays.Clear();

                    if (i == 0)
                    {
                        //keep one day for existing request
                        dto.RequestDays.Add(days[i]);
                        UpdateRequestDomain(requestDomain, dto, location);
                    }
                    else
                    {
                        dto.RequestDays.Add(days[i]);
                        CreateRequestCore(dto);
                    }
                }
            }
            else
            {
                UpdateRequestDomain(requestDomain, dto, location);
            }

            return requestDomain.RequestId;
        }

        private void UpdateRequestDomain(Request requestDomain, RequestDto dto, FacilityLocation location)
        {
            var newDomain = _requestMapper.MapToRequest(requestDomain, dto);
            newDomain.FacilityLocationId = location.FacilityLocationId;

            AddDateForTimeProperties(newDomain);

            _logger.Info($"UpdateRequestCore:: request id = {newDomain.RequestId}");

            _requestRepository.Update(newDomain);
            _requestRepository.SaveChanges();
        }

        private int CreateRequestCore(RequestDto dto)
        {
            var location = _facilityRepository.GetByIdFacilityLocation(dto.FacilityLocationId.Value);

            if(location == null)
                throw new Exception($"CreateRequestCore:: location is null. Facility id = {dto.FacilityId}, Location id = {dto.FacilityLocationId}, Submitter Email = {dto.SubmitterEmail}");

            dto.CreatedBy = dto.SubmitterEmail;

            var domain = _requestMapper.MapToRequest(dto);
            domain.FacilityLocationId = location.FacilityLocationId;

            AddDateForTimeProperties(domain);

            _requestRepository.Add(domain);
            _requestRepository.SaveChanges();

            return domain.RequestId;
        }

        public ValidationStatus ApplyTempToRequest(ApplyTempToRequestDto dto)
        {
            _logger.Info($"{nameof(ApplyTempToRequest)}, dto.TempId = {dto.TempId}, dto.LongGuid = {dto.LongGuid}");

            var result = new ValidationStatus();
            var requestAssignee = _requestRepository.GetRequestAssignee(dto.TempId, dto.LongGuid);

            if (requestAssignee == null)
            {
                _logger.Info($"{nameof(ApplyTempToRequest)}, requestAssignee is null");
                
                result.Success = false;
                result.Messages.Add("Id or Guid is wrong");
                return result;
            }

            if(requestAssignee.Status == (int) ERequestAssigneeStatus.If)
            {
                result.Messages.Add("You have already applied successfully. Administrator should confirm your submission. Thank you.");
                return result;
            }

            if(requestAssignee.Request.Status != (int)ERequestStatus.NeedsApproval && requestAssignee.Request.Status != (int)ERequestStatus.Filled)
            {
                _logger.Info($"{nameof(ApplyTempToRequest)}, temp applied to request when request had status = {requestAssignee.Request.Status} (not NeedsApproval and not Filled)");

                result.Success = false;
                result.Messages.Add("Unfortunately, you have applied too late or request has been canceled. Thank you.");
                return result;
            }

            var requestStartTime = requestAssignee.Request.RequestDays.OrderBy(c => c.RequestDayId).FirstOrDefault()?.ArrivalTime;
            if (DateTime.Now.ToUniversalTime().AddHours(requestAssignee.Request.FacilityLocation.OffsetHours ?? 0) > requestStartTime)
            {
                requestAssignee.Status = (int) ERequestAssigneeStatus.TooLate;
                _logger.Info($"{nameof(ApplyTempToRequest)}, apply too late");

                _requestRepository.SaveChanges();

                result.Success = false;
                result.Messages.Add("Unfortunately, you have applied too late. Thank you.");
                return result;
            }

            requestAssignee.Status = (int)ERequestAssigneeStatus.If;
            _requestRepository.SaveChanges();

            _emailService.AddEmail(new EmailDto()
            {
                To = $"{Setting.AdministratorEmail}",
                Subject = EmailConstants.ApplyTempToRequestSubject,
                Body = string.Format(
                    EmailConstants.ApplyTempToRequestBody,
                    Helper.GetRequestUrl(requestAssignee.Request.RequestId),
                    requestStartTime?.ToString("yyyy-MMM-dd") ?? "N/A",
                    requestAssignee.Request.LicenseType,
                    requestAssignee.Temp.Email)
            });

            result.Messages.Add("You have applied successfully. Administrator should confirm your submission. Thank you.");
            return result;
        }

        public ValidationStatus CancelFilledRequest(int requestAssigneeId, SecurityAccess securityAccess)
        {
            _logger.Info($"{nameof(CancelFilledRequest)}:: requestAssigneeId = {requestAssigneeId}");
            var result = new ValidationStatus();

            var requestAssignee = _requestRepository.GetRequestAssignee(requestAssigneeId);

            if (requestAssignee == null)
                throw new Exception($"{nameof(CancelFilledRequest)}:: Request Assignee is null with {requestAssigneeId}");

            if (securityAccess.ForTemp)
            {
                if (requestAssignee.Status == (int) ERequestAssigneeStatus.Filled)
                    requestAssignee.Request.Status = (int)ERequestStatus.NeedsApproval;

                requestAssignee.ChagnedBy = securityAccess.HttpContextUserEmail;
                requestAssignee.Status = (int)ERequestAssigneeStatus.CanceledByTemp;
            }
            else
            {
                requestAssignee.ChagnedBy = Role.Admin.ToString();
                requestAssignee.Status = (int)ERequestAssigneeStatus.If;
                requestAssignee.Request.Status = (int)ERequestStatus.NeedsApproval;
            }

            requestAssignee.DateChanged = DateTime.Now.ToUniversalTime();

            _emailService.AddEmail(new EmailDto()
            {
                Subject = EmailConstants.AssigneeCancelationSubject,
                Body = string.Format(EmailConstants.AssigneeCancelationBody, Helper.GetTempUrl(requestAssignee.TempId), Helper.GetRequestUrl(requestAssignee.RequestId),
                    securityAccess.ForTemp ? "professional" : "admin", Helper.GetContactUsUrl()),
                To = $"{requestAssignee.Temp.Email},{requestAssignee.Request.FacilityLocation.Facility.Email}",
                Cc = $"{Setting.AdministratorEmail}"
            });

            requestAssignee.Request.PriceForOffice = null;
            requestAssignee.Request.PriceForTemp = null;

            _requestRepository.Update(requestAssignee.Request);
            _requestRepository.SaveChanges();

            return result;
        }

        public void HandleRequestsStatuses()
        {
            MoveRequestsToInProgress();
            MoveRequestsToSuccess();
            MoveRequestsToExpired();
        }

        private void SendRequestAboutTempReview()
        {
            var emails = GetFilledProfessionalReviewRequests();
            var applicationBaseUrl = Setting.ApplicationBaseUrl;

            foreach (var email in emails)
            {
                var linkToReview = string.Format(
                    EmailConstants.ProfessionalReviewLinkTemplate,
                    applicationBaseUrl,
                    email.TempId,
                    email.RequestId);

                var emailBody = string.Format(
                    EmailConstants.ProfessionalReviewTemplate,
                    email.PracticeName,
                    email.RecentTempName,
                    linkToReview);

                _emailService.AddEmail(new EmailDto()
                {
                    To = email.Email,
                    Subject = EmailConstants.ProfessionalReviewSubject,
                    Body = emailBody
                });
            }

            MarkProfessionalReviewEmailsAsSent(emails);
        }

        private void MoveRequestsToSuccess()
        {
            var endedRequests = _requestRepository.GetAlreadySuccessRequests();

            foreach (var endedRequest in endedRequests)
            {
                if(_requestRepository.GetRequestByStatus(endedRequest.RequestId, ERequestStatus.InProgress) != null)
                {
                    endedRequest.Status = (int)ERequestStatus.Success;
                    _requestRepository.Update(endedRequest);
                    _requestRepository.SaveChanges();
                }
            }

            SendRequestAboutTempReview();
        }

        private void MoveRequestsToInProgress()
        {
            var filledRequests = _requestRepository.GetAlreadyInProgressRequests();

            foreach (var filledRequest in filledRequests)
            {
                if(_requestRepository.GetRequestByStatus(filledRequest.RequestId, ERequestStatus.Filled) != null)
                {
                    filledRequest.Status = (int)ERequestStatus.InProgress;
                    _requestRepository.Update(filledRequest);

                    SendEmailAboutChangeRequestStatus(filledRequest.RequestId, ERequestStatus.InProgress,
                        filledRequest.FacilityLocation.Facility.Email);

                    _requestRepository.SaveChanges();
                }
            }
        }

        private void MoveRequestsToExpired()
        {
            var openedRequests = _requestRepository.GetAlreadyExpairedRequests();

            foreach (var openedRequest in openedRequests)
            {
                if(_requestRepository.GetRequestByStatus(openedRequest.RequestId, ERequestStatus.NeedsApproval) != null)
                {
                    openedRequest.Status = (int)ERequestStatus.Expired;
                    _requestRepository.Update(openedRequest);
                    _requestRepository.SaveChanges();
                }
            }
        }

        private void SendEmailAboutChangeRequestStatus(int requestId, ERequestStatus status, string facilityEmail)
        {
            _emailService.AddEmail(new EmailDto()
            {
                To = facilityEmail,
                Cc = Setting.AdministratorEmail,
                Subject = string.Format(EmailConstants.ChangeRequestStatusSubject, status.ToString()),
                Body = string.Format(
                    EmailConstants.ChangeRequestStatusBody,
                    status.ToString(),
                    Helper.GetRequestUrl(requestId),
                    Helper.GetContactUsUrl())
            });
        }

        public void SendToTempFacilityAddress(Request filledRequest)
        {
            var temp = filledRequest.RequestAssignees.FirstOrDefault(c =>
                c.Status == (int) ERequestAssigneeStatus.Filled && c.IsFacilityAddressSent == false);

            if (temp == null)
            {
                _logger.Error($"Error getting temp: requestId: {filledRequest.RequestId}");
                return;
            }

            var firstRequestDay = filledRequest.RequestDays.OrderBy(c => c.ArrivalTime).FirstOrDefault()?.ArrivalTime;
            var lastRequestDay = filledRequest.RequestDays.OrderByDescending(c => c.ArrivalTime).FirstOrDefault()?.PatientHoursEnd;

            var facilityAddress = filledRequest.FacilityLocation.FullAddress;

            _logger.Info($"{nameof(SendToTempFacilityAddress)}:: temp email = {temp.Temp.Email}, firstRequestDay = {firstRequestDay}, " +
                         $"lastRequestDay = {lastRequestDay}, facilityAddress = {facilityAddress}");

            _emailService.AddEmail(new EmailDto()
            {
                To = temp.Temp.Email,
                Subject = string.Format(
                    EmailConstants.SendToTempFacilityAddressSubject,
                    firstRequestDay.Value.ToString("yyyy-MMM-dd")),
                Body = string.Format(
                    EmailConstants.SendToTempFacilityAddressBody,
                    filledRequest.LicenseType,
                    filledRequest.FacilityLocation.Facility.FacilityName,
                    firstRequestDay.Value.ToString("yyyy-MMM-dd hh:mm"),
                    facilityAddress,                
                    Helper.GetContactUsUrl())
            });

            filledRequest.RequestAssignees.ForEach(c =>
            {
                if (c.IsFacilityAddressSent == false && c.Status == (int) ERequestAssigneeStatus.Filled)
                {
                    c.IsFacilityAddressSent = true;
                    _logger.Info($"SendToTempFacilityAddress:: RequestAssigneeId = {c.RequestAssigneeId}, IsFacilityAddressSent = true");
                }
            });

            _requestRepository.Update(filledRequest);
            _requestRepository.SaveChanges();
        }

        public ValidationStatus CancelRequest(RequestCancelDto dto)
        {
            var result = new ValidationStatus(){Success = true};

            var request = _requestRepository.GetById(dto.RequestId);
            if (request == null)
            {
                _logger.Error($"{nameof(CancelRequest)}:: request is null with id = {dto.RequestId}");
                return new ValidationStatus(){Success = false, Messages = new List<string>(){$"Request is null with id = {dto.RequestId}"} };
            }

            if (request.Status == (int) ERequestStatus.InProgress
                || request.Status == (int) ERequestStatus.Filled
                || request.Status == (int) ERequestStatus.Active
                || request.Status == (int)ERequestStatus.NeedsApproval)
            {
                request = _requestMapper.MapToRequest(dto, request);
                _requestRepository.Update(request);
                _requestRepository.SaveChanges();

                var tempEmail = request.RequestAssignees
                    .FirstOrDefault(c => c.Status == (int) ERequestAssigneeStatus.Filled)?.Temp?.Email;

                if (!string.IsNullOrEmpty(tempEmail))
                {
                    _emailService.AddEmail(new EmailDto()
                    {
                        To = tempEmail,
                        Cc = Setting.AdministratorEmail,
                        Subject = EmailConstants.CanсelRequestSubject,
                        Body = string.Format(
                            EmailConstants.CanсelRequestBody,
                            Helper.GetRequestUrl(request.RequestId),
                            Helper.GetContactUsUrl())
                    });
                }

                _logger.Info($"{nameof(CancelRequest)}:: request was successfully canceled, id = {request.RequestId}");
            }
            else
            {
                _logger.Error($"{nameof(CancelRequest)}:: request does not have proper status (In Progress, Filled, Active, Needs Approval) for cancellation, id = {request.RequestId}");
                return new ValidationStatus() { Success = false, Messages = new List<string>() { $"Request does not have proper status (In Progress, Filled, Active, Needs Approval) for cancellation, id = {dto.RequestId}" } };
            }

            return result;
        }

        public RequestSearchDto GetRequestSearchDto(bool isForTemp, string tempEmail = null)
        {
            if(isForTemp && !string.IsNullOrEmpty(tempEmail))
            {
                var licenses = _tempRepository.GetByEmail(tempEmail).Licenses                   
                    .GroupBy(c => c.LicenseType).Select(c => c.Key).ToList();

                return new RequestSearchDto
                {
                    Licenses = licenses
                };
            }

            return new RequestSearchDto
            {
                Licenses = _requestRepository.GetPositionForRequests()
            };
        }

        public List<RequestForInactiveOrDeletedTemp> GetRequestForInactiveOrDeletedTemp(int tempId)
        {
            var requests = _requestRepository.GetNeedsApprovalOrFilledRequestsForTemp(tempId)
                .Where(c =>
                {
                    var status = c.RequestAssignees.FirstOrDefault(v => v.TempId == tempId)?.Status;

                    if(status != null && (status == (int)ERequestAssigneeStatus.Filled 
                                            || status == (int)ERequestAssigneeStatus.If 
                                                || status == (int)ERequestAssigneeStatus.WaitingForResponse))
                    {
                        return true;
                    }

                    return false;
                });

            var result = requests.Select(c => _requestMapper.MapToRequestForInactiveOrDeletedTemp(c)).ToList();

            return result;
        }

        public string GetRequestAssignePrice(int requestAssigneeDesiredHourlyRate, RequestAssigneePriceDto request, bool? isItForAdminOrOffice)
        {
            if (request != null)
            {     
                var requestStartTime = request.RequestStartTime;

                var requestHours = 0.0;

                var tempPlacementFee = request.LicenseType == ELicenseType.Dentist.GetDisplayName() ? Setting.DentistTemporaryPlacementFee : Setting.TemporaryPlacementFee;

                if ((requestStartTime - request.DateCreated).Hours > 24)
                {
                    return GetRequestAssignePrice(request, requestHours, requestAssigneeDesiredHourlyRate, tempPlacementFee, isItForAdminOrOffice);
                }
                else
                {
                    //+ 15% placement fee < 24 hr notice
                    return GetRequestAssignePrice(request, requestHours, requestAssigneeDesiredHourlyRate, tempPlacementFee + (tempPlacementFee * 0.15), isItForAdminOrOffice);
                }
            }
            else
            {
                _logger.Error($"{nameof(GetRequestAssignePrice)}:: request is null with id = {request.RequestId}");
            }

            return string.Empty;
        }

        private string GetRequestAssignePrice(RequestAssigneePriceDto request, double requestHours, int? tempRate, double feePerDay, bool? isItForAdminOrOffice)
        {
            request.RequestDayAssigneePriceDtos.ForEach(c => requestHours += GetCountHoursPerDay(c.ArrivalTime, c.PatientHoursEnd, c.LunchTime));

            var dfiFee = isItForAdminOrOffice == true ? feePerDay * request.RequestDayAssigneePriceDtos.Count : 0;

            return (dfiFee + (requestHours * tempRate ?? 0.0)).GetTwoDecimalPlacesStringOrNa();
        }

        private double GetCountHoursPerDay(DateTime arrivalTime, DateTime? patientHoursEnd, string lunchTime)
        {
            var startDay = arrivalTime;
            var endDay = patientHoursEnd ?? DateTime.MinValue;

            var lunchTimeMinutes = lunchTime.Contains("1 hour") ? 60 : lunchTime.Contains("30 min") ? 30 : 0;

            return ((endDay - startDay).TotalMinutes - lunchTimeMinutes) / 60;
        }

        public RequestPriceDto GetRequestPrice(int requestId)
        {
            var request = _requestRepository.GetById(requestId);

            if (request == null)
            {
                _logger.Error($"{nameof(GetRequestPrice)}:: request id null with id = {requestId}");
            }

            return new RequestPriceDto()
            {
                PriceForTemp = request?.PriceForTemp == null ? "0" : request.PriceForTemp.Value.GetTwoDecimalPlacesStringOrNa(),
                PriceForOffice = request?.PriceForOffice == null ? "0" : request.PriceForOffice.Value.GetTwoDecimalPlacesStringOrNa(),
            }; 
        }

        public ValidationStatus UpdateRequestHours(RequestDto dto)
        {
            var result = UpdateRequest(dto);
            if (result.Success)
            {
                var request = _requestRepository.GetById(dto.Id);
                if (request != null)
                {
                    if(request.Status == (int)ERequestStatus.Filled)
                    {
                        var requestAssignee = request.RequestAssignees.FirstOrDefault(c => c.Status == (int)ERequestAssigneeStatus.Filled);

                        var tempRate = requestAssignee.Temp.Licenses
                                        .FirstOrDefault(c => c.LicenseType == request.LicenseType)?.DesiredHourlyRate;
                        var requestAssigneePriceDto = _requestRepository.GetRequestAssigneePriceDto(request.RequestId);

                        request.PriceForOffice = double.Parse(GetRequestAssignePrice(tempRate.Value, requestAssigneePriceDto, true));
                        request.PriceForTemp = double.Parse(GetRequestAssignePrice(tempRate.Value, requestAssigneePriceDto, false));

                        _requestRepository.Update(request);
                        _requestRepository.SaveChanges();
                    }

                    foreach(var assignee in request.RequestAssignees.Where(c => c.Status == (int)ERequestAssigneeStatus.Filled))
                    {
                        var email = assignee.Temp.Email;

                        _emailService.AddEmail(new EmailDto
                        {
                            Subject = EmailConstants.RequestHoursAreChangedSubject,
                            Body = string.Format(EmailConstants.RequestHoursAreChangedBody, request.FacilityLocation.Facility.FacilityName, Helper.GetRequestUrl(request.RequestId),
                                                                Helper.GetContactUsUrl()),
                            To = email
                        });
                    }
                }
                else
                {
                    result.Success = false;
                    result.Messages.Add($"Request is null. Can not find by id = {dto.Id}");
                }                 
            }
            return result;
        }

        public ManualFillRequestDto ManualFillRequest(int? requestId, int? tempId)
        {
            var result = new ManualFillRequestDto();

            var request = _requestRepository.GetById(requestId);
            if (request == null)
                return result.SetErrorMessage("Request is null");

            if (request.Status != (int)ERequestStatus.Filled && request.Status != (int)ERequestStatus.NeedsApproval)
                return result.SetErrorMessage("Request must have status Filled or Need Approval");

            var temp = _tempRepository.GetById(tempId);

            if(temp == null)
                return result.SetErrorMessage("Temp is null");
            
            if(temp.Status != (int)Helper.EState.Active)
                return result.SetErrorMessage("Temp does not have status Active");

            var longGuid = Helper.GetLongGuid();

            var requestAssigne = new RequestAssignee()
            {
                RequestId = request.RequestId,
                TempId = temp.TempId,
                Status = (int)ERequestAssigneeStatus.WaitingForResponse,
                LongGuid = longGuid
            };

            temp.RequestAssignees.Add(requestAssigne);
            _tempRepository.SaveChanges();

            var firstRequestDay = request.RequestDays.OrderBy(c => c.ArrivalTime).FirstOrDefault()?.ArrivalTime;
            var applyUrl = $"{Setting.ApplicationBaseUrl}Requests/ApplyToRequest?tempId={temp.TempId}&longGuid={longGuid}";

            var curentDistance = request.FacilityLocation.TempFacilityDistances.FirstOrDefault(c => c.TempId == temp.TempId);

            _logger.Info($"{nameof(ManualFillRequest)}:: curentDistance = {curentDistance?.Distance}");

            if (curentDistance == null)
            {
                var distance = GetDistanceFromFacilityToTemp(request, temp);
                if (distance.StatusCode == EStatusCode.Success)
                {
                    curentDistance = new TempFacilityDistance();
                    curentDistance.Distance = distance.DistanceMi;
                }
                else
                {
                    _logger.Error($"{nameof(ManualFillRequest)}:: Distance Error, status = {distance.StatusCode}");
                    return result.SetErrorMessage("Failed to get distance");
                }
            }

            if(temp.IsGetSMSForNewRequest)
            {
                _textMessageService.AddSms(new TextMessageDto()
                {
                    PhoneNumberTo = temp.CellPhone,
                    Body = string.Format(SmsConstants.NotificationForNewRequest, request.FacilityLocation.City, applyUrl)
                });
            }
            else
            {
                _emailService.AddEmail(new EmailDto()
                {
                    To = temp.Email,
                    Subject = EmailConstants.SearchTempsForRequestSubject,
                    Body = string.Format(
                                    EmailConstants.SearchTempsForRequestBody,
                                    firstRequestDay.Value.ToString("dddd MMMM d, yyyy"),
                                    $"{request.FacilityLocation.City}, {request.FacilityLocation.StateName}",
                                    curentDistance.Distance,
                                    applyUrl)
                });
            }


            result.RequestAssigneId = request.RequestAssignees.FirstOrDefault(c => c.TempId == tempId).RequestAssigneeId;

            return result;
        }

        public void SendConfirmationBeforeStartRequest()
        {
            var timeUtc = DateTime.UtcNow;
            var easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var today = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

            var from = today.AddHours(44);
            var to = from.AddMinutes(15);

            var requests = _requestRepository.GetRequestsForSendingReminders(from, to);

            foreach(var request in requests)
            {
                var temp = request.RequestAssignees.FirstOrDefault(c => c.Status == (int)ERequestAssigneeStatus.Filled).Temp;

                if(temp != null)
                {
                    //TODO: need better name for this variable "IsGetSMSForNewRequest". Something like LikeToGetSmsNotifications
                    if (temp.IsGetSMSForNewRequest)
                    {
                        _textMessageService.AddSms(new TextMessageDto()
                        {
                            Body = string.Format(SmsConstants.Confirmation48HoursBeforeRequestStart,
                            request.RequestDays.FirstOrDefault()?.ArrivalTime,
                            request.FacilityLocation.Facility.FacilityName,
                            request.FacilityLocation.FullAddress),
                            PhoneNumberTo = temp.CellPhone
                        });
                    }
                    else
                    {
                        _emailService.AddEmail(new EmailDto()
                        {
                            Subject = EmailConstants.Confirmation48HoursBeforeRequestStartSubject,
                            Body = string.Format(EmailConstants.Confirmation48HoursBeforeRequestStartBody,
                            request.RequestDays.FirstOrDefault()?.ArrivalTime,
                            request.FacilityLocation.Facility.FacilityName,
                            request.FacilityLocation.FullAddress),
                            To = temp.Email
                        });
                    }
                }
            }
        }

        public IdLabel[] GetTempsFullNamesForRequest(int requestId)
        {
            return _requestRepository.GetTempsFullNamesForRequest(requestId);
        }

        public ApplyTempDto GetApplyTempDto(int tempId, string longGuid)
        {
            var requestAssignee = _requestRepository.GetRequestAssignee(tempId, longGuid);

            if(requestAssignee == null)
            {
                throw new Exception("Unable to find the request. It may be possible that you have already filled it.");
            }

            return new ApplyTempDto()
            {
                Email = requestAssignee.Temp.Email,
                RequestId = requestAssignee.RequestId,
                RequestDayId = requestAssignee.Request.RequestDays.FirstOrDefault().RequestDayId
            };
        }
    }

    public interface IRequestService
    {
        ValidationStatus UpdateRequest(RequestDto dto);
        ValidationStatus UpdateRequestHours(RequestDto dto);
        RequestDto GetRequest(GetRequestDto dto);
        RequestDto GetCreateModel(SecurityAccess access);
        int AddEmptyDay(RequestDto request);
        int AddEmptyDayWithSameHours(RequestDto request);
        void RemoveRequestDay(int? id);
        void RemoveRequest(int? id);
        IPagedList<RequestShortDto> GetRequests(RequestSearchDto dto);
        IList<RequestCalendarEvent> GetRequestsAsCalendarEvents(RequestSearchDto dto);
        ValidationStatus ApplyTempToRequest(ApplyTempToRequestDto dto);
        RequestAssigneeListDto GetRequestAssigneeList(GetAssigneesForRequestDto dto);
        ValidationStatus FillRequest(int requestAssigneeId);
        ValidationStatus CancelFilledRequest(int requestAssigneeId, SecurityAccess securityAccess);
        void HandleRequestsStatuses();
        void SearchTempsForRequest(List<ERequestStatus> statuses, bool isForNewActiveTemp = false);
        ValidationStatus CancelRequest(RequestCancelDto dto);
        RequestSearchDto GetRequestSearchDto(bool isForTemp, string tempEmail = null);
        List<RequestForInactiveOrDeletedTemp> GetRequestForInactiveOrDeletedTemp(int tempId);
        ValidationStatus GetAccessToCreateRequest(SecurityAccess access);
        string GetRequestAssignePrice(int requestAssigneeDesiredHourlyRate, RequestAssigneePriceDto request, bool? isItForAdminOrOffice);
        RequestPriceDto GetRequestPrice(int RequestId);
        ManualFillRequestDto ManualFillRequest(int? requestId, int? tempId);
        void SendConfirmationBeforeStartRequest();
        IdLabel[] GetTempsFullNamesForRequest(int requestId);
        ApplyTempDto GetApplyTempDto(int tempId, string longGuid);
    }
}