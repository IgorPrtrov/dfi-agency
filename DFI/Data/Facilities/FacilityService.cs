﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DFI.Data.Accounts;
using DFI.Data.ChartingSoftwares;
using DFI.Data.Emails;
using DFI.Data.GoogleMaps;
using DFI.Data.Requests;
using DFI.Data.XraySoftwares;
using DFI.Factory;
using DFI.Helpers;
using DFI.Models;
using Microsoft.AspNet.Identity;
using PagedList;

namespace DFI.Data.Facilities
{
    public class FacilityService : IFacilityService
    {
        private readonly IFacilityRepository _facilityRepository;
        private readonly IFacilityMapper _facilityMapper;
        private readonly ILogger _logger;
        private readonly IEmailService _emailService;
        private readonly IGoogleMapService _googleMapService;
        private readonly IXraySoftwareService _xraySoftwareService;
        private readonly IChartingSoftwareService _chartingSoftwareService;
        private readonly IApplicationUserManagerFactory _userManagerFactory;

        public FacilityService(
            IFacilityRepository facilityRepository, IFacilityMapper facilityMapper,
            ILogger logger, IEmailService emailService, IGoogleMapService googleMapService,
            IXraySoftwareService xraySoftwareService, IChartingSoftwareService chartingSoftwareService, IApplicationUserManagerFactory userManagerFactory)
        {
            _facilityRepository = facilityRepository;
            _facilityMapper = facilityMapper;
            _logger = logger;
            _emailService = emailService;
            _googleMapService = googleMapService;
            _xraySoftwareService = xraySoftwareService;
            _chartingSoftwareService = chartingSoftwareService;
            _userManagerFactory = userManagerFactory;
        }

        public int SubmitRegistrationForm(FacilityRegistrationDto dto)
        {
            var name = _userManagerFactory.GetUserManager().FindByName(dto.FacilityDto.Email);
            if (name == null)
            {
                ApplicationUser applicationUser = new ApplicationUser
                {
                    Email = dto.FacilityDto.Email,
                    UserName = dto.FacilityDto.Email,
                    Status = (int) Helper.EState.Submitted
                };
                IdentityResult result = _userManagerFactory.GetUserManager().Create(applicationUser, dto.Password);

                if (result.Succeeded)
                {
                    _userManagerFactory.GetUserManager().AddToRole(applicationUser.Id, Role.Facility);

                    _logger.Info($"SubmitRegistrationForm Facility:: ApplicationUser Id = {applicationUser.Id}");

                    string code = _userManagerFactory.GetUserManager().GenerateEmailConfirmationToken(applicationUser.Id);
                    if(string.IsNullOrEmpty(code))
                        throw new Exception("SubmitRegistrationForm Facility:: Email Confirmation Token is null");

                    dto.CallbackUrl = dto.CallbackUrl.Replace("userIdValue", HttpUtility.UrlEncode(applicationUser.Id)).Replace("codeConfirmationValue", HttpUtility.UrlEncode(code));

                    _logger.Info($"SubmitRegistrationForm Facility:: CallbackUrl = {dto.CallbackUrl}");
                }
                else
                   throw new Exception($"SubmitRegistrationForm Facility:: IdentityResult Error:{string.Join("; ", result.Errors)}");
            }
            else
                throw new Exception($"SubmitRegistrationForm Facility:: UserManager Error: user already exists with email = {dto.FacilityDto.Email}");

            dto.FacilityDto.FacilityStatus = (int)Helper.EState.Submitted;
            var facilityId = AddFacility(dto.FacilityDto);

            _emailService.AddEmail(new EmailDto()
            {
                Body = string.Format(EmailConstants.FacilityRegistrationFormBody, dto.CallbackUrl),
                Subject = EmailConstants.FacilityRegistrationFormSubject,
                To = dto.FacilityDto.Email
            });

            _logger.Info($"SubmitRegistrationForm Facility:: facilityId = {facilityId}");

            return facilityId;
        }

        public ValidationStatus SubmitFacilityLocationForm(FacilityLocationDto dto)
        {
            if (dto.Id == null)
            {
                return CreateFacilityLocation(dto);
            }
            else
            {
                return UpdateFacilityLocation(dto);
            }
        }

        private ValidationStatus CreateFacilityLocation(FacilityLocationDto dto)
        {
            var result = new ValidationStatus() { Success = true };

            var facilityLocation = _facilityMapper.MapToFacilityLocation(dto);

            var existFacilityLocations = _facilityRepository.GetFacilityLocations(dto.FacilityId.Value);
            if (existFacilityLocations.Any(c => c.FullAddress == facilityLocation.FullAddress))
            {
                result.Success = false;
                result.Messages.Add("The facility already has location with the same address");
                return result;
            }

            var location = _googleMapService.GetLocation($"{facilityLocation.AddressLine1}, {facilityLocation.City}, {facilityLocation.Zipcode}");
            if (location.Latitude != 0 && location.Longitude != 0)
            {
                facilityLocation.Latitude = location.Latitude;
                facilityLocation.Longitude = location.Longitude;
            }

            facilityLocation.TimeZoneId = _googleMapService.GetTimeZoneId(facilityLocation.Latitude, facilityLocation.Longitude);

            AddOrUpdateSoftwareNames(facilityLocation);
            _facilityRepository.AddFacilityLocation(facilityLocation);

            _logger.Info($"CreateFacilityLocation:: crated facility location {facilityLocation.FacilityLocationId}");

            result.Messages.Add(facilityLocation.FacilityLocationId.ToString());
            return result;
        }

        private ValidationStatus UpdateFacilityLocation(FacilityLocationDto dto)
        {
            var result = new ValidationStatus() { Success = true };

            var facilityLocation = _facilityRepository.GetByIdFacilityLocation(dto.Id.Value);
            if (facilityLocation == null)
                throw new Exception($"UpdateFacilityLocation:: facility location is null with id = {dto.Id}");

            var hasRequests = facilityLocation.Requests.Any(c => c.Status != (int)ERequestStatus.Draft && c.Status != (int)ERequestStatus.Deleted);

            var location = new LocationDto();
            if (facilityLocation.AddressLine1 != dto.AddressFromGoogle.Split(',')[0].Trim() ||
                facilityLocation.City != dto.AddressFromGoogle.Split(',')[1].Trim() ||
                facilityLocation.Zipcode != dto.AddressFromGoogle.Split(',')[3].Trim())
            {
                //for correct historical data we can not update address for location which has requests
                if (hasRequests)
                {
                    result.Success = false;
                    result.Messages.Add("The address can not be updated because this location has requests. Please create a new location");
                    return result;
                }

                location = _googleMapService.GetLocation(dto.AddressFromGoogle);
            }

            facilityLocation = _facilityMapper.MapToFacilityLocation(facilityLocation, dto);

            facilityLocation.FacilityHours.ToList().ForEach(c => _facilityRepository.Delete(c));
            facilityLocation.FacilityHours = dto.FacilityHours.Select(c => _facilityMapper.MapToFacilityHour(c)).ToList();

            if (location.Latitude != 0 && location.Longitude != 0)
            {
                facilityLocation.TempFacilityDistances.ToList().ForEach(c => _facilityRepository.Delete(c));
                facilityLocation.Latitude = location.Latitude;
                facilityLocation.Longitude = location.Longitude;
                facilityLocation.TimeZoneId = _googleMapService.GetTimeZoneId(location.Latitude, location.Longitude);
            }

            AddOrUpdateSoftwareNames(facilityLocation);
            _facilityRepository.UpdateFacilityLocation(facilityLocation);
            _logger.Info($"UpdateFacilityLocation:: updated facility location {facilityLocation.FacilityLocationId}");

            return result;
        }

        public int AddFacility(FacilityDto dto)
        {
            var facility = _facilityMapper.MapToFacility(dto);
            _facilityRepository.Add(facility);
            _facilityRepository.SaveChanges();

            return facility.FacilityId;
        }

        public ListFacilityDto GetListFacilityDtos(FacilitySearchDto dto)
        {
            var facilities = _facilityRepository.GetFacilities(PrepareSearchModel(dto));
            var facilitiesShort = facilities
                .Where(c => c.Status != (int)Helper.EState.Deleted)
                .Select(c => _facilityMapper.MapToFacilityShortDto(c))
                .ToList();

            int pageSize = 10;
            int pageNumber = (dto.Page ?? 1);
            var result = facilitiesShort.ToPagedList(pageNumber, pageSize);

            return new ListFacilityDto()
            {
                FacilityShortDtos = result
            };
        }

        public FacilityDto GetFacility(int? facilityId)
        {
            var facilityDomain = _facilityRepository.GetById(facilityId);

            if (facilityDomain == null)
                throw new Exception($"GetFacility:: Facility is null with id = {facilityId}");

            if(facilityDomain.Status == (int)Helper.EState.New || facilityDomain.Status == (int)Helper.EState.Deleted)
                throw new Exception($"GetFacility:: Facility was deleted or did not submitted, id = {facilityId}");

            var newEmail = _userManagerFactory.GetUserManager().Users.FirstOrDefault(c => c.Email == facilityDomain.Email)?.NewEmail;

            var facilityDto = _facilityMapper.MapToFacilityDto(facilityDomain);
            facilityDto.NewEmail = newEmail;

            return facilityDto;
        }

        public void UpdateFacility(FacilityDto dto)
        {
            var facilityDomain = _facilityRepository.GetById(dto.Id);
            if (facilityDomain == null)
                throw new Exception($"GetTemp:: temp is null with id = {dto.Id}");

            var domain = _facilityMapper.MapToFacility(facilityDomain, dto);

            _facilityRepository.Update(domain);
            _facilityRepository.SaveChanges();
        }

        public ValidationStatus SetStatus(int id, Helper.EState newStatus)
        {
            var result = new ValidationStatus() { Success = true };
            var domain = _facilityRepository.GetById(id);
            if (domain == null)
            {
                result.Success = false;
                result.Messages.Add($"Facility is null with id = {id}");
                _logger.Error($"SetStatus:: facility is null with id = {id}");
                return result;
            }

            var oldStatus = (Helper.EState) domain.Status;

            if (oldStatus == Helper.EState.New && newStatus == Helper.EState.Active)
            {
                result.Success = false;
                result.Messages.Add($"Facility status cannot be changed from {oldStatus.ToString()} to {newStatus.ToString()}");
                return result;
            }

            var appUser = _userManagerFactory.GetUserManager().Users.FirstOrDefault(c => c.Email == domain.Email);
            if (appUser == null)
            {
                result.Success = false;
                result.Messages.Add($"Application user is null with email = {domain.Email}");
                _logger.Error($"SetStatus:: application user is null with email = {domain.Email}");
                return result;
            }

            if (!appUser.EmailConfirmed && newStatus == Helper.EState.Active)
            {
                result.Success = false;
                result.Messages.Add($"The facility can not be Active without email confirmation");
                return result;
            }

            if (!domain.FacilityLocations.Any(c => c.LocationStatus == (int)EFacilityLocationStatus.Active) && newStatus == Helper.EState.Active)
            {
                result.Success = false;
                result.Messages.Add($"The facility can not be Active without a location");
                return result;
            }

            var user = _userManagerFactory.GetUserManager().FindByName(domain.Email);

            //implemented because if Administrator wants to recreate this user again. The email should not exist.
            var emailMerkedAsDeleted = $"{domain.Email}_deleted_on_{DateTime.Now.ToString("yyyy_MMM_d_HH_mm_ss")}";
            if (newStatus == Helper.EState.Deleted)
            {
                domain.Email = emailMerkedAsDeleted;
                domain.FacilityName = $"{domain.FacilityName} (DELETED)";

                var hasActiveRequests = domain.FacilityLocations.Any(v => v.Requests.Any(c =>
                   c.Status == (int)ERequestStatus.Active
                || c.Status == (int)ERequestStatus.NeedsApproval
                || c.Status == (int)ERequestStatus.Filled
                || c.Status == (int)ERequestStatus.InProgress));

                if (hasActiveRequests)
                {
                    result.Success = false;
                    result.Messages.Add($"Facility can not be Deleted because it has request(s) with status Active or Needs Approval or Filled or InProgress.");
                    return result;
                }
            }

            domain.Status = (int) newStatus;
            domain.StatusChangedDate = DateTime.UtcNow;
            domain.StatusChangedBy = (int)Helper.EActor.Admin;
            

            _facilityRepository.Update(domain);
            _facilityRepository.SaveChanges();

            if (user != null)
            {
                user.Status = (int)newStatus;

                if (newStatus == Helper.EState.Deleted)
                {
                    user.Email = emailMerkedAsDeleted;
                    user.UserName = emailMerkedAsDeleted;
                }

                _userManagerFactory.GetUserManager().Update(user);
                OnStatusChange(domain, oldStatus);
            }

            return result;
        }

        private void OnStatusChange(Facility office, Helper.EState oldState)
        {
            var body = string.Format(EmailConstants.ChangeStatusBody, oldState.ToString(), ((Helper.EState)office.Status).ToString());
            var subject = EmailConstants.ChangeStatusSubject;

            if (((Helper.EState)office.Status) == Helper.EState.Active)
            {
                body = string.Format(EmailConstants.ApproveFacilityAccountBody, Helper.GetLoginUrl());
                subject = EmailConstants.ApproveFacilityAccountSubject;
            }

            _emailService.AddEmail(new EmailDto
            {
                Body = body,
                Subject = subject,
                To = office.Email
            });
        }

        public FacilityDto GetProfile(string email)
        {
            var facility = _facilityRepository.GetByEmail(email);
            if (facility == null)
                throw new Exception($"GetProfile::facility is null with email = {email}");

            return GetFacility(facility.FacilityId);
        }

        private FacilitySearchDto PrepareSearchModel(FacilitySearchDto dto)
        {
            if (dto?.PhoneNumber != null)
            {
                dto.PhoneNumber = dto.PhoneNumber.TrimPhoneNumber();
            }

            return dto;
        }

        public IdLabel[] GetFacilityNames(EFacilityNameFor eFacilityNameFor)
        {
            if (eFacilityNameFor == EFacilityNameFor.FacilitiesList)
                return _facilityRepository.GetFacilityNamesForFacilitiesList();

            if (eFacilityNameFor == EFacilityNameFor.RequestsList)
                return _facilityRepository.GetFacilityNamesForRequestsList();

            return _facilityRepository.GetFacilityNamesForCreateEditRequest();
        }
        
        public string[] GetFacilityEmails()
        {
            return _facilityRepository.GetFacilityEmails();
        }
        
        public string[] GetFacilityPhoneNumbers()
        {
            return _facilityRepository.GetFacilityPhoneNumbers();
        }
        
        public string[] GetFacilityCities()
        {
            return _facilityRepository.GetFacilityCities();
        }
        
        private void AddOrUpdateSoftwareNames(FacilityLocation facilityLocation)
        {
            _xraySoftwareService.AddOrUpdate(facilityLocation.XRaySoftware);
            _chartingSoftwareService.AddOrUpdate(facilityLocation.ChartingSoftware);
        }

        public FacilityLocationDto GetFacilityLocationDto(int? facilityId, int? facilityLocationId, bool isAdmin, string facilityEmail)
        {
            var facility = _facilityRepository.GetById(facilityId);

            if (isAdmin == false)
            {
                if(facility.Email.ToLower().Trim() != facilityEmail.ToLower().Trim())
                    throw new Exception($"GetFacilityLocationDto:: access is denied");

                if (facilityLocationId.HasValue && facility.FacilityLocations.All(c => c.FacilityLocationId != facilityLocationId))
                    throw new Exception($"GetFacilityLocationDto:: access is denied");
            }

            if(facilityLocationId.HasValue)
            {
                var facilityLocation = facility.FacilityLocations
                    .Where(c => c.LocationStatus == (int)EFacilityLocationStatus.Active && c.FacilityLocationId == facilityLocationId.Value)
                    .FirstOrDefault();

                if(facilityLocation == null)
                    throw new Exception($"GetFacilityLocationDto:: can not get facility location for facility id = {facilityId}. Facility Location Id = {facilityLocationId} ");

                return _facilityMapper.MapToFacilityLocationDto(facilityLocation);
            }

            if (facilityId.HasValue)
            {
                var dto = new FacilityLocationDto();
                dto.FacilityId = facilityId;
                return dto;
            }

            throw new Exception($"GetFacilityLocationDto:: can not get facility location. FacilityId = {facilityId}, facilityLocationId = {facilityLocationId}");
        }

        public List<RequestFacilityLocationsDto> GetFacilityLocations(int facilityId)
        {
            if (facilityId == 0)
                throw new Exception("GetFacilityLocations:: facility id is 0");

            var locations = _facilityRepository.GetFacilityLocations(facilityId);
            if (locations.Any())
            {
                return locations.Select(_facilityMapper.MapToRequestFacilityLocationsDto).ToList();
            }

            throw new Exception($"GetFacilityLocations:: not locations for facility id {facilityId}");
        }

        public ValidationStatus DeleteLocation(int locationId, bool isAdminDeliting, string facilityEmail)
        {
            _logger.Info($"DeleteLocation:: locationId {locationId}, isAdminDeliting {isAdminDeliting}, facilityEmail {facilityEmail}");

            var result = new ValidationStatus() { Success = true };

            var location = _facilityRepository.GetByIdFacilityLocation(locationId);

            if(location == null)
            {
                return result.FailedValidation($"Location is null with id = {locationId}");   
            }

            var facility = location.Facility;

            if(isAdminDeliting == false && facilityEmail.ToLower().Trim() != facility.Email.ToLower().Trim())
            {
                return result.FailedValidation($"You can not delete location from different facility");
            }

            var hasLocationActiveRequests = location.Requests.Where(c => c.Status == (int)ERequestStatus.Draft ||
                                                                    c.Status == (int)ERequestStatus.Active ||
                                                                    c.Status == (int)ERequestStatus.NeedsApproval ||
                                                                     c.Status == (int)ERequestStatus.Filled ||
                                                                    c.Status == (int)ERequestStatus.InProgress);

            if (hasLocationActiveRequests.Any())
            {
                var requestIds = string.Join(", ", hasLocationActiveRequests.Select(c => c.RequestId));
                return result.FailedValidation($"The location can not be deleted. This location has requests with status (Draft or Active or Needs Approval or Filled or In Progress). " +
                    $"Please delete or wait to be completed those requests (request ids: {requestIds}).");
            }

            location.LocationStatus = (int)EFacilityLocationStatus.Deleted;

            _facilityRepository.UpdateFacilityLocation(location);

            return result;
        }

        public void SendBulkEmails(FacilityEmailLightDto dto)
        {
            var facilities = _facilityRepository.GetFacilities(PrepareSearchModel(new FacilitySearchDto()
            {
                Email = dto.FacilitySearchDto.Email,
                FacilityName = dto.FacilitySearchDto.FacilityName,
                PhoneNumber = dto.FacilitySearchDto.PhoneNumber,
                City = dto.FacilitySearchDto.City,
                FacilityStatuses = dto.FacilitySearchDto.FacilityStatuses,
                Order = dto.FacilitySearchDto.Order
            }));

            _logger.Info($"{nameof(SendBulkEmails)}:: emails = {string.Join(", ", facilities.Select(c => c.Email))}");

            foreach (var facility in facilities)
            {
                _emailService.AddEmail(new EmailDto()
                {
                    Body = dto.EmailLightDto.Body,
                    Subject = dto.EmailLightDto.Subject,
                    To = facility.Email
                });
            }
        }
    }

    public interface IFacilityService
    {
        int SubmitRegistrationForm(FacilityRegistrationDto dto);
        int AddFacility(FacilityDto dto);
        ListFacilityDto GetListFacilityDtos(FacilitySearchDto dto);
        FacilityDto GetFacility(int? facilityId);
        void UpdateFacility(FacilityDto dto);
        ValidationStatus SetStatus(int id, Helper.EState newStatus);
        FacilityDto GetProfile(string email);
        IdLabel[] GetFacilityNames(EFacilityNameFor eFacilityNameFor);
        string[] GetFacilityEmails();
        string[] GetFacilityPhoneNumbers();
        string[] GetFacilityCities();
        ValidationStatus SubmitFacilityLocationForm(FacilityLocationDto dto);
        FacilityLocationDto GetFacilityLocationDto(int? facilityId, int? facilityLocationId, bool isAdminDeliting, string facilityEmail);
        List<RequestFacilityLocationsDto> GetFacilityLocations(int facilityId);
        ValidationStatus DeleteLocation(int locationId, bool isAdminDeliting, string facilityEmail);
        void SendBulkEmails(FacilityEmailLightDto dto);
    }
}