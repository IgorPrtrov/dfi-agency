﻿using System.Collections.Generic;
using System.Linq;
using DFI.Data.Requests;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Facilities
{
    public class FacilityRepository : Repository<Facility>, IFacilityRepository
    {
        private readonly IApplicationDbContext _context;
        public FacilityRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Facility> GetFacilities(FacilitySearchDto dto)
        {
            var facilities = _context.Facilities.Where(c => c.Status != (int)Helper.EState.New && c.Status != (int)Helper.EState.Deleted).AsQueryable();

            if (!string.IsNullOrEmpty(dto.Email))
                facilities = facilities.Where(c => c.Email.ToLower().Contains(dto.Email.ToLower()));

            if (!string.IsNullOrEmpty(dto.FacilityName))
                facilities = facilities.Where(c => c.FacilityName.ToLower().Contains(dto.FacilityName.ToLower()));

            if (!string.IsNullOrEmpty(dto.PhoneNumber))
                facilities = facilities.Where(c => c.FacilityLocations.Any(b => b.PhoneNumber.ToLower().Contains(dto.PhoneNumber.ToLower())));

            if (!string.IsNullOrEmpty(dto.City))
                facilities = facilities.Where(c => c.FacilityLocations.Any(v => v.City.ToLower().Contains(dto.City.ToLower())));

            if (dto.FacilityStatuses.Any())
                facilities = facilities.Where(c => dto.FacilityStatuses.Contains(c.Status));

            switch (dto.FacilityColumn)
            {
                case EFacilityColumn.Email:
                    facilities = dto.Order == Helper.EOrder.Asc
                        ? facilities.OrderBy(c => c.Email)
                        : facilities.OrderByDescending(c => c.Email);
                    break;

                case EFacilityColumn.FacilityName:
                    facilities = dto.Order == Helper.EOrder.Asc
                        ? facilities.OrderBy(c => c.FacilityName)
                        : facilities.OrderByDescending(c => c.FacilityName);
                    break;

                case EFacilityColumn.PhoneNumber:
                    facilities = dto.Order == Helper.EOrder.Asc
                        ? facilities.OrderBy(c => c.FacilityLocations.FirstOrDefault().PhoneNumber)
                        : facilities.OrderByDescending(c => c.FacilityLocations.FirstOrDefault().PhoneNumber);
                    break;

                case EFacilityColumn.City:
                    facilities = dto.Order == Helper.EOrder.Asc
                        ? facilities.OrderBy(c => c.FacilityLocations.FirstOrDefault().City)
                        : facilities.OrderByDescending(c => c.FacilityLocations.FirstOrDefault().City);
                    break;

                case EFacilityColumn.FacilityStatus:
                    facilities = dto.Order == Helper.EOrder.Asc
                        ? facilities.OrderBy(c => c.Status)
                        : facilities.OrderByDescending(c => c.Status);
                    break;
                default:
                    facilities = facilities.OrderByDescending(c => c.FacilityId);
                    break;
            }

            return facilities.ToList();
        }

        public Facility GetByEmail(string email)
        {
            return _context.Facilities.FirstOrDefault(c => c.Email.ToLower() == email.ToLower());
        }

        public Facility GetByName(string name)
        {
            return _context.Facilities.FirstOrDefault(c => c.FacilityName.ToLower() == name.ToLower());
        }

        public List<Facility> GetActiveFacilities()
        {
            return _context.Facilities.Where(c => c.Status == (int) Helper.EState.Active).ToList();
        }

        public IdLabel[] GetFacilityNamesForFacilitiesList()
        {
            return _context.Facilities.Where(c => c.Status != (int)Helper.EState.New && c.Status != (int)Helper.EState.Deleted)
                .Select(c => new IdLabel() { id = c.FacilityId, label = c.FacilityName }).Distinct().ToArray();           
        }

        public IdLabel[] GetFacilityNamesForRequestsList()
        {
            return _context.Requests.Where(c => c.Status != (int) ERequestStatus.Deleted)
                .Select(c => new IdLabel() { id = c.FacilityLocation.FacilityId, label = c.FacilityLocation.Facility.FacilityName }).Distinct().ToArray();
        }

        public IdLabel[] GetFacilityNamesForCreateEditRequest()
        {
            return _context.Facilities.Where(c => c.Status == (int)Helper.EState.Active).Select(c => new IdLabel() {id = c.FacilityId, label = c.FacilityName })
                .Distinct().ToArray();
        }

        public string[] GetFacilityEmails()
        {
            return _context.Facilities.Select(c => c.Email)
                .GroupBy(c => c).Select(c => c.Key).ToArray(); 
        }        
        
        public string[] GetFacilityPhoneNumbers()
        {
            return _context.Facilities.Select(c => c.FacilityLocations.FirstOrDefault().PhoneNumber)
                .GroupBy(c => c).Select(c => c.Key).ToArray(); 
        }        
        
        public string[] GetFacilityCities()
        {
            return _context.Facilities.SelectMany(c => c.FacilityLocations.Select(b => b.City))
                .GroupBy(c => c).Select(c => c.Key).ToArray(); 
        }

        public FacilityLocation GetByIdFacilityLocation(int id)
        {
            return _context.FacilityLocations.FirstOrDefault(c => c.FacilityLocationId == id && c.LocationStatus == (int)EFacilityLocationStatus.Active);
        }

        public void AddFacilityLocation(FacilityLocation domain)
        {
            _context.FacilityLocations.Add(domain);
            _context.SaveChanges();
        }

        public void UpdateFacilityLocation(FacilityLocation domain)
        {
            _context.SetModified(domain);
            _context.SaveChanges();
        }

        public List<FacilityLocation> GetFacilityLocations(int facilityId)
        {
            return _context.Facilities.FirstOrDefault(c => c.FacilityId == facilityId)
                ?.FacilityLocations.Where(c => c.LocationStatus == (int)EFacilityLocationStatus.Active).ToList();
        }
    }

    public interface IFacilityRepository : IRepository<Facility>
    {
        List<Facility> GetFacilities(FacilitySearchDto dto);
        Facility GetByEmail(string email);
        Facility GetByName(string name);
        List<Facility> GetActiveFacilities();
        IdLabel[] GetFacilityNamesForFacilitiesList();
        IdLabel[] GetFacilityNamesForRequestsList();
        IdLabel[] GetFacilityNamesForCreateEditRequest();
        string[] GetFacilityEmails();
        string[] GetFacilityPhoneNumbers();
        string[] GetFacilityCities();
        FacilityLocation GetByIdFacilityLocation(int id);
        void UpdateFacilityLocation(FacilityLocation domain);
        void AddFacilityLocation(FacilityLocation domain);
        List<FacilityLocation> GetFacilityLocations(int facilityId);
    }
}