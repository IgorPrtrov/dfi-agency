﻿using System;
using System.Collections.Generic;
using System.Linq;
using DFI.Data.Documents;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Facilities
{
    public class FacilityMapper : IFacilityMapper
    {
        private readonly IDocumentMapper _documentMapper;

        public FacilityMapper(IDocumentMapper documentMapper)
        {
            _documentMapper = documentMapper;
        }

        public Facility MapToFacility(FacilityDto dto)
        {
            var facility = new Facility
            {
                FacilityName = dto.FacilityName,
                Email = dto.Email,
                BillingEmail = dto.BillingEmail,   
                PayType = dto.PayType,
                Status = dto.FacilityStatus,
                AddDate = DateTime.Now.ToUniversalTime(),
            };

            if (dto.Photo != null)
            {
                facility.Documents = new List<Document>()
                {
                    Helper.GetDocument(dto.Photo, EDocumentType.Photo)
                };
            }

            return facility;
        }

        public FacilityShortDto MapToFacilityShortDto(Facility domain)
        {
            return new FacilityShortDto
            {
                FacilityId = domain.FacilityId,
                Email = domain.Email,
                FacilityName = domain.FacilityName,
                PhoneNumber = domain.FacilityLocations?.FirstOrDefault()?.PhoneNumber.AsPhoneNumber(),
                City = domain.FacilityLocations?.FirstOrDefault()?.City,
                FacilityStatus = Enum.GetName(typeof(Helper.EState), domain.Status)
            };
        }

        public FacilityDto MapToFacilityDto(Facility domain)
        {
            var dto = new FacilityDto
            {
                Id = domain.FacilityId,
                Email = domain.Email,
                PayType = domain.PayType,
                BillingEmail = domain.BillingEmail,
                FacilityName = domain.FacilityName,
                FacilityStatus = domain.Status,
                DocumentDtos = domain.Documents.Select(c => _documentMapper.MapToDocumentDto(c)).ToList(),
                ShortLocations = domain.FacilityLocations.Where(c => c.LocationStatus == (int)EFacilityLocationStatus.Active).Select(MapToShortLocationDto).ToList()
            };
            return dto;
        }

        public Facility MapToFacility(Facility domain, FacilityDto dto)
        {
            domain.FacilityName = dto.FacilityName;
            domain.Email = dto.Email;
            domain.BillingEmail = dto.BillingEmail;
            domain.PayType = dto.PayType;
            domain.Status = dto.FacilityStatus;
            domain.DateOfModified = DateTime.Now.ToUniversalTime();
            if (dto.Photo != null)
            {
                if (dto.DocumentDtos.Any(c => c.Type == EDocumentType.Photo.ToString()))
                    domain.Documents.Remove(
                        domain.Documents.FirstOrDefault(c => dto.DocumentDtos.Any(x => x.Type == EDocumentType.Photo.ToString() && x.Id == c.DocumentId)));

                domain.Documents.Add(Helper.GetDocument(dto.Photo, EDocumentType.Photo));
            }

            return domain;
        }
        
        public FacilityHourDto MapToFacilityHourDto(FacilityHour domain)
        {
            return new FacilityHourDto
            {
                Id = domain.FacilityHourId,
                DayOfWeek = domain.DayOfWeek,
                RangeHours = domain.RangeHours,
                FacilityLocationId = domain.FacilityLocationId ?? 0
            };
        }
        
        public FacilityHour MapToFacilityHour(FacilityHourDto dto)
        {
            return new FacilityHour
            {
                FacilityHourId = dto.Id,
                DayOfWeek = dto.DayOfWeek,
                RangeHours = dto.RangeHours,
                FacilityLocationId = dto.FacilityLocationId
            };
        }

        public FacilityLocation MapToFacilityLocation(FacilityLocation domain, FacilityLocationDto dto)
        {
            domain.AddressLine1 = dto.AddressFromGoogle.Split(',')[0].Trim();
            domain.Apartment = dto.Apartment;
            domain.City = dto.AddressFromGoogle.Split(',')[1].Trim();
            domain.StateName = dto.AddressFromGoogle.Split(',')[2].Trim();
            domain.Zipcode = dto.AddressFromGoogle.Split(',')[3].Trim();
            domain.FullAddress = dto.AddressFromGoogle;
            domain.DoctorsName = dto.DoctorsName;
            domain.WebsiteAddress = dto.WebsiteAddress;
            domain.ChartingSoftware = dto.ChartingSoftware;
            domain.XRaySoftware = dto.XRaySoftware;
            domain.AllowedAdultProphylaxisTime = dto.AllowedAdultProphylaxisTime;
            domain.AllowedChildProphylaxisTime = dto.AllowedChildProphylaxisTime;
            domain.PaidEndOfTheDay = dto.PaidEndOfTheDay;
            domain.AdditionalInfo = dto.AdditionalInfo;
            domain.PhoneNumber = dto.PhoneNumber;
            domain.ModifiedOn = DateTime.UtcNow;
            domain.ModifiedBy = dto.WhoModified;

            return domain;
        }

        public FacilityLocation MapToFacilityLocation(FacilityLocationDto dto)
        {
            var facility = new FacilityLocation
            {
                FacilityId = dto.FacilityId.Value,
                PhoneNumber = dto.PhoneNumber,
                AddressLine1 = dto.AddressFromGoogle.Split(',')[0].Trim(),
                Apartment = dto.Apartment,
                City = dto.AddressFromGoogle.Split(',')[1].Trim(),
                StateName = dto.AddressFromGoogle.Split(',')[2].Trim(),
                Zipcode = dto.AddressFromGoogle.Split(',')[3].Trim(),
                FullAddress = dto.AddressFromGoogle,

                DoctorsName = dto.DoctorsName,
                WebsiteAddress = dto.WebsiteAddress,
                ChartingSoftware = dto.ChartingSoftware,
                XRaySoftware = dto.XRaySoftware,
                AllowedAdultProphylaxisTime = dto.AllowedAdultProphylaxisTime,
                AllowedChildProphylaxisTime = dto.AllowedChildProphylaxisTime,
                PaidEndOfTheDay = dto.PaidEndOfTheDay,
                AdditionalInfo = dto.AdditionalInfo,

                ModifiedOn = DateTime.UtcNow,
                ModifiedBy = dto.WhoCreated,
                CreatedOn = DateTime.UtcNow,
                CreatedBy = dto.WhoCreated,
                LocationStatus = (int)EFacilityLocationStatus.Active,

                FacilityHours = dto.FacilityHours.Select(c =>
                    new FacilityHour
                    {
                        FacilityHourId = c.Id,
                        DayOfWeek = c.DayOfWeek,
                        RangeHours = c.RangeHours
                    })
                    .ToList()
            };

            return facility;
        }

        public FacilityLocationDto MapToFacilityLocationDto(FacilityLocation domain)
        {
            var dto = new FacilityLocationDto
            {
                Id = domain.FacilityLocationId,
                FacilityId = domain.FacilityId,
                PhoneNumber = domain.PhoneNumber,
                Apartment = domain.Apartment,
                AddressFromGoogle = domain.FullAddress,
                DoctorsName = domain.DoctorsName,
                WebsiteAddress = domain.WebsiteAddress,
                ChartingSoftware = domain.ChartingSoftware,
                XRaySoftware = domain.XRaySoftware,
                AllowedAdultProphylaxisTime = domain.AllowedAdultProphylaxisTime,
                AllowedChildProphylaxisTime = domain.AllowedChildProphylaxisTime,
                PaidEndOfTheDay = domain.PaidEndOfTheDay,
                AdditionalInfo = domain.AdditionalInfo,
                IsOnlyOne = domain.Facility.FacilityLocations.Where(c => c.LocationStatus == (int)EFacilityLocationStatus.Active).ToList().Count <= 1
            };

            if (domain.FacilityHours.Any())
                dto.FacilityHours = domain.FacilityHours.Select(MapToFacilityHourDto).ToList();

            return dto;
        }

        public RequestFacilityLocationsDto MapToRequestFacilityLocationsDto(FacilityLocation domain)
        {
            return new RequestFacilityLocationsDto()
            {
                FacilityLocationId = domain.FacilityLocationId,
                FullAddress = domain.FullAddress
            };
        }

        public ShortLocationDto MapToShortLocationDto(FacilityLocation domain)
        {
            return new ShortLocationDto
            {
                LocationId = domain.FacilityLocationId,
                FullAddress = domain.FullAddress,
                DoctorName = domain.DoctorsName
            };
        }
    }

    public interface IFacilityMapper
    {
        Facility MapToFacility(FacilityDto dto);
        FacilityShortDto MapToFacilityShortDto(Facility domain);
        FacilityDto MapToFacilityDto(Facility domain);
        Facility MapToFacility(Facility domain, FacilityDto dto);
        FacilityHourDto MapToFacilityHourDto(FacilityHour domain);
        FacilityHour MapToFacilityHour(FacilityHourDto dto);
        FacilityLocation MapToFacilityLocation(FacilityLocation domain, FacilityLocationDto dto);
        FacilityLocation MapToFacilityLocation(FacilityLocationDto dto);
        FacilityLocationDto MapToFacilityLocationDto(FacilityLocation domain);
        RequestFacilityLocationsDto MapToRequestFacilityLocationsDto(FacilityLocation domain);
        ShortLocationDto MapToShortLocationDto(FacilityLocation domain);
    }
}