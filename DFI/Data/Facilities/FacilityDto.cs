﻿using DFI.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using DFI.Data.Documents;
using DFI.Helpers;
using PagedList;
using System;
using DFI.Data.Emails;

namespace DFI.Data.Facilities
{
    public class FacilityRegistrationDto
    {
        [Required(ErrorMessage = "Password is required")]
        [StringLength(15, ErrorMessage = "Must be between 6 and 15 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public string CallbackUrl { get; set; }
        public FacilityDto FacilityDto { get; set; }

        public FacilityRegistrationDto()
        {
            FacilityDto = new FacilityDto();
        }
    }

    public class FacilityDto : IValidatableObject
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(254, ErrorMessage = "Email cannot be longer than 254 characters.")]
        public string Email { get; set; }

        public string NewEmail { get; set; }

        [Required(ErrorMessage = "Billing Email is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(254, ErrorMessage = "Email cannot be longer than 254 characters.")]
        public string BillingEmail { get; set; }

        [Required(ErrorMessage = "Office Name is required")]
        [StringLength(200, ErrorMessage = "Office Name cannot be longer than 200 characters.")]
        public string FacilityName { get; set; }

        [Required(ErrorMessage = "Pay Type is required")]
        [RegularExpression("^(1099|W4)$", ErrorMessage = "Invalid Pay Type. Choose either 1099 or W4.")]
        public string PayType { get; set; } = "1099"; // Default value

        public int FacilityStatus { get; set; }

        public HttpPostedFileBase Photo { get; set; }

        public List<DocumentDto> DocumentDtos { get; set; }

        public List<ShortLocationDto> ShortLocations { get; set; }

        public FacilityDto()
        {
            DocumentDtos = new List<DocumentDto>();
            ShortLocations = new List<ShortLocationDto>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var maxSize = 5242880;
            if (Photo != null)
            {
                var photoExtension = Path.GetExtension(Photo.FileName).ToLower(); ;
                if (photoExtension != ".jpg" && photoExtension != ".jpeg" && photoExtension != ".png")
                    yield return new ValidationResult("Photo must be JPG, GIF or PNG.");

                if (Photo.ContentLength > maxSize)
                    yield return new ValidationResult("Photo must be less than 5MB.");
            }

            var context = new ApplicationDbContext();
            if (Id == null)
            {
                if (context.Facilities.Any(c => c.Email.Trim().ToLower() == Email.Trim().ToLower()))
                    yield return new ValidationResult("Email already exists");
            }
            else
            {
                if (context.Facilities.Where(c => c.FacilityId != Id).Any(x => x.Email.Trim().ToLower() == Email.Trim().ToLower()))
                    yield return new ValidationResult("Email already exists");
            }

            if (!Helper.IsValidEmail(Email))
                yield return new ValidationResult("Email is not valid");

            if (!Helper.IsValidEmail(BillingEmail))
                yield return new ValidationResult("BillingEmail is not valid");
        }
    }

    public class FacilityLocationDto : IValidatableObject
    {
        public int? Id { get; set; }
        public int? FacilityId { get; set; }

        public string WhoCreated { get; set; }
        public string WhoModified { get; set; }

        [StringLength(100, ErrorMessage = "Apartment cannot be longer than 1000 characters.")]
        public string Apartment { get; set; }

        public string AddressFromGoogle { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = ValidationConstants.PhoneNumberValidationError)]
        [StringLength(15, ErrorMessage = "Phone number cannot be longer than 15 characters.")]
        public string PhoneNumber { get; set; }

        [StringLength(1000, ErrorMessage = "Doctors Name cannot be longer than 1000 characters.")]
        public string DoctorsName { get; set; }

        [Required(ErrorMessage = "Website Address is required")]
        [Url(ErrorMessage = "Please enter a valid URL. Copy URL from browser. Example https://trello.com")]
        [StringLength(500, ErrorMessage = "Website Address cannot be longer than 500 characters.")]
        public string WebsiteAddress { get; set; }

        [Required(ErrorMessage = "Charting Software is required")]
        public string ChartingSoftware { get; set; }

        [Required(ErrorMessage = "X-Ray Software is required")]
        public string XRaySoftware { get; set; }

        [Required(ErrorMessage = "Allowed Adult Prophylaxis Time is required")]
        [StringLength(6, ErrorMessage = "Allowed Adult Prophylaxis Time cannot be longer than 6 characters.")]
        public string AllowedAdultProphylaxisTime { get; set; }

        [Required(ErrorMessage = "Allowed Child Prophylaxis Time is required")]
        [StringLength(6, ErrorMessage = "Allowed Child Prophylaxis Time cannot be longer than 6 characters.")]
        public string AllowedChildProphylaxisTime { get; set; }

        [Required(ErrorMessage = "Paid End Of The Day is required")]
        public string PaidEndOfTheDay { get; set; }

        [StringLength(1000, ErrorMessage = "Additional Info cannot be longer than 1000 characters.")]
        public string AdditionalInfo { get; set; }
        public bool IsOnlyOne { get; set; }

        public List<FacilityHourDto> FacilityHours { get; set; }

        public FacilityLocationDto()
        {
            FacilityHours = new List<FacilityHourDto>();

            Enum.GetNames(typeof(EDayOfWeek)).ToList().ForEach(
                c => FacilityHours.Add(new FacilityHourDto { DayOfWeek = c }));
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(AddressFromGoogle))
            {
                yield return new ValidationResult("FIll in again your address manually. Google map can not find it.");
            }
            else
            {
                var address = AddressFromGoogle.Split(',');

                var isValidGoogleAddress = true;

                if (string.IsNullOrEmpty(address[0].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("Address is not valid");
                }

                if (string.IsNullOrEmpty(address[1].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("City is not valid");
                }

                if (string.IsNullOrEmpty(address[2].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("State Name is not valid");
                }

                if (string.IsNullOrEmpty(address[3].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("Zipcode is not valid");
                }

                if (isValidGoogleAddress == false)
                    yield return new ValidationResult("Please enter your address manually again");
            }
        }
    }

    public class FacilityHourDto
    {
        public int Id { get; set; }

        public string DayOfWeek { get; set; }

        [Required(ErrorMessage = "Range Hours is required")]
        [StringLength(1000, ErrorMessage = "Range Hours cannot be longer than 1000 characters.")]
        public string RangeHours { get; set; }

        public int FacilityLocationId { get; set; }
    }

    public enum EPay
    {
        [Display(Name = "Mailed, received within 2 weeks")]
        Mailed,

        [Display(Name = "Payroll")]
        Payroll,

        [Display(Name = "Paid day of")]
        PaidDayOf
    }

    public class ListFacilityDto
    {
        public IPagedList<FacilityShortDto> FacilityShortDtos { get; set; }
    }

    public class FacilityShortDto
    {
        public int FacilityId { get; set; }

        public string Email { get; set; }

        public string FacilityName { get; set; }

        public string PhoneNumber { get; set; }

        public string City { get; set; }

        public string FacilityStatus { get; set; }
    }

    public class FacilitySearchDto
    {
        public FacilitySearchDto()
        {
            FacilityStatuses = new List<int>();
        }

        public Helper.EOrder Order { get; set; }

        public EFacilityColumn FacilityColumn { get; set; }

        public string Email { get; set; }

        public string FacilityName { get; set; }

        public string PhoneNumber { get; set; }

        public string City { get; set; }

        public List<int> FacilityStatuses { get; set; }

        public int? Page { get; set; }
    }

    public class RequestFacilityLocationsDto
    {
        public int FacilityLocationId { get; set; }
        public string FullAddress { get; set; }
    }

    public class ShortLocationDto 
    {
        public int LocationId { get; set; }
        public string FullAddress { get; set; }
        public string DoctorName { get; set; }
    }


    public enum EFacilityColumn
    {
        Email,
        FacilityName,
        PhoneNumber,
        City,
        FacilityStatus
    }

    public enum EFacilityNameFor
    {
        FacilitiesList,
        RequestsList,
        CreateEditRequest
    }

    public enum EFacilityLocationStatus 
    {
        Active = 1,
        Deleted
    }

    public class FacilityEmailLightDto
    {
        public FacilitySearchDto FacilitySearchDto { get; set; }
        public EmailLightDto EmailLightDto { get; set; }

        public FacilityEmailLightDto()
        {
            FacilitySearchDto = new FacilitySearchDto();
            EmailLightDto = new EmailLightDto();
        }
    }
}