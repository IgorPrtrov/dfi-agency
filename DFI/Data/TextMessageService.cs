﻿using System;
using DFI.Helpers;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace DFI.Data.TextMessages
{
    public class TextMessageService : ITextMessageService
    {
        private readonly ITextMessageRepository _textMessageRepository;
        private readonly ITextMessageMapper _textMessageMapper;
        private readonly ILogger _logger;

        public TextMessageService(ITextMessageRepository textMessageRepository, ITextMessageMapper textMessageMapper, ILogger logger)
        {
            _textMessageRepository = textMessageRepository;
            _textMessageMapper = textMessageMapper;
            _logger = logger;
            TwilioClient.Init(Setting.TwilioAccountSit, Setting.TwilioAuthToken);
        }

        public void SendSms()
        {
            var textMessages = _textMessageRepository.GetSMSForDelivery();

            foreach (var sms in textMessages)
            {
                try
                {
                    var hasPhoneNumberAlias = Setting.HasPhoneNumberFromAlias;
                    var phoneNumberFromAlias = Setting.PhoneNumberFromAlias;
                    var isActiveSendTextMessages = Setting.SendTextMessages;

                    _logger.Info($"Text Messages Send:: hasPhoneNumberAlias: {hasPhoneNumberAlias}");
                    _logger.Info($"Text Messages Send:: phoneNumberFromAlias: {phoneNumberFromAlias}");
                    _logger.Info($"Text Messages Send:: isActiveSendTextMessages: {isActiveSendTextMessages}");

                    if(isActiveSendTextMessages == false)
                    {
                        sms.SentDate = DateTime.Now.ToUniversalTime();
                        sms.IsSent = true;
                        _textMessageRepository.Update(sms);
                        _textMessageRepository.SaveChanges();

                        _logger.Info($"Text Messages Send:: Text Messages id {sms.TextMessageId} has been saved as delivered in DB but really it was not sent");
                        return;
                    }

                    var phoneNumberTo = hasPhoneNumberAlias && !string.IsNullOrEmpty(phoneNumberFromAlias) ? phoneNumberFromAlias : sms.PhoneNumberTo;
                    _logger.Info($"Text Messages Send:: phoneNumberTo: {phoneNumberTo}");


                    if(_textMessageRepository.IsNotSentSms(sms.TextMessageId))
                    {

                        var response = MessageResource.Create(
                            from: new PhoneNumber(sms.PhoneNumberFrom),
                            to: new PhoneNumber(phoneNumberTo),
                            body: sms.Body);


                        if (response.Status == MessageResource.StatusEnum.Sent)
                        {
                            sms.SentDate = DateTime.Now.ToUniversalTime();
                            sms.IsSent = true;
                            _logger.Info(
                                $"TextMessageService:: Send Successfully. id = {sms.TextMessageId}, PhoneNumberTo = {sms.PhoneNumberTo}," +
                                $" Status = {response.Status}");
                        }
                        else
                        {
                            _logger.Warn($"TextMessageService:: Send not good. id = {sms.TextMessageId}, PhoneNumberTo = {sms.PhoneNumberTo}," +
                                         $" Status = {response.Status}, error = {response.ErrorMessage}, error code = {response.ErrorCode}");
                        }             
                                      
                        sms.Attempts ++;
                    }
                }
                catch (Exception e)
                {
                    _logger.Error($"Text message Send:: Exception: {e.Message}");
                    sms.Attempts ++;
                }

                _textMessageRepository.Update(sms);
                _textMessageRepository.SaveChanges();
            }
        }

        public void AddSms(TextMessageDto dto)
        {
            var email = _textMessageMapper.MapToTextMessage(dto);
            _textMessageRepository.Add(email);
            _textMessageRepository.SaveChanges();          
        }
    }

    public interface ITextMessageService
    {
        void SendSms();
        void AddSms(TextMessageDto dto);
    }
}