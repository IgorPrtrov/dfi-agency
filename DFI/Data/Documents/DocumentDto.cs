namespace DFI.Data.Documents
{
    public class DocumentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] Content { get; set; }
        public string MimeType { get; set; }
    }
}