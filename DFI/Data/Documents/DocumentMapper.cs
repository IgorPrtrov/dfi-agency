using System;
using System.Web;
using DFI.Helpers;
using DFI.Models;


namespace DFI.Data.Documents
{
    public class DocumentMapper : IDocumentMapper
    {
        public DocumentDto MapToDocumentDto(Document domain, bool needContent = false)
        {
            var dto = new DocumentDto
            {
                Id = domain.DocumentId,
                Name = domain.Name,
                Type = Enum.GetName(typeof(EDocumentType), domain.DocumentType),
                MimeType = MimeMapping.GetMimeMapping(domain.Name)
            };

            if (needContent)
                dto.Content = domain.Data;

            return dto;
        }
    }
}