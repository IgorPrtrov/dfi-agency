using DFI.Models;

namespace DFI.Data.Documents
{
    public interface IDocumentMapper
    {
        DocumentDto MapToDocumentDto(Document domain, bool needContent = false);
    }
}