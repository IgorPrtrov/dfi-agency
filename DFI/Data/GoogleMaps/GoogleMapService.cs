﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using DFI.Helpers;
using Google.Maps;
using Google.Maps.DistanceMatrix;
using Google.Maps.Geocoding;
using Newtonsoft.Json;

namespace DFI.Data.GoogleMaps
{
    public class GoogleMapService : IGoogleMapService
    {
        private readonly ILogger _logger;

        public GoogleMapService(ILogger logger)
        {
            _logger = logger;
        }

        public LocationDto GetLocation(string address)
        {
            _logger.Info($"GetLocation:: Start. Address {address}");
            GoogleSigned.AssignAllServices(new GoogleSigned(Setting.GoogleMapsApi));

            var request = new GeocodingRequest();
            request.Address = address;
            var response = new GeocodingService().GetResponse(request);

            if (response.Status == ServiceResponseStatus.Ok && response.Results.Any())
            {
                var result = response.Results.First();

                _logger.Info($"GetLocation:: Latitude: {result.Geometry.Location.Latitude}, Longitude: {result.Geometry.Location.Longitude}");
                return new LocationDto()
                {
                    Address = result.FormattedAddress,
                    Latitude = (decimal)result.Geometry.Location.Latitude,
                    Longitude = (decimal)result.Geometry.Location.Longitude,
                    StatusCode = EStatusCode.Success
                };
            }

            _logger.Error($"GetLocation:: Unable to geocode.  Status={response.Status} and ErrorMessage={response.ErrorMessage}");
            return new LocationDto(){StatusCode = EStatusCode.Error};
        }

        public DistanceDto GetDistance(LocationDto origin, LocationDto destination)
        {
            _logger.Info($"GetLocation:: Start. Origin: {origin.Latitude} | {origin.Longitude} Destination: {destination.Latitude} | {destination.Longitude}");
            GoogleSigned.AssignAllServices(new GoogleSigned(Setting.GoogleMapsApi));

            DistanceMatrixRequest request = new DistanceMatrixRequest()
            {
                WaypointsOrigin = new List<Location> { new LatLng(origin.Latitude, origin.Longitude) },
                WaypointsDestination = new List<Location> { new LatLng(destination.Latitude, destination.Longitude) },
                Mode = TravelMode.driving,
                Units = Units.imperial
            };

            var response = new DistanceMatrixService().GetResponse(request);

            if (response.Status == ServiceResponseStatus.Ok && response.Rows.Any())
            {
                if (string.IsNullOrEmpty(response?.Rows?.First()?.Elements?.First()?.duration?.Text))
                {
                    _logger.Error($"GetDistance:: Unable to get duration.  Status={response.Status} and duration is null or empty");
                    return new DistanceDto() { StatusCode = EStatusCode.Error };
                }
                if (string.IsNullOrEmpty(response?.Rows?.First()?.Elements?.First()?.distance?.Text))
                {
                    _logger.Error($"GetDistance:: Unable to get distance.  Status={response.Status} and distance is null or empty");
                    return new DistanceDto() { StatusCode = EStatusCode.Error };
                }

                _logger.Info($"GetDistance:: Duration: {response.Rows.First().Elements.First().duration.Text}, Distance: {response.Rows.First().Elements.First().distance.Text}");
                return new DistanceDto()
                {
                    StatusCode = EStatusCode.Success,
                    DurationSec = response.Rows.First().Elements.First().duration.Value,
                    DistanceMi = (long)Math.Round(response.Rows.First().Elements.First().distance.Value / Setting.MetersInMile)
                };
            }

            _logger.Error($"GetDistance:: Unable to geocode.  Status={response.Status} and ErrorMessage={response.ErrorMessage}");
            return new DistanceDto(){StatusCode = EStatusCode.Error};
        }

        public string GetTimeZoneId(decimal latitude, decimal longitude)
        {
            _logger.Info($"GetTimeZoneId:: latitude: {latitude}, longitude: {longitude}");

            var timeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var uri = $"https://maps.googleapis.com/maps/api/timezone/json?location={latitude},{longitude}&timestamp={timeStamp}&key={Setting.GoogleMapsApi}";

            _logger.Info($"GetTimeZoneId:: uri: {uri}");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (var response = request.GetResponseAsync().Result)
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var googleJson = reader.ReadToEndAsync().Result;
                var resultObj = JsonConvert.DeserializeObject<GooleTimeZone>(googleJson);
                _logger.Info($"GetTimeZoneId:: resultObj?.TimeZoneName: {resultObj?.TimeZoneName}");

                var timeZones = TimeZoneInfo.GetSystemTimeZones();
                var locationTimeZone = timeZones.FirstOrDefault(x => x.DaylightName == resultObj.TimeZoneName);
                
                _logger.Info($"GetTimeZoneId::locationTimeZone id: {locationTimeZone?.Id}");

                return (locationTimeZone ?? CommonConstants.DefaultTimeZone).Id;
            }
        }
        
        
    }

    public interface IGoogleMapService
    {
        LocationDto GetLocation(string address);
        DistanceDto GetDistance(LocationDto origin, LocationDto destination);
        string GetTimeZoneId(decimal latitude, decimal longitude);
    }
}