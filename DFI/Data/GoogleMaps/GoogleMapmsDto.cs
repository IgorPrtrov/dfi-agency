﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFI.Data.GoogleMaps
{
    public class DistanceDto
    {
        public long DistanceMi { get; set; }
        public long DurationSec { get; set; }
        public EStatusCode StatusCode { get; set; }
    }

    public class LocationDto
    {
        public string Address { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public EStatusCode StatusCode { get; set; }
    }

    public class GooleTimeZone
    {
        public int DstOffset { get; set; }
        public int RawOffset { get; set; }
        public string Status { get; set; }
        public string TimeZoneId { get; set; }
        public string TimeZoneName { get; set; }
    }

    public enum EStatusCode
    {
        Success,
        Error,
        LicenseNotFound,
        AlreadyExists,
        AccountWasDeleted,
        ValidationGraderError
    }
}