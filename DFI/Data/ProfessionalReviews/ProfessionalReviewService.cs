using System.Collections.Generic;
using System.Linq;
using DFI.Data.Accounts;
using PagedList;
using DFI.Data.Requests;
using DFI.Data.Temps;
using DFI.Helpers;
using DFI.Factory;

namespace DFI.Data.ProfessionalReviews
{
    public class ProfessionalReviewService : IProfessionalReviewService
    {
        private readonly IProfessionalReviewRepository _professionalReviewRepository;
        private readonly IRequestRepository _requestRepository;
        private readonly ITempRepository _tempRepository;
        private readonly IProfessionalReviewMapper _professionalReviewMapper;
        private readonly IApplicationUserManagerFactory _userManagerFactory;

        public ProfessionalReviewService(
            IProfessionalReviewRepository professionalReviewRepository,
            IRequestRepository requestRepository,
            ITempRepository tempRepository,
            IProfessionalReviewMapper professionalReviewMapper, IApplicationUserManagerFactory userManagerFactory)
        {
            _professionalReviewRepository = professionalReviewRepository;
            _requestRepository = requestRepository;
            _tempRepository = tempRepository;
            _professionalReviewMapper = professionalReviewMapper;
            _userManagerFactory = userManagerFactory;
        }

        public ProfessionalReviewDto CreateInitialModel(int tempId, int requestId)
        {
            var temp = _tempRepository.GetById(tempId);
            var tempDisplayName = $"{temp.FirstName} {temp.LastName}";

            var photo = temp.Documents.FirstOrDefault(x => x.DocumentType == (int)EDocumentType.Photo);

            var request = _requestRepository.GetById(requestId);
            var firstRequestDay = request.RequestDays.OrderBy(x => x.StartDate).First().StartDate;
            var lastRequestDay = request.RequestDays.OrderBy(x => x.StartDate).Last().StartDate;
            var requestDisplayName = $"{firstRequestDay:dd-MMM-yyyy} - {lastRequestDay:dd-MMM-yyyy}";

            //move to mapper
            return new ProfessionalReviewDto
            {
                TempId = tempId,
                RequestId = requestId,
                TempDisplayName = tempDisplayName,
                Position = request.LicenseType,
                RequestDisplayName = requestDisplayName,
                TempPhoto = photo.ToBase64Resource()
            };
        }

        public ProfessionalReviewDto GetReadonlyReview(int tempId, int requestId)
        {
            var request = _requestRepository.GetById(requestId);
            if (request.ProfessionalReviews.Count == 0)
                return null;

            var temp = _tempRepository.GetById(tempId);
            var tempDisplayName = $"{temp.FirstName} {temp.LastName}";

            var photo = temp.Documents.FirstOrDefault(x => x.DocumentType == (int)EDocumentType.Photo);

            var firstRequestDay = request.RequestDays.OrderBy(x => x.StartDate).First().StartDate;
            var lastRequestDay = request.RequestDays.OrderBy(x => x.StartDate).Last().StartDate;
            var requestDisplayName = $"{firstRequestDay:dd-MMM-yyyy} - {lastRequestDay:dd-MMM-yyyy}";

            //move to mapper
            return new ProfessionalReviewDto
            {
                TempId = tempId,
                RequestId = requestId,
                TempDisplayName = tempDisplayName,
                Position = request.LicenseType,
                RequestDisplayName = requestDisplayName,
                TempPhoto = photo?.ToBase64Resource(),
                Rating = request.ProfessionalReviews.FirstOrDefault()?.Rating ?? 0,
                DidArriveInTimelyManner = request.ProfessionalReviews.FirstOrDefault()?.DidArriveInTimelyManner ?? false,
                DidPresentInProfessionalAttire = request.ProfessionalReviews.FirstOrDefault()?.DidPresentInProfessionalAttire ?? false,
                DidInteractWellWithStaff = request.ProfessionalReviews.FirstOrDefault()?.DidInteractWellWithStaff ?? false,
                DidInteractWellWithCustomers = request.ProfessionalReviews.FirstOrDefault()?.DidInteractWellWithCustomers ?? false,
                WasCompetentInDigitalChartingAndDocumentation = request.ProfessionalReviews.FirstOrDefault()?.WasCompetentInDigitalChartingAndDocumentation ?? false,
                IsBlocked = request.ProfessionalReviews.FirstOrDefault()?.IsBlocked ?? false,
                Description = request.ProfessionalReviews.FirstOrDefault()?.Description,
                IsReadonly = true
            };
        }

        public ValidationStatus HasAccessToCreateReview(int tempId, int requestId, string userEmail)
        {
            var result = new ValidationStatus() { Success = true };
            var request = _requestRepository.GetById(requestId);

            if (request == null)
            {
                result.Success = false;
                result.Messages = new List<string>() { "Request can not be found" };
                return result;
            }

            var wasAlreadyAdded = _professionalReviewRepository.WasProfessionalReviewAdded(tempId, requestId, userEmail);
            if (wasAlreadyAdded)
            {
                result.Success = false;
                result.Messages = new List<string>() { "Review is already added" };
                return result;
            }

            var hasAccess = request.RequestAssignees.Any(x => x.TempId == tempId && x.Status == (int)ERequestAssigneeStatus.Filled)
                   && request.Status == (int)ERequestStatus.Success
                   && (request.FacilityLocation.Facility.Email == userEmail || userEmail.ToLower() == Role.Admin.ToLower());

            if (hasAccess)
                return result;

            result.Success = false;
            result.Messages = new List<string>() { "Access is not granted" };
            return result;
        }

        public ValidationStatus AddReview(ProfessionalReviewDto dto, string userEmail)
        {
            var result = HasAccessToCreateReview(dto.TempId, dto.RequestId, userEmail);

            if (result.Success == false)
                return result;

            var domain = _professionalReviewMapper.MapToProfessionalReview(dto);

            if (userEmail == Role.Admin.ToString())
                userEmail = Setting.AdministratorEmail;

            var user = _userManagerFactory.GetUserManager().Users.FirstOrDefault(x => x.Email == userEmail);

            domain.SubmitterId = user.Id;

            _professionalReviewRepository.Add(domain);
            _professionalReviewRepository.SaveChanges();

            return result;
        }

        public PageProfessionalReviews GetListOfReviews(int tempId, int pageNumber)
        {           
            var reviews = _professionalReviewRepository.GetProfessionalReviews(tempId);
            var listOwnReviews = reviews.Select(c => _professionalReviewMapper.MapToOwnProfessionalReview(c)).ToList();

            int pageSize = 10;
            var result = listOwnReviews.ToPagedList(pageNumber, pageSize);

            return new PageProfessionalReviews
            {
                OwnProfessionalReviews = result
            };
        }
    }

    public interface IProfessionalReviewService
    {
        ProfessionalReviewDto CreateInitialModel(int tempId, int requestId);
        ValidationStatus HasAccessToCreateReview(int tempId, int requestId, string userEmail);
        ValidationStatus AddReview(ProfessionalReviewDto dto, string userEmail);
        ProfessionalReviewDto GetReadonlyReview(int tempId, int requestId);
        PageProfessionalReviews GetListOfReviews(int tempId, int pageNumber);
    }
}