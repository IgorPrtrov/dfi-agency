using System.Collections.Generic;
using System.Linq;
using DFI.Data.Accounts;
using DFI.Models;

namespace DFI.Data.ProfessionalReviews
{
    public class ProfessionalReviewRepository : Repository<ProfessionalReview>, IProfessionalReviewRepository
    {
        private readonly IApplicationDbContext _context;
        
        public ProfessionalReviewRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public bool WasProfessionalReviewAdded(int tempId, int requestId, string userEmail)
        {
            return _context.ProfessionalReviews.ToList().Any(x => x.TempId == tempId 
                          && x.RequestId == requestId
                          && (x.Request?.FacilityLocation.Facility?.Email == userEmail || userEmail == Role.Admin.ToString()));
        }

        public List<ProfessionalReview> GetProfessionalReviews(int tempId)
        {
            return _context.ProfessionalReviews.AsQueryable()
                .Where(c => c.TempId == tempId)
                .OrderByDescending(c => c.Id).ToList();
        }
    }
    
    public interface IProfessionalReviewRepository : IRepository<ProfessionalReview>
    {
        bool WasProfessionalReviewAdded(int tempId, int requestId, string userEmail);
        List<ProfessionalReview> GetProfessionalReviews(int tempId);
    }
}