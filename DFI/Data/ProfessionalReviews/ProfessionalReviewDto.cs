using PagedList;
using System;
using System.ComponentModel.DataAnnotations;

namespace DFI.Data.ProfessionalReviews
{
    public class ProfessionalReviewDto
    {
        public int? Id { get; set; }
        
        public int TempId { get; set; }
        
        public int RequestId { get; set; }
        
        public string RequestDisplayName { get; set; }
        
        public string Position { get; set; }
        
        public string TempDisplayName { get; set; }
        
        public string TempPhoto { get; set; }
        
        [Required]
        [Range(1, 10)]
        [Display(Name = "Overall do you feel the professional represented your company well on a scale of 1-10, 10 being the best.")]
        public int Rating { get; set; }
        
        [Display(Name = "Please check this box if you would prefer not have this professional return. This option excludes the professional from seeing and accepting any future posted assignments.")]
        public bool IsBlocked { get; set; }
        
        [DataType(DataType.MultilineText)]
        [Display(Name = "If so Please provide a brief description of why.")]
        [StringLength(1000, ErrorMessage = "Description cannot be longer than 1000 characters.")]
        public string Description { get; set; }
        
        public DateTime AddDate { get; set; }
        
        [Display(Name = "Did the professional arrive in a timely manner?")]
        public bool DidArriveInTimelyManner { get; set; }
        
        [Display(Name = "Did they present in professional attire?")]
        public bool DidPresentInProfessionalAttire { get; set; }
        
        [Display(Name = "Did the professional interact well with your staff?")]
        public bool DidInteractWellWithStaff { get; set; }
        
        [Display(Name = "Did the professional interact well with your patients?")]
        public bool DidInteractWellWithCustomers { get; set; }
        
        [Display(Name = "Was the professional competent in digital charting and documentation?")]
        public bool WasCompetentInDigitalChartingAndDocumentation { get; set; }

        public bool IsReadonly { get; set; }
    }

    public class CreateInitialModelDto
    {
        public int TempId { get; set; }
        public int RequestId { get; set; }
    }

    public class PageProfessionalReviews
    {
        public IPagedList<OwnProfessionalReview> OwnProfessionalReviews { get; set; }
    }

    public class OwnProfessionalReview
    {
        public int RequestId { get; set; }
        public string Position { get; set; }
        public int Rating { get; set; }
        public bool IsBlocked { get; set; }
        public string Description { get; set; }
        public bool DidArriveInTimelyManner { get; set; }
        public bool DidPresentInProfessionalAttire { get; set; }
        public bool DidInteractWellWithStaff { get; set; }
        public bool DidInteractWellWithCustomers { get; set; }
        public bool WasCompetentInDigitalChartingAndDocumentation { get; set; }
    }
}