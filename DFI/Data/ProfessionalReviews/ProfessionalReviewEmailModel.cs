namespace DFI.Data.ProfessionalReviews
{
    public class ProfessionalReviewEmailModel
    {
        public string Email { get; set; }

        public string PracticeName { get; set; }

        public int TempId { get; set; }

        public int RequestId { get; set; }

        public string RecentTempName { get; set; }
    }
}