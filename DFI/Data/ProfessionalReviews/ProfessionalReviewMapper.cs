using System;
using DFI.Models;

namespace DFI.Data.ProfessionalReviews
{
    public class ProfessionalReviewMapper : IProfessionalReviewMapper
    {
        public ProfessionalReview MapToProfessionalReview(ProfessionalReviewDto dto)
        {
            return new ProfessionalReview
            {
                TempId = dto.TempId,
                RequestId = dto.RequestId,
                Rating = dto.Rating,
                IsBlocked = dto.IsBlocked,
                Description = dto.Description,
                AddDate = DateTime.UtcNow,
                DidArriveInTimelyManner = dto.DidArriveInTimelyManner,
                DidPresentInProfessionalAttire = dto.DidPresentInProfessionalAttire,
                DidInteractWellWithStaff = dto.DidInteractWellWithStaff,
                DidInteractWellWithCustomers = dto.DidInteractWellWithCustomers,
                WasCompetentInDigitalChartingAndDocumentation = dto.WasCompetentInDigitalChartingAndDocumentation
            };
        }

        public OwnProfessionalReview MapToOwnProfessionalReview(ProfessionalReview model)
        {
            return new OwnProfessionalReview
            {
                Description = model.Description,
                DidArriveInTimelyManner = model.DidArriveInTimelyManner,
                DidInteractWellWithCustomers = model.DidInteractWellWithCustomers,
                DidInteractWellWithStaff = model.DidInteractWellWithStaff,
                DidPresentInProfessionalAttire = model.DidPresentInProfessionalAttire,
                IsBlocked = model.IsBlocked,
                Position = model.Request.LicenseType,
                Rating = model.Rating,
                RequestId = model.RequestId,
                WasCompetentInDigitalChartingAndDocumentation = model.WasCompetentInDigitalChartingAndDocumentation
            };
        }
    }

    public interface IProfessionalReviewMapper
    {
        ProfessionalReview MapToProfessionalReview(ProfessionalReviewDto dto);
        OwnProfessionalReview MapToOwnProfessionalReview(ProfessionalReview model);
    }
}