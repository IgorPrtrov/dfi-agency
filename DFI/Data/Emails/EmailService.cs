﻿using System;
using System.Collections.Generic;
using System.Linq;
using DFI.Data.Accounts;
using DFI.Data.Facilities;
using DFI.Data.Temps;
using DFI.Helpers;
using DFI.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace DFI.Data.Emails
{
    public class EmailService : IEmailService
    {
        private readonly IEmailRepository _mailRepository;
        private readonly IMailMapper _mailMapper;
        private readonly ILogger _logger;
        private readonly ITempRepository _tempRepository;
        private readonly IFacilityRepository _facilityRepository;
        private readonly IDailyEmailRepository _dailyEmailRepository;

        public EmailService(IEmailRepository mailRepository, IMailMapper mailMapper, ILogger logger, ITempRepository tempRepository, 
            IFacilityRepository facilityRepository, IDailyEmailRepository dailyEmailRepository)
        {
            _mailRepository = mailRepository;
            _mailMapper = mailMapper;
            _logger = logger;
            _tempRepository = tempRepository;
            _facilityRepository = facilityRepository;
            _dailyEmailRepository = dailyEmailRepository;
        }

        public void Send()
        {
            var emails = _mailRepository.GetEmailsForDelivery();

            foreach (var email in emails)
            {
                try
                {
                    var hasEmailAlias = Setting.HasEmailAlias;
                    var emailAlias = Setting.EmailAlias;
                    var isActiveSendEmails = Setting.SendEmails;

                    _logger.Info($"Email Send:: hasEmailAlias: {hasEmailAlias}");
                    _logger.Info($"Email Send:: emailAlias: {emailAlias}");
                    _logger.Info($"Email Send:: isActiveSendEmails: {isActiveSendEmails}");

                    if(isActiveSendEmails == false)
                    {
                        email.SentDate = DateTime.Now.ToUniversalTime();
                        email.Sent = true;
                        _mailRepository.Update(email);
                        _mailRepository.SaveChanges();

                        _logger.Info($"Email Send:: email id {email.EmailId} has been saved as delivered in DB but really it was not sent");
                        return;
                    }

                    var emailTo = hasEmailAlias && !string.IsNullOrEmpty(emailAlias) ? emailAlias : email.ToEmail;
                    _logger.Info($"Email Send:: emailTo: {emailTo}");

                    var options = new RestClientOptions();
                    options.Authenticator = new HttpBasicAuthenticator("api", Setting.KeyMailGun);
                    options.BaseUrl = new Uri(Setting.MailGunUri);

                    RestClient client = new RestClient(options);

                    RestRequest request = new RestRequest();
                    request.AddParameter("domain", Setting.Domain, ParameterType.UrlSegment);
                    request.Resource = "{domain}/messages";
                    request.AddParameter("from", email.FromEmail);
                    request.AddParameter("to", emailTo);

                    if (!string.IsNullOrEmpty(email.Cc))
                        request.AddParameter("cc", email.Cc);

                    request.AddParameter("subject", email.Subject);
                    request.AddParameter("html", email.Body);
                    request.AddParameter("o:tracking-opens", "yes");
                    request.AddParameter("o:tracking-clicks", "yes");
                    request.Method = Method.Post;

                    if(_mailRepository.IsNotDeliveredEmail(email.EmailId))
                    {
                        var result = client.Execute(request);

                        if (result.IsSuccessful)
                        {
                            email.SentDate = DateTime.Now.ToUniversalTime();
                            email.Sent = true;
                            _logger.Info(
                                $"EmailService:: Send Successfully. id = {email.EmailId}, toEmail = {email.ToEmail}," +
                                $" Status = {result.StatusCode}, result.IsSuccessful = {result.IsSuccessful}");
                        }
                        else
                        {
                            _logger.Warn($"EmailService:: Send. id = {email.EmailId}, toEmail = {email.ToEmail}," +
                                         $" Status = {result.StatusCode}, result.IsSuccessful = {result.IsSuccessful}");
                        }             
                                      
                        if (email.Attempt == null)
                            email.Attempt = 1;
                        email.Attempt += 1;
                    }
                }
                catch (Exception e)
                {
                    _logger.Error($"Email Send:: Exception: {e.Message}");

                    if (email.Attempt == null)
                        email.Attempt = 1;
                    email.Attempt += 1;
                }

                _mailRepository.Update(email);
                _mailRepository.SaveChanges();
            }
        }

        public void AddEmail(EmailDto dto)
        {
            var email = _mailMapper.MapToEmail(dto);
            _mailRepository.Add(email);
            _mailRepository.SaveChanges();          
        }

        public void AddEmails(List<EmailDto> dtoEmails)
        {
            AddRegularEmail(dtoEmails);

            AddDailyEmail(dtoEmails);
        }

        public void AddRegularEmail(List<EmailDto> dtoEmails)
        {
            foreach (var dtoEmail in dtoEmails.Where(c => !c.IsDailyRequestEmail))
            {
                var email = _mailMapper.MapToEmail(dtoEmail);
                _mailRepository.Add(email);
            }

            if (dtoEmails.Any(c => !c.IsDailyRequestEmail))
                _mailRepository.SaveChanges();
        }

        public void AddDailyEmail(List<EmailDto> dtoEmails)
        {
            foreach (var dtoEmail in dtoEmails.Where(c => c.IsDailyRequestEmail))
            {
                var dailyEmail = _mailMapper.MapToDailyEmail(dtoEmail);
                _dailyEmailRepository.Add(dailyEmail);
            }

            if (dtoEmails.Any(c => c.IsDailyRequestEmail))
                _dailyEmailRepository.SaveChanges();
        }

        public void SendDailyEmails()
        {
            var dailyEmails = _dailyEmailRepository.GetDailyEmails().GroupBy(c => c.ToEmail);
            
            foreach(var groupEmails in dailyEmails)
            {
                try
                {
                    _logger.Info($"{nameof(SendDailyEmails)}:: email to = {groupEmails.Key}, count = {groupEmails.Count()}");

                    var emailHeader = string.Format(EmailConstants.HeaderForDailyEmail, $"{Setting.ApplicationBaseUrl}Requests/Calendar?eCalendarEvent=History");

                    AddEmail(new EmailDto()
                    {
                        Body = $"{emailHeader}{string.Join(string.Empty, groupEmails.Select(c => c.Body))}{EmailConstants.FooterForDailyEmail}",
                        Subject = groupEmails.FirstOrDefault().Subject,
                        To = groupEmails.Key
                    });

                    UpdateDailyEmails(groupEmails);
                }
                catch(Exception ex)
                {
                    _logger.Error($"{nameof(SendDailyEmails)}::ERROR:: {ex.Message}, {ex.StackTrace}, email to = {groupEmails.Key}, count = {groupEmails.Count()}");
                }
            }
        }

        private void UpdateDailyEmails(IGrouping<string, DailyEmail> dailyEmails)
        {
            foreach (var dailyEmail in dailyEmails)
            {
                dailyEmail.Sent = true;
                dailyEmail.SentOn = DateTime.UtcNow;

                _dailyEmailRepository.Update(dailyEmail);
                _dailyEmailRepository.SaveChanges();
            }
        }

        public ApplicationUser UpdateEmails(ApplicationUser user, string newEmail)
        {
            var temp = _tempRepository.GetByEmail(user.Email);
            if (temp != null)
            {
                temp.Email = newEmail;
                _tempRepository.SaveChanges();
            }

            var facility = _facilityRepository.GetByEmail(user.Email);
            if (facility != null)
            {
                if (facility.BillingEmail == facility.Email)
                {
                    facility.Email = newEmail;
                    facility.BillingEmail = newEmail;
                }
                else
                    facility.Email = newEmail;

                _facilityRepository.SaveChanges();
            }

            user.NewEmail = string.Empty;
            user.Email = newEmail;
            user.UserName = newEmail;

            return user;
        }

        public ValidationEmail PostValidate(string email)
        {
            if (!_mailRepository.IsEmailValidated(email))
            {
                _logger.Info($"{nameof(PostValidate)}:: email = {email}");

                var options = new RestClientOptions();
                options.Authenticator = new HttpBasicAuthenticator("api", Setting.KeyMailGun);
                options.BaseUrl = new Uri("https://api.mailgun.net/v4");

                RestClient client = new RestClient(options);
                RestRequest request = new RestRequest();
                request.Resource = "/address/validate";
                request.AddParameter("address", email);
                request.Method = Method.Post;

                var result = client.Execute(request);
                _logger.Info($"{nameof(PostValidate)}:: Result:  Content = {result.Content}, " +
                    $"IsSuccessful = {result.IsSuccessful}, StatusCode = {result.StatusCode}");

                var dto = JsonConvert.DeserializeObject<ValidationEmailDto>(result.Content);
                var domain = _mailMapper.MapToValidationEmail(dto);

                _mailRepository.Add(domain);
                _mailRepository.SaveChanges();

                return domain;
            }
            else
            {
                _logger.Info($"{nameof(PostValidate)}:: email = {email} already validated");
                return _mailRepository.GetValidationEmail(email);
            }
        }

        public void SendTempEmails()
        {
            foreach (var email in Setting.TempEmails)
            {
                if (!string.IsNullOrEmpty(email))
                {
                    var emailDomain = _mailMapper.MapToTempEmail(new Email()
                    {
                        ToEmail = email,
                        Subject = "Introducing our New Website"
                    });

                    _mailRepository.Add(emailDomain);
                    _mailRepository.SaveChanges();
                }     
            }
        }

        public void SendOfficeEmails()
        {
            foreach (var email in Setting.OfficeEmails)
            {
                if (!string.IsNullOrEmpty(email))
                {
                    var emailDomain = _mailMapper.MapToOfficeEmail(new Email()
                    {
                        ToEmail = email,
                        Subject = "Introducing our New Website"
                    });

                    _mailRepository.Add(emailDomain);
                    _mailRepository.SaveChanges();
                }
            }
        }

        public void ResendRegistrationEmail(string emailAddress)
        {
            var email = _mailRepository.GetRegistrationEmail(emailAddress);
            if(email == null)
            {
                _logger.Error($"ResendRegistrationEmail:: registration email is null for emailAddress = {emailAddress}");
                return;
            }    

            if(email.Attempt == 5)
            {
                email.Attempt--;
            }

            email.Sent = false;

            _mailRepository.Update(email);
            _mailRepository.SaveChanges();
        }
    }

    public interface IEmailService
    {
        void Send();
        void AddEmail(EmailDto dto);
        ApplicationUser UpdateEmails(ApplicationUser user, string newEmail);
        ValidationEmail PostValidate(string email);
        void SendTempEmails();
        void SendOfficeEmails();
        void AddEmails(List<EmailDto> dtoEmails);
        void ResendRegistrationEmail(string emailAddress);
        void SendDailyEmails();
    }
}