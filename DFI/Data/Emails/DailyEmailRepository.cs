﻿using System.Collections.Generic;
using System.Linq;
using DFI.Models;

namespace DFI.Data.Emails
{
    public class DailyEmailRepository : Repository<DailyEmail>, IDailyEmailRepository
    {
        private readonly IApplicationDbContext _context;
        public DailyEmailRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public List<DailyEmail> GetDailyEmails()
        {
            return _context.DailyEmails.AsQueryable().Where(c => c.Sent == false).ToList();
        }
    }

    public interface IDailyEmailRepository : IRepository<DailyEmail>
    {
        List<DailyEmail> GetDailyEmails();
    }
}