﻿using System.Collections.Generic;
using System.Linq;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Emails
{
    public class EmailRepository : Repository<Email>, IEmailRepository
    {
        private readonly IApplicationDbContext _context;
        public EmailRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public bool IsEmailValidated(string email)
        {
            return _context.ValidationEmails.Any(c => c.Address.ToLower() == email.ToLower());
        }

        public ValidationEmail GetValidationEmail(string email)
        {
            return _context.ValidationEmails.FirstOrDefault(c => c.Address.ToLower() == email.ToLower()); 
        }

        public bool IsNotDeliveredEmail(int emailId)
        {
            var email = _context.Emails.FirstOrDefault(c => c.EmailId == emailId);

            return email.Sent == false && (email.Attempt == null || email.Attempt < 5);
        }

        public List<Email> GetEmailsForDelivery()
        {
            return _context.Emails.AsQueryable().Where(c => c.Sent == false && (c.Attempt == null || c.Attempt < 5)).Take(20).ToList();
        }

        public Email GetRegistrationEmail(string emailAddress)
        {
            return _context.Emails.FirstOrDefault(c => c.ToEmail == emailAddress && c.Subject == "Registration form");
        }
    }

    public interface IEmailRepository : IRepository<Email>
    {
        bool IsEmailValidated(string email);
        ValidationEmail GetValidationEmail(string email);
        bool IsNotDeliveredEmail(int emailId);
        List<Email> GetEmailsForDelivery();
        Email GetRegistrationEmail(string emailAddress);
    }
}