﻿using System;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Emails
{
    public class EmailMapper : IMailMapper
    {
        public Email MapToEmail(EmailDto dto)
        {
            return new Email()
            {
                FromEmail = Setting.FromMailGun,
                ToEmail = dto.To,
                BodyHtml = true,
                AddDate = DateTime.Now.ToUniversalTime(),
                Subject = dto.Subject,
                Body = EmailTemplate.GetHtmlByView("Template.cshtml")
                            .Replace("{{Body}}", dto.Body)
                            .Replace("{{linkToWebSite}}", Setting.ApplicationBaseUrl)
                            .Replace("{{emailTo}}", dto.To)
                            .Replace("{{urlContactUs}}", Helper.GetContactUsUrl()),
                Cc = dto.Cc
            };
        }

        public Email MapToTempEmail(Email dto)
        {
            return new Email
            {
                FromEmail = Setting.FromMailGun,
                ToEmail = dto.ToEmail,
                BodyHtml = true,
                AddDate = DateTime.Now.ToUniversalTime(),
                Subject = dto.Subject,
                Body = EmailTemplate.GetHtmlByView("TemplateTemp.cshtml").Replace("{{linkToTempRegistration}}", 
                    Helper.GetTempRegistrationFormUrl()).Replace("{{linkToWebSite}}", Setting.ApplicationBaseUrl),
                Cc = dto.Cc
            };
        }

        public Email MapToOfficeEmail(Email dto)
        {
            return new Email
            {
                FromEmail = Setting.FromMailGun,
                ToEmail = dto.ToEmail,
                BodyHtml = true,
                AddDate = DateTime.Now.ToUniversalTime(),
                Subject = dto.Subject,
                Body = EmailTemplate.GetHtmlByView("TemplateOffice.cshtml").Replace("{{linkToOfficeRegistration}}", 
                    Helper.GetOfficeRegistrationFormUrl()).Replace("{{linkToWebSite}}", Setting.ApplicationBaseUrl),
                Cc = dto.Cc
            };
        }

        public ValidationEmail MapToValidationEmail(ValidationEmailDto dto)
        {
            return new ValidationEmail()
            {
                Address = dto.Address,
                IsDisposableAddress = dto.IsDisposableAddress,
                IsRoleAddress = dto.IsRoleAddress,
                Reason = string.Join(", ", dto.Reason),
                Result = dto.Result,
                Risk = dto.Risk.ToString()
            };
        }

        public DailyEmail MapToDailyEmail(EmailDto emailDto)
        {
            return new DailyEmail()
            {
                Body = emailDto.Body,
                Subject = emailDto.Subject,
                ToEmail = emailDto.To,
                CreatedOn = DateTime.UtcNow
            };
        }
    }
    public interface IMailMapper
    {
        Email MapToEmail(EmailDto dto);
        ValidationEmail MapToValidationEmail(ValidationEmailDto dto);
        Email MapToTempEmail(Email dto);
        Email MapToOfficeEmail(Email dto);
        DailyEmail MapToDailyEmail(EmailDto emailDto);
    }
}