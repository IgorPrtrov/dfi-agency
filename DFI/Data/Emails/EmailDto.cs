﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DFI.Data.Emails
{
    public class EmailDto
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public bool IsDailyRequestEmail { get; set; }
    }

    public class ValidationEmailDto
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("is_disposable_address")]
        public bool IsDisposableAddress { get; set; }

        [JsonProperty("is_role_address")]
        public bool IsRoleAddress { get; set; }

        [JsonProperty("reason")]
        public string[] Reason { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }

        [JsonProperty("risk")]
        public EValidationRisk Risk { get; set; }
    }

    public enum EValidationRisk
    {
        high, medium, low, unknown
    }

    public class EmailChangeDto
    {
        public string Email { get; set; }

        [Required(ErrorMessage = "New Email is required")]
        [EmailAddress(ErrorMessage = "Invalid New Email Address")]
        [StringLength(254, ErrorMessage = "New Email cannot be longer than 254 characters.")]
        public string NewEmail { get; set; }
    }

    public class EmailLightDto
    {
        [Required]
        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }
    }
}