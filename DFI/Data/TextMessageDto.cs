﻿namespace DFI.Data.TextMessages
{
    public class TextMessageDto
    {
        public string PhoneNumberTo { get; set; }
        public string Body { get; set; }
    }
}