﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using DFI.Data.Documents;
using DFI.Data.Emails;
using DFI.Data.GoogleMaps;
using DFI.Helpers;
using DFI.Models;
using PagedList;
using static DFI.Helpers.Helper;

namespace DFI.Data.Temps
{
    public class TempRegistrationFormDto
    {
        [Required(ErrorMessage = "Password is required")]
        [StringLength(15, ErrorMessage = "Must be between 6 and 15 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public EStatusCode FindingResult { get; set; }
        public string ValidationGraderError { get; set; }
        public string CallbackUrl { get; set; }
        public TempDto TempDto { get; set; }

        public TempRegistrationFormDto()
        {
            TempDto = new TempDto();
        }
    }

    public class TempDto : IValidatableObject
    {
        public int? Id { get; set; }
        public double Rating { get; set; }
        public int ReviewCount { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(254, ErrorMessage = "Email cannot be longer than 254 characters.")]
        public string Email { get; set; }
        public string NewEmail { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(100, ErrorMessage = "First Name cannot be longer than 100 characters.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(100, ErrorMessage = "Last Name cannot be longer than 100 characters.")]
        public string LastName { get; set; }
        public int? PersonId { get; set; }
        [Required(ErrorMessage = "Cell Phone is required")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = ValidationConstants.PhoneNumberValidationError)]
        [StringLength(15, ErrorMessage = "Phone number cannot be longer than 15 characters.")]
        public string CellPhone { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = ValidationConstants.PhoneNumberValidationError)]
        [StringLength(15, ErrorMessage = "Phone number cannot be longer than 15 characters.")]
        public string HomePhone { get; set; }
        [Required(ErrorMessage = "Distance To Work is required")]
        [Range(1, 200, ErrorMessage = "Please enter valid Distance To Work In Practice (1-200)")]
        public int DistanceToWork { get; set; }
        [StringLength(1000, ErrorMessage = "Who Referring You cannot be longer than 1000 characters.")]
        public string WhoReferenced { get; set; }
        public bool IsReadyToPermanentWork { get; set; }
        [StringLength(100, ErrorMessage = "Apartment cannot be longer than 1000 characters.")]
        public string Apartment { get; set; }
        public string AddressFromGoogle { get; set; }
        [StringLength(2000, ErrorMessage = "Administrators notes cannot be longer than 2000 characters.")]
        public string AdministratorsNotes { get; set; }
        public bool IsAdmin { get; set; }
        public HttpPostedFileBase Certificate { get; set; }
        public HttpPostedFileBase Resume { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public HttpPostedFileBase RadiologyLicense { get; set; }
        public int TempStatus { get; set; }
        public List<LicenseDto> LicenseDtos { get; set; }
        public List<DocumentDto> DocumentDtos { get; set; }
        public bool IsGetSMSForNewRequest { get; set; }
        public List<TempAvailabilityDto> TempAvailableDaysDto { get; set; }

        public TempDto()
        {
            LicenseDtos = new List<LicenseDto>();
            DocumentDtos = new List<DocumentDto>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(AddressFromGoogle))
            {
                var address = AddressFromGoogle.Split(',');

                var isValidGoogleAddress = true;

                if (string.IsNullOrEmpty(address[0].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("Address is not valid");
                }

                if (string.IsNullOrEmpty(address[1].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("City is not valid");
                }

                if (string.IsNullOrEmpty(address[2].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("State Name is not valid");
                }

                if (string.IsNullOrEmpty(address[3].Trim()))
                {
                    isValidGoogleAddress = false;
                    yield return new ValidationResult("Zipcode is not valid");
                }

                if (isValidGoogleAddress == false)
                    yield return new ValidationResult("Please enter your address manually again");
            }
            else
                yield return new ValidationResult("FIll in again your address manually. Google map can not find it.");

            if (!DocumentDtos.Any(c => c.Type == EDocumentType.Photo.ToString()) && Photo == null)
                yield return new ValidationResult("Photo is required.");

            var maxSize = 5242880;

            if (Certificate != null)
            {
                var certificateExtension = Path.GetExtension(Certificate.FileName).ToLower();
                if (certificateExtension != ".doc" && certificateExtension != ".docx" && certificateExtension != ".pdf" && certificateExtension != ".pages")
                    yield return new ValidationResult("Certificate must be DOC, DOCX, PAGES or PDF.");

                if (Certificate.ContentLength > maxSize)
                    yield return new ValidationResult("Certificate must be less than 5MB.");
            }

            if (Resume != null)
            {
                var resumeExtension = Path.GetExtension(Resume.FileName).ToLower(); ;
                if (resumeExtension != ".doc" && resumeExtension != ".docx" && resumeExtension != ".pdf" && resumeExtension != ".pages")
                    yield return new ValidationResult("Resume must be DOC, DOCX, PAGES or PDF.");

                if (Resume.ContentLength > maxSize)
                    yield return new ValidationResult("Resume must be less than 5MB.");
            }

            if (Photo != null)
            {
                var photoExtension = Path.GetExtension(Photo.FileName).ToLower(); ;
                if (photoExtension != ".jpg" && photoExtension != ".jpeg" && photoExtension != ".png")
                    yield return new ValidationResult("Photo must be JPG, GIF or PNG.");

                if (Photo.ContentLength > maxSize)
                    yield return new ValidationResult("Photo must be less than 5MB.");
            }

            if (RadiologyLicense != null)
            {
                var radiologyLicenseExtension = Path.GetExtension(RadiologyLicense.FileName).ToLower();
                if (radiologyLicenseExtension != ".jpg" && radiologyLicenseExtension != ".jpeg" && radiologyLicenseExtension != ".png" 
                    && radiologyLicenseExtension != ".doc" && radiologyLicenseExtension != ".docx" 
                    && radiologyLicenseExtension != ".pdf" && radiologyLicenseExtension != ".pages")
                    yield return new ValidationResult("Radiology license must be DOC, DOCX, PAGES, PDF or JPG, GIF, PNG.");

                if (RadiologyLicense.ContentLength > maxSize)
                    yield return new ValidationResult("Radiology license must be less than 5MB.");
            }

            var context = new ApplicationDbContext();
            if (Id == null)
            {
                if(context.Temps.Any(c => c.Email.Trim().ToLower() == Email.Trim().ToLower()))
                    yield return new ValidationResult("Email already exists");
            }
            else
            {
                if(context.Temps.Where(c => c.TempId != Id).Any(x => x.Email.Trim().ToLower() == Email.Trim().ToLower()))
                    yield return new ValidationResult("Email already exists");
            }

            if (!Helper.IsValidEmail(Email))
                yield return new ValidationResult("Email is not valid");
        }
    }

    public class TempAvailabilityDto
    {
        public DayOfWeek DayOfWeekAvailable { get; set; }
        public bool IsSelected { get; set; }
    }

    public class LicenseDto
    {
        public int? Id { get; set; }
        public string Profession { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public string Status { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string BoardName { get; set; }
        public bool IsDeleted { get; set; }

        [Required(ErrorMessage = "Desired Hourly Rate is required")]
        [Range(1, 99, ErrorMessage = "Please enter valid Hourly Rate (1-99)")]
        public int DesiredHourlyRate { get; set; }

        [Required(ErrorMessage = "Years In Practice is required")]
        [Range(1, 50, ErrorMessage = "Please enter valid Years In Practice (1-50)")]
        public int YearsInPractice { get; set; }

        [StringLength(1000, ErrorMessage = "Unique qualities employer cannot be longer than 1000 characters.")]
        public string UniqueQualities { get; set; }

        [StringLength(1000, ErrorMessage = "Current employer cannot be longer than 1000 characters.")]
        public string CurrentEmployer { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = ValidationConstants.PhoneNumberValidationError)]
        [StringLength(15, ErrorMessage = "Phone number cannot be longer than 15 characters.")]
        public string CurrentEmployerPhoneNumber { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(254, ErrorMessage = "Email cannot be longer than 254 characters.")]
        public string CurrentEmployerEmail { get; set; }
    }

    public class GetRegistrationFormDto : IValidatableObject
    {
        public string LicenseNumber { get; set; }
        public string LastName { get; set; }
        public bool IsReceptionist { get; set; }
        public bool IsOfficeManager { get; set; }
        public bool IsDentalAssistant { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(string.IsNullOrEmpty(LicenseNumber) && string.IsNullOrEmpty(LastName) && IsReceptionist == false && IsOfficeManager == false && IsDentalAssistant == false)
                yield return new ValidationResult("All fields or at least one checkbox required");

            if(string.IsNullOrEmpty(LastName) && !string.IsNullOrEmpty(LicenseNumber) || !string.IsNullOrEmpty(LastName) && string.IsNullOrEmpty(LicenseNumber))
                yield return new ValidationResult("License Number and Last Name required");

            var context = new ApplicationDbContext();
            var isLisenceNomberExist =
                context.Licenses.Any(c => !string.IsNullOrEmpty(LicenseNumber) && c.LicenseNumber.ToLower().Trim() == LicenseNumber.ToLower().Trim() && c.Temp.Status != (int)EState.New);

            if (isLisenceNomberExist)
                yield return new ValidationResult("License Number already exists in system");
        }
    }

    public class ListTempDto
    {
        public IPagedList<TempShortDto> TempShortDtos { get; set; }
    }

    public class TempShortDto
    {
        public int TempId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Licenses { get; set; }
        public string Status { get; set; }
        public string Days { get; set; }
    }

    public class TempSearchDto
    {
        public EOrder Order { get; set; }
        public ETempColumn TempColumn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public List<string> Licenseses { get; set; }
        public List<int> Statuses { get; set; }
        public List<DayOfWeek> Days { get; set; }
        public int? Page { get; set; }

        public TempSearchDto()
        {
            Licenseses = new List<string>();
            Statuses = new List<int>();
            Days = new List<DayOfWeek>();
        }
    }

    public class TempEmailLightDto
    {
        public TempSearchDto TempSearchDto { get; set; }
        public EmailLightDto EmailLightDto { get; set; }

        public TempEmailLightDto()
        {
            TempSearchDto = new TempSearchDto();
            EmailLightDto = new EmailLightDto();
        }
    }

    public enum ETempColumn
    {
        FirstName,
        LastName,
        Status
    }

    public enum ENoLicensePosition
    {
        [Display(Name = "Receptionist")]
        Receptionist,

        [Display(Name = "Office Manager")]
        OfficeManager,

        [Display(Name = "Dental Assistant")]
        DentalAssistant
    }

    public class LabelValue
    {
        public int id { get; set; }
        public string label { get; set; }
    }
}