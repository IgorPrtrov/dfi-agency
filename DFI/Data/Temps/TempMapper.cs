using System;
using System.Collections.Generic;
using System.Linq;
using DFI.Data.Documents;
using DFI.Helpers;
using DFI.Models;

namespace DFI.Data.Temps
{
    public class TempMapper : ITempMapper
    {
        private readonly IDocumentMapper _documentMapper;

        public TempMapper(IDocumentMapper documentMapper)
        {
            this._documentMapper = documentMapper;
        }
        
        public TempDto MapToTempDto(Temp domain)
        {
            var tempDto = new TempDto()
            {
                Id = domain.TempId,
                Rating = domain.ProfessionalReviews.Count == 0 ? 10 : ((domain.ProfessionalReviews.Average(c => c.Rating) + 10) / 2),
                ReviewCount = domain.ProfessionalReviews.Count,
                Email = domain.Email,
                FirstName = domain.FirstName,
                LastName = domain.LastName,
                PersonId = domain.PersonId,
                CellPhone = domain.CellPhone,
                HomePhone = domain.HomePhone,
                TempStatus = domain.Status ?? (int)Helper.EState.InActive,
                DistanceToWork = domain.DistanceToWork,
                WhoReferenced = domain.WhoReferenced,
                IsReadyToPermanentWork = domain.IsReadyToPermanentWork ?? false,
                AdministratorsNotes = domain.AdministratorsNotes,

                Apartment = domain.Apartment,
                AddressFromGoogle = domain.FullAddress,
                IsGetSMSForNewRequest = domain.IsGetSMSForNewRequest,

                LicenseDtos = domain.Licenses.Select(c => MapToLicenseDto(c)).ToList(),
                DocumentDtos = domain.Documents.Select(c => _documentMapper.MapToDocumentDto(c)).ToList(),
                TempAvailableDaysDto = new List<TempAvailabilityDto>()
            };

            var allDaysOfWeek = Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>();

            foreach (var dayOfWeek in allDaysOfWeek)
            {
                var tempAvailabilityDto = new TempAvailabilityDto()
                {
                    DayOfWeekAvailable = dayOfWeek,
                    IsSelected = domain.TempAvailabilities.Any(t => t.DayOfWeekAvailable == dayOfWeek)
                };

                tempDto.TempAvailableDaysDto.Add(tempAvailabilityDto);
            }

            return tempDto;
        }

        public LicenseDto MapToLicenseDto(ENoLicensePosition position)
        {
            return new LicenseDto()
            {
                BoardName = position == ENoLicensePosition.OfficeManager 
                                                ? ENoLicensePosition.OfficeManager.GetDisplayName() 
                                                : position == ENoLicensePosition.Receptionist 
                                                        ? ENoLicensePosition.Receptionist.GetDisplayName() : "Radiology Personnel",
                Profession = position == ENoLicensePosition.OfficeManager
                                                ? ENoLicensePosition.OfficeManager.GetDisplayName()
                                                : position == ENoLicensePosition.Receptionist
                                                        ? ENoLicensePosition.Receptionist.GetDisplayName() : "Radiology Personnel",        
                LicenseType = position == ENoLicensePosition.OfficeManager
                                                ? ENoLicensePosition.OfficeManager.GetDisplayName()
                                                : position == ENoLicensePosition.Receptionist
                                                        ? ENoLicensePosition.Receptionist.GetDisplayName() : ENoLicensePosition.DentalAssistant.GetDisplayName(),
                LicenseNumber = string.Empty,
                Status = Helper.EState.Active.ToString(),
                CurrentEmployer = string.Empty,
                CurrentEmployerEmail = string.Empty,
                CurrentEmployerPhoneNumber = string.Empty,
                UniqueQualities = string.Empty,
                ExpiryDate = DateTime.MaxValue
            };
        }

        public LicenseDto MapToLicenseDto(License domain)
        {
            return new LicenseDto()
            {
                Profession = domain.Profession,
                LicenseType = domain.LicenseType,
                LicenseNumber = domain.LicenseNumber,
                Status = domain.Status,
                ExpiryDate = domain.ExpiryDate,
                BoardName = domain.BoardName,
                CurrentEmployer = domain.CurrentEmployer ?? string.Empty,
                CurrentEmployerEmail = domain.CurrentEmployerEmail ?? string.Empty,
                CurrentEmployerPhoneNumber = domain.CurrentEmployerPhoneNumber ?? string.Empty,
                DesiredHourlyRate = domain.DesiredHourlyRate,
                UniqueQualities = domain.UniqueQualities ?? string.Empty,
                YearsInPractice = domain.YearsInPractice
            };
        }

        public Temp MapToTemp(TempDto dto)
        {
            var temp = new Temp()
            {
                 FirstName = dto.FirstName,
                 LastName = dto.LastName,
                 PersonId = dto.PersonId,
                 CellPhone = dto.CellPhone,
                 HomePhone = dto.HomePhone,
                 Email = dto.Email,
                 DistanceToWork = dto.DistanceToWork,
                 WhoReferenced = dto.WhoReferenced,
                 IsReadyToPermanentWork = dto.IsReadyToPermanentWork,
                 AddressLine1 = dto.AddressFromGoogle.Split(',')[0].Trim(),
                 Apartment = dto.Apartment,
                 City = dto.AddressFromGoogle.Split(',')[1].Trim(),
                 StateName = dto.AddressFromGoogle.Split(',')[2].Trim(),
                 Zipcode = dto.AddressFromGoogle.Split(',')[3].Trim(),
                 FullAddress = dto.AddressFromGoogle,
                 Status = dto.TempStatus,
                 AddedDate = DateTime.Now.ToUniversalTime(),
                 AdministratorsNotes = dto.AdministratorsNotes,
                 Licenses = dto.LicenseDtos.Select(c => MapToLicense(c)).ToList(),
                 Documents = new List<Document>(),
                IsGetSMSForNewRequest = dto.IsGetSMSForNewRequest,
                TempAvailabilities = new List<TempAvailability>()
            };

            for (int i = 0; i < dto.TempAvailableDaysDto.Count; i++)
            {
                if (dto.TempAvailableDaysDto[i].IsSelected)
                {
                    temp.TempAvailabilities.Add(new TempAvailability(dto.TempAvailableDaysDto[i].DayOfWeekAvailable));
                }
            }

            if (dto.Certificate != null)
                temp.Documents.Add(Helper.GetDocument(dto.Certificate, EDocumentType.Certificate));

            if(dto.Resume != null)
                temp.Documents.Add(Helper.GetDocument(dto.Resume, EDocumentType.Resume));

            if(dto.Photo != null)
                temp.Documents.Add(Helper.GetDocument(dto.Photo, EDocumentType.Photo));

            if (dto.RadiologyLicense != null)
                temp.Documents.Add(Helper.GetDocument(dto.RadiologyLicense, EDocumentType.RadiologyLicense));

            return temp;
        }

        private License MapToLicense(LicenseDto dto, int? tempId = null)
        {
            var license = new License()
            {
                Profession = dto.Profession,
                LicenseType = dto.LicenseType,
                LicenseNumber = dto.LicenseNumber,
                Status = dto.Status,
                ExpiryDate = dto.ExpiryDate,
                BoardName = dto.BoardName,
                DesiredHourlyRate = dto.DesiredHourlyRate,
                YearsInPractice = dto.YearsInPractice,
                UniqueQualities = dto.UniqueQualities,
                CurrentEmployer = dto.CurrentEmployer,
                CurrentEmployerPhoneNumber = dto.CurrentEmployerPhoneNumber,
                CurrentEmployerEmail = dto.CurrentEmployerEmail
            };

            if (tempId != null)
                license.TempId = tempId;

            return license;
        }

        private void MapToLicense(License domain, LicenseDto dto)
        {
            domain.Profession = dto.Profession;
            domain.LicenseType = dto.LicenseType;
            domain.LicenseNumber = dto.LicenseNumber;
            domain.Status = dto.Status;
            domain.ExpiryDate = dto.ExpiryDate;
            domain.BoardName = dto.BoardName;
            domain.DesiredHourlyRate = dto.DesiredHourlyRate;
            domain.YearsInPractice = dto.YearsInPractice;
            domain.UniqueQualities = dto.UniqueQualities;
            domain.CurrentEmployer = dto.CurrentEmployer;
            domain.CurrentEmployerPhoneNumber = dto.CurrentEmployerPhoneNumber;
            domain.CurrentEmployerEmail = dto.CurrentEmployerEmail;
        }

        public TempShortDto MapToTempShortDto(Temp domain)
        {
            return new TempShortDto()
            {
                Status = Enum.GetName(typeof(Helper.EState), domain.Status),
                Email = domain.Email,
                Licenses = string.Join(", ", domain.Licenses.Select(c => c.LicenseType)),
                FirstName = domain.FirstName,
                LastName = domain.LastName,
                PhoneNumber = domain.CellPhone.AsPhoneNumber(),
                TempId = domain.TempId,
                Days = string.Join(", ", domain.TempAvailabilities.OrderBy(d => (int)d.DayOfWeekAvailable).Select(d => d.DayOfWeekAvailable.ToString().Substring(0, 2)))
        };
        }

        public Temp MapToTemp(Temp domain, TempDto dto)
        {
            domain.FirstName = dto.FirstName;
            domain.LastName = dto.LastName;
            domain.PersonId = dto.PersonId;
            domain.CellPhone = dto.CellPhone;
            domain.HomePhone = dto.HomePhone;
            domain.Email = dto.Email;
            domain.DistanceToWork = dto.DistanceToWork;
            domain.WhoReferenced = dto.WhoReferenced;
            domain.IsReadyToPermanentWork = dto.IsReadyToPermanentWork;
            domain.AddressLine1 = dto.AddressFromGoogle.Split(',')[0].Trim();
            domain.Apartment = dto.Apartment;
            domain.City = dto.AddressFromGoogle.Split(',')[1].Trim();
            domain.StateName = dto.AddressFromGoogle.Split(',')[2].Trim();
            domain.Zipcode = dto.AddressFromGoogle.Split(',')[3].Trim();
            domain.FullAddress = dto.AddressFromGoogle;
            domain.Status = dto.TempStatus;
            domain.DateOfModified = DateTime.Now.ToUniversalTime();
            domain.IsGetSMSForNewRequest = dto.IsGetSMSForNewRequest;
            
            //update tempavailabilities
            //add days
            foreach (var newDay in dto.TempAvailableDaysDto.Where(c => domain.TempAvailabilities.All(v => v.DayOfWeekAvailable != c.DayOfWeekAvailable & c.IsSelected == true)))
            {
                domain.TempAvailabilities.Add(MapToTempAvailability(newDay, domain.TempId));
            }
            //delete days
            var deletedDays = domain.TempAvailabilities.Where(c => dto.TempAvailableDaysDto.Any(v => v.DayOfWeekAvailable == c.DayOfWeekAvailable && !v.IsSelected)).ToList();
            foreach (var deletedDay in deletedDays)
            {
                domain.TempAvailabilities.Remove(deletedDay);                
            }

            if(dto.IsAdmin)
                domain.AdministratorsNotes = dto.AdministratorsNotes;
            
            //update existing licenses
            foreach (var license in domain.Licenses)
            {
                var dtoLicence = dto.LicenseDtos.FirstOrDefault(c => c.LicenseType == license.LicenseType);
                
                if(dtoLicence != null)
                    MapToLicense(license, dtoLicence);
            }
            //add new licenses
            foreach(var newLicense in dto.LicenseDtos.Where(c => domain.Licenses.All(v => v.LicenseType != c.LicenseType)))
            {
                domain.Licenses.Add(MapToLicense(newLicense, domain.TempId));
            }
            //delete licenses
            var deleteLicenses = domain.Licenses.Where(c => dto.LicenseDtos.All(v => v.LicenseType != c.LicenseType)).ToList();
            foreach (var deleteLicense in deleteLicenses)
            {
                domain.Licenses.Remove(deleteLicense);
            }

            if (dto.Certificate != null)
            {
                if (dto.DocumentDtos.Any(c => c.Type == EDocumentType.Certificate.ToString()))
                    domain.Documents.Remove(
                        domain.Documents.FirstOrDefault(c => dto.DocumentDtos.Any(x => x.Type == EDocumentType.Certificate.ToString() && x.Id == c.DocumentId)));

                domain.Documents.Add(Helper.GetDocument(dto.Certificate, EDocumentType.Certificate));
            }

            if (dto.Resume != null)
            {
                if (dto.DocumentDtos.Any(c => c.Type == EDocumentType.Resume.ToString()))
                    domain.Documents.Remove(
                        domain.Documents.FirstOrDefault(c => dto.DocumentDtos.Any(x => x.Type == EDocumentType.Resume.ToString() && x.Id == c.DocumentId)));

                domain.Documents.Add(Helper.GetDocument(dto.Resume, EDocumentType.Resume));
            }

            if (dto.Photo != null)
            {
                if (dto.DocumentDtos.Any(c => c.Type == EDocumentType.Photo.ToString()))
                    domain.Documents.Remove(
                        domain.Documents.FirstOrDefault(c => dto.DocumentDtos.Any(x => x.Type == EDocumentType.Photo.ToString() && x.Id == c.DocumentId)));

                domain.Documents.Add(Helper.GetDocument(dto.Photo, EDocumentType.Photo));
            }

            if (dto.RadiologyLicense != null)
            {
                if (dto.DocumentDtos.Any(c => c.Type == EDocumentType.RadiologyLicense.ToString()))
                    domain.Documents.Remove(
                        domain.Documents.FirstOrDefault(c => dto.DocumentDtos.Any(x => x.Type == EDocumentType.RadiologyLicense.ToString() && x.Id == c.DocumentId)));

                domain.Documents.Add(Helper.GetDocument(dto.RadiologyLicense, EDocumentType.RadiologyLicense));
            }
            return domain;
        }

        private TempAvailability MapToTempAvailability(TempAvailabilityDto newDay, int tempId)
        {
            var tempavailability = new TempAvailability()
            {
                DayOfWeekAvailable = newDay.DayOfWeekAvailable
            };

            tempavailability.TempId = tempId;

            return tempavailability;
        }
    }

    public interface ITempMapper
    {
        TempDto MapToTempDto(Temp domain);
        LicenseDto MapToLicenseDto(ENoLicensePosition position);
        Temp MapToTemp(TempDto dto);
        TempShortDto MapToTempShortDto(Temp domain);
        Temp MapToTemp(Temp domain, TempDto dto);
        LicenseDto MapToLicenseDto(License domain);
    }
}