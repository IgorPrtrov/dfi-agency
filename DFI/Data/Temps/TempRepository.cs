﻿using System;
using System.Collections.Generic;
using System.Linq;
using DFI.Helpers;
using DFI.Models;
using static DFI.Helpers.Helper;

namespace DFI.Data.Temps
{
    public class TempRepository : Repository<Temp>, ITempRepository
    {
        private readonly IApplicationDbContext _context;
        public TempRepository(IApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public Temp GetByPersoneId(int? personId)
        {
            return _context.Temps.FirstOrDefault(c => c.PersonId == personId);
        }

        public int? GetTempIdForSendTestEmail()
        {
            return _context.Temps.FirstOrDefault(c => string.IsNullOrEmpty(c.Email))?.TempId;
        }

        public List<Temp> GetTemps(TempSearchDto dto)
        {
            var temps = _context.Temps.Where(c => c.Status != (int)Helper.EState.New && c.Status != (int)Helper.EState.Deleted).AsQueryable();

            if (!string.IsNullOrEmpty(dto.Email))
                temps = temps.Where(c => c.Email.ToLower().Contains(dto.Email.ToLower()));

            if (!string.IsNullOrEmpty(dto.FirstName))
                temps = temps.Where(c => c.FirstName.ToLower().Contains(dto.FirstName.ToLower()));

            if (!string.IsNullOrEmpty(dto.LastName))
                temps = temps.Where(c => c.LastName.ToLower().Contains(dto.LastName.ToLower()));

            if (dto.Licenseses.Any())
                temps = temps.Where(c => c.Licenses.Any(x => dto.Licenseses.Any(d => d == x.LicenseType)));

            if (dto.Statuses.Any())
                temps = temps.Where(c => dto.Statuses.Any(x => x == c.Status));

            if (dto.Days.Any())
                temps = temps.Where(c => dto.Days.All(v => c.TempAvailabilities.Any(d => (int)d.DayOfWeekAvailable == (int)v)));
            

            if (!string.IsNullOrEmpty(dto.PhoneNumber))
                temps = temps.Where(c => c.CellPhone.ToLower().Contains(dto.PhoneNumber));

            switch (dto.TempColumn)
            {
                case ETempColumn.FirstName:
                    temps = dto.Order == Helper.EOrder.Asc
                        ? temps.OrderBy(c => c.FirstName)
                        : temps.OrderByDescending(c => c.FirstName);
                    break;

                case ETempColumn.LastName:
                    temps = dto.Order == Helper.EOrder.Asc
                        ? temps.OrderBy(c => c.LastName)
                        : temps.OrderByDescending(c => c.LastName);
                    break;

                case ETempColumn.Status:
                    temps = dto.Order == Helper.EOrder.Asc
                        ? temps.OrderBy(c => c.Status)
                        : temps.OrderByDescending(c => c.Status);
                    break;
                default:
                    temps = temps.OrderByDescending(c => c.TempId);
                    break;
            }

            return temps.ToList();
        }

        public List<string> GetSearchLicenses()
        {
            return _context.Temps.Where(c => c.Status != (int)EState.Deleted && c.Status != (int)EState.New)
                .SelectMany(v => v.Licenses).Where(c => c.Status == EState.Active.ToString())
                .GroupBy(c => c.LicenseType).Select(c => c.Key).ToList();
        }

        public Document GetFile(int? id)
        {
            return _context.Documents.FirstOrDefault(c => c.DocumentId == id);
        }

        public Temp GetByEmail(string email)
        {
            return _context.Temps.FirstOrDefault(c => c.Email.ToLower() == email.ToLower());
        }

        public bool HasLicenseNumber(string licenseNumber)
        {
            return _context.Licenses.Any(c => c.LicenseNumber.ToLower().Trim() == licenseNumber.ToLower().Trim());
        }

        public List<License> GetExpiredLicenses(EState tempState)
        {
            return _context.Licenses.ToList().Where(c =>
                !string.IsNullOrEmpty(c.LicenseNumber) && c.ExpiryDate != null && DateTime.Now.ToUniversalTime().AddHours(c.Temp?.OffsetHours ?? 0) > c.ExpiryDate && c.Temp?.Status == (int)tempState).ToList();
        }

        public string[] GetTempFirstName()
        {
            return _context.Temps
                .Where(c => c.Status != (int) Helper.EState.New && c.Status != (int) Helper.EState.Deleted)
                .Select(c => c.FirstName).GroupBy(c => c).Select(c => c.Key).ToArray();
        }

        public LabelValue[] GetTempFullNameExceptTempsAlreadyAreThisRequest(int requestId)
        {
            var requestLicenseType = _context.Requests.FirstOrDefault(c => c.RequestId == requestId).LicenseType;
            var tempsOnRequestAlready = _context.Requests.FirstOrDefault(c => c.RequestId == requestId).RequestAssignees.Select(c => c.TempId).ToHashSet();

            return _context.Temps
                .Where(c => c.Status == (int)EState.Active
                && c.Licenses.Any(b => b.LicenseType == requestLicenseType))
                .Select(temp => new LabelValue()
                {
                    label = temp.FirstName + " " + temp.LastName,
                    id = temp.TempId
                }).ToList().Where(c => !tempsOnRequestAlready.Contains(c.id)).ToArray();
        }

        public string[] GetTempLastNames()
        {
            return _context.Temps
                .Where(c => c.Status != (int)Helper.EState.New && c.Status != (int)Helper.EState.Deleted)
                .Select(c => c.LastName).GroupBy(c => c).Select(c => c.Key).ToArray();
        }

        public string[] GetTempEmail()
        {
            return _context.Temps
                .Where(c => c.Status != (int)Helper.EState.New && c.Status != (int)Helper.EState.Deleted)
                .Select(c => c.Email).GroupBy(c => c).Select(c => c.Key).ToArray();
        }

        public string[] GetTempPhoneNumber()
        {
            return _context.Temps
                .Where(c => c.Status != (int)Helper.EState.New && c.Status != (int)Helper.EState.Deleted)
                .Select(c => c.CellPhone).GroupBy(c => c).ToArray().Select(c => c.Key.AsPhoneNumber()).ToArray();
        }

        public void DeleteTempDocument(int tempId, EDocumentType type)
        {
            var doc = _context.Documents.FirstOrDefault(c => c.TempId == tempId && c.DocumentType == (int)type);
            if (doc == null)
                return;

            _context.Documents.Remove(doc);
            _context.SaveChanges();
        }
        public void DeleteTempAvailability()
        {
            var tempAvailabilitiesToDelete = _context.TempAvailabilities.Where(t => t.TempId == null).ToList();

            if (tempAvailabilitiesToDelete.Any())
            {
                foreach (var item in tempAvailabilitiesToDelete)
                {
                    _context.TempAvailabilities.Remove(item);
                    _context.SaveChanges();
                }                
            }
        }
    }
    public interface ITempRepository : IRepository<Temp>
    {
        Temp GetByPersoneId(int? personId);
        int? GetTempIdForSendTestEmail();
        List<Temp> GetTemps(TempSearchDto dto);
        List<string> GetSearchLicenses();
        Document GetFile(int? id);
        Temp GetByEmail(string email);
        bool HasLicenseNumber(string licenseNumber);
        List<License> GetExpiredLicenses(EState tempState);
        string[] GetTempFirstName();
        string[] GetTempLastNames();
        string[] GetTempEmail();
        string[] GetTempPhoneNumber();
        LabelValue[] GetTempFullNameExceptTempsAlreadyAreThisRequest(int requestId);
        void DeleteTempDocument(int tempId, EDocumentType type);
        void DeleteTempAvailability();
    }
}