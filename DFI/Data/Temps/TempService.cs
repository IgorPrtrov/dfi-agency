using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DFI.Data.Accounts;
using DFI.Data.Documents;
using DFI.Data.Emails;
using DFI.Data.GoogleMaps;
using DFI.Data.Graders;
using DFI.Data.Requests;
using DFI.Factory;
using DFI.Helpers;
using DFI.Models;
using Microsoft.AspNet.Identity;
using PagedList;

namespace DFI.Data.Temps
{
    public class TempService : ITempService
    {
        private readonly IGraderService _graderService;
        private readonly ITempRepository _tempRepository;
        private readonly ITempMapper _tempMapper;
        private readonly ILogger _logger;
        private readonly IEmailService _emailService;
        private readonly IDocumentMapper _documentMapper;
        private readonly IGoogleMapService _googleMapService;
        private readonly IRequestRepository _requestRepository;
        private readonly IApplicationUserManagerFactory _userManagerFactory;

        public const int PageSize = 10;

        public TempService(IGraderService graderService, ITempRepository tempRepository, ITempMapper tempMapper, ILogger logger,
            IEmailService emailService, IDocumentMapper documentMapper, IGoogleMapService googleMapService, IRequestRepository requestRepository, IApplicationUserManagerFactory userManagerFactory)
        {
            _graderService = graderService;
            _tempRepository = tempRepository;
            _tempMapper = tempMapper;
            _logger = logger;
            _emailService = emailService;
            _documentMapper = documentMapper;
            _googleMapService = googleMapService;
            _requestRepository = requestRepository;
            _userManagerFactory = userManagerFactory;
        }

        public TempRegistrationFormDto GetRegistrationForm(GetRegistrationFormDto dto)
        {
            _logger.Info($"GetRegistrationForm Temp:: Start. LicenseNumber = {dto.LicenseNumber}, IsReceptionist = {dto.IsReceptionist}," +
                         $" IsOfficeManager = {dto.IsOfficeManager}, IsDentalAssistant = {dto.IsDentalAssistant}, Last Name = {dto.LastName}");
            var result = new TempRegistrationFormDto();

            if (!string.IsNullOrEmpty(dto.LicenseNumber) && !string.IsNullOrEmpty(dto.LastName))
            {
                var validationResult = _graderService.StartGrader(new StartGraderDto()
                {
                    LicenseNumber = dto.LicenseNumber,
                    LastName = dto.LastName,
                    GraderType = EGragerType.CreateTemp
                });

                _logger.Info($"GetRegistrationForm Temp:: TempId = {validationResult.TempId} Last Name = {dto.LastName}, " +
                             $"Validation messages = {string.Join(", ", validationResult.Messages)}, Validation status = {validationResult.Success}");

                if (validationResult.Success)
                {
                    var temp = _tempRepository.GetById(validationResult.TempId);

                    if (temp.Status != null && temp.Status != (int) Helper.EState.New && temp.Status != (int) Helper.EState.Deleted)
                    {
                        result.FindingResult = EStatusCode.AlreadyExists;
                        return result;
                    }

                    if (temp.Status != null && temp.Status == (int)Helper.EState.Deleted)
                    {
                        result.FindingResult = EStatusCode.AccountWasDeleted;
                        return result;
                    }

                    if (temp.LastName.ToLower() == dto.LastName.ToLower())
                        result.TempDto = _tempMapper.MapToTempDto(temp);

                    else
                    {
                        result.FindingResult = EStatusCode.LicenseNotFound;
                        return result;
                    }
                }
                else
                {
                    result.FindingResult = EStatusCode.ValidationGraderError;
                    result.ValidationGraderError = string.Join(", ", validationResult.Messages);
                    return result;
                }
            }

            if (dto.IsReceptionist && result.TempDto.LicenseDtos.All(c => c.LicenseType != ENoLicensePosition.Receptionist.GetDisplayName()))
                result.TempDto.LicenseDtos.Add(_tempMapper.MapToLicenseDto(ENoLicensePosition.Receptionist));

            if(dto.IsOfficeManager && result.TempDto.LicenseDtos.All(c => c.LicenseType != ENoLicensePosition.OfficeManager.GetDisplayName()))
                result.TempDto.LicenseDtos.Add(_tempMapper.MapToLicenseDto(ENoLicensePosition.OfficeManager));

            if (dto.IsDentalAssistant && result.TempDto.LicenseDtos.All(c => c.LicenseType != ENoLicensePosition.DentalAssistant.GetDisplayName()))
                result.TempDto.LicenseDtos.Add(_tempMapper.MapToLicenseDto(ENoLicensePosition.DentalAssistant));

            _logger.Info("GetRegistrationForm Temp:: Finish.");
            return result;
        }

        public void SubmitRegistrationForm(TempRegistrationFormDto dto)
        {
            var name = _userManagerFactory.GetUserManager().FindByName(dto.TempDto.Email);
            if (name == null)
            {
                if (dto.TempDto.LicenseDtos == null || dto.TempDto.LicenseDtos.All(c => c.IsDeleted))
                    throw new Exception("At least one license is required.");

                var licensesForDelete = dto.TempDto.LicenseDtos.Where(c => c.IsDeleted).ToList();
                if(licensesForDelete.Any(c => c.LicenseType == ENoLicensePosition.DentalAssistant.ToString()))
                {
                    _tempRepository.DeleteTempDocument(dto.TempDto.Id ?? 0, EDocumentType.RadiologyLicense);
                }
                licensesForDelete.ForEach(c => dto.TempDto.LicenseDtos.Remove(c));

                ApplicationUser applicationUser = new ApplicationUser
                {
                    Email = dto.TempDto.Email,
                    UserName = dto.TempDto.Email,
                    Status = (int)Helper.EState.Submitted
                };
                IdentityResult result = _userManagerFactory.GetUserManager().Create(applicationUser, dto.Password);

                if (result.Succeeded)
                {
                    _userManagerFactory.GetUserManager().AddToRole(applicationUser.Id, Role.Temp);

                    _logger.Info($"SubmitRegistrationForm Temp:: ApplicationUser Id = {applicationUser.Id}");

                    string code = _userManagerFactory.GetUserManager().GenerateEmailConfirmationToken(applicationUser.Id);
                    if(string.IsNullOrEmpty(code))
                        throw new Exception("SubmitRegistrationForm Temp:: Email Confirmation Token is null");

                    dto.CallbackUrl = dto.CallbackUrl.Replace("userIdValue", HttpUtility.UrlEncode(applicationUser.Id)).Replace("codeConfirmationValue", HttpUtility.UrlEncode(code));

                    _logger.Info($"SubmitRegistrationForm Temp:: CallbackUrl = {dto.CallbackUrl}");
                }
                else
                    throw new Exception($"SubmitRegistrationForm Temp:: IdentityResult Error:{string.Join("; ", result.Errors)}");
            }
            else
                throw new Exception($"SubmitRegistrationForm Temp:: UserManager Error: user already exists with email = {dto.TempDto.Email}");

            dto.TempDto.TempStatus = (int)Helper.EState.Submitted;

            if (dto.TempDto.Id == null)
                AddTemp(dto.TempDto);
            else
                UpdateTemp(dto.TempDto);

            _emailService.AddEmail(new EmailDto()
            {
                Body = string.Format(EmailConstants.TempRegistrationFormBody, dto.CallbackUrl),
                Subject = string.Format(EmailConstants.TempRegistrationFormSubject),
                To = dto.TempDto.Email
            });
        }

        public void AddTemp(TempDto dto)
        {
            var temp = _tempMapper.MapToTemp(dto);

            var location = _googleMapService.GetLocation($"{temp.AddressLine1}, {temp.City}, {temp.Zipcode}");
            if (location.Latitude != 0 && location.Longitude != 0)
            {
                temp.Latitude = location.Latitude;
                temp.Longitude = location.Longitude;
                temp.TimeZoneId = _googleMapService.GetTimeZoneId(location.Latitude, location.Longitude);
            }

            _tempRepository.Add(temp);
            _tempRepository.SaveChanges();
        }

        public List<TempDto> GetTemps()
        {
            var temps = _tempRepository.GetAll().Where(c => c.Status != (int) Helper.EState.Deleted).ToList();
            return temps.Select(c => _tempMapper.MapToTempDto(c)).ToList();
        }

        public ListTempDto GetListTempDtos(TempSearchDto dto)
        {
            var temps = _tempRepository.GetTemps(PrepareSearchModel(dto));
            var tempsShort = temps
                .Where(c => c.Status != (int)Helper.EState.Deleted)
                .Select(c => _tempMapper.MapToTempShortDto(c))
                .ToList();

            int pageNumber = (dto.Page ?? 1);
            var result = tempsShort.ToPagedList(pageNumber, PageSize);

            return new ListTempDto
            {
                TempShortDtos = result
            };
        }

        public TempSearchDto GetTempSearchDto()
        {
            return new TempSearchDto()
            {
                Licenseses = _tempRepository.GetSearchLicenses()
            };
        }

        public TempDto GetTemp(int? tempId)
        {
            var tempDomain = _tempRepository.GetById(tempId);

            if (tempDomain == null)
                throw new Exception($"GetTemp:: temp is null with id = {tempId}");

            if (tempDomain.Status == (int)Helper.EState.New || tempDomain.Status == (int)Helper.EState.Deleted)
                throw new Exception($"GetTemp:: temp was deleted or did not submitted, id = {tempId}");

            var newEmail = _userManagerFactory.GetUserManager().Users.FirstOrDefault(c => c.Email == tempDomain.Email)?.NewEmail;

            var tempDto = _tempMapper.MapToTempDto(tempDomain);
          
            tempDto.NewEmail = newEmail;

            return tempDto;
        }

        public byte[] GetTempPhoto(int? tempId)
        {
            var tempDomain = _tempRepository.GetById(tempId);
            if (tempDomain == null)
                throw new Exception($"GetTempPhoto:: temp is null with id = {tempId}");

            return tempDomain.Documents.FirstOrDefault(c => c.DocumentType == (int) EDocumentType.Photo)?.Data;
        }

        public void UpdateTemp(TempDto dto)
        {
            var tempDomain = _tempRepository.GetById(dto.Id);
            if (tempDomain == null)
                throw new Exception($"GetTemp:: temp is null with id = {dto.Id}");

            if (dto.LicenseDtos == null || dto.LicenseDtos.All(c => c.IsDeleted))
                throw new Exception("At least one license is required.");

            var licensesForDelete = dto.LicenseDtos.Where(c => c.IsDeleted).ToList();
            if (licensesForDelete.Any(c => c.LicenseType == ENoLicensePosition.DentalAssistant.ToString()))
            {
                _tempRepository.DeleteTempDocument(dto.Id ?? 0, EDocumentType.RadiologyLicense);
            }
            licensesForDelete.ForEach(c => dto.LicenseDtos.Remove(c));

            _tempRepository.DeleteTempAvailability();

            var location = new LocationDto();
            if (tempDomain.AddressLine1 != dto.AddressFromGoogle.Split(',')[0].Trim() ||
                tempDomain.City != dto.AddressFromGoogle.Split(',')[1].Trim() ||
                tempDomain.Zipcode != dto.AddressFromGoogle.Split(',')[3].Trim())
            {
                location = _googleMapService.GetLocation(dto.AddressFromGoogle);
            }

            var domain = _tempMapper.MapToTemp(tempDomain, dto);

            if (location.Latitude != 0 && location.Longitude != 0)
            {
                domain.TempFacilityDistances.ToList().ForEach(c => _tempRepository.Delete(c));
                domain.Latitude = location.Latitude;
                domain.Longitude = location.Longitude;
                domain.TimeZoneId = _googleMapService.GetTimeZoneId(location.Latitude, location.Longitude);
            }
            _tempRepository.Update(domain);
            _tempRepository.SaveChanges();
        }

        public ValidationStatus SetStatus(int id, Helper.EState newStatus)
        {
            var result = new ValidationStatus(){Success = true};
            var domain = _tempRepository.GetById(id);
            if (domain == null)
            {
                result.Success = false;
                result.Messages.Add($"Professional is null with id = {id}");
                _logger.Error($"SetStatus:: professional is null with id = {id}");
                return result;
            }

            var oldStatus = (Helper.EState)domain.Status;

            if (oldStatus == Helper.EState.New && newStatus == Helper.EState.Active)
            {
                result.Success = false;
                result.Messages.Add($"Professional status cannot be changed from {oldStatus.ToString()} to {newStatus.ToString()}");
                return result;
            }

            var appUser = _userManagerFactory.GetUserManager().Users.FirstOrDefault(c => c.Email == domain.Email);
            if (appUser == null)
            {
                result.Success = false;
                result.Messages.Add($"Application user is null with email = {domain.Email}");
                _logger.Error($"SetStatus:: application user is null with email = {domain.Email}");
                return result;
            }

            if (!appUser.EmailConfirmed && newStatus == Helper.EState.Active)
            {
                result.Success = false;
                result.Messages.Add($"Professional can not be Active without email confirmation");
                return result;
            }

            HandleRequestsIfTempInactiveOrDeletedOrBackActive(newStatus, id);

            domain.Status = (int)newStatus;
            domain.StatusChangedDate = DateTime.UtcNow;
            domain.StatusChangedBy = (int)Helper.EActor.Admin;

            var user = _userManagerFactory.GetUserManager().FindByName(domain.Email);

            //implemented because if Administrator wants to recreate this user again. The email should not exist.
            var emailMerkedAsDeleted = $"{domain.Email}_deleted_on_{DateTime.Now.ToString("yyyy_MMM_d_HH_mm_ss")}";
            if(newStatus == Helper.EState.Deleted)
            {
                domain.Email = emailMerkedAsDeleted;
            }

            _tempRepository.Update(domain);
            _tempRepository.SaveChanges();

            if (user != null)
            {
                user.Status = (int)newStatus;

                if (newStatus == Helper.EState.Deleted)
                {
                    user.Email = emailMerkedAsDeleted;
                    user.UserName = emailMerkedAsDeleted;
                }

                var resultUpdate = _userManagerFactory.GetUserManager().Update(user);
                OnStatusChange(domain, oldStatus);
            }
            
            return result;
        }

        private void HandleRequestsIfTempInactiveOrDeletedOrBackActive(Helper.EState newStatus, int tempId)
        {
            if (newStatus == Helper.EState.Deleted || newStatus == Helper.EState.InActive || newStatus == Helper.EState.Active)
            {
                var requests = _requestRepository.GetNeedsApprovalOrFilledRequestsForTemp(tempId);

                foreach (var request in requests)
                {
                    var tempFromRequest = request.RequestAssignees.FirstOrDefault(c => c.TempId == tempId);
                    if (tempFromRequest != null)
                    {
                        tempFromRequest.Status = newStatus == Helper.EState.Active ? (int)ERequestAssigneeStatus.WaitingForResponse
                                                            : newStatus == Helper.EState.InActive ? (int)ERequestAssigneeStatus.Inactive
                                                            : (int)ERequestAssigneeStatus.Deleted;

                        if(request.Status == (int)ERequestStatus.Filled 
                           && request.RequestAssignees.All(c => c.Status != (int)ERequestAssigneeStatus.Filled))
                        {
                            request.Status = (int)ERequestStatus.NeedsApproval;
                            request.PriceForOffice = null;
                            request.PriceForTemp = null;

                            _emailService.AddEmail(new EmailDto()
                            {
                                To = request.FacilityLocation.Facility.Email,
                                Cc = Setting.AdministratorEmail,
                                Subject = EmailConstants.RequestChangedStatusToNeedApprovalSubject,
                                Body = string.Format(
                                    EmailConstants.RequestChangedStatusToNeedApprovalBody,
                                    Helper.GetRequestUrl(request.RequestId))
                            });
                        }
                        _requestRepository.Update(request);
                        _requestRepository.SaveChanges();
                    }
                }
            }
        }

        public DocumentDto GetFile(int? fileId)
        {
            if(fileId == null)
                return new DocumentDto();

            var file = _tempRepository.GetFile(fileId);
            if (file == null)
                throw new Exception($"GetTemp:: file is null with id = {fileId}");

            return _documentMapper.MapToDocumentDto(file, needContent: true);
        }

        private void OnStatusChange(Temp temp, Helper.EState oldState)
        {
            var body = string.Format(EmailConstants.ChangeStatusBody, oldState.ToString(), ((Helper.EState)temp.Status).ToString());
            var subject = EmailConstants.ChangeStatusSubject;

            if (((Helper.EState)temp.Status) == Helper.EState.Active)
            {
                body = string.Format(EmailConstants.ApproveTempAccountBody, Helper.GetLoginUrl());
                subject = EmailConstants.ApproveTempAccountSubject;
            }

            _emailService.AddEmail(new EmailDto
            {
                Body = body,
                Subject = subject,
                To = temp.Email
            });
        }

        public TempDto GetProfile(string email)
        {
            var temp = _tempRepository.GetByEmail(email);
            if (temp == null)
                throw new Exception($"GetProfile::temp is null with email = {email}");

            return GetTemp(temp.TempId);
        }

        public void SendBulkEmails(TempEmailLightDto dto)
        {
            var temps = _tempRepository.GetTemps(new TempSearchDto()
            {
                Email = dto.TempSearchDto.Email,
                FirstName = dto.TempSearchDto.FirstName,
                LastName = dto.TempSearchDto.LastName,
                PhoneNumber = dto.TempSearchDto.PhoneNumber,
                Licenseses = dto.TempSearchDto.Licenseses,
                Statuses = dto.TempSearchDto.Statuses,
                Order = dto.TempSearchDto.Order,
                TempColumn = dto.TempSearchDto.TempColumn,
                Days = dto.TempSearchDto.Days
            });

            _logger.Info($"{nameof(SendBulkEmails)}:: emails = {string.Join(", ", temps?.Select(c => c.Email))}");

            foreach (var temp in temps)
            {
                _emailService.AddEmail(new EmailDto()
                {
                    Body = dto.EmailLightDto?.Body,
                    Subject = dto.EmailLightDto?.Subject,
                    To = temp.Email
                });
            }
        }

        private TempSearchDto PrepareSearchModel(TempSearchDto dto)
        {
            if (dto?.PhoneNumber != null)
            {
                dto.PhoneNumber = dto.PhoneNumber.TrimPhoneNumber();
            }

            return dto;
        }

        public void CheckTempLicensesExpirationDay()
        {
            var expiratedLicenses = _tempRepository.GetExpiredLicenses(Helper.EState.Active);
            foreach (var license in expiratedLicenses)
            {
                var result = _graderService.StartGrader(new StartGraderDto()
                {
                    LicenseNumber = license.LicenseNumber,
                    LastName = license.Temp.LastName,
                    GraderType = EGragerType.CheckLicense
                });

                if(result.Success == false)
                {
                    _emailService.AddEmail(new EmailDto()
                    {
                        To = $"{license.Temp.Email}",
                        Body = string.Format(EmailConstants.LicenseVerificationBody, string.Join(", ", result.Messages),
                                                    license.LicenseNumber, license.Temp.LastName),

                        Subject = string.Format(EmailConstants.LicenseVerificationSubject)
                    });
                }  
            }
        }

        public string[] GetTempFirstName()
        {
            return _tempRepository.GetTempFirstName();
        }

        public LabelValue[] GetTempFullName(int requestId)
        {
            return _tempRepository.GetTempFullNameExceptTempsAlreadyAreThisRequest(requestId);
        }

        public string[] GetTempLastNames()
        {
            return _tempRepository.GetTempLastNames();
        }

        public string[] GetTempEmail()
        {
            return _tempRepository.GetTempEmail();
        }

        public string[] GetTempPhoneNumber()
        {
            return _tempRepository.GetTempPhoneNumber();
        }
    }

    public interface ITempService
    {
        TempRegistrationFormDto GetRegistrationForm(GetRegistrationFormDto dto);
        void AddTemp(TempDto dto);
        void SubmitRegistrationForm(TempRegistrationFormDto dto);
        List<TempDto> GetTemps();
        ListTempDto GetListTempDtos(TempSearchDto dto);
        TempSearchDto GetTempSearchDto();
        TempDto GetTemp(int? tempId);
        byte[] GetTempPhoto(int? tempId);
        void UpdateTemp(TempDto dto);
        DocumentDto GetFile(int? fileId);
        ValidationStatus SetStatus(int id, Helper.EState newStatus);
        TempDto GetProfile(string email);
        void SendBulkEmails(TempEmailLightDto dto);
        void CheckTempLicensesExpirationDay();
        string[] GetTempFirstName();
        string[] GetTempLastNames();
        string[] GetTempEmail();
        string[] GetTempPhoneNumber();
        LabelValue[] GetTempFullName(int requestId);
    }
}