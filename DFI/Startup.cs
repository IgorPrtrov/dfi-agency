﻿using System;
using DFI.Data.Accounts;
using DFI.Models;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup(typeof(DFI.Startup))]

namespace DFI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            app.CreatePerOwinContext(ApplicationDbContext.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString(Setting.LoginPath)
            });
            var options = new BackgroundJobServerOptions { WorkerCount = Environment.ProcessorCount * 5 };
            app.UseHangfireServer(options);

            var dashboardOptions = new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() },
                AppPath = Setting.LocalLoginPageUrl
            };
            app.UseHangfireDashboard(Setting.HangfireUrl, dashboardOptions);
        }
    }

    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var owinContext = new OwinContext(context.GetOwinEnvironment());
            var user = owinContext.Authentication.User;

            return user != null && user.Identity.IsAuthenticated && user.IsInRole(Setting.Admin);
        }
    }
}
