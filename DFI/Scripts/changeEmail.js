﻿
var ajaxWorking;

(function () {
    $("body").on('click', "[data-target='#changeEmailModal']", function() {
        $("#changeEmailValidationMessage").hide();
        $("#changeEmailSuccessMessage").hide();
    });

    $("body").on('click', "#submitNewEmail", function (event) {

        event.preventDefault();

        var dto = {};
        dto.Email = $("#EmailChange_Email").val();
        dto.NewEmail = $("#EmailChange_NewEmail").val();

        if ($("#formChangeEmailModal").valid() && !ajaxWorking) {

            ajaxWorking = true;

            $.post("/Account/ChangeEmail", dto)
                .done(function (data) {
                    if (data.Success) {
                        $("#newEmailInfo").show();
                        $("#newEmailRequest").text(dto.NewEmail)
                        ajaxWorking = false;
                        $("#EmailChange_NewEmail").val("");
                        $("#changeEmailSuccessMessage").show();
                        setTimeout(function() { $("#formChangeEmailModal [data-dismiss='modal']").click(); }, 1000);

                    } else {
                        $("#changeEmailValidationMessage").text(data.Messages.join('; '));
                        $("#changeEmailValidationMessage").show();
                        ajaxWorking = false;
                    }
                });
        }
    });
})();