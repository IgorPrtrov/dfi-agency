$(function () {
    loadTemps();

    $("#search").click(function() {
        var dto = getSearchDto();
        loadTemps(dto);
    });

    $("#cleanSearch").click(function() {
        cleanSearch();
        loadTemps();
    });

    $(document).on("click", "[data-group='column']", function () {
        sortting(this);
    });

    $("#sendEmailToAll").click(function() {
        sendBulkEmails();
    })

    $(document).on("click", "[data-id='sendEmailDirectly']", function() {
        emailDirectly = $(this).data("email");
        var title = "Email will send directly to " + $(this).data("fullname");
        $("#sendEmailDirectlyModalLabel").text(title);
    })

    $("#sendEmailDirectly").click(function() {
        sendEmailDirectly();
    })
});

var emailDirectly;

var sendEmailDirectly = function() {
    if ($("#formSendDirectlyEmail").valid()) {
        var dto = {};
        dto.To = emailDirectly;
        dto.Subject = $("#directlyEmailSubject").val();
        dto.Body = $("#directlyEmailBody").val();

        $.ajax({
            type: "POST",
            url: '/professionals/SendDirectlyEmail',
            data: JSON.stringify(dto),
            contentType: "application/json; charset=UTF-8",
            success: function (result) {
                $("#directlyEmailSubject").val("");
                $("#directlyEmailBody").val("");

                $("#sendEmailDirectlyModal").modal('toggle');
                $("#toast-body-success").text("Email was sent successfully");
                $("#toast-changes-saved").toast('show');
            },
            error: function (ex) {
            }
        });
    }
}

var sendBulkEmails = function() {
    if ($("#formSendBulkEmail").valid()) {

        var emailDto = {
            Subject: $("#bulkEmailSubject").val(),
            Body: $("#bulkEmailBody").val()
        }

        var dto = {
            TempSearchDto: getSearchDto(),
            EmailLightDto: emailDto
        }

        $.ajax({
            type: "POST",
            url: '/professionals/SendBulkEmails',
            data: JSON.stringify(dto),
            contentType: "application/json; charset=UTF-8",
            success: function (result) {
                $("#bulkEmailSubject").val("");
                $("#bulkEmailBody").val("");

                $("#sendEmailToAllModal").modal('toggle');
                $("#toast-body-success").text("Emails were sent successfully");
                $("#toast-changes-saved").toast('show');
            },
            error: function (ex) {
            }
        });
    }
}

var sortting = function(item) {
    var oldClass = $(item).next().attr('class');
    var column = $(item).data('id');

    var newClass = '';
    var odred = '';

    var classSort = oldClass.split('-')[3];
    if (classSort === "desc") {
        newClass = "fa-sort-alpha-asc";
        odred = "asc"
    } else {
        newClass = "fa-sort-alpha-desc";
        odred = "desc"
    }

    var dto = getSearchDto();
    dto.TempColumn = column;
    dto.Order = odred;

    var sortDto = {};
    sortDto.OldClass = oldClass.split(' ')[1];
    sortDto.NewClass = newClass;
    sortDto.Column = column

    loadTemps(dto, sortDto);
}

var loadTemps = function (data, sortDto) {
    if (!data)
        data = getSearchDto();

    $("#spinner").show();
    $.ajax({
        type: "POST",
        url: '/professionals/Search',
        data: JSON.stringify(data),
        contentType: "application/json; charset=UTF-8",
        success: function (result) {
            $("#spinner").hide();
            $("#tempListPartialView").html(result)

            if (sortDto) {
                $("[data-id='" + sortDto.Column + "']").next().removeClass(sortDto.OldClass).addClass(sortDto.NewClass).show();
            }

            paginationInit(sortDto);
        },
        error: function (ex) {           
        }
    });
}

var getSearchDto = function() {
    var dto = {};
    dto.Odred = 0;
    dto.TempColumn = -1
    dto.Email = $("#Email").val();
    dto.LastName = $("#LastName").val();
    dto.FirstName = $("#FirstName").val();
    dto.PhoneNumber = $("#PhoneNumber").val();
    dto.Licenseses = $.map($("[data-group='licenses']"),
        function (element) {
            if($(element).prop("checked"))
                return $(element).val();
        });
    dto.Statuses = $.map($("[data-group='statuses']"),
        function (element) {
            if ($(element).prop("checked"))
                return $(element).val();
        });

    dto.Days = $.map($("[data-group='days']"),
        function (element) {
            if ($(element).prop("checked"))
                return $(element).val();
        });

    return dto;
}

var cleanSearch = function()
{
    $("#Email").val('');
    $("#LastName").val('');
    $("#FirstName").val('');
    $("#PhoneNumber").val('');
    $("[data-group='licenses']").each(function (index, item) {
        $(item).prop("checked", false)
    });
    $("[data-group='statuses']").each(function (index, item) {
        $(item).prop("checked", false)
    });
    $("[data-group='days']").each(function (index, item) {
        $(item).prop("checked", false)
    });
}

var paginationInit = function (sortDto)
{
    $(".pagination").on("click", "a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        if (href) {
            var page = href.split("=")[1];
            var dto = getSearchDto();
            dto.Page = page;
            if (sortDto) {
                dto.TempColumn = sortDto.Column;
                dto.Order = sortDto.NewClass.split('-')[3];
            }

            loadTemps(dto, sortDto);
        }
    });
}