﻿$(function () {

    var profitChar = undefined;

    loadChart();

    $(document).on("click", "#submit_profit_chart", function () {
        loadChart();
    });

    $(document).on("click", "#all-charts .list-group-item.list-group-item-action", function () {
        loadChart();
    });


    function loadChart() {
        // Destroy the existing chart if it exists
        if (profitChar !== undefined) {
            profitChar.destroy();
        }
        $('#spinner').show();
        var from = $("#From").val();
        var to = $("#To").val();
        var ajaxUrl = $(".list-group-item.list-group-item-action.active").data("ajax-url");

        $.ajax({
            type: "GET",
            url: '/Charts/' + ajaxUrl + '?from=' + from + '&to=' + to,
            contentType: "application/json; charset=UTF-8",
            success: function (result) {

                var resultOfDatasets = $.map(result.DataSets, function (item) {
                    return {
                        label: item.Label,
                        data: item.Data
                    };
                });

                var ctx = document.getElementById('myChart').getContext('2d');
                profitChar = new Chart(ctx, {
                    type: result.ChartType,
                    data: {
                        labels: result.Labels,
                        datasets: resultOfDatasets
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });

                $('#spinner').hide();
            },
            error: function (ex) {
                console.log(ex);
            }
        });
    }
});