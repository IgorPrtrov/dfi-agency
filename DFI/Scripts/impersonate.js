﻿(function () {
    $("body").on("click", "#impersonateUser", function (e) {
        var email = $("#email").val();
        if (!email) {
            email = $("#Email").val();
        }
            
        $.post("/Account/Impersonate", {
            email: email
        })
        .done(function () {
            window.location.href = window.location.origin;
        });
    });
})();