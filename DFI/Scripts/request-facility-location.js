﻿(function () {

    if (!$("#FacilityName").length) {
        CreateLocationList();
    }

    $("body").on("change", "#FacilityId", function () {
        CreateLocationList();
    });

    $("body").on("change", "#FacilityLocationFullAddress", function () {
        var locationId = $("#FacilityLocationFullAddress option:selected").val();
        $("#FacilityLocationId").val(locationId);
    });

    function CreateLocationList() {

        if ($("#IsAdminLogIn").val() !== 'True') {
            return;
        }

        var facilityId = $("#FacilityId").val();

        $.ajax({
            type: "GET",
            url: "/Facilities/GetLocationsForFacility?FacilityId=" + facilityId,
            cache: false,
            datatype: "json",
            success: function (data) {
                var dropdown = $('#FacilityLocationFullAddress');

                $('#FacilityLocationFullAddress option').remove();

                $.each(data, function (key, entry) {
                    dropdown.append($('<option></option>').val(entry.FacilityLocationId).html(entry.FullAddress));
                });

                var locationId = $("#FacilityLocationFullAddress option:first").val();
                $("#FacilityLocationId").val(locationId);
            }
        });
    }

})();