$(function () {
    loadReviews(1, getTempId());
});

var getTempId = function () {
    return location.search.split('&')[0].split('=')[1];
}
var loadReviews = function (pageNumber, tempId) {

    $("#spinner").show();
    $.ajax({
        type: "GET",
        url: '/ProfessionalReviews/GetAllProfessionalReviews?tempId=' + tempId + '&pageNumber=' + pageNumber,
        contentType: "application/json; charset=UTF-8",
        success: function (result) {
            $("#spinner").hide();
            $("#allReviewsDiv").html(result)

            paginationInit();
        },
        error: function (ex) {           
        }
    });
}

var paginationInit = function ()
{
    $(".pagination").on("click", "a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        if (href) {
            var pageNumber = href.split("=")[1];
            var tempId = getTempId();

            loadReviews(pageNumber, tempId);
        }
    });
}