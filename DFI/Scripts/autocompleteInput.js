﻿$(function () {
    initAutocompleteInput();
});

var initAutocompleteInput = function () {

    var elements = $("[data-type='autocomplete-input']");
    
    elements.each(function (index, item) 
    {
        $.ajax({
            type: "GET",
            url: $(item).data("action"),
            cache: false,
            datatype: "json",
            success: function (data) {

                $(item).autocomplete({
                    source: function (request, response) {
                        var results = $.ui.autocomplete.filter(data, request.term);
                        response(results.slice(0, 10));
                    },
                    autoSelectFirst: true,
                    autoFocus: true,
                    select: function (event, ui) {
                        if ($('#FacilityId') && (location.pathname.indexOf("/Requests/Edit/") == 0 || location.pathname.indexOf("/Requests/Create") == 0) && item.id == "FacilityName") {
                            $('#FacilityId').val(ui.item.id).trigger('change');
                        }

                        if ($("#ProfessionalId_ManulFill") && item.id == "ProfessionalFullName") {
                            $("#ProfessionalId_ManulFill").val(ui.item.id);
                        }

                        if ($("#SearchRequestAssigneeId")) {
                            $("#SearchRequestAssigneeId").val(ui.item.id);
                        }
                    }
                });
            }
        });
    })
}



