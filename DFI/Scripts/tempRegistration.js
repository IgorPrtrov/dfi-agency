﻿(function () {
    $('#spinner').hide();

    if ($("#validationMessageGetForm").text()) {
        $("#validationMessageGetForm").show();
    }

    $("#btnGetRegistrationForm").click(function () {
        $('#spinner').show();
    });

    $("body").on("click", "#validationMessageGetForm", function (e) {
        $(this).hide();
    });

    $(document).ready(function () {
        setTimeout(function () {
            $('#validationMessageGetForm').hide();
        }, 2000);
    });

})();


