﻿(function () {

    var ajaxIsWorking = false;

    $("body").on("click", "#fill-temp-manually", function (e) {
        var tempId = $("#ProfessionalId_ManulFill").val();
        var requestId = $("#Id").val();

        $("#manual-fill-spinner").show();

        if (ajaxIsWorking == false) {
            ajaxIsWorking = true;

            $.post("/Requests/ManualFillRequest", {
                requestId: requestId,
                tempId: tempId
            })
                .done(function (data) {
                    $("#manual-fill-spinner").hide();
                    ajaxIsWorking = false;

                    if (data.Success) {
                        $("#manually-fill-successMessageId").show();
                        setTimeout(function () { location.reload(); }, 2000);
                    } else {
                        $("#manually-fill-validationMessageId").text(data.Messages.join(' '));
                        $("#manually-fill-validationMessageId").show();
                        setTimeout(function () { $("#manually-fill-validationMessageId").hide(); }, 4000);
                    }
                });
        }   
    });
})();