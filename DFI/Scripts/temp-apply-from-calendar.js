(function () {

    var ajaxIsWorking = false;

    $("body").on("click", "#apply-from-calendar", function (e) {

        var tempId = $(this).data("temp-id");
        var longGuid = $(this).data("long-guid");
        var applyFromCalendar = $(this).data("apply-from-calendar");

        $("#manual-apply-spinner").show();

        if (ajaxIsWorking == false) {
            ajaxIsWorking = true;

            $.get("/Requests/Apply", {
                tempId: tempId,
                longGuid: longGuid,
                applyFromCalendar: applyFromCalendar
            })
                .done(function (data) {
                    $("#manual-apply-spinner").hide();
                    ajaxIsWorking = false;

                    if (data.Success) {
                        $("#apply-from-calendar").remove();
                        $("#manually-apply-successMessageId").show();
                        setTimeout(function () { $("#manually-apply-successMessageId").hide() }, 2000);
                    } else {
                        $("#manually-apply-validationMessageId").text(data.Messages.join(' '));
                        $("#manually-apply-validationMessageId").show();
                        setTimeout(function () { $("#manually-apply-validationMessageId").hide() }, 4000);
                    }
                });
        }   
    });

    $("#requestModal").on("hidden.bs.modal", function () {
        $("body").off("click", "#apply-from-calendar");
    });

})();