﻿var RequestAssigneesController = (function () {

    var requestId;
    var isAjaxRequestProcessing;
    var sortDtoGlobal;

    var RequestAssigneeId;

    var Init = function () {
        requestId = $("#Id").val();
    }

    var FillRequestOrCancel = function (id, actionName, button) {
        if (id === 0)
            return false;

        if (isAjaxRequestProcessing)
            return false

        isAjaxRequestProcessing = true;
        $("#spinner").show();

        $.ajax({
            type: "POST",
            url: "/Requests/" + actionName,
            data: {
                requestAssigneeId: id
            },
            cache: false,
            datatype: "json",
            success: function (data) {
                if (data.Success) {
                    var activePage = $(".pagination-container li.active a").text()
                    LoadRequestAssignees(getDto(activePage), sortDtoGlobal)
                    UpdateRequestPrice();
                } else {
                    $("#toast-changes-error-message").html("<p>" + data.Messages.join("</p><p>") + "</p>");
                    $("#toast-changes-error").toast('show');
                }
                $("#spinner").hide();
                isAjaxRequestProcessing = false;
            },
            error: function (error) {
                isAjaxRequestProcessing = false;
                $("#spinner").hide();
                condole.log(error);
            }
        });
    };

    function UpdateRequestPrice() {
        $.ajax({
            type: "POST",
            url: "/Requests/GetRequestPrice",
            data: {
                requestId: requestId
            },
            cache: false,
            datatype: "json",
            success: function (data) {
                if (data) {

                    if ($("#priceForOffice")) {
                        $("#priceForOffice").text(data.PriceForOffice)
                    }

                    if ($("#priceForTemp")) {
                        $("#priceForTemp").text(data.PriceForTemp)
                    }
                }
            },
            error: function (error) {
                condole.log(error);
            }
        });
    }


    var LoadRequestAssignees = function (dto, sortDto) {

        $("#spinner").show();
        if (!dto) {
            dto = getDto();
            dto.RequestAssigneeColumn = 2;
            dto.Order = 1;
        }

        $.ajax({
            type: "GET",
            url: "/Requests/GetAssigneesForRequest?RequestId=" + dto.RequestId + "&Order=" + dto.Order + "&RequestAssigneeColumn=" + dto.RequestAssigneeColumn + "&Page=" + dto.Page + "&RequestAssigneeId=" + dto.RequestAssigneeId,
            cache: false,
            datatype: "json",
            success: function (data) {
                $("#ProfessionalsAssignes").html(data);
                if (!sortDto) {
                    $("[data-id='Status']").next().addClass("fa fa-sort-alpha-desc").show();
                }

                if (sortDto) {
                    $("[data-id='" + sortDto.Column + "']").next().removeClass(sortDto.OldClass).addClass(sortDto.NewClass).show();
                }

                paginationInit(sortDto);

                sortDtoGlobal = sortDto;
                $("#spinner").hide();
            }
        });
    }

    var Sorting = function (item) {
        var oldClass = $(item).next().attr('class');
        var column = $(item).data('id');

        var newClass = '';
        var order = '';

        var classSort = oldClass.split('-')[3];
        if (classSort === "desc") {
            newClass = "fa-sort-alpha-asc";
            order = "asc"
        } else {
            newClass = "fa-sort-alpha-desc";
            order = "desc"
        }

        var dto = {};
        dto.RequestId = requestId
        dto.RequestAssigneeColumn = column;
        dto.Order = order;

        var sortDto = {};
        sortDto.OldClass = oldClass.split(' ')[1];
        sortDto.NewClass = newClass;
        sortDto.Column = column;

        LoadRequestAssignees(dto, sortDto);
    }

    var paginationInit = function (sortDto) {
        $(".pagination").on("click", "a", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if (href) {
                var page = href.split("=")[1];
                var dto = getDto(page);
                if (sortDto) {
                    dto.RequestAssigneeColumn = sortDto.Column;
                    dto.Order = sortDto.NewClass.split('-')[3];
                }

                LoadRequestAssignees(dto, sortDto);
            }
        });
    }

    var CancelRequest = function () {

        var dto = {};
        dto.RequestId = $("#Id").val();
        dto.ReasonOfCancelation = $("#CancelationReason").val();

        if (isAjaxRequestProcessing || dto.RequestId === null || dto.RequestId === "")
            return false

        isAjaxRequestProcessing = true;

        $.ajax({
            type: "POST",
            url: "/Requests/CancelRequest",
            data: JSON.stringify(dto),
            cache: false,
            contentType: "application/json; charset=UTF-8",
            success: function (data) {
                if (data.Success) {
                    location.reload();
                } else {
                    $("#toast-changes-error-message").text(data.Messages.join());
                    $("#toast-changes-error").toast('show');
                }
                isAjaxRequestProcessing = false;
            },
            error: function (error) {
                isAjaxRequestProcessing = false;
                condole.log(error);
            }
        });
    };

    function getDto(page) {
        var dto = {};
        dto.RequestAssigneeId = parseInt($("#SearchRequestAssigneeId").val());
        dto.RequestId = requestId
        dto.Order = 0;
        dto.RequestColumn = 1
        dto.Page = page;

        return dto;
    }

    return {
        LoadRequestAssignees: LoadRequestAssignees,
        Init: Init,
        Sorting: Sorting,
        FillRequestOrCancel: FillRequestOrCancel,
        CancelRequest: CancelRequest,
        RequestAssigneeId: RequestAssigneeId
    }
})();

$(function () {
    RequestAssigneesController.Init();

    $(document).on("click", "[data-group='column']", function () {
        RequestAssigneesController.Sorting(this);
    });

    $("#ProfessionalsAssignes-tab").click(function () {
        RequestAssigneesController.LoadRequestAssignees();
    });

    $(document).on("click", "[data-action='set-request-assignee-id']", function () {
        RequestAssigneesController.RequestAssigneeId = $(this).data("requestassigneeid");
        console.log($(this).data("isWaitingForResponse"));
        if ($(this).data("isWaitingForResponse") === "True") {
            $("#modal-text-fill-request").html("Request assigne will be filled, professional, practice and administrator will receive email. You can change your decision in the future. " +
                "<b>A you sure you want to fill the request without professional's response?</b>");
        } else {
            $("#modal-text-fill-request").html("Request assigne will be filled, professional, practice and administrator will receive email. You can change your decision in the future.");
        }
    })

    $(document).on("click", "[data-id='fillrequestorcancel']", function () {
        var id = RequestAssigneesController.RequestAssigneeId;
        var action = $(this).data("action");
        RequestAssigneesController.FillRequestOrCancel(id, action, this);
    });

    $(document).on("click", "[data-action='cancel-request']", function () {
        RequestAssigneesController.CancelRequest();
    });

    $(document).on("click", "#searchTempsOnRequestPage", function () {
        RequestAssigneesController.LoadRequestAssignees();
    });

    $(document).on("click", "#cleanSearchOnRequestPage", function () {
        $("#SearchRequestAssigneeId").val("");
        RequestAssigneesController.LoadRequestAssignees();
    });
});