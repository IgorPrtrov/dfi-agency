﻿(function () {

    $("body").on('click', "[data-action='accept_permanent_placement_agreement']", function () {
        localStorage.setItem("permanentPlacementAgreement", true);
        var isReaddentalFillinsFees = localStorage.getItem("dentalFillinsFees")
        var isReadTemporaryStaffingAgreementForOffice = localStorage.getItem("temporaryStaffingAgreementForOffice")

        if (isReaddentalFillinsFees == null || isReaddentalFillinsFees == "false") {
            setTimeout(function () { $("#dentalFillinsFeesModal").modal('show'); }, 500);          
        }
        else if (isReadTemporaryStaffingAgreementForOffice == null || isReadTemporaryStaffingAgreementForOffice == "false") {
            setTimeout(function () { $("#temporaryStaffingAgreementModal").modal('show'); }, 500);
        }
        else {
            submitAndDisabledBtn();
        }  
    });

    $("body").on('click', "[data-action='accept_dental_fillins_fees_agreement']", function () {
        localStorage.setItem("dentalFillinsFees", true);
        var isReadPermanentPlacementAgreement = localStorage.getItem("permanentPlacementAgreement")
        var isReadTemporaryStaffingAgreementForOffice = localStorage.getItem("temporaryStaffingAgreementForOffice")

        if (isReadPermanentPlacementAgreement == null || isReadPermanentPlacementAgreement == "false") {
            setTimeout(function () { $("#permanentPlacementAgreementModal").modal('show'); }, 500);         
        }
        else if (isReadTemporaryStaffingAgreementForOffice == null || isReadTemporaryStaffingAgreementForOffice == "false") {
            setTimeout(function () { $("#temporaryStaffingAgreementModal").modal('show'); }, 500);
        }
        else {
            submitAndDisabledBtn();
        }
    });

    $("body").on('click', "[data-action='accept_temporary_staffing_agreement_for_office']", function () {
        localStorage.setItem("temporaryStaffingAgreementForOffice", true);

        var isReadPermanentPlacementAgreement = localStorage.getItem("permanentPlacementAgreement")
        var isReaddentalFillinsFees = localStorage.getItem("dentalFillinsFees")

        if (isReaddentalFillinsFees == null || isReaddentalFillinsFees == "false") {
            setTimeout(function () { $("#dentalFillinsFeesModal").modal('show'); }, 500);
        }
        if (isReadPermanentPlacementAgreement == null || isReadPermanentPlacementAgreement == "false") {
            setTimeout(function () { $("#permanentPlacementAgreementModal").modal('show'); }, 500);
        }
        else {
            submitAndDisabledBtn();
        }
    });

    var submitAndDisabledBtn = function () {
        $("#createFacilityAccount").click();
        $("#createFacilityAccount").prop('disabled', true);
    }

    $("body").on("click", "#createFacilityAccount", function (e) {

        var file = $(selectors.photoInput);
        var validationMassages = '';
        validationMassages = validateFile(file[0]);

        var isValid = $("#FacilityRegistrationForm").valid();

        if (!isValid) {
            validationMassages += "Validation error, take a look at the form above.";
        }

        if (validationMassages.length > 5) {
            e.preventDefault();
            $('[data-id="validationMessage"]').html(validationMassages);
            $('[data-id="validationMessage"]').show();
        } else {
            $('[data-id="validationMessage"]').hide();

            var isReadPermanentPlacementAgreement = localStorage.getItem("permanentPlacementAgreement")
            var isReaddentalFillinsFees = localStorage.getItem("dentalFillinsFees")
            var isReadTemporaryStaffingAgreementForOffice = localStorage.getItem("temporaryStaffingAgreementForOffice")

            var isUrlRegistrationForm = window.location.href.indexOf("Registrator/FacilityRegistration")

            if (isUrlRegistrationForm > 0 && (isReadPermanentPlacementAgreement == null || isReadPermanentPlacementAgreement == "false" ||
                isReaddentalFillinsFees == null || isReaddentalFillinsFees == "false" ||
                isReadTemporaryStaffingAgreementForOffice == null || isReadTemporaryStaffingAgreementForOffice == "false")) {

                e.preventDefault();

                if (isReadPermanentPlacementAgreement == null || isReadPermanentPlacementAgreement == "false") {
                    $("#permanentPlacementAgreementModal").modal('show');
                    return;
                }
                if (isReaddentalFillinsFees == null || isReaddentalFillinsFees == "false") {
                    $("#dentalFillinsFeesModal").modal('show');
                    return;
                }
                if (isReadTemporaryStaffingAgreementForOffice == null || isReadTemporaryStaffingAgreementForOffice == "false") {
                    $("#temporaryStaffingAgreementModal").modal('show');
                    return;
                }

            } else {
                localStorage.setItem("permanentPlacementAgreement", false);
                localStorage.setItem("dentalFillinsFees", false);
                localStorage.setItem("temporaryStaffingAgreementForOffice", false);

                if ($("#spinner").is(":visible"))
                    e.preventDefault();
            }
        }
    });

    $("body").on('change', selectors.photoInput, function () {
        var validExts = ".jpg, .png, .gif";
        $(selectors.customFileValidationMessage).removeClass('field-validation-error');
        $(selectors.customFileValidationMessage).addClass('field-validation-valid');

        if (checkFile(this, validExts)){
            readURL(this);
            setNameFile(this);
        } else {
            $(selectors.customFileValidationMessage).addClass('field-validation-error');
            $(selectors.customFileValidationMessage).removeClass('field-validation-valid');
        }
    });

    $("body").on('click', '#IsBillingEmailSame', function () {
        var email = $(selectors.email).val();

        if ($("#IsBillingEmailSame").prop("checked")) {
            if (email) {
                $(selectors.billingEmail).val(email);
            }
        } else {
            var billingEmail = $(selectors.billingEmail).val();
            if (email === billingEmail){
                $(selectors.billingEmail).val('');
            }
        }
    });

    $("body").on('change', selectors.email, function () {
        checkEmails();
    });

    $("body").on('change', selectors.billingEmail, function () {
        checkEmails();
    });

    function checkEmails() {
        var email = $(selectors.email).val();
        var billingEmail = $(selectors.billingEmail).val();
        var isChecked = $("#IsBillingEmailSame").prop("checked");

        if (email === billingEmail && !isChecked) {
            $("#IsBillingEmailSame").prop("checked", true);
        } else {
            $("#IsBillingEmailSame").prop("checked", false);
        }
    }

    $('[data-id="validationMessage"]').click(function () {
        $(this).hide();
    });

    $("#deletePhoto").on("click", function () {
        $(selectors.photoInput).val('');
        $(selectors.photoInput).files = [];
        $(selectors.photo).attr("src", "/Professionals/GetTempPhoto");
        setNameFile($(selectors.photoInput)[0]);
    });

    $("body").on("click", "#activateFacility", function (e) {
        $("#spinner").show();
        $.post("/Facilities/Activate", {
            id: $("#Id").val()
        })
        .done(function (data) {
            handlerResponseChangeStatus(data);
        });
    });

    $("body").on("click", "#deactivateFacility", function (e) {
        $("#spinner").show();
        $.post("/Facilities/Deactivate", {
            id: $("#Id").val()
        })
        .done(function (data) {
            handlerResponseChangeStatus(data);
        });
    });

    $("body").on("click", "#deleteFacility", function (e) {
        $("#spinner").show();
        $.post("/Facilities/Delete", {
            id: $("#Id").val()
        })
        .done(function (data) {
            handlerResponseChangeStatus(data);
            setTimeout(function () { window.location.href = "/Facilities" }, 2000)
        });
    });
})();

function handlerResponseChangeStatus(data) {
    $("#spinner").hide();
    $('[data-id="validationMessage"]').hide();

    if (data.Success) {
        updateStatusCallback(data.Messages[0]);  
    }
    else{
        $('[data-id="validationMessage"]').html(data.Messages.join('; '));
        $('[data-id="validationMessage"]').show();
    }
}

function updateStatusCallback(status) {
    var statusBadgeClass = "badge badge-pill text-capitalize badge-lg ";
    switch (status) {
    case "active":
        statusBadgeClass += "badge-success";
        $("#activateFacility").addClass("d-none");
        $("#deactivateFacility").removeClass("d-none");
        $("#deleteFacility").removeClass("d-none");
        $("#deleteFacilityBtn").removeClass("d-none");
        $("#FacilityStatus").val("2");
        break;
    case "inactive":
        statusBadgeClass += "badge-danger";
        $("#deactivateFacility").addClass("d-none");
        $("#activateFacility").removeClass("d-none");
        $("#deleteFacility").removeClass("d-none");
        $("#deleteFacilityBtn").removeClass("d-none");
        $("#FacilityStatus").val("3");
        break;
    case "deleted":
        statusBadgeClass += "badge-dark";
        $("#deactivateFacility").addClass("d-none");
        $("#activateFacility").removeClass("d-none");
        $("#deleteFacility").addClass("d-none");
        $("#deleteFacilityBtn").addClass("d-none");
        $("#FacilityStatus").val("4");
        break;
    }

    $('.entity-state > span').removeClass();
    $('.entity-state > span').addClass(statusBadgeClass);
    $('.entity-state > span').text(status);
    $("#toast-changes-saved").toast('show');
    var body = $("html, body");
    body.stop().animate({ scrollTop: 0 }, 500, 'swing');
}

var setNameFile = function(input) {
    if (input.files && input.files[0]) {
        $(input).next().text(input.files[0].name);
    } else{
        $(input).next().text("Select file...")
    }
}

var readURL = function (input) {
    var validationMassages = validateFile(input);
    if (validationMassages.length === 0) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(selectors.photo).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $(selectors.photo).show();

            $(input).next().text(input.files[0].name);
            $('[data-id="validationMessage"]').hide();
        }
    } else {
        $(selectors.photo).hide();
        $('[data-id="validationMessage"]').html(validationMassages);
        $('[data-id="validationMessage"]').show();
    }
}

var checkFile = function (sender, validExts) {
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0 && fileExt != "") {
        $(sender).val("");
        return false;
    }
    else return true;
}

var validateFile = function (file) {

    var validationMessages = '';
    var filename = $(file).val();
    var typeFile = $(file).attr("id").split('_')[1];

    var extension = filename.replace(/^.*\./, '');

    if (extension === filename) {
        extension = '';
    } else {
        extension = extension.toLowerCase();
    }

    if (typeFile === "Photo" && file.files.length > 0) {
        if (extension !== 'jpg' && extension !== 'jpeg' && extension !== 'png') {
            validationMessages += 'Photo must be JPG, GIF or PNG. </br>';
        }
    }

    if (file.files.length > 0 && file.files[0] && file.files[0].size > 5242880) {
        validationMessages += typeFile + ' must be less than 5MB. </br>';
    }

    return validationMessages;
}