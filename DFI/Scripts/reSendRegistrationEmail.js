﻿(function () {
    $("body").on("click", "#resend-registration-email", function (e) {
        var email = $("#email").val();
        if (!email) {
            email = $("#Email").val();
        }
            
        $.post("/Account/ReSendRegistrationEmail", {
            email: email
        })
        .done(function () {
            $("#successMessage").show();
            setTimeout(function () { $("#successMessage").hide() }, 2000);
        });
    });
})();