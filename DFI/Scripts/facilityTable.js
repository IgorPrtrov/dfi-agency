﻿$(function () {
    loadFacilities();

    $("#search").click(function() {
        var dto = getSearchDto();
        loadFacilities(dto);
    });

    $("#cleanSearch").click(function() {
        cleanSearch();
        loadFacilities();
    });

    $(document).on("click", "[data-group='column']", function () {
        sortting(this);
    });

    $("#sendEmailToAll").click(function () {
        sendBulkEmails();
    });

    $(document).on("click", "[data-id='sendEmailDirectly']", function () {
        emailDirectly = $(this).data("email");
        var title = "Email will send directly to " + $(this).data("fullname");
        $("#sendEmailDirectlyModalLabel").text(title);
    });

    $("#sendEmailDirectly").click(function () {
        sendEmailDirectly();
    });
});

var sortting = function(item) {
    var oldClass = $(item).next().attr('class');
    var column = $(item).data('id');

    var newClass = '';
    var order = '';

    var classSort = oldClass.split('-')[3];
    if (classSort === "desc") {
        newClass = "fa-sort-alpha-asc";
        order = "asc"
    } else {
        newClass = "fa-sort-alpha-desc";
        order = "desc"
    }

    var dto = getSearchDto();
    dto.FacilityColumn = column;
    dto.Order = order;

    var sortDto = {};
    sortDto.OldClass = oldClass.split(' ')[1];
    sortDto.NewClass = newClass;
    sortDto.Column = column;

    loadFacilities(dto, sortDto);
}

var loadFacilities = function (data, sortDto) {
    if (!data)
        data = getSearchDto();

    $("#spinner").show();
    $.ajax({
        type: "POST",
        url: '/facilities/Search',
        data: JSON.stringify(data),
        contentType: "application/json; charset=UTF-8",
        success: function (result) {
            $("#spinner").hide();
            $("#facilityListPartialView").html(result);

            if (sortDto) {
                $("[data-id='" + sortDto.Column + "']").next().removeClass(sortDto.OldClass).addClass(sortDto.NewClass).show();
            }

            paginationInit(sortDto);
        },
        error: function (ex) {           
        }
    });
}

var getSearchDto = function() {
    var dto = {};
    dto.Order = 0;
    dto.FacilityColumn = -1;
    dto.Email = $("#Email").val();
    dto.FacilityName = $("#FacilityName").val();
    dto.PhoneNumber = $("#PhoneNumber").val();
    dto.City = $("#City").val();
    dto.PracticeName = $("#PracticeName").val();
    dto.FacilityStatuses = $.map($("[data-group='statuses']"),
        function (element) {
            if ($(element).prop("checked"))
                return $(element).val();
        });

    return dto;
}

var cleanSearch = function()
{
    $("#Email").val('');
    $("#FacilityName").val('');
    $("#PhoneNumber").val('');
    $("#City").val('');
    $("#PracticeName").val('');
    $("[data-group='statuses']").each(function (index, item) {
        $(item).prop("checked", false)
    });
}

var paginationInit = function (sortDto)
{
    $(".pagination").on("click", "a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        if (href) {
            var page = href.split("=")[1];
            var dto = getSearchDto();
            dto.Page = page;
            if (sortDto) {
                dto.FacilityColumn = sortDto.Column;
                dto.Order = sortDto.NewClass.split('-')[3];
            }

            loadFacilities(dto, sortDto);
        }
    });
}


var emailDirectly;

var sendEmailDirectly = function () {
    if ($("#formSendDirectlyEmail").valid()) {
        var dto = {};
        dto.To = emailDirectly;
        dto.Subject = $("#directlyEmailSubject").val();
        dto.Body = $("#directlyEmailBody").val();

        $.ajax({
            type: "POST",
            url: '/Facilities/SendDirectlyEmail',
            data: JSON.stringify(dto),
            contentType: "application/json; charset=UTF-8",
            success: function (result) {
                $("#directlyEmailSubject").val("");
                $("#directlyEmailBody").val("");

                $("#sendEmailDirectlyModal").modal('toggle');
                $("#toast-body-success").text("Email was sent successfully");
                $("#toast-changes-saved").toast('show');
            },
            error: function (ex) {
            }
        });
    }
}

var sendBulkEmails = function () {
    if ($("#formSendBulkEmail").valid()) {

        var emailDto = {
            Subject: $("#bulkEmailSubject").val(),
            Body: $("#bulkEmailBody").val()
        }

        var dto = {
            FacilitySearchDto: getSearchDto(),
            EmailLightDto: emailDto
        }

        $.ajax({
            type: "POST",
            url: '/Facilities/SendBulkEmails',
            data: JSON.stringify(dto),
            contentType: "application/json; charset=UTF-8",
            success: function (result) {
                $("#bulkEmailSubject").val("");
                $("#bulkEmailBody").val("");

                $("#sendEmailToAllModal").modal('toggle');
                $("#toast-body-success").text("Emails were sent successfully");
                $("#toast-changes-saved").toast('show');
            },
            error: function (ex) {
            }
        });
    }
}