var RequestController = (function () {
    var requestId;

    var isAjaxRequestProcessing;

    var Init = function() {
        requestId = $("#Id").val();
    }

    var AddEmptyDat = function() {
        if (isAjaxRequestProcessing){
            return;
        }
        
        isAjaxRequestProcessing = true;
        $("#spinner").show();
        $.ajax({
            type: "POST",
            url: "/Requests/AddEmptyDay",
            data: $('#ajax-form').serialize(),
            datatype: "json",
            cache: false,
            success: function (id) {
                requestId = id;
                loadRequest();
            },
            complete: function (){
                isAjaxRequestProcessing = false;
                $("#spinner").hide();
            }       
        }); 
    };

    var AddEmptyDayWithSameHours = function(item) {
        if (isAjaxRequestProcessing){
            return;
        }
        
        isAjaxRequestProcessing = true;
        $("#spinner").show();
        var data = $('#ajax-form').serialize();
        data += "&SelectedRequestDayIndex=" + $(item).data('id');

        $.ajax({
            type: "POST",
            url: "/Requests/AddEmptyDayWithSameHours",
            data: data,
            cache: false,
            datatype: "json",
            success: function (id) {
                requestId = id;
                loadRequest();
            },
            complete: function (){
                isAjaxRequestProcessing = false;
                $("#spinner").hide();
            }
        });
    };

    var RemoveRequestDay = function(item) {
        if (isAjaxRequestProcessing){
            return;
        }

        isAjaxRequestProcessing = true;
        $("#spinner").show();
        $.ajax({
            type: "POST",
            url: "/Requests/RemoveRequestDay",
            data: {
                EntityId: $(item).data("id")
            },
            cache: false,
            datatype: "json",
            success: function () {
                loadRequest();
            },
            complete: function (){
                isAjaxRequestProcessing = false;
                $("#spinner").hide();
            }
        });
    };

    var RemoveRequest = function() {
        if (requestId === 0){
            return;
        }
        
        isAjaxRequestProcessing = true;
        $("#spinner").show();
        $.ajax({
            type: "POST",
            url: "/Requests/RemoveRequest",
            data: {
                EntityId: $("#Id").val()
            },
            cache: false,
            datatype: "json",
            success: function (){
                window.location.href = "/Requests";
            }
        });
    };

    var Submit = function (actionName, item) {

        if (requestId === 0)
            return;
        
        $("#validationMessageId").hide();

        if (!$("#ajax-form").valid())
            return;

        isAjaxRequestProcessing = true;
        var formData = $("#ajax-form").serialize();

        if ($(item).data('auto-searching') != null) {
            formData += "&IsAutoSearching=" + $(item).data('auto-searching');
        }

        if ($(item).data('auto-searching-without-exp-licenses-check') != null) {
            formData += "&IsAutoSearchingWithoutCheckingLicenseExpirationDay=" + $(item).data('auto-searching-without-exp-licenses-check');
            formData += "&IsAutoSearching=true";
        }

        $("#spinner").show();
        $.ajax({
            type: "POST",
            url: "/Requests/" + actionName,
            data: formData,
            success: function (data) {
                isAjaxRequestProcessing = false;
                $("#spinner").hide();

                if (data.Success) {
                    $("#successMessageId").show();
                    setTimeout(function() {
                        window.location.href = window.location.origin + "/Requests/Edit/" + data.Messages[0];
                    }, 1000)
                } else {
                    $("#validationMessageId").text(data.Messages.join("; "))
                    $("#validationMessageId").show();
                }
            }
        });
    };

    function loadRequest() {
        $.ajax({
            type: "GET",
            url: "/Requests/Get?id=" + requestId,
            cache: false,
            datatype: "json",
            success: function (data) {
                $("#ajax-partial-container").html(data);
            }
        });
    }

    var CheckRadioButton = function(item) {
        $(item).prev().click();
    }

    return {
        AddEmptyDat: AddEmptyDat,
        AddEmptyDayWithSameHours: AddEmptyDayWithSameHours,
        RemoveRequestDay: RemoveRequestDay,
        RemoveRequest: RemoveRequest,
        Init: Init,
        Submit: Submit,
        CheckRadioButton: CheckRadioButton
    }
})();

$(function () {
    RequestController.Init();

    $(document).on('click', '[data-action="add-empty-day"]', function (e) {
        e.preventDefault();
        RequestController.AddEmptyDat();
    });
    
    $(document).on('click', '[data-action="add-empty-day-with-same-hours"]', function (e) {
        e.preventDefault();
        RequestController.AddEmptyDayWithSameHours(this)
    });

    $(document).on("click", '[data-action="delete-request-day"]', function (e) {
        e.preventDefault();
        RequestController.RemoveRequestDay(this);
    });
    
    $(document).on("click", '[data-action="delete-request"]', function (e) {
        e.preventDefault();
        RequestController.RemoveRequest();
    });

    $(document).on("click", '[data-action="save-and-submit"]', function (e) {
        e.preventDefault();
        RequestController.Submit("SaveAndSubmit", this);
    });
    
    $(document).on("click", '[data-action="save-changes"]', function (e) {
        e.preventDefault();
        RequestController.Submit("Update", this);
    });

    $(document).on("click", '[data-action="update-request-hours"]', function (e) {
        e.preventDefault();
        RequestController.Submit("UpdateHours", this);
    });

    $(document).on("click", '[data-id="labelRadioButton"]', function() {
        RequestController.CheckRadioButton(this);
    })
});