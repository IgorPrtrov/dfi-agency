﻿(function () {
    setTimeout(function () {

        var address = $('[data-id = "addressFromGoogle"]').val();
        if (address) {

            $("#AddressAutoComplete").val(address);

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({ 'address': address },
                function (results, status) {
                    if (status === 'OK') {

                        map.setCenter(results[0].geometry.location);

                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location
                        });

                        markers.push(marker);
                    }
                });
        }

    }, 200);


})();