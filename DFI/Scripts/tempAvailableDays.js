﻿
$(document).ready(function () {
    // Set the checkboxes as checked by default
    var defaultCheckedDays = [0,1,2,3,4,5,6]; 

    defaultCheckedDays.forEach(function (dayIndex) {
        $(".day-checkbox").eq(dayIndex).prop("checked", true);
    });
});

