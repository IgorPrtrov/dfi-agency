﻿var isBusy = false;
(function () {
    $("body").on("click", "#createFacilityLocation", function (e) {

        if (isBusy) {
            e.preventDefault();
            return;
        }
            
        isBusy = true;

        var validationMassages = '';

        var isValid = $("#FacilityLocationForm").valid();

        if (!isValid) {
            validationMassages += "Validation error, take a look at the form above.";
        }

        if ($("[data-id='addressFromGoogle']").val().length === 0) {
            validationMassages += "Fill in again your address manually. Google map can not find it.";
        }

        if (validationMassages.length > 5) {
            e.preventDefault();
            $('[data-id="validationMessage"]').html(validationMassages);
            $('[data-id="validationMessage"]').show();
            isBusy = false;
        } else {
            $('[data-id="validationMessage"]').hide();
        }
    });

    var ajaxWorking;

    $("body").on("click", "#deleteLocation", function (e) {

        var locationId = $(this).attr("data-id");

        event.preventDefault();

        if (!ajaxWorking) {

            ajaxWorking = true;

            $.post("/Facilities/DeleteFacilityLocation", { locationId: locationId })
                .done(function (data) {
                    if (data.Success) {
                        ajaxWorking = false;

                        $("#deleteLocationSuccessMessage").show();
                        setTimeout(function () {
                            window.location.href = window.location.origin + $("#backButton").attr("href");
                        }, 1000);

                    } else {
                        $("#deleteLocationValidationMessage").text(data.Messages.join('; '));
                        $("#deleteLocationValidationMessage").show();
                        ajaxWorking = false;
                    }
                });
        }
    });
})();