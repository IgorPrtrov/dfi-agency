$(function () {
    $("#review-tab").click(function () {
        loadReviewDetail();
    });
});

function loadReviewDetail() {
    var tempId = $("#WhoCompletedRequestTempId").val();
    var requestId = $("#Id").val();

    if (tempId && requestId) {

        $("#spinner").show();

        $.ajax({
            type: "GET",
            url: "/ProfessionalReviews/GetReadonlyReviewDetail?tempId=" + tempId + "&requestId=" + requestId,
            cache: false,
            datatype: "json",
            success: function(data) {
                $("#review_info").html(data);
                setDisabled();
                clickOnLabel();
                $("#spinner").hide();
            }
        });
    }
}


function setDisabled() {
    if ($("#IsReadonly").val() === "True") {
        $("[data-group='disabled']").each(function (index, item) {
            $(item).prop("disabled", true);
        });
    }
}

function clickOnLabel() {
    $("[data-group='label-radio']").click(function () {

        var radio = $(this).prev();

        if (!$(radio).prop("disabled")) {
            $(radio).prop("checked", true);
        }
    });
}