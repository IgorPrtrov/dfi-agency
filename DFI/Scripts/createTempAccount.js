﻿(function () {
    var licenseForDelete = '';

    $("body").on('click', "[data-toggle='collapse']", function (event) {
        event.preventDefault();
    });

    $("body").on('click', "[data-action='temp_accept_agreement']", function() {
        localStorage.setItem("isTempReadAgreement", true);
        $("#createAccount").click();
    });

    $("body").on("click", "#createAccount", function (e) {

        var files = $('[data-id="file"]');
        var validationMassages = '';
        validationMassages += validateFile(files[0]);
        validationMassages += validateFile(files[1]);
        validationMassages += validateFile(files[2]);

        $("#licensesSection").find('.collapse').toggleClass('collapse show');
        var isValid = $("#TempForm").valid();

        if (!isValid) {
            validationMassages += "Validation error, take a look at the form above."
        }

        if ($("[data-id='addressFromGoogle']").val().length === 0) {
            validationMassages += "FIll in again your address manually. Google map can not find it.";
        }

        if (validationMassages.length > 5) {
            e.preventDefault();
            $('[data-id="validationMessage"]').html(validationMassages);
            $('[data-id="validationMessage"]').show();
        } else {
            $('[data-id="validationMessage"]').hide();

            var isTempReadAgreement = localStorage.getItem("isTempReadAgreement")
            var isUrlRegistrationForm = window.location.href.indexOf("Registrator/TempRegistrationForm")

            if (isUrlRegistrationForm > 0 && (isTempReadAgreement == null || isTempReadAgreement == "false")) {
                e.preventDefault();
                $('#tempAgreementModal').modal('show');
            } else {
                localStorage.setItem("isTempReadAgreement", false);
                if ($("#spinner").is(":visible"))
                    e.preventDefault();
            }
        }
    });

    $("body").on('click', '#deleteLicense', function () {
        $(licenseForDelete).hide();
        $(licenseForDelete + ' [data-name="IsDeletedLicense"]').val(true);

        //in order to ignore validation
        $(licenseForDelete + ' [data-name = "desired_hourly_rate"]').val(1);
        $(licenseForDelete + ' [data-name = "years_in_practice"]').val(1);

        if ($('[data-action="deleteLicense"]:visible').length === 1) {
            $('[data-action="deleteLicense"]:visible').hide();
        }
    });

    $("body").on('click', '[data-action="deleteLicense"]', function () {
        var dataId = $(this).data("id");
        $('#deleteLicenseModalLabel').text("Delete " + $(this).data('licensename') + " License. At least one license required.");
        licenseForDelete = '[data-id="' + dataId + '"]';
    });

    $("body").on("click", "#activateTemp", function (e) {
        $("#spinner").show();
        $.post("/Professionals/Activate", {
            id: $("#Id").val()
        })
        .done(function (data) {
            handlerResponseChangeStatus(data);
        });
    });

    var actionName;

    $("body").on("click", "#deactivateTemp", function (e) {
        actionName = "Deactivate";
        getValueForModal();
    });

    $("body").on("click", "#deleteTempBtn", function (e) {
        actionName = "Delete";
        getValueForModal();
    });

    function getValueForModal() {
        $.get("/Requests/GetRequestForInactiveOrDeletedTemp?tempId=" + $("#Id").val())
            .done(function (data) {
                $("#deleteOrInactiveTempLabel").text(actionName + " professional");
                $("#deleteOrInactiveTempBody").html(data);

                $("#deleteOrInactiveTempModal").show();
            });
    }

    $("body").on("click", "#deleteOrInactiveTemp", function (e) {

        $("#spinner").show();

        if (actionName === "Delete") {
            $("#deleteOrInactiveTempModal").hide();

            $.post("/Professionals/Delete", {
                id: $("#Id").val()
            })
                .done(function (data) {
                    handlerResponseChangeStatus(data);
                    setTimeout(function () { window.location.href = "/Professionals" }, 2000)
                });
        }
        else if (actionName === "Deactivate") {
            $("#deleteOrInactiveTempModal").hide();

            $.post("/Professionals/Deactivate", {
                    id: $("#Id").val()
                })
                .done(function (data) {
                    handlerResponseChangeStatus(data);
                });
        }

    });

    function handlerResponseChangeStatus(data) {
        $("#spinner").hide();
        $('[data-id="validationMessage"]').hide();

        if (data.Success) {
            updateStatusCallback(data.Messages[0]);
        } else {
            $('[data-id="validationMessage"]').html(data.Messages.join('; '));
            $('[data-id="validationMessage"]').show();
        }
    }

    $("body").on('change', '#TempDto_Photo', function () {
        readURL(this);
    });

    $("body").on('change', '#TempDto_Resume', function () {
        setNameFile(this);
    });

    $("body").on('change', '#TempDto_Certificate', function () {
        setNameFile(this);
    });

    $("body").on('change', '#TempDto_RadiologyLicense', function () {
        setNameFile(this);
    });

    $('[data-id="validationMessageGetForm"]').click(function () {
        $(this).hide();
    });
})();

function updateStatusCallback(status){
    var statusBadgeClass = "badge badge-pill text-capitalize badge-lg ";
    switch(status){
        case "active":
            statusBadgeClass += "badge-success";
            $("#activateTemp").addClass("d-none");
            $("#deactivateTemp").removeClass("d-none");
            $("#deleteTemp").removeClass("d-none");
            $("#deleteTempBtn").removeClass("d-none");
            $("#TempStatus").val("2");
            break;
        case "inactive":
            statusBadgeClass += "badge-danger";
            $("#deactivateTemp").addClass("d-none");
            $("#activateTemp").removeClass("d-none");
            $("#deleteTemp").removeClass("d-none");
            $("#deleteTempBtn").removeClass("d-none");
            $("#TempStatus").val("3");
            break;
        case "deleted":
            statusBadgeClass += "badge-dark";
            $("#activateTemp").removeClass("d-none");
            $("#deactivateTemp").addClass("d-none");
            $("#deleteTemp").addClass("d-none");
            $("#deleteTempBtn").addClass("d-none");
            $("#TempStatus").val("4");
            break;
    }

    $('#professionalStatus > span').removeClass();
    $('#professionalStatus > span').addClass(statusBadgeClass);
    $('#professionalStatus > span').text(status);
    $("#toast-changes-saved").toast('show');
    var body = $("html, body");
    body.stop().animate({scrollTop:0}, 500, 'swing');
}

var setNameFile = function(input) {
    if (input.files && input.files[0]) {
        $(input).next().text(input.files[0].name);
    }
}

var readURL = function (input) {
    var validationMassages = validateFile(input);
    if (validationMassages.length === 0) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tempPhoto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $('#tempPhoto').show();

            $(input).next().text(input.files[0].name);
            $('[data-id="validationMessage"]').hide();
        }
    } else {
        $('#tempPhoto').hide();
        $('[data-id="validationMessage"]').html(validationMassages);
        $('[data-id="validationMessage"]').show();
    }
}

var validateFile = function (file) {

    var validationMessages = '';
    var filename = $(file).val();
    var typeFile = $(file).attr("id").split('_')[1];

    var extension = filename.replace(/^.*\./, '');

    if (extension === filename) {
        extension = '';
    } else {
        extension = extension.toLowerCase();
    }

    if (file.files[0] && file.files[0].size > 5242880) {
        validationMessages += typeFile + ' must be less than 5MB. </br>';
    }

    if (typeFile === "Photo") {
        var alreadyExistsFile = $("[data-group='documents'][data-type='Photo']")

        if (filename === "" && alreadyExistsFile.length === 0) {
            validationMessages += 'Photo is required.</br>';
        }            
        else if (filename !== "" && extension !== 'jpg' && extension !== 'jpeg' && extension !== 'png') {
            validationMessages += 'Photo must be JPG, GIF or PNG. </br>';
        }
    }
    if (typeFile === "Certificate") {

        if (filename !== "" && extension !== 'doc' && extension !== 'docx' && extension !== 'pdf' && extension !== 'pages') {
            validationMessages += 'Certificate must be DOC, DOCX, PAGES or PDF. </br>';
        }
    }

    if (typeFile === "Resume") {

        if (filename !== "" && extension !== 'doc' && extension !== 'docx' && extension !== 'pdf' && extension !== 'pages') {
            validationMessages += 'Resume must be DOC, DOCX, PAGES or PDF. </br>';
        }
    }

    if (typeFile === "RadiologyLicense") {

        if (filename !== "" && extension !== 'doc' && extension !== 'docx' && extension !== 'pdf' && extension !== 'pages'
            && extension !== 'jpg' && extension !== 'jpeg' && extension !== 'png') {
            validationMessages += 'Radiology license must be DOC, DOCX, PAGES, PDF or JPG, GIF, PNG. </br>';
        }
    }

    return validationMessages;
} 