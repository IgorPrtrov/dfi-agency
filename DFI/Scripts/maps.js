﻿$("#insertMap").show();

var map;
var markers;

function initAutocomplete() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: { lat: 39.96, lng: -75.60 }
    });

    google.maps.event.addDomListener(window, 'load', function () {
        var options = {
            types: ['geocode'],
            componentRestrictions: { country: "usa" }
        };
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('AddressAutoComplete');

    var searchBox = new google.maps.places.SearchBox(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        updateAddress(places[0]);

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });

        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
}

function updateAddress(result) {
    var street_number = '';
    var route = '';
    var locality = '';
    var administrative_area_level_1 = '';
    var postal_code = '';

    $.each(result.address_components, function (i, val) {
        if ($.inArray('street_number', val.types) === 0) {
            street_number = val.short_name;
        }
        if ($.inArray('route', val.types) === 0) {
            route = val.short_name;
        }
        if ($.inArray('locality', val.types) === 0) {
            locality = val.short_name;
        }
        if ($.inArray('administrative_area_level_1', val.types) === 0) {
            administrative_area_level_1 = val.short_name;
        }
        if ($.inArray('postal_code', val.types) === 0) {
            postal_code = val.short_name;
        }
    })
    var value = street_number + " " + route + ', ' + locality + ', ' + administrative_area_level_1 + ', ' + postal_code;

    $('[data-id="addressFromGoogle"]').val(value);
}