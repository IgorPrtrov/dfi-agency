﻿$(function () {
    loadRequests();

    $("#search").click(function() {
        var dto = getSearchDto();
        loadRequests(dto);
    });

    $("#cleanSearch").click(function() {
        cleanSearch();
        loadRequests();
    });

    $(document).on("click", "[data-group='column']", function () {
        sorting(this);
    });
});

var sorting = function(item) {
    var oldClass = $(item).next().attr('class');
    var column = $(item).data('id');

    var newClass = '';
    var order = '';

    var classSort = oldClass.split('-')[3];
    if (classSort === "desc") {
        newClass = "fa-sort-alpha-asc";
        order = "asc"
    } else {
        newClass = "fa-sort-alpha-desc";
        order = "desc"
    }

    var dto = getSearchDto();
    dto.RequestColumn = column;
    dto.Order = order;

    var sortDto = {};
    sortDto.OldClass = oldClass.split(' ')[1];
    sortDto.NewClass = newClass;
    sortDto.Column = column;

    loadRequests(dto, sortDto);
}

var loadRequests = function (data, sortDto) {
    if (!data)
        data = getSearchDto();

    $("#spinner").show();
    $.ajax({
        type: "POST",
        url: '/Requests/Search',
        data: JSON.stringify(data),
        contentType: "application/json; charset=UTF-8",
        success: function (result) {
            $("#spinner").hide();
            $("#requestListPartialView").html(result);

            if (sortDto) {
                $("[data-id='" + sortDto.Column + "']").next().removeClass(sortDto.OldClass).addClass(sortDto.NewClass).show();
            }

            paginationInit(sortDto);
        },
        error: function (ex) {
        }
    });
}

var getSearchDto = function() {
    var dto = {};
    dto.Order = 0;
    dto.RequestColumn = -1; //need to go default case to sort by id desc.
    dto.FacilityName = $("#FacilityName").val();
    dto.RequestStatuses = $.map($("[data-group='statuses']"),
        function (element) {
            if ($(element).prop("checked"))
                return $(element).val();
        });
    dto.Licenses = $.map($("[data-group='licenses']"),
        function (element) {
            if($(element).prop("checked"))
                return $(element).val();
        });

    return dto;
}

var cleanSearch = function()
{
    $("#FacilityName").val('');
    $("[data-group='statuses']").each(function (index, item) {
        $(item).prop("checked", false)
    });
    $("[data-group='licenses']").each(function(index, el) {
         $(el).prop("checked", false)
    })
}

var paginationInit = function (sortDto)
{
    $(".pagination").on("click", "a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        if (href) {
            var page = href.split("=")[1];
            var dto = getSearchDto();
            dto.Page = page;
            if (sortDto) {
                dto.RequestColumn = sortDto.Column;
                dto.Order = sortDto.NewClass.split('-')[3];
            }

            loadRequests(dto, sortDto);
        }
    });
}