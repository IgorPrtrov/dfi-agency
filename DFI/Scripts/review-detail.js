$(function () {
    loadReviewDetail();
});

function loadReviewDetail() {
    var tempId = getUrlParameter('tempId');
    var requestId = getUrlParameter('requestId');

    if (tempId && requestId) {
        $.ajax({
            type: "GET",
            url: "/ProfessionalReviews/GetReviewDetailToLeaveReview?tempId=" + tempId + "&requestId=" + requestId,
            cache: false,
            datatype: "json",
            success: function (data) {
                $("#review-detail").html(data);
                initFancyProfessionalReview();
                setDisabled();
                clickOnLabel();
                $("#spinner").hide();
            }
        }); 
    }
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function setDisabled() {
    if ($("#IsReadonly").val() === "True") {
        $("[data-group='disabled']").each(function (index, item) {
            $(item).prop("disabled", true);
        });
    }
}

function clickOnLabel() {
    $("[data-group='label-radio']").click(function () {

        var radio = $(this).prev();

        if (!$(radio).prop("disabled")) {
            $(radio).prop("checked", true);
        }
    });
}