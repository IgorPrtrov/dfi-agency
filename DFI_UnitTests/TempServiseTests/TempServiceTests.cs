﻿using DFI.Data.Documents;
using DFI.Data.Emails;
using DFI.Data.GoogleMaps;
using DFI.Data.Graders;
using DFI.Data.Requests;
using DFI.Data.Temps;
using DFI.Data.Accounts;
using DFI.Factory;
using DFI.Helpers;
using DFI.Models;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using System;
using System.Net.Http.Headers;
using System.Diagnostics.Contracts;

namespace DFI_UnitTests.TempServiceTests
{
    public class TempServiceTests
    {
        public Mock<IGraderService> MockGraderService = new Mock<IGraderService>();
        public Mock<ITempRepository> MockTempRepository = new Mock<ITempRepository>();
        public Mock<ITempMapper> MockTempMapper = new Mock<ITempMapper>();
        public Mock<ILogger> MockLogger = new Mock<ILogger>();
        public Mock<IEmailService> MockEmailService = new Mock<IEmailService>();
        public Mock<IDocumentMapper> MockDocMapper = new Mock<IDocumentMapper>();
        public Mock<IGoogleMapService> MockGoogleMapService = new Mock<IGoogleMapService>();
        public Mock<IRequestRepository> MockRequestRepo = new Mock<IRequestRepository>();
        public Mock<IApplicationUserManagerFactory> MockAppUserFactory = new Mock<IApplicationUserManagerFactory>();

        private TempService GetTempService()
        {
            return new TempService(MockGraderService.Object, MockTempRepository.Object,
                MockTempMapper.Object, MockLogger.Object, MockEmailService.Object, MockDocMapper.Object,
                MockGoogleMapService.Object, MockRequestRepo.Object, MockAppUserFactory.Object);
        }

        [Fact]
        public void AddTemp_ValidLocation_AddsToRepository()
        {  
            //Arrange
            var tempDto = new TempDto();

            var temp = new Temp()
            {
                AddressLine1 = "test1",
                City = "Minsk",
                Zipcode = "123456"
            };

            MockTempMapper.Setup(m => m.MapToTemp(tempDto)).Returns(temp);

            var location = new LocationDto
            {
                Latitude = 42345,
                Longitude = -71765
            };

            MockGoogleMapService.Setup(m => m.GetLocation(It.IsAny<string>())).Returns(location);
            MockGoogleMapService.Setup(m => m.GetTimeZoneId(location.Latitude, location.Longitude)).Returns("SampleTimeZone");

            var tempManager = GetTempService();

            //Act
            tempManager.AddTemp(tempDto);

            //Assert
            MockTempMapper.Verify(m => m.MapToTemp(tempDto), Times.Once);
            MockGoogleMapService.Verify(m => m.GetLocation($"{temp.AddressLine1}, {temp.City}, {temp.Zipcode}"), Times.Once);
            MockGoogleMapService.Verify(m => m.GetTimeZoneId(location.Latitude, location.Longitude), Times.Once);
            MockTempRepository.Verify(m => m.Add(It.IsAny<Temp>()), Times.Once);
            MockTempRepository.Verify(m => m.SaveChanges(), Times.Once);
        }

        [Fact]
        public void GetTemps_ReturnsNonDeletedTemps()
        {
            //Arrange
            var tempsInRepository = new List<Temp>
            {
                new Temp {TempId = 1, Status = (int)Helper.EState.Active },
                new Temp {TempId = 2, Status = (int)Helper.EState.Deleted },
                new Temp {TempId = 3, Status = (int)Helper.EState.InActive },
            };

            MockTempRepository.Setup(repo => repo.GetAll()).Returns(tempsInRepository);

            MockTempMapper.Setup(mapper => mapper.MapToTempDto(It.IsAny<Temp>()))
                .Returns((Temp temp) => new TempDto { Id = temp.TempId });

            var tempService = GetTempService();

            //Act
            var result = tempService.GetTemps();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.All(result, dto => Assert.True(dto.Id > 0));
        }

        [Fact]
        public void GetListTempDtos_Returns_ListTempDto()
        {
            //Arrange
            var tempService = GetTempService();

            var dto = new TempSearchDto
            {
                Licenseses = new List<string>() { "license 1", "license 2" },
                FirstName = "Masha",
                LastName = "Petrov",
                PhoneNumber = "1231231212",
                Page = 3
            };

            var temps = new List<Temp>();
            var tempShortDtos = new List<TempShortDto>();

            MockTempRepository.Setup(repo => repo.GetTemps(It.IsAny<TempSearchDto>()))
                .Returns(temps);

            MockTempMapper.Setup(mapper => mapper.MapToTempShortDto(It.IsAny<Temp>()))
            .Returns((Temp temp) => tempShortDtos.FirstOrDefault(c => c.TempId == temp.TempId));

            //Act
            var result = tempService.GetListTempDtos(dto);

            //Assert
            Assert.NotNull(result);
            Assert.IsType<ListTempDto>(result);
            Assert.NotNull(result.TempShortDtos);
        }


        [Fact]
        public void GetTempSearchDto_Returns_TempSearchDto_With_Licenses()
        {
            //Arrange
            MockTempRepository.Setup(repo => repo.GetSearchLicenses())
                .Returns(new List<string>() { "license 1", "license 2"});

            var tempService = GetTempService();

            //Act
            var result = tempService.GetTempSearchDto();

            //Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Licenseses);
        }

        [Fact]
        public void GetProfile_Returns_TempDto()
        {
            //Arrange
            MockTempRepository.Setup(r => r.GetByEmail(It.IsAny<string>()))
                .Returns(new Temp() { TempId = 2 });

            MockTempRepository.Setup(r => r.GetById(new int?(2)))
                .Returns(new Temp() { TempId = 2, Email = "xt@gmail.com" });

            var listUsers = new List<ApplicationUser>() { new ApplicationUser() 
            { Email = "xt@gmail.com", NewEmail = "newEmail@gmail.com" } };

            var MockAppUserManager = new Mock<ApplicationUserManager>();
            MockAppUserManager.Setup(c => c.Users).Returns(listUsers.AsQueryable());

            MockAppUserFactory.Setup(r => r.GetUserManager())
                .Returns(MockAppUserManager.Object);

            var tempService = GetTempService();

            MockTempMapper.Setup(r => r.MapToTempDto(It.IsAny<Temp>()))
                .Returns(new TempDto());

            //Act
            var result = tempService.GetProfile("xt@gmail.com");

            //Assert
            Assert.NotNull(result);
            Assert.True(result.NewEmail == "newEmail@gmail.com");
        }

        [Fact]
        public void GetProfile_ThrowsException()
        {
            //Arrange
            var tempService = GetTempService();

            string email = "testemail.com";

            //Act and Assert
            var exception = Assert.Throws<Exception>(() => tempService.GetProfile(email));

            //Assert
            Assert.Equal($"GetProfile::temp is null with email = {email}", exception.Message);
        }

        [Fact]
        public void SendBulkEmails_SendsEmails()
        {
            //Arrange
            MockTempRepository.Setup(r => r.GetTemps(It.IsAny<TempSearchDto>()))
                .Returns(new List<Temp>
                { new Temp { Email = "temp1example@gmail.com"},
                  new Temp { Email = "temp2example@gmail.com"}
                });

            MockLogger.Setup(l => l.Info(It.IsAny<string>()));

            var tempService = GetTempService();

            //Act
            tempService.SendBulkEmails(new TempEmailLightDto());

            //Assert
            MockEmailService.Verify(c => c.AddEmail(It.IsAny<EmailDto>()), Times.Exactly(2));
        }

        [Fact]
        public void GetTempFirstName_Returns_ArrayOfStrings()
        {
            //Arrange
            MockTempRepository.Setup(r => r.GetTempFirstName())
                .Returns(new string[] { "Masha", "Sasha", "Dasha" });

            var tempService = GetTempService();

            //Act
            var result = tempService.GetTempFirstName();

            //Assert
            Assert.IsType<string[]>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetTempLastNames_Returns_ArrayOfStrings()
        {
            //Arramge
            MockTempRepository.Setup(r => r.GetTempLastNames())
                .Returns(new string[] { "Smith", "Jones" });

            var tempService = GetTempService();

            //Act
            var result = tempService.GetTempLastNames();

            //Assert
            Assert.IsType<string[]>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetTempEmail_Returns_ArrayOfStrings()
        {
            //Arrange
            MockTempRepository.Setup(r => r.GetTempEmail())
                .Returns(new string[] { "test.com", "test2.com" });

            var tempService = GetTempService();

            //Act
            var result = tempService.GetTempEmail();

            //Assert
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetTempPhoneNumber_Returns_ArrayOfStrings()
        {
            //Arrange
            MockTempRepository.Setup(r => r.GetTempPhoneNumber())
                .Returns(new string[] { "12345678", "87654321" });

            var tempService = GetTempService();

            //Act
            var result = tempService.GetTempPhoneNumber();

            //Assert
            Assert.NotEmpty(result);
        }
    }
}
