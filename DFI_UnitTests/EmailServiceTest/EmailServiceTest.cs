﻿using System;
using System.Collections.Generic;
using System.Linq;
using DFI;
using DFI.Data.Accounts;
using DFI.Data.Emails;
using DFI.Data.Facilities;
using DFI.Data.Temps;
using DFI.Data.TextMessages;
using DFI.Helpers;
using DFI.Models;
using Moq;
using Xunit;

namespace DFI_UnitTests.EmailServiceTest
{

    public class EmailServiceTests
    {
        private readonly Mock<IEmailRepository> MailRepositoryMock = new Mock<IEmailRepository>();
        private readonly Mock<IMailMapper> MailMapperMock = new Mock<IMailMapper>();
        private readonly Mock<ILogger> LoggerMock = new Mock<ILogger>();
        private readonly Mock<ITempRepository> TempRepositoryMock = new Mock<ITempRepository>();
        private readonly Mock<IFacilityRepository> FacilityRepositoryMock = new Mock<IFacilityRepository>();
        private readonly Mock<IDailyEmailRepository> DailyEmailRepositoryMock = new Mock<IDailyEmailRepository>();

        private EmailService GetEmailService()
        {
            return new EmailService(MailRepositoryMock.Object, MailMapperMock.Object, LoggerMock.Object, TempRepositoryMock.Object, FacilityRepositoryMock.Object, DailyEmailRepositoryMock.Object);
        }
        [Fact]
        public void Send_EmailIsActiveAndSuccessful_ShouldUpdateSentStatus()
        {
            // Arrange
            var email = new Email { EmailId = 1, ToEmail = "simple@gmail.com", Subject = "Test", Body = "This is a test email" };
            var emails = new List<Email> { email };
            MailRepositoryMock.Setup(repo => repo.GetEmailsForDelivery()).Returns(emails);
            MailRepositoryMock.Setup(repo => repo.IsNotDeliveredEmail(email.EmailId)).Returns(true);
            var emailService = GetEmailService();

            // Act
            emailService.Send();

            // Assert
            MailRepositoryMock.Verify(repo => repo.Update(email), Times.Once);
            MailRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
            LoggerMock.Verify(
                logger => logger.Info(It.Is<string>(s => s.Contains("EmailService:: Send Successfully"))),
                Times.Exactly(emails.Count));

        }

        [Fact]
        public void Send_EmailIsActiveAndFails_ShouldNotUpdateSentStatus()
        {
            // Arrange
            var email = new Email { EmailId = 1, ToEmail = "simple@gmail.com", Subject = "Test", Body = "This is a test email" };
            var emails = new List<Email> { email };
            MailRepositoryMock.Setup(repo => repo.GetEmailsForDelivery()).Returns(emails);
            MailRepositoryMock.Setup(repo => repo.IsNotDeliveredEmail(email.EmailId)).Returns(true);

            var emailService = GetEmailService();

            // Act
            emailService.Send();

            // Assert
            MailRepositoryMock.Verify(repo => repo.Update(email), Times.Never);
            MailRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Never);
            LoggerMock.Verify(logger => logger.Warn(It.Is<string>(s => s.Contains("EmailService:: Send"))), Times.Once);
            LoggerMock.Verify(logger => logger.Error(It.IsAny<string>()), Times.Never);
        }
        [Fact]
        public void AddEmail_ShouldAddEmail()
        {
            // Arrange
            MailMapperMock.Setup(mapper => mapper.MapToEmail(It.IsAny<EmailDto>())).Returns(new Email());
            var mockLogger = new Mock<ILogger>();
            var emailService = GetEmailService();

            // Act
            emailService.AddEmail(new EmailDto { To = "test@example.com", Subject = "Test", Body = "This is a test email" });

            // Assert
            MailMapperMock.Verify(mapper => mapper.MapToEmail(It.IsAny<EmailDto>()), Times.Once);
            MailRepositoryMock.Verify(repo => repo.Add(It.IsAny<Email>()), Times.Once);
            MailRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
        }
        [Fact]
        public void UpdateEmails_ShouldUpdateUserEmails()
        {
            // Arrange
            var user = new ApplicationUser { Email = "old@example.com", NewEmail = "new@example.com" };
            var newUserEmail = "updated@example.com";
            TempRepositoryMock.Setup(repo => repo.GetByEmail(user.Email)).Returns(new Temp { Email = user.Email });
            FacilityRepositoryMock.Setup(repo => repo.GetByEmail(user.Email)).Returns(new Facility { Email = user.Email });
            var emailService = GetEmailService();

            // Act
            var updatedUser = emailService.UpdateEmails(user, newUserEmail);

            // Assert
            TempRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
            FacilityRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
            Assert.Equal(newUserEmail, updatedUser.Email);
            Assert.Equal(newUserEmail, updatedUser.UserName);
            Assert.Equal(string.Empty, updatedUser.NewEmail);
        }
        [Fact]
        public void PostValidate_ValidEmail_ShouldReturnValidatedEmail()
        {
            // Arrange
            var email = "example@example.com";

            // Act
            var result = GetEmailService().PostValidate(email);

            // Assert
            MailRepositoryMock.Verify(repo => repo.Add(It.IsAny<ValidationEmail>()), Times.Once);
            MailRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Once);
            Assert.NotNull(result);
        }
        [Fact]
        public void SendTempEmails_AddsEmails()
        {
            // Arrange
            var emailService = GetEmailService();
            var tempEmails = new List<string> { "email1@example.com", "email2@example.com" };
            Setting.TempEmails = tempEmails.ToArray();

            // Act
            emailService.SendTempEmails();

            // Assert
            foreach (var email in tempEmails)
            {
                MailRepositoryMock.Verify(repo => repo.Add(It.Is<Email>(e => e.ToEmail == email && e.Subject == "Introducing our New Website")), Times.Once);
            }
            MailRepositoryMock.Verify(repo => repo.SaveChanges(), Times.Exactly(tempEmails.Count));
        }


    }
}
       
