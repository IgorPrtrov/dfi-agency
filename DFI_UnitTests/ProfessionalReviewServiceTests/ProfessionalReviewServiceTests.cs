﻿using DFI.Data.ProfessionalReviews;
using DFI.Data.Requests;
using DFI.Data.Temps;
using DFI.Factory;
using DFI.Helpers;
using DFI.Models;
using Moq;
using System;
using System.Collections.Generic;
using Twilio.Rest.Serverless.V1.Service;
using Xunit;

namespace DFI.Tests.ProfessionalReviewsTests
{
    public class ProfessionalReviewServiceTests
    {
        public Mock<IProfessionalReviewRepository> MockProfessionalReviewRepository = new Mock<IProfessionalReviewRepository>();
        public Mock<IRequestRepository> MockRequestRepository = new Mock<IRequestRepository>();
        public Mock<ITempRepository> MockTempRepository = new Mock<ITempRepository>();
        public Mock<IProfessionalReviewMapper> MockProfessionalReviewMapper = new Mock<IProfessionalReviewMapper>();
        public Mock<IApplicationUserManagerFactory> MockApplicationUserManagerFactory = new Mock<IApplicationUserManagerFactory>();

        private ProfessionalReviewService GetProfessionalReviewService()
        {
            return new ProfessionalReviewService(MockProfessionalReviewRepository.Object, MockRequestRepository.Object, MockTempRepository.Object,
                MockProfessionalReviewMapper.Object, MockApplicationUserManagerFactory.Object);
        }

        [Fact]
        public void CreateInitialMode_Returns_ProfessionalReviewDto()
        {
            //Arrange
            var tempId = 1;
            var requestId = 2;
            var temp = new Temp { FirstName = "Mike", LastName = "Petrov" };
            MockTempRepository.Setup(r => r.GetById(tempId)).Returns(temp);

            var request = new Request { LicenseType = "Dentist" };
            request.RequestDays = new List<RequestDay>
            {
                new RequestDay { StartDate = DateTime.Parse("2023-01-01") },
                new RequestDay { StartDate = DateTime.Parse("2023-01-05") }
            };

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns(request);

            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result = professionalReviewService.CreateInitialModel(tempId, requestId);

            //Assert
            Assert.Equal(tempId, result.TempId);
            Assert.Equal(requestId, result.RequestId);
            Assert.Equal("Mike Petrov", result.TempDisplayName);
            Assert.Equal("Dentist", result.Position);
            Assert.Equal("01-Jan-2023 - 05-Jan-2023", result.RequestDisplayName);
            Assert.Empty(result.TempPhoto);
        }

        [Fact]
        public void GetReadonlyReview_WithNoProfessionalReview_ReturnsNull()
        {
            //Arrange
            var requestId = 2;
            var tempId = 3;

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns(new Request() { ProfessionalReviews = new List<ProfessionalReview>() });

            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result = professionalReviewService.GetReadonlyReview(tempId, requestId);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void GetReadonlyReview_WithProfessionalReviews_ReturnsProfessionalReviewDto()
        {
            var requestId = 2;
            var tempId = 3;

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns(new Request
                {
                    ProfessionalReviews = new List<ProfessionalReview>()
                    {
                        new ProfessionalReview
                        {
                            Rating = 5,
                            IsBlocked = false,
                            Description = "good",
                            AddDate = It.IsAny<DateTime>(),
                            DidArriveInTimelyManner = true,
                            DidPresentInProfessionalAttire = true,
                            DidInteractWellWithStaff = false,
                            DidInteractWellWithCustomers = true,
                            WasCompetentInDigitalChartingAndDocumentation = false
                        }
                    }, 
                    RequestDays = new List<RequestDay>() { new RequestDay() { StartDate = DateTime.Now } }
                });

            MockTempRepository.Setup(r => r.GetById(tempId))
                .Returns(new Temp
                {
                    FirstName = "Natashka",
                    LastName = "Petrov",
                    Documents = new List<Document>
                    {
                        new Document { DocumentType = (int)EDocumentType.Photo }
                    }
                });

            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result = professionalReviewService.GetReadonlyReview(tempId, requestId);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(tempId, result.TempId);
            Assert.Equal(requestId, result.RequestId);
            Assert.Equal(5, result.Rating);
            Assert.False(result.IsBlocked);
            Assert.Equal("good", result.Description);
            Assert.True(result.DidArriveInTimelyManner);
            Assert.True(result.DidPresentInProfessionalAttire);
            Assert.False(result.DidInteractWellWithStaff);
            Assert.True(result.DidInteractWellWithCustomers);
            Assert.False(result.WasCompetentInDigitalChartingAndDocumentation);
        }

        [Fact]
        public void HasAccessToCreateReview_RequestNotFound_ReturnsFailureStatus()
        {
            //Arrange
            var tempId = 2;
            var requestId = 123;
            var userEmail = "email";

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns((Request)null);

            MockProfessionalReviewRepository.Setup(r => r.WasProfessionalReviewAdded(tempId, requestId, userEmail))
                .Returns(false);

            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result = professionalReviewService.HasAccessToCreateReview(tempId, requestId, userEmail);

            //Assert
            Assert.False(result.Success);
            Assert.Contains("Request can not be found", result.Messages);
        }

        [Fact]
        public void HasAccesToCreateReview_ReviewWasAlreadyAdded_ReturnsFailureStatus()
        {
            //Arrange
            var tempId = 2;
            var requestId = 123;
            var userEmail = "email";

            var request = new Request();

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns(request);

            MockProfessionalReviewRepository.Setup(r => r.WasProfessionalReviewAdded(tempId, requestId, userEmail))
                .Returns(true);

            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result = professionalReviewService.HasAccessToCreateReview(tempId, requestId, userEmail);

            //Assert
            Assert.False(result.Success);
            Assert.Contains("Review is already added", result.Messages);
        }

        [Fact]
        public void HasAccessToCreateReview_AccessNotGranted_ReturnsFailureStatus()
        {
            //Arrange
            var tempId = 2;
            var requestId = 123;
            var userEmail = "email";

            var request = new Request
            {
                Status = (int)ERequestStatus.Filled,
                FacilityLocation = new FacilityLocation { Facility = new Facility { Email = "other@example.com" } },
                RequestAssignees = new List<RequestAssignee> { new RequestAssignee { TempId = tempId, Status = (int)ERequestAssigneeStatus.Filled } }
            };

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns(request);

            MockProfessionalReviewRepository.Setup(r => r.WasProfessionalReviewAdded(tempId, requestId, userEmail))
                .Returns(false);
            
            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result =  professionalReviewService.HasAccessToCreateReview(tempId, requestId, userEmail);

            //Assert
            Assert.False(result.Success);
            Assert.Contains("Access is not granted", result.Messages);
        }

        [Fact]
        public void HasAccessToCreateReview_AdminAccess_ReturnsSuccess()
        {
            //Arrange
            var tempId = 2;
            var requestId = 123;
            var userEmail = "facility@example.com";

            var request = new Request
            {
                Status = (int)ERequestStatus.Success,
                FacilityLocation = new FacilityLocation { Facility = new Facility { Email = "facility@example.com" } },
                RequestAssignees = new List<RequestAssignee> { new RequestAssignee { TempId = tempId, Status = (int)ERequestAssigneeStatus.Filled } }
            };

            MockRequestRepository.Setup(r => r.GetById(requestId))
                .Returns(request);

            MockProfessionalReviewRepository.Setup(r => r.WasProfessionalReviewAdded(tempId, requestId, userEmail))
                .Returns(false);

            var professionalReviewService = GetProfessionalReviewService();

            //Act
            var result = professionalReviewService.HasAccessToCreateReview(tempId, requestId, userEmail);

            //Assert
            Assert.True(result.Success);
            Assert.Empty(result.Messages);
        }

        [Fact]
        public void GetReviews_Returns_ListOfReviews()
        {
            //Arrange
            var reviews = new List<ProfessionalReview>() {
                new ProfessionalReview(){Id = 1},
                new ProfessionalReview(){Id = 11},
                new ProfessionalReview(){Id = 12},
                new ProfessionalReview(){Id = 13},
                new ProfessionalReview(){Id = 14},
                new ProfessionalReview(){Id = 15},
                new ProfessionalReview(){Id = 16},
                new ProfessionalReview(){Id = 17},
                new ProfessionalReview(){Id = 18},
                new ProfessionalReview(){Id = 19},
                new ProfessionalReview(){Id = 10},
                new ProfessionalReview(){Id = 110}
            };

            MockProfessionalReviewRepository.Setup(c => c.GetProfessionalReviews(1)).Returns(reviews);

            MockProfessionalReviewMapper.Setup(c => c.MapToOwnProfessionalReview(It.IsAny<ProfessionalReview>()))
                .Returns(new OwnProfessionalReview());

            var reviewService = GetProfessionalReviewService();

            //Act
            var result = reviewService.GetListOfReviews(1, 1);

            //Assert
            Assert.NotNull(result.OwnProfessionalReviews);
            Assert.True(result.OwnProfessionalReviews.HasNextPage);
            Assert.True(result.OwnProfessionalReviews.PageNumber == 1);
            Assert.True(result.OwnProfessionalReviews.TotalItemCount == reviews.Count);
            Assert.True(result.OwnProfessionalReviews.PageSize == 10);
        }

        [Fact]
        public void GetReviews_ShouldReturnEmptyList()
        {
            //Arrange
            var reviews = new List<ProfessionalReview>();

            MockProfessionalReviewRepository.Setup(c => c.GetProfessionalReviews(1)).Returns(reviews);

            MockProfessionalReviewMapper.Setup(c => c.MapToOwnProfessionalReview(It.IsAny<ProfessionalReview>()))
                .Returns(new OwnProfessionalReview());

            var reviewService = GetProfessionalReviewService();

            //Act
            var result = reviewService.GetListOfReviews(1, 1);

            //Assert
            Assert.NotNull(result.OwnProfessionalReviews);
            Assert.False(result.OwnProfessionalReviews.HasNextPage);
            Assert.True(result.OwnProfessionalReviews.PageNumber == 1);
            Assert.True(result.OwnProfessionalReviews.TotalItemCount == reviews.Count);
            Assert.True(result.OwnProfessionalReviews.PageSize == 10);
        }
    }
}
