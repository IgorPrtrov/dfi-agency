﻿using DFI.Data.TextMessages;
using DFI.Helpers;
using DFI.Models;
using Moq;
using System.Collections.Generic;
using Twilio.Rest.Api.V2010.Account;
using Xunit;

namespace DFI.Tests.TextMessagesTests
{
    public class TextMessageServiceTests
    {
        public Mock<ITextMessageRepository> MockTextMessageRepository = new Mock<ITextMessageRepository>();
        public Mock<ITextMessageMapper> MockTextMessageMapper = new Mock<ITextMessageMapper>();
        public Mock<ILogger> MockLogger = new Mock<ILogger>();
        public Mock<ITwilioService> MockTwilioService = new Mock<ITwilioService>();

        private TextMessageService GetTextMessageService()
        {
            return new TextMessageService(MockTextMessageRepository.Object, MockTextMessageMapper.Object, MockLogger.Object, MockTwilioService.Object);
        }

        [Fact]
        public void SendSms_ShouldSendSms()
        {
            //Arrange
            var textMessage = new List<TextMessage>() { new TextMessage(), new TextMessage() };

            MockTextMessageRepository.Setup(r => r.GetSMSForDelivery())
                .Returns(textMessage);
            MockTextMessageRepository.Setup(r => r.IsNotSentSms(It.IsAny<int>()))
                .Returns(true);

            MockTwilioService.Setup(r => r.SendTwilioSms(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new TwilioSmsResponse { Status = MessageResource.StatusEnum.Sent });

            var textMessageService = GetTextMessageService();

            //Act
            textMessageService.SendSms();

            //Assert
            MockTextMessageRepository.Verify(r => r.Update(It.IsAny<TextMessage>()), Times.Exactly(textMessage.Count));
            MockTextMessageRepository.Verify(r => r.SaveChanges(), Times.Exactly(textMessage.Count));

            MockLogger.Verify(x => x.Info(It.Is<string>(s => s.Contains("TextMessageService:: Send Successfully"))),
                Times.Exactly(textMessage.Count));
        }

        [Fact]
        public void AddSms_ShouldAddSms()
        {
            //Arrange
            MockTextMessageMapper.Setup(c => c.MapToTextMessage(It.IsAny<TextMessageDto>())).Returns(new TextMessage());
            var mockLogger = new Mock<ILogger>();

            var textMessageService = GetTextMessageService();

            //Act
            textMessageService.AddSms(new TextMessageDto() { Body = "test", PhoneNumberTo = "123" });

            //Assert
            MockTextMessageMapper.Verify(c => c.MapToTextMessage(It.IsAny<TextMessageDto>()), Times.Once);

            MockTextMessageRepository.Verify(c => c.Add(It.IsAny<TextMessage>()), Times.Once);
            MockTextMessageRepository.Verify(c => c.SaveChanges(), Times.Once);
        }

        [Theory]
        [InlineData("Sms is not null")]
        [InlineData("Sms is null")]
        public void UpdateSmsStatus_ShouldUpdateSmsStatus(string condition)
        {
            //Arrange
            var sid = "123";
            var status = "Delivered";

            if(condition == "Sms is not null")
            {
                MockTextMessageRepository.Setup(r => r.GetBySid(sid))
                    .Returns(new TextMessage { MessageSid = sid, Status = "Pending" });
            }

            var textMessageService = GetTextMessageService();

            //Act
            textMessageService.UpdateSmsStatus(sid, status);

            //Assert
            if (condition == "Sms is not null")
            {
                MockTextMessageRepository.Verify(r => r.Update(It.IsAny<TextMessage>()), Times.Once);
                MockTextMessageRepository.Verify(r => r.SaveChanges(), Times.Once);
            }
            else
            {
                MockTextMessageRepository.Verify(r => r.Update(It.IsAny<TextMessage>()), Times.Never);
                MockTextMessageRepository.Verify(r => r.SaveChanges(), Times.Never);
                MockLogger.Verify(logger => logger.Error($"TextMessageService:: UpdateSmsStatus:: SMS is null with sid = {sid}, status = {status}"), Times.Once());
            }
        }
    }
}